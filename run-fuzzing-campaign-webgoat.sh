#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR WEBGOAT IN BATCH MODE
NUMBER_OF_EXPERIMENTS=3
CAMPAIGN_RUNTIME=300
MAX_500_REPORTED=10000
MAX_XSS_REPORTED=10000
MAX_INJECTIONS_REPORTED=10000
WEBGOAT_WAR=examples/webgoat/webgoat-server-v8.1.0.jar
echo "buildig project"
mvn clean package

for ((i=1; i<=${NUMBER_OF_EXPERIMENTS}; i++))
do
   echo "iteration ${i}"
   echo "starting server"
   java -javaagent:tools/aspectjweaver-1.9.6.jar -jar ${WEBGOAT_WAR}  > /dev/null 2>&1 &
   server_pid=$!
   echo "server process is ${server_pid}"
   sleep 5m

   #start fuzzing campaign

   # copy dependency to easily build classpath
   mvn dependency:copy-dependencies
   echo "starting fuzzer"
   java -Xmx20g -cp target/cornetto.jar:target/dependency/* nz.ac.wgtn.cornetto.jee.FuzzWebGoat81 -outputRootFolder results-webgoat-${i} -campaignLength ${CAMPAIGN_RUNTIME} -max500Reported ${MAX_500_REPORTED} -maxXSSReported ${MAX_XSS_REPORTED} -maxInjectionsReported ${MAX_INJECTIONS_REPORTED} -trials 500000 -requesttimeout 5000 -requestqueuesize 1000 -responsequeuesize 1000 -responseevaluationthreads 2 -requestexecutionthreads 3 -requestgenerationthreads 2

   #kill server for clean restart
   echo "killing server process ${server_pid}"
   kill -9 ${server_pid}
   sleep 10s

done

echo "done !"