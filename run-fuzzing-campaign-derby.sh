#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR DERBBY IN BATCH MODE
NUMBER_OF_EXPERIMENTS=5
CAMPAIGN_RUNTIME=300

echo "buildig preparer"
cd ./war-preparer
mvn clean package
echo "buildig fuzzer"
cd ..
mvn clean package

for ((i=1; i<=${NUMBER_OF_EXPERIMENTS}; i++))
do
   echo "iteration ${i}"
   ant -buildfile run.xml -propertyfile campaigns/derby.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-derby-${i}
done

echo "done !"