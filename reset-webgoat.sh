#!/bin/bash

## SCRIPT TO RESET WebGoat

echo "reseting WebGoat"
mvn clean package dependency:copy-dependencies
java -cp target/cornetto.jar:target/dependency/* nz.ac.wgtn.cornetto.jee.ResetWebgoat
echo "done !"