## About

This project contains an utility to prepare war files for fuzzing. It can be used as follows:

1. build it with `mvn package`, this will create the executable `target/war-preparer.jar`
2. run the utility with `java -jar war-preparer.jar`  -- the script will provide feedback on the parameters expected

The script will create a new war that differs from the original file in a number of ways:

1. it will add a servlet filter `nz.ac.vuw.httpfuzz.jee.rt.InvocationTrackerManagerFilter` -- this is used to add a special header that can be used by the fuzzing client to query information collected via instrumentation
2. it will register an additional servlet `nz.ac.vuw.httpfuzz.jee.rt.TrackedInvocationsPickupServlet.java` in `web.xml` -- this is used by the fuzzing client to query information collected via instrumentation
3. it will add the aspect library that defines code instrumentation

