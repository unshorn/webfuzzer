package nz.ac.wgtn.cornetto.jee.instrumentation;

import com.google.common.base.Preconditions;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * Script to prepare war.
 * Note that classes are not actually being instrumented (they were in olrder versions of this class), as this
 * is done at load time. However, additional classes are added to the  war, and an additional end point and filter is added
 * to web.xml that  is used by fuzzing clients to request coverage-type feedback.
 * @author jens dietrich
 */
public class WarInstrumenter {

    // Copy from nz.ac.vuw.httpfuzz.commons.LogSystem
    // TODO: reorganise project structure and remove redundancies
    static {
        initLogSystem();
    }

    public static Logger getLogger(String name) {
        return Logger.getLogger(name);
    }
    public static String TMP = ".tmp";
    public static final String MONITORING_URL_PATTERN = RTConstants.FUZZING_FEEDBACK_PATH_TOKEN+"/*";
    private static final Logger LOGGER = getLogger("war-preparer");

    public static void main(String[] args) throws Exception {

        Options options = new Options()
            .addOption("war", true, "The war file to be transformed (required)")
            //.addOption("aspectj", true, "The aspectj home folder (with bin and lib subfolders) (required)")
            .addOption("out", true, "The location and name of the instrumented war file (required)")
            .addOption("fuzzrt", true, "The jar file that contains the compiled aspects and the webfuzzer runtime component (required)")
            .addOption("excludesignaturefiles",true,"Whether to exclude signature files from the war file (defaults to false)");

        CommandLine cmd = new DefaultParser().parse(options, args);

        if (!cmd.hasOption("war") ||
                //!cmd.hasOption("aspectj") ||
                !cmd.hasOption("out")
                || !cmd.hasOption("fuzzrt")) {
            LOGGER.error("Arguments missing");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java <options> " + WarInstrumenter.class.getName(), options);
            System.exit(1);
        }

        File war = new File(cmd.getOptionValue("war"));
        File instrumentedWarFile = new File(cmd.getOptionValue("out"));
        File fuzzRt = new File(cmd.getOptionValue("fuzzrt"));

        // option to exclude signature files to address this issue:
        // https://stackoverflow.com/questions/34855649/invalid-signature-file-digest-for-manifest-main-attributes-exception-while-tryin
        boolean excludeSignatureFiles = false;
        if (cmd.hasOption("excludesignaturefiles")) {
            String value = cmd.getOptionValue("excludesignaturefiles");
            excludeSignatureFiles = "true".equals(value);
        }

        LOGGER.info("War preparer will exclude crypto signature: " + excludeSignatureFiles);

        prepare(war,instrumentedWarFile,fuzzRt,excludeSignatureFiles);
    }


    public static void prepare(File war, File instrumentedWarFile, File fuzzRt,boolean excludeSignatureFiles) throws Exception {

        Preconditions.checkArgument(war.exists(),"war file does not exist: " + war.getAbsolutePath());
        //Preconditions.checkArgument(aspectjHome.exists(),"aspectj home folder does not exist: " + aspectjHome.getAbsolutePath());
        Preconditions.checkArgument(fuzzRt.exists(),"webfuzzer-runtime does not exist: " + fuzzRt.getAbsolutePath());
        //Preconditions.checkArgument(new File(aspectjHome,"bin").exists(),"aspectj home bin folder does not exist: " + new File(aspectjHome,"bin").getAbsolutePath());
        //Preconditions.checkArgument(new File(aspectjHome,"bin/ajc").exists(),"aspectj home compiler executable does not exist: " + new File(aspectjHome,"bin/ajc").getAbsolutePath());
        //Preconditions.checkArgument(new File(aspectjHome,"lib").exists(),"aspectj home lib folder does not exist: " + new File(aspectjHome,"lib").getAbsolutePath());
        //Preconditions.checkArgument(new File(aspectjHome,"lib/aspectjrt.jar").exists(),"aspectj rt library does not exist: " + new File(aspectjHome,"lib/aspectjrt.jar").getAbsolutePath());

        LOGGER.info("Preparing war " + war.getAbsolutePath());


        File tmp = createTmpDir("__" + WarInstrumenter.class.getName() + "__" + war.getName());
        LOGGER.info("Unzipping war into "  + tmp.getAbsolutePath());
        unzip(war, tmp);

        File tmpInstrumented = createTmpDir("__" + WarInstrumenter.class.getName() + "__" + war.getName() + "-preparer");
        LOGGER.info("Created temporary folder "  + tmpInstrumented.getAbsolutePath());

        Path classesFolder = Paths.get(new File(tmp, "WEB-INF/classes").getAbsolutePath());
        Path libFolder = Paths.get(new File(tmp, "WEB-INF/lib").getAbsolutePath());
        Path webxml = Paths.get(new File(tmp, "WEB-INF/web.xml").getAbsolutePath());

        LOGGER.info("Copying resources");
        Files.walk(Paths.get(tmp.getAbsolutePath()))
        // .filter(Files::isRegularFile)
        .forEach(path -> {
            File f = path.toFile();
            File f2 = getTargetFile(f, tmp, tmpInstrumented);
            if (!path.equals(webxml)) {
                // web.xml done later as it will be manipulated
                try {
                    if (excludeSignatureFiles && isSignature(path)) {}
                    else {
                        copy(f, f2);
                    }
                }
                catch (IOException x) {
                    LOGGER.error("Error copying " + f.getAbsolutePath(),x);
                }
            }
        });

        LOGGER.info("manipulating and copying web.xml");
        File webXmlFile = webxml.toFile();
        Preconditions.checkState(webXmlFile.exists());
        File manipulatedWebXmlFile = getTargetFile(webXmlFile,tmp,tmpInstrumented);
        manipulateWebXml(webXmlFile,manipulatedWebXmlFile);

        LOGGER.info("adding runtime to war libraries");
        File targetFuzzRt = new File(tmpInstrumented,"WEB-INF/lib/" + fuzzRt.getName());
        copy(fuzzRt,targetFuzzRt);

        LOGGER.info("packing new war");
        zip(tmpInstrumented,instrumentedWarFile);

        LOGGER.info("finished war preparation, result written to " + instrumentedWarFile.getAbsolutePath());

    }

    private static boolean isSignature(Path path) {
        File f = path.toFile();
        if (f.getParentFile().getName().equals("META-INF")) {
            String name = f.getName();
            name = name.toUpperCase();
            return (name.endsWith(".SF") || name.endsWith(".RSA") || name.endsWith(".DSA"));
        }
        return false;
    }

    private static void copy(File srcFile, File destFile) throws IOException {
        if (!srcFile.isDirectory()) {
            FileUtils.copyFile(srcFile, destFile);
        }
    }

    // copy web.xml, isNew mapping for monitoring servlet
    private static void manipulateWebXml(File srcFile, File destFile) throws JDOMException, IOException {
        SAXBuilder builder = new SAXBuilder();
        Document doc = null;
        try {
            doc = builder.build(srcFile);
        }
        catch (java.net.UnknownHostException x) {
            // problem accessing schema -- resolving entities etc
            LOGGER.warn("Error accessing XML schema -- try without",x);
            builder.setValidation(false);
            builder.setEntityResolver(new EntityResolver() {
                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                    return new InputSource(new StringBufferInputStream(""));
                }
            });
            doc = builder.build(srcFile);
        }

        // isNew servlet mapping
        Namespace ns = doc.getRootElement().getNamespace();
        doc.getRootElement().addContent(new Comment("servlet mapping for monitoring servlet providing feedback to fuzzer inserted by " + WarInstrumenter.class.getName()));


        // note: elements are inserted at the beginning
        // TOMCAT seems to match mapping patterns as they occur - first one wins
        // and if patterns like * are used and the new mappings are appended at the end
        // they will defacto be ignored
        Element servletElement = new Element("servlet",ns);
        doc.getRootElement().addContent(0,servletElement);
        Element servletNameElement1 = new Element("servlet-name",ns);
        servletNameElement1.addContent("webfuzz_monitor");
        servletElement.addContent(servletNameElement1);
        Element servletClassElement = new Element("servlet-class",ns);
        servletClassElement.addContent("nz.ac.wgtn.cornetto.jee.rt.TrackedInvocationsPickupServlet");
        servletElement.addContent(servletClassElement);

        Element servletMappingElement = new Element("servlet-mapping",ns);
        doc.getRootElement().addContent(1,servletMappingElement);
        Element servletNameElement2 = new Element("servlet-name",ns);
        servletNameElement2.addContent("webfuzz_monitor");
        servletMappingElement.addContent(servletNameElement2);
        Element urlPatternElement = new Element("url-pattern",ns);
        urlPatternElement.addContent(MONITORING_URL_PATTERN); // TODO - make configurable
        servletMappingElement.addContent(urlPatternElement);

        doc.getRootElement().addContent(new Comment("filter mapping for monitoring servlet providing feedback to fuzzer inserted by " + WarInstrumenter.class.getName()));

        Element filterElement = new Element("filter",ns);
        doc.getRootElement().addContent(2,filterElement);
        Element filterNameElement1 = new Element("filter-name",ns);
        filterNameElement1.addContent("webfuzz_filter");
        filterElement.addContent(filterNameElement1);
        Element filterClassElement = new Element("filter-class",ns);
        filterClassElement.addContent("nz.ac.wgtn.cornetto.jee.rt.InvocationTrackerManagerFilter");
        filterElement.addContent(filterClassElement);

        Element filterMappingElement = new Element("filter-mapping",ns);
        doc.getRootElement().addContent(3,filterMappingElement);
        Element filterNameElement2 = new Element("filter-name",ns);
        filterNameElement2.addContent("webfuzz_filter");
        filterMappingElement.addContent(filterNameElement2);
        Element urlPatternElement2 = new Element("url-pattern",ns);
        urlPatternElement2.addContent("/*"); // TODO - make configurable
        filterMappingElement.addContent(urlPatternElement2);

        // output updated web.xml
        XMLOutputter xmlOutput = new XMLOutputter();
        xmlOutput.setFormat(Format.getPrettyFormat());
        xmlOutput.output(doc, new FileWriter(destFile));
    }

    // get the equivalent file for f in dir within targetDir
    private static File getTargetFile(File f, File dir, File targetDir) {
        Preconditions.checkArgument(f.getAbsolutePath().startsWith(f.getAbsolutePath()));
        String path = f.getAbsolutePath().replace(dir.getAbsolutePath(),targetDir.getAbsolutePath());
        File newFile = new File(path);
        if (f.isDirectory()) {
            newFile.mkdirs();
        }
        else {
            newFile.getParentFile().mkdirs();
        }
        return newFile;
    }

    private static void unzip(File war, File tmp) throws IOException {
        try (ZipFile zip = new ZipFile(war)) {
            LOGGER.info("Extracting war into tmp folder: " + tmp);
            for (Enumeration en = zip.entries(); en.hasMoreElements(); ) {
                ZipEntry next = (ZipEntry) en.nextElement();
                File file = new File(tmp, next.getName());
                if (next.isDirectory()) {
                    file.mkdirs();
                }
                else {
                    file.getParentFile().mkdirs();
                    Files.copy(zip.getInputStream(next), Paths.get(file.getAbsolutePath()));
                }
            }
        }
    }

    // copy from nz.ac.vuw.httpfuzz.commons.FileUtils: TODO: reorganise project structure and remove redundancies
    private static File createTmpDir(String name) {
        File dir = new File(TMP);
        dir.mkdirs();
        dir = new File(dir,name);
        if (dir.exists()) {
            try {
                FileUtils.deleteDirectory(dir);
            }
            catch (IOException x) {
                LOGGER.error("Error deleting existing tmp folder " + dir.getAbsolutePath(),x);
            }
        }
        dir.mkdir();
        return dir;
    }

    private static void initLogSystem() {
        File configFile = new File("log4j.properties");
        if (configFile.exists()) {
            org.apache.log4j.PropertyConfigurator.configure(configFile.getAbsolutePath());
        }
        else {
            System.out.println("Cannot find log4j config file + " + configFile.getAbsolutePath());
            System.out.println("Will use basic default configuration instead");
            BasicConfigurator.configure();
        }
    }

    private static void zip(File folder,File zipFile) throws IOException {
        Preconditions.checkArgument(folder.exists());
        Preconditions.checkArgument(folder.isDirectory());
        FileOutputStream fos = new FileOutputStream(zipFile);
        ZipOutputStream zipOut = new ZipOutputStream(fos);

        for (File f:folder.listFiles()) {
            addToZip(f, f.getName(), zipOut);
        }

        zipOut.close();
        fos.close();
    }

    private static void addToZip(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isHidden()) {
            return;
        }
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                addToZip(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }
}
