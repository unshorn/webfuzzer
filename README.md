# Cornetto -- A Greybox Fuzzer for Java Web Applications
 

![alt text](./doc/images/cornetto.jpeg)

Cornetto is an experimental web application fuzzing framework, consisting of a generic client, and advanced support for JEE-based servers, including static pre-analysis, and runtime feedback from the targeted web application.

Prerequisite: build this project, and the two subprojects `/war-preparer` and `/webfuzz-rt` with `mvn package`. Note: separate builds are required for those subprojects.

`ant` is used to run the preanalysis - fuzzing pipeline. To execute this, create an analysis parameter file (e.g., by cloning and editing `campaigns/jenkins.properties`), and the run 
`ant -buildfile run.xml -propertyfile <name-of-propertyfile>`.  

Log files will by default be generated in `logs/` (log settings are in `log4j.properties`), and selected logs will be displayed on the console. 
It is highly recommended to check logs for warnings, in particular in the start-up sequence. If this (ANT-based) approach does not work, a custom Java class
can be implemented. The repository contains as base class to facilitate this (`AbstractFuzzer`), a default implementation that can be used via an at script (`JEEFuzzer`) and 
an example used to fuzz the spring-based application `WebGoat-8.1` (`FuzzWebGoat81`).

### Architecture Overview

![alt text](./doc/images/architecture.png)

### Static Pre-Analysis 

The application performs several static analyses in order to improve fuzzing throughput. 

Analyses the war file in order to extract entry point URLs. At the moment, only `web.xml` is analysed. Support for similar sources (web annotations, JSPs) will be added later.

At the moment, the following information is used: 

1. string literals 
2. constants used to query headers and request parameters
3. forms and links found in web pages within the application

This information is used to build the *static model* that can be used to optimise requests, for instance, to compose GET requests using certain paths and query parameters. 


### Dynamic (Runtime) Analysis

For runtime analysis, the server application (war file) has to be instrumented. The instrumenter uses an agent (loadtime instrumentation),
the code is in the `webfuzz-rt` module that has its own readme. War files need to be  prepared for fuzzing, a script for this can be found in the `war-preparer`
module that again has its own readme with details. Information gathered at runtime is returned to the client va a special header (ticket) that can be used to fetch details
in a separate request. This includes information about methods called, and keys used when request parameters are accessed.

The following information is gathered using dynamic analysis: 

1. invoked methods
2. request parameter values used (at call site of `javax.servlet.http.HttpServletRequest::getParameter`)
3. stack traces of invocations of critical methods 

Information from 1. and 2. is used to build the *dynamic model*, which is used together with the *static model* to drive input generation.

When running the fuzzer using the ant script `run.xml`, check the `rt.lib` property for the location of the `webfuzz-rt` binary.

### Input Generation

Input generation takes place in classes in `nz.ac.wgtn.cornetto.http`. Generation is based on a containment hierachy of types (this can be thought about as the "grammar" that is being mutated):

```bash
RequestSpec
	|_  URISpec
	|_  MethodSpec
	|_  HeadersSpec
	    |_  HeaderSpec
   |_  ParametersSpec
       |_  ParameterSpec
       
```

The actual http request will then be generated from the `RequestSpec` instance. To create this instance (these instances), for each type in this hierachy a matching `*Generator` is used that extends `BiFunction<SourceOfRandomness,Context,T>`. Given a source of randomness and a context (which contains references to both the dynamic and the static model), an object of type `T` is generated. Those generators form a parallel hierachy.


This simplified `RequestSpecForm` illustrates the principle:

```java
public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
   // use information like the number of forms recorded to compute probabilties 
   // `prob1`, .. `prob4` for different strategies to use to generate the next request
   Collection<Form> forms = context.getDynamicModel().getForms();
   int prob1 = ..
   ..

   Supplier<RequestSpec> requestSpecs = ProbabilisticSupplierFactory.create()
       .withProbability(prob1).createValue(() -> randomGenerator.apply(sourceOfRandomness, context))
       .withProbability(prob2).createValue(() -> mutatingGenerator.apply(sourceOfRandomness, context))
       .withProbability(prob3).createValue(() -> crossoverGenerator.apply(sourceOfRandomness, context))
       .withProbability(prob4).createValue(() -> sourceOfRandomness.choose(forms).apply(sourceOfRandomness,context))
       .build();

     return requestSpecs.get();
}
```

The `ProbabilisticSupplierFactory` is a utility to set the probabilties used to select a particular strategy. These probabilities can be adjusted. Most of the work is delegated to specialised generators, or generators for parts of the request further down the grammar hierarchy. 

This example also shows the interaction with the dynamic model: the dynamic model records, amongst other properties, forms returned by responses. These forms can then be used to generate new requests (using the request parameters encoded in forms). Information from the static `context.getStaticModel()` can be used accourdingly.


### Architecture

The fuzzer using threadpools for three different tasks: 

1. generate new requests
2. execute generated requests
3. analyse responses

Those threads communicate using blocking lists. The size of the respective threadpools and queues can be configured in order to finetune the application.

Callbacks can be registered to add functionality at different stages of this process. This is achieved by implementing the `FuzzerListener` interface. Most of the reporting is implemented like this.   

### Result Reporting

Several results and metrics are reported in the `logs` folder. This includes the following: 

| File(s)                | Description                              | 
| ---------------------- |------------------------------------------| 
| `all-methods.txt`      | application methods found by the static analysis (*)  | 
| `coverage-forms.log`   | new forms discovered in HTTP responses      | 
| `coverage-links.log`   | new links discovered in HTTP responses      | 
| `coverage-methods.log`   | new methods (application, library and system) discovered when handling of requests |
| `coverage-request-parameters.log`| new request parameter names discovered when handling of requests |
| `covered-methods-not-triggered-by-requests-<count>.txt`   | application  methods recorded outside application handling threads, frequently updated (recording interval can be set)|
| `covered-methods-triggered-by-requests-< ount>.txt`   | application  methods recorded in application handling threads, frequently updated (recording interval can be set) |
| `injection-<instance>.json`| JSON-encoded details of a potenial injection flaws detected, including details about the taint flows | 
| `prob-<count>.txt`   | frequently logged HTTP requests and matching responses, for debugging (recording interval can be set)|
| `results-500.log`   | details about requests resulting in responses with a status code of `500` or greater|
| `results-sink-methods.log` | new stacktraces showing how usafe methods are reached when handling requests |
| `server.log`   | redirected server logs (note: only recorded when fuzzer starts server, depending on the deployment option used)|
| `stats.csv`   | frequently updated table with statictics (recording interval can be set, default is 1 min)|
| `xss-<confidence>-<group>-<instance>.json`| JSON-encoded details of a potenial XSS Vulnerability detected. By default, only `confidence -high` is reported, instances are grouped together by similarity | 


* methods are represented as strings using the syntax `<class-name>::<method-name><descriptor>` with the descriptors string as defined in the JVM Specification 


###  Configuration

The following properties can be used to cofigure the fuzzer. Many properties have defaults defined in `run.xml` but can be overridden in propject-specific property files such as `jenskins.properties`.

#### General

| Property               | Description                              | 
| ---------------------- |------------------------------------------| 
| `rt.lib`               | the runtime component used for instrumentation, a jar to be generated by building `/webfuzz-rt` with `mvn package` |
| `war.preparer`		     | the tool used to prepare (manipulate) wars for fuzzing, a jar to be generated by `/war-preparer` with `mvn package`| 
| `war.excludesignaturefiles` | can exclude signature files (`*.RSA`) from the generated war file, this can prevent certain [errors](https://stackoverflow.com/questions/34855649/invalid-signature-file-digest-for-manifest-main-attributes-exception-while-tryin)|
| `randomseed` | sets the randomseed for input generation (not that this wont be sufficient to make the fuzzer completely deterministic due to issues like netwrok timeouts and concurrency |



#### Project Setup

| Property               | Description                              | 
| ---------------------- |------------------------------------------| 
| `war`                 | the location of web application to be fuzzed -- must be a valid war file |
| `war.prepared`        | the location of modified web application |
| `applicationPackages` | prefixes of packages that are part of the application (as oppossed to library packages), comma-separated, example: `org.jenkins,org.hudson`|
| `contextName`         | The name of the context to be used, e.g. if context is `foo` then the application will usually (depending on deployment) become available at `http://localhost:8080/foo` |
| `authenticator`       | The name of an instantiable class implemeting the interface `Authenticator` containing authentication logic |
| `trials`		        | the max number of requests to be generated |
| `campaignLength`      | the max duration of a fuzzing session, in minutes, defaults to 1 day , a campeign is halted once either the max number of requests has been passed, or this time has been reached |
| `outputRootFolder` | The root folder where results (but not all logs - @TODO) will be written |
| `max500Reported`   | The max number of server errors (status code >=500) for which details are logged |
| `maxInjectionsReported` | The max number of injection flaws for which details are logged |
| `maxXSSReported` | The max number of XSS vulnerabilities for which details are logged |


#### HTTP / Server Setup

| Property               | Description                              | 
| ---------------------- |------------------------------------------| 
| `requesttimeout`		  | the request timeout in ms |
| `httpKeepAlive`		  | property to be used by the HTTP client |
| `deployment`		     | the deployment to be used, name of an instantiable class implementing `Deployment`, implementations include a manual deployment option |
| `extraHeaders`| extra headers to be appended to requests, comma-separated list, each element uses the format `<header-name>:<header-value>` -- note that the fuzzer already manages a cookie store, and the authenticator can also set up extra headers to be injected, for instance for crumbs used for CSRF protection, see jenkins example for details |



#### Tuning and Debugging

| Property               | Description                              | 
| ---------------------- |------------------------------------------| 
| `requestqueuesize`		        | the max size of the queue containing HTTP requests (specs) not yet send to the server |
| `responsequeuesize`		        | the max size of the queue containing HTTP responses not yet analysed |
| `requestexecutionthreads`		 | the number of threads used for request execution |
| `responseevaluationthreads`		 | the number of threads used for response evaluation (analysis) |
| `requestgenerationthreads`		 | the number of threads used for request generation |
| `probInterval`| after this the number of requests a prob file is generated |
| `debug` | if `true` fuzzer JVM will be started with debug option (see `run.xml` for details, something like `-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=127.0.0.1:8000`) |

### Debugging and Monitoring

The fuzzer can be remotely debugged when started with the `debug` option. The actual size of the queues can be monitored using mbeans with tools such as `jvisualvm` (this requires the installation of the mbean plugin). Log files provide a lot of information that can be used for monitoring. The fuzzer for webgoat can be directly started in debug mode using standard IDE debugging.

### Fuzzing Spring-Based Applications

The mechanism described so far focuses on JEE-based applications. It is possible to fuzz spring-based applications as well, see `FuzzWebGoat81`for an example.

### Fuzzing Web Applications Not Written in Java

This is possible but requires the server compone to be ported, including:

1. the instrumentation of code
2. the addition of an end point to pick up feedback data 

### License

The project is licensed under the The Universal Permissive License (UPL). This project is supported by [Oracle Labs, Australia](https://labs.oracle.com/pls/apex/f?p=LABS:location:0::::P23_LOCATION_ID:46).

Copyright (c) 2019-21 Jens Dietrich





