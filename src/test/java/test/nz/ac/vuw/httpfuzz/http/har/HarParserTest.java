package test.nz.ac.vuw.httpfuzz.http.har;

import nz.ac.wgtn.cornetto.http.*;
import nz.ac.wgtn.cornetto.http.har.HarParser;
import org.junit.Assume;
import org.junit.Test;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Test for importing har files.
 * @author jens dietrich
 */
public class HarParserTest {

    private void checkHeaders (RequestSpec req, Map<String,String> headers) {
        HeadersSpec headersSpec = req.getHeadersSpec();
        assertEquals(headers.size(),headersSpec.getHeaders().size());

        for (HeaderSpec headerSpec:req.getHeadersSpec().getHeaders()) {
            String key = headerSpec.getKey().getValue();
            assertTrue(headers.containsKey(key));
            String val = headerSpec.getValue().getValue();
            assertEquals(headers.get(key),val);
        }
    }

    private void checkParameters (RequestSpec req,Map<String,String> parameters) {
        ParametersSpec parametersSpec = req.getParametersSpec();
        assertEquals(parameters.size(),parametersSpec.getParams().size());

        for (ParameterSpec paramSpec:req.getParametersSpec().getParams()) {
            String key = paramSpec.getKey().getValue();
            assertTrue(parameters.containsKey(key));
            String val = paramSpec.getValue().getValue();
            assertEquals(parameters.get(key),val);
        }
    }

    @Test
    public void testIndex() throws Exception {

        File harFile = new File(HarParserTest.class.getResource("/har/index.har").getFile());
        Assume.assumeTrue(harFile.exists());
        List<RequestSpec> requests = HarParser.parse(harFile);

        assertEquals(1,requests.size());
        RequestSpec req =  requests.get(0);
        assertEquals("http://localhost:8080/servlet-examples",req.getUriSpec().toURI().toString());
        assertEquals(MethodSpec.GET,req.getMethodSpec());

        // check headers
        Map<String,String> headers = new HashMap<>();
        headers.put("Host","localhost:8080");
        headers.put("Connection","keep-alive");
        headers.put("Cache-Control","max-age=0");
        headers.put("DNT","1");
        headers.put("Upgrade-Insecure-Requests","1");
        headers.put("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36");
        headers.put("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        headers.put("Sec-Fetch-Site","none");
        headers.put("Sec-Fetch-Mode","navigate");
        headers.put("Sec-Fetch-User","?1");
        headers.put("Sec-Fetch-Dest","document");
        headers.put("Accept-Encoding","gzip, deflate, br");
        headers.put("Accept-Language","en-US,en;q=0.9,de;q=0.8,ja;q=0.7");
        headers.put("If-None-Match","W/\"9277-1557108158000\"");
        headers.put("If-Modified-Since","Mon, 06 May 2019 02:02:38 GMT");
        checkHeaders(req,headers);
    }

    @Test
    public void testFormSubmissionWithGet() throws Exception {

        File harFile = new File(HarParserTest.class.getResource("/har/submit-form-with-get.har").getFile());
        Assume.assumeTrue(harFile.exists());
        List<RequestSpec> requests = HarParser.parse(harFile);

        assertEquals(1,requests.size());
        RequestSpec req =  requests.get(0);
        assertEquals("http://localhost:8080/servlet-examples/FormAnalyzer",req.getUriSpec().toURI().toString());
        assertEquals(MethodSpec.GET,req.getMethodSpec());

        // TODO headers

        // check parameters
        Map<String,String> parameters = new HashMap<>();
        parameters.put("name","Jones");
        parameters.put("firstname","Tom");
        checkParameters(req,parameters);

    }

    @Test
    public void testFormSubmissionWithPost() throws Exception {

        File harFile = new File(HarParserTest.class.getResource("/har/submit-form-with-post.har").getFile());
        Assume.assumeTrue(harFile.exists());
        List<RequestSpec> requests = HarParser.parse(harFile);

        assertEquals(1,requests.size());
        RequestSpec req =  requests.get(0);
        assertEquals("http://localhost:8080/servlet-examples/FormAnalyzer",req.getUriSpec().toURI().toString());
        assertEquals(MethodSpec.POST,req.getMethodSpec());

        // TODO headers

        // check parameters
        Map<String,String> parameters = new HashMap<>();
        parameters.put("name","Jones");
        parameters.put("firstname","Tom");
        checkParameters(req,parameters);
    }

    // check whether we cvan parse a minimal har file only containing a request
    @Test
    public void testFormSubmissionWithPostRequestOnly() throws Exception {

        File harFile = new File(HarParserTest.class.getResource("/har/submit-form-with-post-requestonly.har").getFile());
        Assume.assumeTrue(harFile.exists());
        List<RequestSpec> requests = HarParser.parse(harFile);

        assertEquals(1,requests.size());
        RequestSpec req =  requests.get(0);
        assertEquals("http://localhost:8080/servlet-examples/FormAnalyzer",req.getUriSpec().toURI().toString());
        assertEquals(MethodSpec.POST,req.getMethodSpec());

        // TODO headers

        // check parameters
        Map<String,String> parameters = new HashMap<>();
        parameters.put("name","Jones");
        parameters.put("firstname","Tom");
        checkParameters(req,parameters);
    }
}
