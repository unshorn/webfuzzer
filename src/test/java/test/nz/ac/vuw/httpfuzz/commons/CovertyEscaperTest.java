package test.nz.ac.vuw.httpfuzz.commons;

import com.google.common.escape.Escaper;
import com.google.common.html.HtmlEscapers;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Tests the functionality of the coverty escaper.
 * @author jens dietrich
 */
public class CovertyEscaperTest {

    @Test
    public void test1() {
        Escaper escaper = HtmlEscapers.htmlEscaper();
        String safeHTML = escaper.escape("<script>console.log('MrPRUxIkIt')</script>");
        System.out.println(safeHTML);
        assertTrue(true);
    }
}
