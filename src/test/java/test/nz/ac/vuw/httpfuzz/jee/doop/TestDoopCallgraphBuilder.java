package test.nz.ac.vuw.httpfuzz.jee.doop;

import nz.ac.wgtn.cornetto.jee.doop.CallgraphBuilder;
import nz.ac.wgtn.cornetto.stan.callgraph.Callgraph;
import nz.ac.wgtn.cornetto.stan.callgraph.Invocation;
import org.junit.BeforeClass;
import org.junit.Test;;
import java.io.Reader;
import java.io.StringReader;
import static org.junit.Assert.assertEquals;

public class TestDoopCallgraphBuilder {

    private static Callgraph callgraph = null;

    @BeforeClass
    public static void init() throws Exception {
        String doopOutput = "<<immutable-context>>\t<JEEDriver: void main(java.lang.String[])>/JEEDriver.entry/0\t<<immutable-context>>\t<JEEDriver: void entry()>";
        Reader in = new StringReader(doopOutput);
        callgraph = new CallgraphBuilder().build(in);
    }


    @Test
    public void test1() throws Exception {
        assertEquals(2,callgraph.getVertexCount());
        assertEquals(1,callgraph.getEdgeCount());

        Invocation edge = callgraph.getEdges().iterator().next();

        assertEquals("JEEDriver",edge.getStart().getClassName());
        assertEquals("main",edge.getStart().getMethodName());
        assertEquals("(java.lang.String[])void",edge.getStart().getDescriptor());

        assertEquals("JEEDriver",edge.getEnd().getClassName());
        assertEquals("entry",edge.getEnd().getMethodName());
        assertEquals("()void",edge.getEnd().getDescriptor());
    }


}