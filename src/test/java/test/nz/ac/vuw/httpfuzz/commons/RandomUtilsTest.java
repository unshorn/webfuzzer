package test.nz.ac.vuw.httpfuzz.commons;

import nz.ac.wgtn.cornetto.commons.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Tests for random utilities.
 * Mainly visual tests -- plotting distributions.
 * @author jens dietrich
 */

public class RandomUtilsTest {

    private static final int MAX = 1000;
    private static final int MEAN = 40; // for exponential distribution
    private static final int TRIALS = 20_000;
    private static final int CUTOFF = 50;  // only plot those
    private  Map<Integer,Integer> counts = new HashMap<>();

    @Before
    public void setup() {
        for (int i=0;i<MAX;i++) {
            counts.put(i,0);
        }
    }

    @Test
    public void testParetoDistribution() {
        System.out.println("Testing pareto distribution with MAX=" + MAX);

        for (int i=0;i<TRIALS;i++) {
            int next = RandomUtils.getRandomPositiveNumberFromParetoDistribution(MAX);
            assertTrue(next>=0);
            assertTrue(next<MAX);
            counts.compute(next,(k,v) -> v==null?1:v+1);
        }

        // this is statistical, so the test will be flaky !!!
        assertTrue(counts.get(0)>0);
    }

    @Test
    public void testFlatParetoDistribution() {
        System.out.println("Testing (relatively flat) pareto distribution  with MAX=" + MAX);

        for (int i=0;i<TRIALS;i++) {
            int next = RandomUtils.getRandomPositiveNumberFromParetoDistribution(0.2,MAX);
            assertTrue(next>=0);
            assertTrue(next<MAX);
            counts.compute(next,(k,v) -> v==null?1:v+1);
        }

        // this is statistical, so the test will be flaky !!!
        assertTrue(counts.get(0)>0);
    }


    @Test
    public void testExponentialDistribution () {
        System.out.println("Testing pareto distribution with MAX=" + MAX + " MEAN=" + MEAN);

        for (int i=0;i<TRIALS;i++) {
            int next = RandomUtils.getRandomPositiveNumberFromExponentialDistribution(MEAN,MAX);
            assertTrue(next>=0);
            assertTrue(next<MAX);
            counts.compute(next,(k,v) -> v==null?1:v+1);
        }

        // this is statistical, so the test will be flaky !!!
        assertTrue(counts.get(0)>0);
    }


    @After
    public void plotDistribution() {
        for (int i=0;i<CUTOFF;i++) {
            int count = counts.get(i)/10;  // scale  !!
            for (int j=0;j<count;j++) {
                System.out.print('+');
            }
            System.out.println(" - " + i);
        }
    }

}
