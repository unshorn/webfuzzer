package test.nz.ac.vuw.httpfuzz.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import nz.ac.wgtn.cornetto.postanalysis.InferErrorStackTracesFromWebGoatResponses;
import org.junit.Test;
import java.io.File;
import static org.junit.Assert.*;


public class InferErrorStackTracesFromWebGoatResponsesTest {

    private StackTrace readStacktrace(String resourceName) throws Exception {
        File input = new File(getClass().getResource(resourceName).getFile());
        ObjectMapper objectMapper = new ObjectMapper();
        ServerError error = objectMapper.readValue(input,ServerError.class);
        return new InferErrorStackTracesFromWebGoatResponses().extractStackTrace(error);
    }

    @Test
    public void test1() throws Exception {
        StackTrace stacktrace = readStacktrace("/postanalysis/webgoat/server-error-1.json");
        assertNotNull(stacktrace);
        assertEquals("org.springframework.security.web.firewall.RequestRejectedException",stacktrace.getExceptionType());
        assertEquals("The request was rejected because the URL was not normalized.",stacktrace.getExceptionMessage());
        assertEquals("org.springframework.security.web.firewall.StrictHttpFirewall.getFirewalledRequest(StrictHttpFirewall.java:340)",stacktrace.getEntries().get(0));
    }

}
