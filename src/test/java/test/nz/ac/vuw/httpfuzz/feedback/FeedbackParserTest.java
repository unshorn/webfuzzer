package test.nz.ac.vuw.httpfuzz.feedback;

import nz.ac.wgtn.cornetto.feedback.*;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.Test;
import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FeedbackParserTest {

    @Test
    public void test () throws Exception {
        InputStream input = FeedbackParser.class.getResourceAsStream("/feedback/taintFlows.json");
        JSONTokener tokener = new JSONTokener(input);
        JSONObject json = new JSONObject(tokener);

        assertNotNull(json);

        List<TaintFlowSpec> taintFlows = FeedbackParser.extractTaintflows(json);

        assertEquals(69,taintFlows.size());

        // some spot checks
        TaintFlowSpec taintFlowSpec = taintFlows.get(0);

        List<String> stackTrace = taintFlowSpec.getStackTrace();
        assertEquals(100,stackTrace.size());
        assertEquals("java.sql.Statement.executeUpdate",stackTrace.get(0));
        assertEquals("org.owasp.webgoat.sql_injection.introduction.SqlInjectionLesson4.injectableQuery(SqlInjectionLesson4.java:59)",stackTrace.get(1));
        assertEquals("org.owasp.webgoat.sql_injection.introduction.SqlInjectionLesson4.completed(SqlInjectionLesson4.java:53)",stackTrace.get(2));
        assertEquals("java.base/java.lang.Thread.run(Thread.java:834)",stackTrace.get(stackTrace.size()-1));

        TaintFlow flow = taintFlowSpec.getTaintFlow();

        assertEquals(1,flow.getArguments().size());
        assertEquals("1776011150",flow.getArguments().get("1"));
        assertEquals(1, flow.getSourceStrings().size());

        List<TaintedObject> provenance = flow.getProvenance();
        assertEquals(1, provenance.size());
        TaintedObject taintedObject = provenance.get(0);

        assertEquals("foobar", taintedObject.getAsString());
        assertEquals("java.lang.String", taintedObject.getType());

        List<TaintPropagationEvent> taintEvents = taintedObject.getTaintEvents();
        assertEquals(1, taintEvents.size());
        TaintPropagationEvent taintEvent = taintEvents.get(0);

        assertEquals("call: String java.lang.StringBuilder.toString(), introduces taint in returned string",taintEvent.getTaintedBy());
        assertEquals("private void io.undertow.server.handlers.form.FormEncodedDataDefinition.FormEncodedDataParser.doParse(org.xnio.channels.StreamSourceChannel), FormEncodedDataDefinition.java:202",taintEvent.getContext());


    }
}
