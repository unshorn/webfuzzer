package test.nz.ac.vuw.httpfuzz.feedback;

import edu.uci.ics.jung.graph.DirectedGraph;
import nz.ac.wgtn.cornetto.feedback.CoverageGraph;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.BitSet;
import static org.junit.Assert.*;


/**
 * Testing the internal bitset representation of the coverage graph.
 * @author jens dietrich
 */
public class TestCoverageGraphInternals {


    private CoverageGraph coverageGraph = null;
    private DirectedGraph<BitSet,Integer> dag = null;
    private BitSet root = null;

    @Before
    public void setup() {
        coverageGraph = new CoverageGraph();
        dag = coverageGraph.getDigraph();
        root = coverageGraph.getRoot();
    }

    @After
    public void teardown() {
        coverageGraph = null;
        dag = null;
        root = null;
    }

    @Test
    public void test1() {

        assertTrue(dag.containsVertex(root));
        BitSet v1 = bitsetOf(1);
        coverageGraph.insert(v1);

        BitSet v12 = bitsetOf(1,2);
        coverageGraph.insert(v12);

        BitSet v13 = bitsetOf(1,3);
        coverageGraph.insert(v13);

        BitSet v123 = bitsetOf(1,2,3);
        coverageGraph.insert(v123);

        assertEquals(5,dag.getVertexCount());
        assertTrue(dag.containsVertex(root));
        assertTrue(dag.containsVertex(v1));
        assertTrue(dag.containsVertex(v12));
        assertTrue(dag.containsVertex(v13));
        assertTrue(dag.containsVertex(v123));

        assertEquals(1,dag.getSuccessorCount(root));
        assertEquals(2,dag.getSuccessorCount(v1));
        assertEquals(1,dag.getSuccessorCount(v12));
        assertEquals(1,dag.getSuccessorCount(v13));
        assertEquals(0,dag.getSuccessorCount(v123));

        assertTrue(dag.getSuccessors(root).contains(v1));
        assertTrue(dag.getSuccessors(v1).contains(v12));
        assertTrue(dag.getSuccessors(v1).contains(v13));
        assertTrue(dag.getSuccessors(v12).contains(v123));
        assertTrue(dag.getSuccessors(v13).contains(v123));

        // insert duplicate
        BitSet v12a = bitsetOf(1,2);
        assertTrue(dag.containsVertex(v12a)); // eq semantics !
        coverageGraph.insert(v12a);
        assertEquals(5,dag.getVertexCount()); // no change

        assertTrue(dag.containsVertex(v12a));
        assertTrue(dag.containsVertex(v12));

        assertEquals(1,dag.getSuccessorCount(root));
        assertEquals(2,dag.getSuccessorCount(v1));
        assertEquals(1,dag.getSuccessorCount(v12));
        assertEquals(1,dag.getSuccessorCount(v13));
        assertEquals(0,dag.getSuccessorCount(v123));

        assertTrue(dag.getSuccessors(root).contains(v1));
        assertTrue(dag.getSuccessors(v1).contains(v12));
        assertTrue(dag.getSuccessors(v1).contains(v13));
        assertTrue(dag.getSuccessors(v12).contains(v123));
        assertTrue(dag.getSuccessors(v13).contains(v123));


    }


    private static BitSet bitsetOf(int... values) {
        BitSet bitset = new BitSet();
        for (int i:values) {
            bitset.set(i);
        }
        return bitset;
    }


}
