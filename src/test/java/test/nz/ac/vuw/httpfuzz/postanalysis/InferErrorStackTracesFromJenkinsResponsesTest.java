package test.nz.ac.vuw.httpfuzz.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import nz.ac.wgtn.cornetto.postanalysis.InferErrorStackTracesFromJenkinsResponses;
import org.junit.Test;
import java.io.File;

import static org.junit.Assert.*;


// @TODO test must be rewritten for JSON - based representation of errors
public class InferErrorStackTracesFromJenkinsResponsesTest {

    private StackTrace readStacktrace(String resourceName) throws Exception {
        File input = new File(getClass().getResource(resourceName).getFile());
        ObjectMapper objectMapper = new ObjectMapper();
        ServerError error = objectMapper.readValue(input,ServerError.class);
        return new InferErrorStackTracesFromJenkinsResponses().extractStackTrace(error);
    }

    @Test
    public void test1() throws Exception {
        StackTrace stacktrace = readStacktrace("/postanalysis/jenkins/server-error-1.json");
        assertNotNull(stacktrace);
        assertEquals("javax.servlet.ServletException",stacktrace.getExceptionType());
        assertTrue(stacktrace.getExceptionMessage()==null || stacktrace.getExceptionMessage().trim().length()==0);
        assertEquals("jenkins.model.Jenkins.doIconSize(Jenkins.java:4599)",stacktrace.getEntries().get(0));
        assertEquals("java.base/java.lang.invoke.MethodHandle.invokeWithArguments(MethodHandle.java:710)",stacktrace.getEntries().get(1));
    }

    @Test
    public void test2() throws Exception {
        StackTrace stacktrace = readStacktrace("/postanalysis/jenkins/server-error-2.json");
        assertNotNull(stacktrace);
        assertEquals("java.lang.StackOverflowError",stacktrace.getExceptionType());
        assertTrue(stacktrace.getExceptionMessage()==null || stacktrace.getExceptionMessage().trim().length()==0);
        assertEquals("org.aspectj.runtime.reflect.JoinPointImpl$StaticPartImpl.toString(JoinPointImpl.java:64)",stacktrace.getEntries().get(0));
        assertEquals("org.aspectj.runtime.reflect.JoinPointImpl.toString(JoinPointImpl.java:128)",stacktrace.getEntries().get(1));
    }

}
