package test.nz.ac.vuw.httpfuzz.jee.doop;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.StaticModel;
import nz.ac.wgtn.cornetto.jee.doop.PointstoBuilder;
import nz.ac.wgtn.cornetto.jee.doop.StaticModelFromDoop;
import nz.ac.wgtn.cornetto.stan.pointsto.Pointsto;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import java.io.File;
import java.io.FileReader;
import java.util.EnumSet;

import static org.junit.Assert.*;

@Ignore
public class TestExample1DoopPointsTo {

    private static StaticModelFromDoop model = null;

    @BeforeClass
    public static void buildStaticAnalysisModel() throws Exception {
        File file = new File("examples/example1/staticanalysis/doop/VarPointsTo.csv");
        Preconditions.checkState(file.exists());
        System.out.println("Importing VarPointsTo.csv from " + file);

        PointstoBuilder.Config config = new PointstoBuilder.Config();
        config.lineFilter = s -> s.contains("example1.MainServlet") || s.contains("Mock");

        Pointsto pointsto = new PointstoBuilder().build(new FileReader(file),config);
        model = new StaticModelFromDoop(null,pointsto, cl -> cl.startsWith("example1."));

    }

    @Test
    public void testRequestKeys() throws Exception {
        assertEquals(2,model.getRequestParameterNames().size());
        assertTrue(model.getRequestParameterNames().contains("magic-request-key-1"));
        assertTrue(model.getRequestParameterNames().contains("magic-request-key-2"));
    }

    @Test
    public void testHeaderKeys() throws Exception {
        assertEquals(1,model.getHeaderNames().size());
        assertTrue(model.getHeaderNames().contains("magic-header-key-1"));
    }

    @Test
    public void testStringLiterals() throws Exception {
        assertTrue(model.getStringLiterals(EnumSet.allOf(StaticModel.Scope.class)).contains("magic-request-key-1"));
        assertTrue(model.getStringLiterals(EnumSet.allOf(StaticModel.Scope.class)).contains("magic-request-key-2"));
        assertTrue(model.getStringLiterals(EnumSet.allOf(StaticModel.Scope.class)).contains("magic-header-key-1"));
        assertTrue(model.getStringLiterals(EnumSet.allOf(StaticModel.Scope.class)).contains("magic-request-value-1"));
        assertTrue(model.getStringLiterals(EnumSet.allOf(StaticModel.Scope.class)).contains("magic-header-value-1"));
    }
}
