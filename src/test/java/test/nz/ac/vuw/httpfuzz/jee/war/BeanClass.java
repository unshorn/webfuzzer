package test.nz.ac.vuw.httpfuzz.jee.war;

public class BeanClass {

    public String stringField;
    public boolean booleanField;
    public int intField;

    public int getInt() {
        return intField;
    }

    public void setInt(int value) {
        intField = value;
    }

    public boolean isBoolean() {
        return booleanField;
    }

}
