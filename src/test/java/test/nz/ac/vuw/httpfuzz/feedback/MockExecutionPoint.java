package test.nz.ac.vuw.httpfuzz.feedback;

import nz.ac.wgtn.cornetto.ExecutionPoint;

public class MockExecutionPoint implements ExecutionPoint {
    private String name = null;

    public MockExecutionPoint(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MockExecutionPoint that = (MockExecutionPoint) o;

        return name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "MockExecutionPoint{" + name + '}';
    }
}
