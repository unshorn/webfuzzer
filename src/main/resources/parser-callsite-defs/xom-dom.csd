<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>xom-dom</name>
    <patterns>
        <pattern>
            <classname>nu.xom.Builder</classname>
            <methodname>build</methodname>
        </pattern>
    </patterns>
    <namespace>nu.xom</namespace>
</callsites>

