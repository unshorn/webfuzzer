<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>fastjson-dom</name>
    <patterns>
        <pattern>
            <classname>com.alibaba.fastjson.JSON</classname>
            <methodname>parseObject</methodname>
            <descriptor>.*Lcom/alibaba/fastjson/JSONObject;</descriptor>
        </pattern>
        <pattern>
            <classname>com.alibaba.fastjson.JSON</classname>
            <methodname>parseArray</methodname>
            <descriptor>.*Lcom/alibaba/fastjson/JSONArray;</descriptor>
        </pattern>
    </patterns>
    <namespace>com.alibaba.fastjson</namespace>
</callsites>

