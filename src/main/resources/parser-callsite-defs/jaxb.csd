<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>XML</format>
    <generated>true</generated>
    <name>jaxb-bind</name>
    <patterns>
        <pattern>
            <classname>javax.xml.bind.Unmarshaller</classname>
            <methodname>unmarshal</methodname>
        </pattern>
    </patterns>
    <namespace>javax.xml.bind</namespace>
</callsites>

