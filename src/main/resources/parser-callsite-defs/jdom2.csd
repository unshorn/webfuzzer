<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jdom2-dom</name>
    <patterns>
        <!-- different lower-level parsing strategies can be used to build the jdom representation -->
        <pattern>
            <classname>org.jdom2.input.SAXBuilder</classname>
            <methodname>build</methodname>
        </pattern>
        <pattern>
            <classname>org.jdom2.input.DOMBuilder</classname>
            <methodname>build</methodname>
        </pattern>
        <pattern>
            <classname>org.jdom2.input.StAXEventBuilder</classname>
            <methodname>build</methodname>
        </pattern>
        <pattern>
            <classname>org.jdom2.input.StAXStreamBuilder</classname>
            <methodname>build</methodname>
        </pattern>
    </patterns>
    <namespace>org.jdom2</namespace>
</callsites>

