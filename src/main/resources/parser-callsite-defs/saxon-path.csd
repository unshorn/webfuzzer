<?xml version="1.0"?>
<callsites>
    <type>PATHQUERY</type>
    <format>XML</format>
    <generated>false</generated>
    <name>saxon-path</name>
    <patterns>
        <pattern>
            <classname>net.sf.saxon.s9api.XPathCompiler</classname>
        </pattern>
        <pattern>
            <classname>net.sf.saxon.xpath.XPathFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>net.sf.saxon</namespace>
</callsites>

