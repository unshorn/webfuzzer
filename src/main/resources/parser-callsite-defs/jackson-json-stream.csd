<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>jackson-pull</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.jackson.core.JsonParser</classname>
            <methodname>nextToken</methodname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.jackson</namespace>
</callsites>

