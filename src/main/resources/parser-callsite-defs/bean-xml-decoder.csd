<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>XML</format>
    <generated>false</generated>
    <name>beandecoder-bind</name>
    <patterns>
        <pattern>
            <classname>java.beans.XMLDecoder</classname>
            <methodname>readObject</methodname>
        </pattern>
    </patterns>
    <namespace>java.beans</namespace>
</callsites>

