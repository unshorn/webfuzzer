<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>XML</format>
    <generated>false</generated>
    <name>xerces-pull</name>
    <patterns>
        <pattern>
            <classname>org.apache.xerces.stax.XMLEventFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.apache.xerces</namespace>
</callsites>

