<?xml version="1.0"?>
<callsites>
    <type>PUSH</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jaxp-push</name>
    <patterns>
        <pattern>
            <classname>javax.xml.parsers.SAXParser</classname>
            <methodname>parse</methodname>
        </pattern>
    </patterns>
    <namespace>javax.xml</namespace>
</callsites>

