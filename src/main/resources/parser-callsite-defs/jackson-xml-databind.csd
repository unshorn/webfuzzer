<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jackson-bind</name>
    <patterns>
        <pattern>
            <classname>com.fasterxml.jackson.dataformat.xml.XmlMapper</classname>
            <methodname>readValue</methodname>
        </pattern>
    </patterns>
    <namespace>com.fasterxml.jackson</namespace>
</callsites>

