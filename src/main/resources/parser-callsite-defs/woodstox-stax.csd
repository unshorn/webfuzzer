<?xml version="1.0"?>
<callsites>
    <type>PULL</type>
    <format>XML</format>
    <generated>false</generated>
    <name>woodstox-pull</name>
    <patterns>
        <pattern>
            <classname>com.ctc.wstx.evt.WstxEventReader</classname>
        </pattern>
        <pattern>
            <classname>com.ctc.wstx.sr.StreamScanner</classname>
        </pattern>
        <pattern>
            <classname>com.ctc.wstx.sr.BasicStreamReader</classname>
        </pattern>
        <pattern>
            <classname>com.ctc.wstx.sr.TypedStreamReader</classname>
        </pattern>
    </patterns>
    <namespace>com.ctc.wstx</namespace>
</callsites>

