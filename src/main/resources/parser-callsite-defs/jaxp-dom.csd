<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>jaxp-dom</name>
    <patterns>
        <pattern>
            <classname>javax.xml.parsers.DocumentBuilder</classname>
            <methodname>parse</methodname>
        </pattern>
    </patterns>
    <namespace>javax.xml</namespace>
</callsites>

