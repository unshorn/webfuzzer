<?xml version="1.0"?>
<callsites>
    <type>DOM</type>
    <format>XML</format>
    <generated>false</generated>
    <name>xerces-dom</name>
    <patterns>
        <pattern>
            <classname>org.apache.xerces.parsers.DOMParser</classname>
        </pattern>
        <pattern>
            <classname>org.apache.xerces.parsers.DOMParserImpl</classname>
        </pattern>
        <pattern>
            <classname>org.apache.xerces.jaxp.DocumentBuilderImpl</classname>
        </pattern>
        <pattern>
            <classname>org.apache.xerces.jaxp.DocumentBuilderFactoryImpl</classname>
        </pattern>
    </patterns>
    <namespace>org.apache.xerces</namespace>
</callsites>

