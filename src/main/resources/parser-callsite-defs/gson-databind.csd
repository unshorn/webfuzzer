<?xml version="1.0"?>
<callsites>
    <type>DATABIND</type>
    <format>JSON</format>
    <generated>false</generated>
    <name>gson-bind</name>
    <patterns>
        <pattern>
            <classname>com.google.gson.Gson</classname>
            <methodname>fromJson</methodname>
        </pattern>
    </patterns>
    <namespace>com.google.gson</namespace>
</callsites>

