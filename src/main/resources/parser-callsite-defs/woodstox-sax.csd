<?xml version="1.0"?>
<callsites>
    <type>PUSH</type>
    <format>XML</format>
    <generated>false</generated>
    <name>woodstox-push</name>
    <patterns>
        <pattern>
            <classname>com.ctc.wstx.sax.WstxSAXParser</classname>
        </pattern>
        <pattern>
            <classname>com.ctc.wstx.sax.WstxSAXParser</classname>
        </pattern>
        <pattern>
            <classname>com.ctc.wstx.sax.WstxSAXParserFactory</classname>
        </pattern>
    </patterns>
    <namespace>com.ctc.wstx</namespace>
</callsites>

