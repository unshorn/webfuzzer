package nz.ac.wgtn.cornetto;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import java.util.concurrent.atomic.AtomicLong;

/**
 * History implementation based on a cache with limited size.
 * @author jens dietrich
 */

public class DefaultHistory implements History {

    private Cache<RequestSpec,Boolean> requests = CacheBuilder.newBuilder()
        .maximumSize(500_000)
        .build();

    private AtomicLong duplicateCount =  new AtomicLong();

    // should only be called ones per spec  instance (not enforced) !
    @Override
    public boolean isNew(RequestSpec spec) {
        boolean value = null == requests.getIfPresent(spec);
        if (!value) {
            duplicateCount.incrementAndGet();
        }
        return value;
    }

    @Override
    public void keep(RequestSpec spec) {
        requests.put(spec,Boolean.TRUE);
    }

    @Override
    public long getDuplicatesDetectedCount() {
        return duplicateCount.get();
    }
}
