package nz.ac.wgtn.cornetto.output;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import nz.ac.wgtn.cornetto.jee.HttpRecord;
import nz.ac.wgtn.cornetto.jee.IssuePriority;

import java.util.Objects;

/**
 * Representation of a server error.
 * Can be serialized to / deserialized from JSON with bean-based databinding frameworks such as gson or jackson.
 * @author jens dietrich
 */
@JsonPropertyOrder({ "priority","method","uri","requestHeaders","parameters","requestEntity","statusCode","statusReasonPhrase","responseHeaders","responseEntity","stacktrace","requestGenerationProvenance"})
public class ServerError extends Issue {

    private StackTrace stacktrace = null;

    public static ServerError newFrom(HttpRecord httpRecord, String requestEntity, String responseEntity, IssuePriority priority, StackTrace stacktrace) {
        ServerError error = new ServerError();
        error.initWith(httpRecord,requestEntity,responseEntity,priority);
        error.stacktrace = stacktrace;
        return error;
    }

    public StackTrace getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(StackTrace stacktrace) {
        this.stacktrace = stacktrace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ServerError that = (ServerError) o;
        return Objects.equals(stacktrace, that.stacktrace);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), stacktrace);
    }
}
