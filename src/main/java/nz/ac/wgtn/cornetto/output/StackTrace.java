package nz.ac.wgtn.cornetto.output;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple representation of a stacktrace extracted during post analysis.
 * Sufficient to match exceptions and stacktraces, including line numbers.
 * @author jens dietrich
 */
public class StackTrace {

    private String exceptionType = null;
    private String exceptionMessage = null;
    private List<String> entries = new ArrayList<>();
    private StackTrace cause = null;

    public StackTrace getCause() {
        return cause;
    }

    public void setCause(StackTrace cause) {
        this.cause = cause;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public List<String> getEntries() {
        return entries;
    }

    public void addEntry(String entry) {
        this.entries.add(entry);
    }

    public void addEntries(List<String> entries) { this.entries.addAll(entries);}

    // no equals and hashCode !! see aggregators

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        StackTrace that = (StackTrace) o;
//        return Objects.equals(exceptionType, that.exceptionType) &&
//                Objects.equals(exceptionMessage, that.exceptionMessage) &&
//                Objects.equals(entries, that.entries) &&
//                Objects.equals(cause, that.cause);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(exceptionType, exceptionMessage, entries, cause);
//    }
}
