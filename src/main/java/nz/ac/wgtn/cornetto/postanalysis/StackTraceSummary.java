package nz.ac.wgtn.cornetto.postanalysis;

import nz.ac.wgtn.cornetto.output.StackTrace;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Data structure representing the top segment of a stacktrace, used for aggregation.
 * @author jens dietrich
 */
public class StackTraceSummary {

    public static final int ALL = -1;

    private String exception = null;
    private List<String> topEntries = null;

    StackTraceSummary(StackTrace stacktrace, int maxDepth) {
        this.exception = stacktrace.getExceptionType();
        if (maxDepth==ALL) {
            this.topEntries = new ArrayList<>();
            this.topEntries.addAll(stacktrace.getEntries());
        }
        else {
            this.topEntries = stacktrace.getEntries().stream().limit(maxDepth).collect(Collectors.toList());
        }
    }

    @Override
    public String toString() {
        return this.exception;
    }

    public StackTraceSummary(String exception, List<String> topEntries) {
        this.exception = exception;
        this.topEntries = topEntries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StackTraceSummary that = (StackTraceSummary) o;
        return Objects.equals(exception, that.exception) &&
                Objects.equals(topEntries, that.topEntries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exception, topEntries);
    }
}
