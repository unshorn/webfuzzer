package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import nz.ac.wgtn.cornetto.listeners.CoverageDetailsListener;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static nz.ac.wgtn.cornetto.postanalysis.Commons.addProvenance2Latex;

/**
 * Compare results with zap results.
 * For n runs, for both cornetto and zap compute the union and the intersection of methods covered.
 * The report the total covered, and the set differences.
 * @author jens dietrich
 */
public class CompareCoverageWithZap {


    protected final static Logger LOGGER = Logger.getLogger(CompareCoverageWithZap.class);

    enum Metrics {
        ZAP_RUN_COUNT,
        CORNETTO_REQUEST_SIZE_UNION,
        CORNETTO_SYSTEM_SIZE_UNION,
        ZAP_REQUEST_SIZE_UNION,
        ZAP_SYSTEM_SIZE_UNION,
        REQUEST_UNION_CORNETTO_MIN_ZAP,
        REQUEST_UNION_ZAP_MIN_CORNETTO,
        REQUEST_INTERSECTION_CORNETTO_MIN_ZAP,
        REQUEST_INTERSECTION_ZAP_MIN_CORNETTO,
        SYSTEM_UNION_CORNETTO_MIN_ZAP,
        SYSTEM_UNION_ZAP_MIN_CORNETTO,
        SYSTEM_INTERSECTION_CORNETTO_MIN_ZAP,
        SYSTEM_INTERSECTION_ZAP_MIN_CORNETTO,
    }

    public static void main (String[] args) {

        Preconditions.checkArgument(args.length==1,"this script requires one argument -- the name of the output files (.tex)");
        File outputFile = new File(args[0]);

        BasicConfigurator.configure();

        Map<String,Map<Metrics,String>> metricsByDataset = new LinkedHashMap<>();

        for (String dataset : DataSets.DATA_SETS) {
            LOGGER.info("Analysing dataset " + dataset);

            Set<String> allMethods = null;
            List<List<Set<String>>> cornettoRequestMethodCoverageTimeseries = new ArrayList<>();
            List<List<Set<String>>> cornettoSystemMethodCoverageTimeseries = new ArrayList<>();
            List<List<Set<String>>> zapRequestMethodCoverageTimeseries = new ArrayList<>();
            List<List<Set<String>>> zapSystemMethodCoverageTimeseries    = new ArrayList<>();

            for (File folder:DataSets.getResults(dataset)) {
                Set<String> _allMethods = AnalyseCoverage.readMethods(new File(folder, CoverageDetailsListener.fileNameAll), m -> true);
                if (allMethods == null) {
                    allMethods = _allMethods;
                }
                assert allMethods.equals(_allMethods);

                final Set<String> allMethods2 = allMethods; // alias final to use in lambdas

                LOGGER.info("\tReading cornetto request coverage timeseries in " + folder.getAbsolutePath());
                cornettoRequestMethodCoverageTimeseries.add(
                    computeCoverageTimeseries(folder,
                        CoverageDetailsListener.fileName4MethodsTriggeredByRequests + "$" + CoverageDetailsListener.fileExtension,
                        m -> allMethods2.contains(m)
                    )
                );

                LOGGER.info("\tReading cornetto system coverage timeseries in " + folder.getAbsolutePath());
                cornettoSystemMethodCoverageTimeseries.add(
                    computeCoverageTimeseries(folder,
                        CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + "$" + CoverageDetailsListener.fileExtension,
                        m -> allMethods2.contains(m)
                    )
                );
            }

            for (File folder:DataSets.getZapResults(dataset)) {
                Set<String> _allMethods = AnalyseCoverage.readMethods(new File(folder, CoverageDetailsListener.fileNameAll), m -> true);
                assert allMethods.equals(_allMethods);

                final Set<String> allMethods2 = allMethods; // alias final to use in lambdas

                LOGGER.info("\tReading zap request coverage timeseries in " + folder.getAbsolutePath());
                zapRequestMethodCoverageTimeseries.add(
                    computeCoverageTimeseries(folder,
                        CoverageDetailsListener.fileName4MethodsTriggeredByRequests + "$" + CoverageDetailsListener.fileExtension,
                        m -> allMethods2.contains(m)
                    )
                );

                LOGGER.info("\tReading zap system coverage timeseries in " + folder.getAbsolutePath());
                zapSystemMethodCoverageTimeseries.add(
                    computeCoverageTimeseries(folder,
                        CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + "$" + CoverageDetailsListener.fileExtension,
                        m -> allMethods2.contains(m)
                    )
                );
            }


            // comparison timepoint is last in series, whatever this is

            LOGGER.info("\tPicking coverage sets at dedicated timepoints from timeseries");
            Function<List,Integer> pickLast = list -> list.size()-1;
            List<Set<String>> cornettoRequestMethodSetsAtComparisonTimepoint = pick(cornettoRequestMethodCoverageTimeseries,pickLast);
            List<Set<String>> cornettoSystemMethodSetsAtComparisonTimepoint = pick(cornettoSystemMethodCoverageTimeseries,pickLast);
            List<Set<String>> zapRequestMethodSetsAtComparisonTimepoint = pick(zapRequestMethodCoverageTimeseries,pickLast);
            List<Set<String>> zapSystemMethodSetsAtComparisonTimepoint = pick(zapSystemMethodCoverageTimeseries,pickLast);

            LOGGER.info("\tAggregating values across runs");
            Set<String> unionOfCornettoRequestMethods = union(cornettoRequestMethodSetsAtComparisonTimepoint);
            Set<String> unionOfCornettoSystemMethods = union(cornettoSystemMethodSetsAtComparisonTimepoint);
            Set<String> unionOfZapRequestMethods = union(zapRequestMethodSetsAtComparisonTimepoint);
            Set<String> unionOfZapSystemMethods = union(zapSystemMethodSetsAtComparisonTimepoint);

            Map<Metrics, String> metrics = new HashMap<>();
            metricsByDataset.put(dataset, metrics);
            metrics.put(Metrics.ZAP_RUN_COUNT,Commons.INT_FORMAT.format(DataSets.getZapResults(dataset).size()));
            metrics.put(
                Metrics.REQUEST_UNION_CORNETTO_MIN_ZAP,
                Commons.INT_FORMAT.format(Sets.difference(unionOfCornettoRequestMethods, unionOfZapRequestMethods).size())
            );
            metrics.put(
                Metrics.REQUEST_UNION_ZAP_MIN_CORNETTO,
                Commons.INT_FORMAT.format(Sets.difference(unionOfZapRequestMethods, unionOfCornettoRequestMethods).size())
            );
            metrics.put(
                Metrics.SYSTEM_UNION_CORNETTO_MIN_ZAP,
                Commons.INT_FORMAT.format(Sets.difference(unionOfCornettoSystemMethods, unionOfZapSystemMethods).size())
            );
            metrics.put(
                Metrics.SYSTEM_UNION_ZAP_MIN_CORNETTO,
                Commons.INT_FORMAT.format(Sets.difference(unionOfZapSystemMethods, unionOfCornettoSystemMethods).size())
            );
        }

        LOGGER.info("Compiling comparison results");
        try (PrintWriter out = new PrintWriter(new FileWriter(outputFile))) {

            addProvenance2Latex(out, AnalyseCoverage.class);

            out.println("\\begin{table}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{l r r r}");

            // header
            out.println("\\hline");
            out.print("\\textbf{program} & \\textbf{zap runs} & \\textbf{COR-ZAP} & \\textbf{ZAP-COR}");
            out.println(" \\\\ \\hline");

            for (String dataset:DataSets.DATA_SETS) {
                Map<Metrics,String> metrics = metricsByDataset.get(dataset);
                out.print(dataset);
                out.print(" & ");
                out.print(metrics.get(Metrics.ZAP_RUN_COUNT));
                out.print(" & ");
                out.print(metrics.get(Metrics.REQUEST_UNION_CORNETTO_MIN_ZAP));
                out.print(" & ");
                out.print(metrics.get(Metrics.REQUEST_UNION_ZAP_MIN_CORNETTO));
//                out.print(" & ");
//                out.print(metrics.get(Metrics.SYSTEM_UNION_CORNETTO_MIN_ZAP));
//                out.print(" & ");
//                out.print(metrics.get(Metrics.SYSTEM_UNION_ZAP_MIN_CORNETTO));
//                out.print(" & ");
                out.println(" \\\\");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{Coverage comparison with ZAP -- program, number of runs, request methods discovered by Cornetto not discovered by ZAP in any run (COR-ZAP), request methods discovered by ZAP not discovered by Cornetto (ZAP-COR)}");
            out.println("\\label{tab:results:zap-comparison}");
            out.println("\\end{table}");


        } catch (Exception e) {
            LOGGER.error(e);
        }
        System.out.println("Done, results written to " + outputFile.getAbsolutePath());
    }

    /**
     * Read coverage files, and aggregate results. Start with index 1.
     * @param folder
     * @param fileNameTemplate $ is the variable to be replaced by a timepoint (int). Example:
     *   covered-methods-not-triggered-by-requests-*.txt
     * @return list of sets
     * @throws Exception
     */
     static List<Set<String>> computeCoverageTimeseries(File folder, String fileNameTemplate, Predicate<String> filter)  {
        Preconditions.checkArgument(fileNameTemplate.contains("$"));
        List<Set<String>> aggregatedCoverageSets = new ArrayList<>();
        int timepoint = 1;
        File file = null;
        while ((file = new File(folder,fileNameTemplate.replace("$",""+timepoint))).exists()) {
            Set<String> methods = AnalyseCoverage.readMethods(file,filter);
            computeNext(aggregatedCoverageSets,methods);
            timepoint = timepoint+1;
        }
        return aggregatedCoverageSets;
    }


    private static void computeNext (List<Set<String>> sets, Set<String> nextSet) {
        Set<String> aggregated = new HashSet<>();
        if (!sets.isEmpty()) {
            aggregated.addAll(sets.get(sets.size()-1));
        }
        aggregated.addAll(nextSet);
        sets.add(aggregated);
    }


    static Set<String> union (Collection<Set<String>> sets) {
         Set<String> union = new HashSet<>();
         for (Set<String> set:sets) {
             union.addAll(set);
         }
         return union;
    }

    /**
     * From each time series, pick a single coverage set (e.g. at a fixed timepoint, from the beginning or end).
     * @param coverageTimeseries
     * @return
     */
    static List<Set<String>>  pick(List<List<Set<String>>> coverageTimeseries, Function<List,Integer> timepointSelector) {
        List<Set<String>> selectedCoverageSets = new ArrayList<>();
        for (List<Set<String>> timeseries:coverageTimeseries) {
            int pos = timepointSelector.apply(timeseries);
            assert pos>-1;
            assert pos<timeseries.size();
            selectedCoverageSets.add(timeseries.get(pos));
        }
        return selectedCoverageSets;
    }



}

