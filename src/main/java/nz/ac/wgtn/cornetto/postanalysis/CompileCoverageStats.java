package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.listeners.CoverageDetailsListener;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Utility to compile coverage stats.
 * @author jens dietrich
 */

@Deprecated
public class CompileCoverageStats {

    public static void main (String [] args) throws Exception {

        Preconditions.checkArgument(args.length==1,"One argument expected, the folder containing logs/results");
        File outputRootFolder = new File(args[0]);

        File fileWithAllMethods = new File(outputRootFolder,CoverageDetailsListener.fileNameAll);
        Preconditions.checkState(fileWithAllMethods.exists());

        Set<String> allMethods = readMethods(fileWithAllMethods);

        File resultFile = new File("coverage-stats.csv");
        List<String> csvData = new ArrayList<>();

        String header = Stream.of("all methods","methods triggered by requests","methods triggered by server","all methods recorded","all methods recorded and known").collect(Collectors.joining("\t"));
        csvData.add(header);

        // base sets are cumulative
        Set<String> methodsTriggeredByRequests = new HashSet<>();
        Set<String> methodsTriggeredBySystem = new HashSet<>();

        int step = 0;
        while (true) {
            step = step+1;
            System.out.println("Processing coverage logs at step " + step);
            File fileWithRecordedMethodsTriggeredByRequests = new File(outputRootFolder,CoverageDetailsListener.fileName4MethodsTriggeredByRequests + step + CoverageDetailsListener.fileExtension);
            File fileWithRecordedMethodsTriggeredBySystem = new File(outputRootFolder,CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + step + CoverageDetailsListener.fileExtension);
            if (fileWithRecordedMethodsTriggeredByRequests.exists() && fileWithRecordedMethodsTriggeredBySystem.exists()) {
                methodsTriggeredByRequests.addAll(readMethods(fileWithRecordedMethodsTriggeredByRequests));
                methodsTriggeredBySystem.addAll(readMethods(fileWithRecordedMethodsTriggeredBySystem));

                // metrics
                int all = allMethods.size();
                int reqTriggered = methodsTriggeredByRequests.size();
                int sysTriggered = methodsTriggeredBySystem.size();

                Set<String> allCovered = new HashSet<>(); // construct new set, to remove duplicates
                allCovered.addAll(methodsTriggeredByRequests);
                allCovered.addAll(methodsTriggeredBySystem);
                int allTriggered = allCovered.size();


                int methodsGeneratedAtRuntimeCounter = 0;
                int otherMethodsCounter = 0;

                // remove eventual methods covered not available in allMethods , e.g. methods generated at runtime
                Set<String> allCoveredSantitised = new HashSet<>();
                for (String m:allCovered) {
                    if (allMethods.contains(m)) {
                        allCoveredSantitised.add(m);
                    }
                    else {
                        if (m.contains("$$")) {
                            methodsGeneratedAtRuntimeCounter = methodsGeneratedAtRuntimeCounter+1;
                        }
                        else {
                            otherMethodsCounter = otherMethodsCounter +1;
                            System.out.println("\tmethod encountered at runtime not seen in static model: " + m);
                        }
                    }
                }
                int allTriggeredSanitised = allCoveredSantitised.size();

                System.out.println("Methods generated at runtime (containing $$): " + methodsGeneratedAtRuntimeCounter);
                System.out.println("Other methods not captured in static model: " + otherMethodsCounter);

                String line = IntStream.of(all,reqTriggered,sysTriggered,allTriggered,allTriggeredSanitised).mapToObj(c -> ""+c).collect(Collectors.joining("\t"));
                csvData.add(line);

            }
            else {
                Files.write(resultFile.toPath(),csvData, Charset.defaultCharset());
                System.out.println("Done -- coverage stats written to " + resultFile.getAbsolutePath());
                break;
            }
        }


    }

    private static Set<String> readMethods(File file)  {
        try {
            return Files.lines(Paths.get(file.getPath())).collect(Collectors.toSet());
        }
        catch (Exception x) {
            // causeing crash is ok, this makes it easier to use in lambdas
            throw new RuntimeException(x);
        }
    }

}
