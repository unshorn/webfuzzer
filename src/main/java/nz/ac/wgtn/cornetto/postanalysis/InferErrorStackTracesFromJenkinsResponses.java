package nz.ac.wgtn.cornetto.postanalysis;

import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import java.util.Iterator;

/**
 * Utility to extract stacktraces from logged jenkins responses.
 * @author jens dietrich
 */
public class InferErrorStackTracesFromJenkinsResponses extends StackTraceInference<ServerError> {

    @Override
    public StackTrace extractStackTrace(ServerError error) throws Exception {

        String responseEntity = error.getResponseEntity();

        Document dom = Jsoup.parse(responseEntity);
        Iterator<Element> iter = dom.select("#main-panel").iterator();
        if (iter.hasNext()) {
            Element main = iter.next();
            Iterator<Element> iter2 = main.select("pre").iterator();
            if (iter2.hasNext()) {
                Element pre = iter2.next();
                String trace = pre.text();
                return parse(trace);
            }
        }
        return null;

    }

}
