package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import nz.ac.wgtn.cornetto.listeners.CoverageDetailsListener;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Simple script to analyse the methods collected, aggregated them, and print the results to the console.
 * To be used in some quick and dirty adhoc analysis. Only analyses one dataset / run at a time.
 * @author jens dietrich
 */
public class DiffAggregatedMethodsCovered {


    public static final int[] COVERAGE_REPORTING_STEPS = {10,60,120,240,300};
    protected final static Logger LOGGER = Logger.getLogger(DiffAggregatedMethodsCovered.class);

    public static void main (String[] args) {

        BasicConfigurator.configure();
        File resultFolder1 = new File("/Users/jens/Development/tmp/zapped/results-jenkins-1");
        File resultFolder2 = new File("results-jenkins-1");

        LOGGER.info("Comparing coverage data from " + resultFolder1.getAbsolutePath());
        LOGGER.info("\t and " + resultFolder2.getAbsolutePath());

        Set<String> allMethods1 = AnalyseCoverage.readMethods(new File(resultFolder1, CoverageDetailsListener.fileNameAll), m -> true);
        Set<String> allMethods2 = AnalyseCoverage.readMethods(new File(resultFolder1, CoverageDetailsListener.fileNameAll), m -> true);

        Preconditions.checkState(allMethods1.equals(allMethods2));
        Set<String> allMethods = allMethods1;
        System.out.println("\tbaseline (number of application methods gathered in pre-analysis): " + allMethods1.size());


        List<Set<String>> requestMethods1 = new ArrayList<>();
        List<Set<String>> requestMethods2 = new ArrayList<>();
        List<Set<String>> systemMethods1 = new ArrayList<>();
        List<Set<String>> systemMethods2 = new ArrayList<>();
        int maxSteps = COVERAGE_REPORTING_STEPS[COVERAGE_REPORTING_STEPS.length-1] +1;

        for (int timepoint=1;timepoint<=maxSteps;timepoint++) {
            computeNext(requestMethods1,AnalyseCoverage.readMethods(new File(resultFolder1,CoverageDetailsListener.fileName4MethodsTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension),m -> allMethods.contains(m)));
            computeNext(requestMethods2,AnalyseCoverage.readMethods(new File(resultFolder2,CoverageDetailsListener.fileName4MethodsTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension),m -> allMethods.contains(m)));
            computeNext(systemMethods1,AnalyseCoverage.readMethods(new File(resultFolder1,CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension),m -> allMethods.contains(m)));
            computeNext(systemMethods2,AnalyseCoverage.readMethods(new File(resultFolder2,CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension),m -> allMethods.contains(m)));
        }

        LOGGER.info("step : all , s1 , s2 , (s1-s2) , (s2-s1) ");
        for (int step:COVERAGE_REPORTING_STEPS) {
            int diff12 = Sets.difference(requestMethods1.get(step),requestMethods2.get(step)).size();
            int diff21 = Sets.difference(requestMethods2.get(step),requestMethods1.get(step)).size();
            LOGGER.info("step " + step + " (req): " + allMethods.size() + " , " + requestMethods1.get(step).size() + " , " + requestMethods2.get(step).size() + " , " + diff12 + " , " + diff21);

//            diff12 = Sets.difference(systemMethods1.get(step),systemMethods1.get(step)).size();
//            diff21 = Sets.difference(systemMethods2.get(step),systemMethods2.get(step)).size();
//            LOGGER.info("step " + step + " (sys): " + allMethods.size() + systemMethods1.get(step).size() + " , " + systemMethods2.get(step).size() + " , " + diff12 + " , " + diff21);
        }

        System.out.println("done");
    }


    private static void computeNext (List<Set<String>> sets, Set<String> nextSet) {
        Set<String> aggregated = new HashSet<>();
        if (!sets.isEmpty()) {
            aggregated.addAll(sets.get(sets.size()-1));
        }
        aggregated.addAll(nextSet);
        sets.add(aggregated);
    }

}

