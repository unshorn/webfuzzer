package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.listeners.CoverageDetailsListener;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static nz.ac.wgtn.cornetto.postanalysis.Commons.INT_FORMAT;
import static nz.ac.wgtn.cornetto.postanalysis.Commons.addProvenance2Latex;

/**
 * Utility to compile coverage stats over multiple runs, including some stats about variability.
 * @author jens dietrich
 */
public class AnalyseCoverage {

    protected final static Logger LOGGER = Logger.getLogger(AnalyseCoverage.class);

    // steps correspond to minutes into the analysis -- they are used in file names, it is required that all of those files exist for all runs
    // otherwise a runtime exception will be thrown
    public static final int MIN_STEP = 1;
    public static final int MAX_STEP = 300;

    public static final int[] COVERAGE_REPORTING_STEPS = {2,3,5,10,30,60,120,240,300};
    public static final String COVERAGE_TABLE_CAPTION = "Average number of application methods executed within N mins after fuzzing started, averages over a number of runs (column 2) are reported. Column 3 shows the number of application methods computed by the static pre-analysis. The first row for a program shows the number of request invocations, the second row shows the number system invocations. The last column reports the time (min from start) when top coverage was reached";
    public static final String COVERAGE_TABLE_LABEL = "tab:results:coverage";

    public static final int[] COVERAGE_CONVERGENCE_REPORTING_STEPS = {5,30,60,120,240,300};
    public static final String COVERAGE_CONVERGENCE_TABLE_CAPTION = "Maximum Jaccard distance between request (first row per dataset) and system (second row per dataset) invocations for selected timepoints in mins after fuzzing started";
    public static final String COVERAGE_CONVERGENCE_TABLE_LABEL = "tab:results:coverage-convergence";

    public static void main (String [] args) throws Exception {
        Preconditions.checkArgument(args.length==2,"this script requires two arguments -- the name of the output files for coverage and coverage convergence stats (.tex)");
        File coverageStatsFile = new File(args[0]);
        File coverageConvergenceStatsFile = new File(args[1]);

        BasicConfigurator.configure();

        Map<String,List<List<Set<String>>>> requestMethodsByRunByDataset = new LinkedHashMap<>();
        Map<String,List<List<Set<String>>>> systemMethodsByRunByDataset = new LinkedHashMap<>();
        Map<String,Set<String>> allApplicationMethodsByDataset = new LinkedHashMap<>();
        Map<String,List<Set<String>>> allApplicationMethodsByRunByDataset = new LinkedHashMap<>();

        Map<String,Integer> lastTimepointWhenNewRequestMethodWasDiscovered = new HashMap<>();
        Map<String,Integer> lastTimepointWhenNewSystemMethodWasDiscovered = new HashMap<>();

        for (String dataset : DataSets.DATA_SETS) {
            LOGGER.info("Analysing dataset " + dataset);

            lastTimepointWhenNewRequestMethodWasDiscovered.put(dataset,0);
            lastTimepointWhenNewSystemMethodWasDiscovered.put(dataset,0);

            // request methods -- triggered by request
            List<List<Set<String>>> requestMethodsByRun = new ArrayList<>(); // each element represents one run, the inner list is sets of methods by timepoint
            requestMethodsByRunByDataset.put(dataset,requestMethodsByRun);
            // system methods -- not triggered by request
            List<List<Set<String>>> systemMethodsByRun = new ArrayList<>();
            systemMethodsByRunByDataset.put(dataset,systemMethodsByRun);
            // all methods -- base line, application methods recorded by static pre-analysis
            List<Set<String>> allMethodsByRun = new ArrayList<>();
            allApplicationMethodsByRunByDataset.put(dataset,allMethodsByRun);

            List<File> resultsByRun = DataSets.getResults(dataset);
            int count = 0;
            for (File run:resultsByRun) {
                LOGGER.info("Analysing run " + ++count);
                List<Set<String>> requestMethods = new ArrayList<>();
                requestMethodsByRun.add(requestMethods);
                List<Set<String>> systemMethods = new ArrayList<>();
                systemMethodsByRun.add(systemMethods);


                LOGGER.info("Reading all methods");
                Set<String> allMethods = readMethods(new File(run,CoverageDetailsListener.fileNameAll),m -> true);
                allMethodsByRun.add(allMethods);

                LOGGER.info("Reading request methods, and accumulating sets");
                Set<String> tmp = new HashSet<>();
                for (int timepoint=MIN_STEP;timepoint<=MAX_STEP;timepoint++) {
                    File file = new File(run,CoverageDetailsListener.fileName4MethodsTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension);
                    Preconditions.checkState(file.exists());
                    Set<String> methods = readMethods(file,m -> allMethods.contains(m));  // filter out compiler-generated methods to use baseline coverage !
                    if (!tmp.containsAll(methods)) {
                        // record that a new method has been discovered
                        int timepoint2 = timepoint; // to make final
                        lastTimepointWhenNewRequestMethodWasDiscovered.compute(dataset,(k,v) -> Math.max(v,timepoint2));
                    }
                    methods.addAll(tmp); // accumulate
                    requestMethods.add(methods);
                    tmp = methods;
                }

                LOGGER.info("Reading system methods, and accumulating sets");
                tmp = new HashSet<>();
                for (int timepoint=MIN_STEP;timepoint<=MAX_STEP;timepoint++) {
                    File file = new File(run,CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension);
                    Preconditions.checkState(file.exists());
                    Set<String> methods = readMethods(file,m -> allMethods.contains(m));  // filter out compiler-generated methods to use baseline coverage !
                    if (!tmp.containsAll(methods)) {
                        // record that a new method has been discovered
                        int timepoint2 = timepoint; // to make final
                        lastTimepointWhenNewSystemMethodWasDiscovered.compute(dataset,(k,v) -> Math.max(v,timepoint2));
                    }
                    methods.addAll(tmp); // accumulate
                    systemMethods.add(methods);
                    tmp = methods;
                }

            }

            LOGGER.info("Checking for base line consistency across runs");
            Set<String> previous = null;
            for (Set<String> methods:allMethodsByRun) {
                if (previous==null) {
                    previous = methods;
                    allApplicationMethodsByDataset.put(dataset,methods);
                }
                else if (!previous.equals(methods)) {
                    throw new IllegalStateException("Inconsistent base line encountered");
                }
            }
            LOGGER.info("ok");
        }

        LOGGER.info("Compiling coverage stats");
        try (PrintWriter out = new PrintWriter(new FileWriter(coverageStatsFile))) {

            addProvenance2Latex(out, AnalyseCoverage.class);
            String columFormats = new String(new char[COVERAGE_REPORTING_STEPS.length]).replace("\0", " r");
            columFormats = "l r r" + columFormats + " r";  // dataset column
            out.println("\\begin{table*}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{" + columFormats + "}");
            // header
            out.println("\\hline");
            out.print("\\textbf{program} & \\textbf{runs} & \\textbf{all} ");
            for (int step : COVERAGE_REPORTING_STEPS) {
                out.print(" & \\textbf{" + step + "m}");
            }
            out.print(" & \\textbf{last}");
            out.print(" \\\\ ");
            out.println("\\hline");

            // body
            for (String dataset : requestMethodsByRunByDataset.keySet()) {
                List<List<Set<String>>> requestMethodsByRun = requestMethodsByRunByDataset.get(dataset);
                List<List<Set<String>>> systemMethodsByRun = systemMethodsByRunByDataset.get(dataset);

                // first line: request methods
                out.print(dataset);
                out.print(" & ");
                out.print(DataSets.getResults(dataset).size());
                out.print(" & ");
                out.print(INT_FORMAT.format(allApplicationMethodsByDataset.get(dataset).size()));

                for (int step : COVERAGE_REPORTING_STEPS) {
                    out.print(" & ");
                    List<Integer> requestMethodCountsAtTimePoint = new ArrayList<>();
                    for (List<Set<String>> methods : requestMethodsByRun) {
                        requestMethodCountsAtTimePoint.add(methods.get(step-1).size());
                    }
                    String val = Commons.means2Latex(requestMethodCountsAtTimePoint);
                    out.print(val);
                }
                out.print(" & ");
                out.print(lastTimepointWhenNewRequestMethodWasDiscovered.get(dataset)+"m");
                out.println(" \\\\ ");

                // second line: system methods
                out.print(" & & ");
                for (int step : COVERAGE_REPORTING_STEPS) {
                    out.print(" & ");
                    List<Integer> systemMethodCountsAtTimePoint = new ArrayList<>();
                    for (List<Set<String>> methods : systemMethodsByRun) {
                        systemMethodCountsAtTimePoint.add(methods.get(step-1).size());
                    }
                    String val = Commons.means2Latex(systemMethodCountsAtTimePoint);

                    out.print(val);
                }
                out.print(" & ");
                out.print(lastTimepointWhenNewSystemMethodWasDiscovered.get(dataset)+"m");
                out.println(" \\\\ ");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{" + COVERAGE_TABLE_CAPTION + "}");
            out.println("\\label{" + COVERAGE_TABLE_LABEL + "}");
            out.println("\\end{table*}");

            LOGGER.info("Report written to: " + coverageStatsFile.getAbsolutePath());
        }


        LOGGER.info("Compiling coverage convergence stats");
        try (PrintWriter out = new PrintWriter(new FileWriter(coverageConvergenceStatsFile))) {

            addProvenance2Latex(out, AnalyseCoverage.class);
            String columFormats = new String(new char[COVERAGE_CONVERGENCE_REPORTING_STEPS.length]).replace("\0", " r");
            columFormats = "l" + columFormats;  // dataset column
            out.println("\\begin{table}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{" + columFormats + "}");

            // header
            out.println("\\hline");
            out.print("\\textbf{program} ");
            for (int step : COVERAGE_CONVERGENCE_REPORTING_STEPS) {
                out.print(" & \\textbf{" + step + "m}");
            }
            out.print(" \\\\ ");
            out.println("\\hline");

            // body
            for (String dataset : requestMethodsByRunByDataset.keySet()) {
                List<List<Set<String>>> requestMethodsByRun = requestMethodsByRunByDataset.get(dataset);
                List<List<Set<String>>> systemMethodsByRun = systemMethodsByRunByDataset.get(dataset);

                // first line: request methods
                out.print(dataset);

                for (int step : COVERAGE_CONVERGENCE_REPORTING_STEPS) {
                    out.print(" & ");
                    List<Set<String>> requestMethodsAtTimePointByRun = new ArrayList<>();
                    for (List<Set<String>> methods : requestMethodsByRun) {
                        requestMethodsAtTimePointByRun.add(methods.get(step-1));
                    }
                    String val = Commons.computeMaxJaccardDistance(requestMethodsAtTimePointByRun);
                    out.print(val);
                }
                out.println(" \\\\ ");

                // second line: system methods
                for (int step : COVERAGE_CONVERGENCE_REPORTING_STEPS) {
                    out.print(" & ");
                    List<Set<String>> systemMethodsAtTimePointByRun = new ArrayList<>();
                    for (List<Set<String>> methods : systemMethodsByRun) {
                        systemMethodsAtTimePointByRun.add(methods.get(step-1));
                    }
                    String val = Commons.computeMaxJaccardDistance(systemMethodsAtTimePointByRun);
                    out.print(val);
                }
                out.println(" \\\\ ");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{" + COVERAGE_CONVERGENCE_TABLE_CAPTION + "}");
            out.println("\\label{" + COVERAGE_CONVERGENCE_TABLE_LABEL + "}");
            out.println("\\end{table}");

            LOGGER.info("Report written to: " + coverageConvergenceStatsFile.getAbsolutePath());
        }
    }



    static Set<String> readMethods(File file, Predicate<String> methodFilter)  {
        try (Reader reader = new FileReader(file)) {
            List<String> lines = IOUtils.readLines(reader);
            return lines.stream().filter(methodFilter).collect(Collectors.toSet());
        }
        catch (Exception x) {
            // causing crash is ok, this makes it easier to use in lambdas
            throw new RuntimeException(x);
        }
    }
}
