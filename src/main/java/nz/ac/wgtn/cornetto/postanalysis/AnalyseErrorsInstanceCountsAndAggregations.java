package nz.ac.wgtn.cornetto.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import java.io.File;
import java.util.*;
import java.util.function.Function;

/**
 * Script to analyse recorded errors.
 * @author jens dietrich
 */
public class AnalyseErrorsInstanceCountsAndAggregations extends AbstractAnalyseIssuesInstanceCountsAndAggregations<ServerError> {

    private ObjectMapper mapper = new ObjectMapper();

    public static void main (String[] args) throws Exception {
        new AnalyseErrorsInstanceCountsAndAggregations().analyse(args);
    }

    @Override
    public Map<String, Function<ServerError, Object>> getAggregations() {
        return Aggregations.SERVER_ERROR_AGGREGATIONS;
    }

    @Override
    public String getTableCaption() {
        return "Average number of errors recorded over a number of runs and hash stacking aggregations using the full stacktrace, and the top 10, 3 and 1 frames, respectively.";
    }

    @Override
    public String getTableLabel() {
        return "tab:results:500";
    }

    @Override
    public boolean isSerialisedIssue(File f) {
        return f.getName().startsWith("server-error-") && f.getName().endsWith(".json");
    }

    @Override
    public ServerError readIssue(String dataset,File file) throws Exception {
        StackTraceInference<ServerError> stackTraceInference = DataSets.getStackTraceInference(dataset);
        ServerError error = mapper.readValue(file, ServerError.class);
        if (error.getStacktrace() == null) {
            try {
                StackTrace stacktrace = stackTraceInference.extractStackTrace(error);
                error.setStacktrace(stacktrace);
            } catch (Exception x) {
                LOGGER.error("Error inferring stacktraces", x);
            }
        }
        return error;
    }
}
