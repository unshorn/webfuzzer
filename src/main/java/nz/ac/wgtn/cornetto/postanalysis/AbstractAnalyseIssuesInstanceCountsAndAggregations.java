package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.output.Issue;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static nz.ac.wgtn.cornetto.postanalysis.Commons.addProvenance2Latex;
import static nz.ac.wgtn.cornetto.postanalysis.Commons.means2Latex;

/**
 * Abstract script to analyse recorded issues.
 * @author jens dietrich
 */
public abstract class AbstractAnalyseIssuesInstanceCountsAndAggregations<T extends Issue>  {

    public abstract Map<String, Function<T,Object>> getAggregations() ;

    public abstract T readIssue(String dataset,File file) throws Exception;

    public abstract String getTableCaption();

    public abstract String getTableLabel();

    public abstract boolean isSerialisedIssue(File file) ;

    public Logger LOGGER = Logger.getLogger(this.getClass());

    public void analyse(String[] args) {
        BasicConfigurator.configure();
        Preconditions.checkArgument(args.length==1,"this script requires one argument -- the name of the output file (.tex)");
        File resultFile = new File(args[0]);
        Map<String, Function<T,Object>> aggregations = getAggregations();

        Map<String, Map<String, String>> instanceStatsByAggregationByDataset = new LinkedHashMap<>();
        Map<String, Integer> runsByDataset = new LinkedHashMap<>();

        for (String dataset : DataSets.DATA_SETS) {
            LOGGER.info("Analysing dataset " + dataset);
            List<Map<String, Set<Object>>> eqClassesByAggregationOverMultipleRuns = new ArrayList<>();
            AtomicInteger runCounter = new AtomicInteger();
            runsByDataset.put(dataset,DataSets.getResults(dataset).size());
            DataSets.getResults(dataset).stream()
                .forEach(resultFolder -> {
                    LOGGER.info("Analysing dataset " + dataset + " run " + runCounter.incrementAndGet());

                    Map<String, Set<Object>> eqClassesByAggregation = new HashMap<>();
                    eqClassesByAggregationOverMultipleRuns.add(eqClassesByAggregation);

                    for (String aggregationName:aggregations.keySet()) {
                        eqClassesByAggregation.put(aggregationName, new HashSet<>());
                    }

                    for (File errorFile:resultFolder.listFiles(f -> isSerialisedIssue(f))) {
                        T issue = null;

                        try {
                            issue = readIssue(dataset,errorFile);
                            for (String aggregationName : aggregations.keySet()) {
                                Object aggregation = aggregations.get(aggregationName).apply(issue);
                                Set<Object> eqClass = eqClassesByAggregation.get(aggregationName);
                                eqClass.add(aggregation);
                            }
                        } catch (Exception x) {
                            LOGGER.error("Error reading serialized error", x);
                        }
                    }
                });

            LOGGER.info("Aggregating results for dataset " + dataset);
            Map<String, String>instanceStatsByAggregation = new HashMap<>();
            instanceStatsByAggregationByDataset.put(dataset,instanceStatsByAggregation);

            for (String aggregationName:aggregations.keySet()) {
                List<Integer> counts = new ArrayList<>();
                for (Map<String, Set<Object>> eqClassByAggregation:eqClassesByAggregationOverMultipleRuns) {
                    Set<Object> eqClass = eqClassByAggregation.get(aggregationName);
                    counts.add(eqClass.size());
                }
                String value = means2Latex(counts);
                instanceStatsByAggregation.put(aggregationName,value);
            }

        }

        LOGGER.info("Producing report");
        try (PrintWriter out = new PrintWriter(new FileWriter(resultFile))) {

            addProvenance2Latex(out,this);

            String columFormats = new String(new char[aggregations.size()]).replace("\0", " r");
            columFormats = "l r" + columFormats;  // dataset column
            out.println("\\begin{table}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{" + columFormats + "}");

            // header
            out.println("\\hline");
            out.print("\\textbf{program} & \\textbf{runs} ");
            for (String aggregation:aggregations.keySet()) {
                out.print(" & ");
                out.print("\\textbf{" + aggregation + "}");
            }
            out.println(" \\\\ ");
            out.println("\\hline");

            // body
            for (String dataset:instanceStatsByAggregationByDataset.keySet()) {
                Map<String, String> instanceStatsByAggregation = instanceStatsByAggregationByDataset.get(dataset);
                out.print(dataset);
                out.print(" & ");
                out.print(runsByDataset.get(dataset));
                for (String aggregation:aggregations.keySet()) {
                    out.print(" & ");
                    out.print(instanceStatsByAggregation.get(aggregation));
                }
                out.println(" \\\\ ");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{" + getTableCaption() + "}");
            out.println("\\label{" + getTableLabel() + "}");
            out.println("\\end{table}");

            LOGGER.info("Report written to: " + resultFile.getAbsolutePath());

        }
        catch (Exception ex) {
            LOGGER.error(ex);
        }



    }
}
