package nz.ac.wgtn.cornetto.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;

import java.io.File;

/**
 * Abstract taints for recorded XSS.
 * Note that the tainted values are often long , and this will make it difficult to represent this in latex table.
 * @author jens dietrich
 */
public class AnalyseXSSTaints extends AbstractAnalyseTaints<XSSVulnerability>  {

    private ObjectMapper mapper = new ObjectMapper();

    public static void main (String[] args) throws Exception {
        new AnalyseXSSTaints().analyse(args);
    }

    @Override
    public String getTaint(XSSVulnerability issue) {
        // TODO: fix typo -- note that databinding depends on this .. !!
        return issue.getTaintedInput();
    }

    @Override
    public String getTableLabel() {
        return "tab:results:xss-cs";
    }

    @Override
    public String getTableCaption() {
        return "TODO";
    }

    @Override
    public boolean isSerialisedIssue(File f) {
        return f.getName().startsWith("xss-") && f.getName().endsWith(".json");
    }

    @Override
    public XSSVulnerability readIssue(String dataset, File file) throws Exception {
        return mapper.readValue(file, XSSVulnerability.class);
    }
}
