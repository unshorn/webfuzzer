package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import nz.ac.wgtn.cornetto.output.ServerError;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Descriptor of the datasets used.
 * @author jens dietrich
 */
public class DataSets {

    public static final String WEBGOAT = "webgoat";
    public static final String JENKINS = "jenkins";
    public static final String DERBY = "derby";
    public static final List<String> DATA_SETS = Lists.newArrayList(WEBGOAT,JENKINS,DERBY);

    public static File workingDir = new File("experiment-data");

    // the result sets - each element is a folder containing files for individuals error or vulnerability instances recorded
    public static final List<File> getResults(String dataset) {
        Preconditions.checkArgument(DATA_SETS.contains(dataset));

        if (dataset.equals(WEBGOAT)) {
            return Stream.of(workingDir.listFiles())
                .filter(f -> f.isDirectory())
                .filter(f -> f.getName().startsWith("results-webgoat-"))
                .collect(Collectors.toList());
        }
        else if (dataset.equals(JENKINS)) {
            return Stream.of(workingDir.listFiles())
                    .filter(f -> f.isDirectory())
                    .filter(f -> f.getName().startsWith("results-jenkins-"))
                    .collect(Collectors.toList());
        }
        else if (dataset.equals(DERBY)) {
            return Stream.of(workingDir.listFiles())
                    .filter(f -> f.isDirectory())
                    .filter(f -> f.getName().startsWith("results-derby-"))
                    .collect(Collectors.toList());
        }
        throw new IllegalStateException("Dataset not yet supported: " + dataset);
    }

    public static final List<File> getZapResults(String dataset) {
        Preconditions.checkArgument(DATA_SETS.contains(dataset));

        File folder = new File(workingDir,"zap/"+dataset+"/");
        Preconditions.checkState(folder.exists());

        if (dataset.equals(WEBGOAT)) {
            return Stream.of(folder.listFiles())
                    .filter(f -> f.isDirectory())
                    .filter(f -> f.getName().startsWith("results-webgoat-"))
                    .collect(Collectors.toList());
        }
        else if (dataset.equals(JENKINS)) {
            return Stream.of(folder.listFiles())
                    .filter(f -> f.isDirectory())
                    .filter(f -> f.getName().startsWith("results-jenkins-"))
                    .collect(Collectors.toList());
        }
        else if (dataset.equals(DERBY)) {
            return Stream.of(folder.listFiles())
                    .filter(f -> f.isDirectory())
                    .filter(f -> f.getName().startsWith("results-derby-"))
                    .collect(Collectors.toList());
        }
        throw new IllegalStateException("Dataset not yet supported: " + dataset);
    }

    // specific scripts to process datascripts

    public static final StackTraceInference<ServerError> getStackTraceInference(String dataset) {
        Preconditions.checkArgument(DATA_SETS.contains(dataset));
        if (dataset.equals(WEBGOAT)) {
            return new InferErrorStackTracesFromWebGoatResponses();
        }

        else if (dataset.equals(JENKINS)) {
            return new InferErrorStackTracesFromJenkinsResponses();
        }

        return null;
    }


}
