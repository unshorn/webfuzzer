package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.output.Issue;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.*;

import static nz.ac.wgtn.cornetto.postanalysis.Commons.addProvenance2Latex;

/**
 * Abstract script to analyse recorded issues for callsites (methods on top of stacks). Useful for analysis errors and injections.
 * @author jens dietrich
 */
public abstract class AbstractAnalyseIssuesCallsites<T extends Issue>  {

    public abstract String getCallsite(T issue);

    public abstract T readIssue(String dataset,File file) throws Exception;

    public abstract String getTableCaption();

    public abstract String getTableLabel();

    public abstract boolean isSerialisedIssue(File file) ;

    public Logger LOGGER = Logger.getLogger(this.getClass());

    public void analyse(String[] args) {
        BasicConfigurator.configure();
        Preconditions.checkArgument(args.length==1,"this script requires one argument -- the name of the output file (.tex)");
        File resultFile = new File(args[0]);

        Map<String, Map<String, String>> instanceStatsByCallsiteByDataset = new LinkedHashMap<>();  // values are latex-formatted averages

        for (int i=0;i<DataSets.DATA_SETS.size();i++) {
            String dataset = DataSets.DATA_SETS.get(i);
            LOGGER.info("Analysing dataset " + dataset);
            Map<String,Integer>[] callsiteCounters = new Map[DataSets.getResults(dataset).size()]; // slot for each run
            for (int j=0;j<DataSets.getResults(dataset).size();j++) {
                File resultFolder = DataSets.getResults(dataset).get(j);
                LOGGER.info("Analysing dataset " + dataset + " , run: " + resultFolder);
                callsiteCounters[j] = new HashMap<>();
                for (File errorFile:resultFolder.listFiles(f -> isSerialisedIssue(f))) {
                    try {
                        T issue = readIssue(dataset,errorFile);
                        String callsite = getCallsite(issue);
                        callsiteCounters[j].compute(callsite,(k,v) -> v==null?1:(v+1));
                    } catch (Exception x) {
                        LOGGER.error("Error reading serialized error", x);
                    }
                }
            };
            LOGGER.info("Aggregating results for dataset " + dataset);
            Map<String, String> instanceStatsByCallsite = new HashMap<>();
            instanceStatsByCallsiteByDataset.put(dataset,instanceStatsByCallsite);

            // collect all callsites
            Set<String> callsites = new HashSet<>();
            for (Map<String,Integer> callsiteCounter:callsiteCounters) {
                callsites.addAll(callsiteCounter.keySet());
            }
            for (String callsite:callsites) {
                List<Integer> counts = new ArrayList<>();
                for (Map<String,Integer> callsiteCounter:callsiteCounters) {
                    int count = callsiteCounter.computeIfAbsent(callsite,cs -> 0); // important: record missing value for correct stats
                    counts.add(count);
                }
                String value = Commons.means2Latex(counts);
                instanceStatsByCallsite.put(callsite,value);
            }
        }

        LOGGER.info("Producing report");

        // collect ALL callsites across datasets
        Set<String> callsites = new HashSet<>();
        for (Map<String, String> instanceStatsByCallsite:instanceStatsByCallsiteByDataset.values()) {
            callsites.addAll(instanceStatsByCallsite.keySet());
        }

        LOGGER.info("" + callsites.size() + " found across all data sets and runs");
        try (PrintWriter out = new PrintWriter(new FileWriter(resultFile))) {

            addProvenance2Latex(out,this);

            String columFormats = new String(new char[DataSets.DATA_SETS.size()]).replace("\0", " r");
            columFormats = "l" + columFormats ;  // 1st column is method
            out.println("\\begin{table}[h]");
            out.println("\\center");
            out.println("\\begin{tabular}{" + columFormats + "}");

            // header
            out.println("\\hline");
            out.print("\\textbf{method invoked}");
            for (String dataset:DataSets.DATA_SETS) {
                out.print(" & ");
                out.print("\\textbf{" + dataset + "}");
            }
            out.println(" \\\\ ");
            out.println("\\hline");

            // body
            for (String callsite:callsites) {
                int lastDotPosition = callsite.lastIndexOf(".");
                String callsite2 = callsite.substring(0,lastDotPosition) + "::" + callsite.substring(lastDotPosition+1);
                out.print(callsite2);
                for (String dataset:DataSets.DATA_SETS) {
                    out.print(" & ");
                    Map<String, String> instanceStatsByCallsite = instanceStatsByCallsiteByDataset.get(dataset);
                    String value = instanceStatsByCallsite.computeIfAbsent(callsite, k -> "-");
                    out.print(value);
                }
                out.println(" \\\\ ");
            }
            out.println("\\hline");
            out.println("\\end{tabular}");
            out.println("\\caption{" + getTableCaption() + "}");
            out.println("\\label{" + getTableLabel() + "}");
            out.println("\\end{table}");

            LOGGER.info("Report written to: " + resultFile.getAbsolutePath());

        }
        catch (Exception ex) {
            LOGGER.error(ex);
        }



    }
}
