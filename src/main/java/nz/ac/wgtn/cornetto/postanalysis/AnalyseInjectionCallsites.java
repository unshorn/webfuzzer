package nz.ac.wgtn.cornetto.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.Issue;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.*;

/**
 * Abstract callsites (methods on top of the stack) for recorded injections.
 * @author jens dietrich
 */
public class AnalyseInjectionCallsites extends AbstractAnalyseIssuesCallsites<InjectionVulnerability>  {

    private ObjectMapper mapper = new ObjectMapper();

    public static void main (String[] args) throws Exception {
        new AnalyseInjectionCallsites().analyse(args);
    }

    public String getCallsite(InjectionVulnerability issue) {
        return issue.getTaintFlowSpec().getStackTrace().get(0);
    }

    @Override
    public String getTableLabel() {
        return "tab:results:injection-cs";
    }

    @Override
    public String getTableCaption() {
        return "Average number of methods on top of the stack in detected injections across runs (the number of runs is the same as reported in Table~\\ref{tab:results:injection})";
    }

    @Override
    public boolean isSerialisedIssue(File f) {
        return f.getName().startsWith("injection-") && f.getName().endsWith(".json");
    }

    @Override
    public InjectionVulnerability readIssue(String dataset,File file) throws Exception {
        return mapper.readValue(file, InjectionVulnerability.class);
    }
}
