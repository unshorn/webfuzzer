package nz.ac.wgtn.cornetto.postanalysis;

import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import org.json.JSONObject;

/**
 * Utility to extract stacktraces from logged webgoat responses.
 * @author jens dietrich
 */
public class InferErrorStackTracesFromWebGoatResponses extends StackTraceInference<ServerError> {

    @Override
    public StackTrace extractStackTrace(ServerError error) throws Exception {

        String responseEntity = error.getResponseEntity();
        JSONObject jsonObject = new JSONObject(responseEntity);
        String trace = jsonObject.getString("trace");
        return parse(trace);
    }

}
