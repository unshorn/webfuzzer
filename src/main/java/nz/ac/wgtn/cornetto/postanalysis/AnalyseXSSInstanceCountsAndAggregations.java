package nz.ac.wgtn.cornetto.postanalysis;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import java.io.File;
import java.util.Map;
import java.util.function.Function;

/**
 * Script to analyse recorded XSS instances.
 * @author jens dietrich
 */
public class AnalyseXSSInstanceCountsAndAggregations extends AbstractAnalyseIssuesInstanceCountsAndAggregations<XSSVulnerability> {

    private ObjectMapper mapper = new ObjectMapper();

    public static void main (String[] args) throws Exception {
        new AnalyseXSSInstanceCountsAndAggregations().analyse(args);
    }

    @Override
    public Map<String, Function<XSSVulnerability, Object>> getAggregations() {
        return Aggregations.XSS_AGGREGATIONS;
    }

    @Override
    public String getTableCaption() {
        return "Average number of XSS vulnerabilities recorded over a number of runs and aggregations using URI, taint, URI+taint";
    }

    @Override
    public String getTableLabel() {
        return "tab:results:xss";
    }

    @Override
    public boolean isSerialisedIssue(File f) {
        return f.getName().startsWith("xss-") && f.getName().endsWith(".json");
    }

    @Override
    public XSSVulnerability readIssue(String dataset,File file) throws Exception {
        return mapper.readValue(file, XSSVulnerability.class);
    }
}
