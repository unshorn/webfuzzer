package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.listeners.CoverageDetailsListener;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Simple script to analyse the methods collected, aggregated them, and print the results to the console.
 * To be used in some quick and dirty adhoc analysis. Only analyses one dataset / run at a time.
 * @author jens dietrich
 */
public class PrintAggregatedMethodsCovered {


    public static void main (String[] args) {
        Preconditions.checkArgument(args.length==1,"One argument required -- the folder to analyse");
        File resultFolder = new File(args[0]);

        System.out.println("Analysing coverage data in " + resultFolder.getAbsolutePath());

        Set<String> allMethods = AnalyseCoverage.readMethods(new File(resultFolder, CoverageDetailsListener.fileNameAll), m -> true);
        System.out.println("\tbaseline (number of application methods gathered in pre-analysis): " + allMethods.size());

        System.out.println("reporting request methods discovered for each step (1 step = 1 min)");
        Set<String> requestMethods = new HashSet<>();
        int timepoint = 1;
        File coverageDataFile = null;
        while ((coverageDataFile = new File(resultFolder,CoverageDetailsListener.fileName4MethodsTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension)).exists()){
            Set<String> methods = AnalyseCoverage.readMethods(coverageDataFile,m -> allMethods.contains(m));
            requestMethods.addAll(methods);
            System.out.println("step " + timepoint + " request methods: " + requestMethods.size());
            timepoint = timepoint+1;
        }

        System.out.println("reporting system methods discovered for each step (1 step = 1 min)");
        Set<String> systemMethods = new HashSet<>();
        timepoint = 1;
        coverageDataFile = null;
        while ((coverageDataFile = new File(resultFolder,CoverageDetailsListener.fileName4MethodsNotTriggeredByRequests + timepoint + CoverageDetailsListener.fileExtension)).exists()) {
            Set<String> methods = AnalyseCoverage.readMethods(coverageDataFile,m -> allMethods.contains(m));
            systemMethods.addAll(methods);
            System.out.println("step " + timepoint + " system methods: " + systemMethods.size());
            timepoint = timepoint+1;
        }

        System.out.println("done");
    }
}

