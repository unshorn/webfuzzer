package nz.ac.wgtn.cornetto.postanalysis;

import com.google.common.collect.Sets;
import com.google.common.math.Stats;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * Setting how statistical values are being reported (as latex-formatted strings)
 * @author jens dietrich
 */
public class Commons {

    // for reporting results
    public static NumberFormat DEFAULT_NUMBER_FORMAT = new DecimalFormat("###,###,##0.00");
    public static NumberFormat HIGH_PRECISION_NUMBER_FORMAT = new DecimalFormat("#0.0000");
    public static NumberFormat INT_FORMAT = NumberFormat.getNumberInstance(Locale.US);

    public static DateFormat PROVENANCE_TIMESTAMP_FORMAT = DateFormat.getDateTimeInstance();

    public static String means2Latex(Collection<? extends Number> counts, NumberFormat format) {
        return avg(counts,format);
    }

    public static String means2Latex(Collection<? extends Number> counts) {
        return avg(counts,DEFAULT_NUMBER_FORMAT);
    }

    public static String max2Latex(Collection<? extends Number> counts, NumberFormat format) {
        return max(counts,format);
    }

    public static String max2Latex(Collection<? extends Number> counts) {
        return max2Latex(counts,DEFAULT_NUMBER_FORMAT);
    }

    public static String max (Collection<? extends Number> counts,NumberFormat format) {
        Stats stats = Stats.of(counts);
        double max = stats.max();
        return "" + format.format(max);
    }

    public static String avg (Collection<? extends Number> counts,NumberFormat format) {
        Stats stats = Stats.of(counts);
        double mean = stats.mean();
        return "" + format.format(mean);
    }

    public static String avgNStddev (Collection<? extends Number> counts,NumberFormat format) {
        Stats stats = Stats.of(counts);
        double mean = stats.mean();
        double stdev = stats.sampleStandardDeviation();
        return "" + format.format(mean) + " $\\pm$ " + format.format(stdev);
    }




    public static void addProvenance2Latex (PrintWriter out, Object script) throws Exception {


        String scriptName = null;
        if (script==null) {
            scriptName = "?";
        }
        else if (script instanceof Class) {
            scriptName = ((Class)script).getName();
        }
        else if (script instanceof String) {
            scriptName = (String)script;
        }
        else {
            scriptName = script.getClass().getName();
        }
        out.println("% generated results");
        out.println("% time: " + PROVENANCE_TIMESTAMP_FORMAT.format(new Date()));
        out.println("% script: " + scriptName);
        out.println("% user: " + System.getProperty("user.name"));
        out.println("% data (incl last modified): ");
        for (String dataset:DataSets.DATA_SETS) {
            for (File folder:DataSets.getResults(dataset)) {
                out.println("% \t " + folder.getName() + " " + PROVENANCE_TIMESTAMP_FORMAT.format(new Date(folder.lastModified())));
            }
        }
    }


    private static List<Double> computePairwiseJaccardDistances(List<Set<String>> sets) {
        List<Double> distances = new ArrayList<>();
        for (int i=0;i<sets.size();i++) {
            Set<String> set1 = sets.get(i);
            for (int j=0;j<sets.size();j++) {
                if (i<j) {
                    Set<String> set2 = sets.get(j);
                    int intersectionSize = Sets.intersection(set1,set2).size();
                    int unionSize = Sets.union(set1,set2).size();
                    double distance = 1 - (((double)intersectionSize)/((double)unionSize));
                    distances.add(distance);
                }
            }
        }
        return distances;
    }

    public static String computeAvgJaccardDistance(List<Set<String>> sets){
        return means2Latex(computePairwiseJaccardDistances(sets),HIGH_PRECISION_NUMBER_FORMAT);
    }

    public static String computeMaxJaccardDistance(List<Set<String>> sets){
        return max2Latex(computePairwiseJaccardDistances(sets),HIGH_PRECISION_NUMBER_FORMAT);

    }


}
