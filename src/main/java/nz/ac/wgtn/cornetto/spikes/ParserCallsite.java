package nz.ac.wgtn.cornetto.spikes;

import java.util.Objects;

/**
 * Represents a callsite to a parser API.
 * @author jens dietrich
 */
public class ParserCallsite {
    private String war = null;
    private String context = null; // jar inside war, or <classes> for classes from .class files within the war
    private String caller = null;
    private String callee = null;
    private ParserType apiKind = ParserType.OTHER ;
    private DataFormat dataKind = DataFormat.JSON ;
    private String apiName = null ;   // jackson, gson etc

    public ParserCallsite(String war, String context, String caller, String callee, ParserType apiKind, DataFormat dataKind, String apiName) {
        this.war = war;
        this.context = context;
        this.caller = caller;
        this.callee = callee;
        this.apiKind = apiKind;
        this.dataKind = dataKind;
        this.apiName = apiName;
    }

    public String getWar() {
        return war;
    }

    public String getApiName() {
        return apiName;
    }

    public String getContext() {
        return context;
    }

    public String getCaller() {
        return caller;
    }

    public String getCallee() {
        return callee;
    }

    public ParserType getApiKind() {
        return apiKind;
    }

    public DataFormat getDataKind() {
        return dataKind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParserCallsite callsite = (ParserCallsite) o;
        return war.equals(callsite.war) &&
            context.equals(callsite.context) &&
            caller.equals(callsite.caller) &&
            callee.equals(callsite.callee) &&
            apiName.equals(callsite.apiName) &&
            apiKind == callsite.apiKind &&
            dataKind == callsite.dataKind;
    }

    @Override
    public int hashCode() {
        return Objects.hash(war, context, caller, callee, apiKind, dataKind,apiName);
    }

    @Override
    public String toString() {
        return "Callsite{" +
                "war='" + war + '\'' +
                ", context='" + context + '\'' +
                ", caller='" + caller + '\'' +
                ", callee='" + callee + '\'' +
                ", apiName='" + apiName + '\'' +
                ", apiKind=" + apiKind +
                ", dataKind=" + dataKind +
                '}';
    }
}
