package nz.ac.wgtn.cornetto.spikes;

/**
 * Data format.
 * @author jens dietrich
 */
enum DataFormat {XML , JSON}
