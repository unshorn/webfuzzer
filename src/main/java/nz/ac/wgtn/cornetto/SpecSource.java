package nz.ac.wgtn.cornetto;

/**
 * Describes the (type of) source for this spec.
 * @author jens dietrich
 */
public enum SpecSource {

    FIXED, RANDOM,CROSSOVER,MUTATED,STATIC_ANALYSIS,DYNAMIC_ANALYSIS,TAINTED,PRERECORDED;

}
