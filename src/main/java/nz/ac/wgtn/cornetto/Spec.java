package nz.ac.wgtn.cornetto;

import java.util.EnumSet;

/**
 * The input specification representing randomised input.
 * @author jens dietrich
 */
public interface Spec {
    EnumSet<SpecSource> getSources();
}
