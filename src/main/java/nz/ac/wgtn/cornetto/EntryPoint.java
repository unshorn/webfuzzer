package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.http.MethodSpec;
import java.util.*;
import java.util.function.BiFunction;

/**
 * Specification of an entry point into a web application.
 * An entry has a URL pattern that can be used for fuzzing, and links this with Java classes implementing this, these
 * associations can be used to create additional static pre-analyses.
 * @author jens dietrich
 */
public class EntryPoint {

    /**
     * This models a variable part of a path, such as a wildcard used in JEE configuration or annotations (such as /foo/*)
     * or a spring variable (such as /foo/{id})
     * @author jens dietrich
     */
    public static class VariablePathPart {

        // the original path, example: /foo/*
        private String path = null;

        // the variable token to be replaced, such as *
        private String token = null;

        // the first char of the token (for instance, in case there are multiple wildcards), such as 5
        private int position = -1;

        // the type of the token (default is string, but it could for instance be a numeric value)
        private Class type = String.class;

        public VariablePathPart(String path, String token, int position, Class type) {
            this.path = path;
            this.token = token;
            this.position = position;
            this.type = type;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public Class getType() {
            return type;
        }

        public void setType(Class type) {
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            VariablePathPart that = (VariablePathPart) o;
            return position == that.position &&
                    Objects.equals(path, that.path) &&
                    Objects.equals(token, that.token) &&
                    Objects.equals(type, that.type);
        }

        @Override
        public int hashCode() {
            return Objects.hash(path, token, position, type);
        }
    }


    private String urlPattern = null;
    private Set<MethodSpec> supportedMethods = null;
    private String className = null;
    private List<VariablePathPart> variablePathParts = null;

    public EntryPoint(String urlPattern, Set<MethodSpec> supportedMethods, String className, List<VariablePathPart> variablePathParts) {
        this.urlPattern = urlPattern;
        this.supportedMethods = supportedMethods;
        this.className = className;
        this.variablePathParts = variablePathParts;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    /**
     * Bind variables.
     * @param bindingFunction  : varPart x VarPartIndex -> value , index starts with 0
     * @return
     */
    public String bindVariables(BiFunction<VariablePathPart,Integer,String> bindingFunction) {
        StringBuffer sb = new StringBuffer();
        int offset = 0;
        for (int i=0;i<variablePathParts.size();i++) {
            VariablePathPart var = variablePathParts.get(i);
            int pos = var.position;
            sb.append(urlPattern.substring(offset,pos));
            offset = pos;
            String replacement = bindingFunction.apply(var,i);
            sb.append(replacement);
            offset = offset + var.token.length();
        }
        sb.append(urlPattern.substring(offset,urlPattern.length()));
        return sb.toString();
    }

    public Set<MethodSpec> getSupportedMethods() {
        return supportedMethods;
    }

    public String getClassName() {
        return className;
    }

    public List<VariablePathPart> getVariablePathParts() {
        return variablePathParts;
    }

    public boolean hasVariables() {
        return variablePathParts.size()>0;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntryPoint that = (EntryPoint) o;
        return Objects.equals(urlPattern, that.urlPattern) &&
                Objects.equals(supportedMethods, that.supportedMethods) &&
                Objects.equals(className, that.className) &&
                Objects.equals(variablePathParts, that.variablePathParts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(urlPattern, supportedMethods, className, variablePathParts);
    }

    @Override
    public String toString() {
        return "EntryPoint{" +
                "urlPattern='" + urlPattern + '\'' +
                ", supportedMethods=" + supportedMethods +
                ", className='" + className + '\'' +
                ", variablePathParts=" + variablePathParts +
                '}';
    }
}
