package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.war.MethodInvocation;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Produces files with updated and achieved coverage, the format is a simple text file, with one method per line.
 * @author jens dietrich
 */
public class CoverageDetailsListener extends AbstractFuzzerListener  {

    private static final Logger LOGGER = LogSystem.getLogger("coverage");
    private final AtomicInteger count = new AtomicInteger(0);

    public static final  String fileName4MethodsTriggeredByRequests = "covered-methods-triggered-by-requests-";
    public static final  String fileName4MethodsNotTriggeredByRequests = "covered-methods-not-triggered-by-requests-";
    public static final  String fileExtension = ".txt";
    public static final  String fileNameAll = "all-methods" +fileExtension;

    private long delay = 0;
    private int snapshotInterval = 60_000;

    private Supplier<Set<String>> systemInvocationsSupplier = null;

    private Timer timer = null;

    public CoverageDetailsListener(Supplier<Set<String>> systemInvocationsSupplier,int snapshotInterval) {
        this.snapshotInterval = snapshotInterval;
        this.systemInvocationsSupplier = systemInvocationsSupplier;
    }


    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                int batchNo = count.incrementAndGet();
                if (batchNo == 1) {
                    TreeSet<String> allApplicationMethods = new TreeSet<>();
                    File fileAllMethods = new File(outputRootFolder,fileNameAll);
                    allApplicationMethods.addAll(Context.getDefault().getStaticModel().getApplicationMethods());
                    try (PrintWriter writer = new PrintWriter(new FileWriter(fileAllMethods))) {
                        for (String method : allApplicationMethods) {
                            writer.println(method);
                        }
                    } catch (IOException e) {
                        LOGGER.error("Error writing list of all present application methods to " + fileAllMethods.getAbsolutePath(), e);
                    }
                    LOGGER.info("List of all present application methods written to " + fileAllMethods.getAbsolutePath());
                }

                // update regulary
                Set<String> recordedApplicationMethods = Context.getDefault().getDynamicModel()
                        .getRecordedExecutionPoints()
                        .parallelStream()
                        .filter(MethodInvocation.class::isInstance)
                        .map(MethodInvocation.class::cast)
                        .filter(m -> Context.getDefault().getStaticModel().isApplicationClass(m.getClassName()))
                        .map(m -> CoverageDetailsListener.toString(m))
                        .collect(Collectors.toSet());

                TreeSet<String> recordedApplicationMethodsSorted = new TreeSet<>();
                recordedApplicationMethodsSorted.addAll(recordedApplicationMethods);

                File output = new File(outputRootFolder, fileName4MethodsTriggeredByRequests + batchNo + fileExtension);
                try (PrintWriter writer = new PrintWriter(new FileWriter(output))) {
                    for (String method : recordedApplicationMethodsSorted) {
                        writer.println(method);
                    }
                } catch (IOException e) {
                    LOGGER.info("Error writing list of all recorded application methods triggered by requests to " + output.getAbsolutePath(), e);
                }
                LOGGER.info("List of all recorded application methods triggered by requests written to " + output.getAbsolutePath());

                // system methods
                Set<String> recordedNonApplicationMethodsSorted = new TreeSet<>();
                try {
                    recordedNonApplicationMethodsSorted.addAll(systemInvocationsSupplier.get());
                    output = new File(outputRootFolder, fileName4MethodsNotTriggeredByRequests + batchNo + fileExtension);
                    try (PrintWriter writer = new PrintWriter(new FileWriter(output))) {
                        for (String method : recordedNonApplicationMethodsSorted) {
                            writer.println(method);
                        }
                    } catch (IOException e) {
                        LOGGER.error("Error writing list of all recorded application methods not triggered by requests to " + output.getAbsolutePath(), e);
                    }
                    LOGGER.info("List of all recorded application methods not triggered by requests written to " + output.getAbsolutePath());
                }
                catch (Exception x) {
                    LOGGER.info("Error fetching  application methods not triggered by requests", x);
                }

            }
        };

        this.timer = new Timer();
        timer.scheduleAtFixedRate(task,delay,snapshotInterval);
        LOGGER.info("Coverage recording service started");
    }

    /**
     * Generate a string representation of an invocation consistent with the methods extracted from the static model
     * so that the strings can be easily diffed.
     * @param m
     * @return
     */
    private static String toString(MethodInvocation m) {
        String name = m.getClassName() + "::" + m.getMethodName() + m.getDescriptor();
        return name;

    }

    @Override
    public void fuzzingEnds() {
        super.fuzzingEnds();
        this.timer.cancel();
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{snapshotInterval=" + snapshotInterval + " ms}";
    }
}
