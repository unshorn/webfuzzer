package nz.ac.wgtn.cornetto.listeners;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.output.ServerError;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Listener that exports details of responses with server errors (>=500 status).
 * @author Jens Dietrich
 */
public class ServerErrorExportListener extends AbstractFuzzerListener  {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private int MAX_LOGS = 0;

    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        MAX_LOGS = options.getIntOption("max500Reported",Integer.MAX_VALUE,v -> v>=-1);
        if (MAX_LOGS==-1) MAX_LOGS = Integer.MAX_VALUE;
        Loggers.FUZZER.info("Upto " + MAX_LOGS + " 500 error details will be reported");
    }

    @Override
    public void newServerErrorDiscovered(ServerError error) {
        Preconditions.checkArgument(error.getStatusCode() >= 500);

        error.setTimestamp(System.currentTimeMillis() - this.fuzzingStartsTimestamp);

        int index = counter.incrementAndGet();
        if (index<=MAX_LOGS) {
            File file = new File(outputRootFolder,"server-error-"+index+".json");
            ObjectMapper mapper = new ObjectMapper();
            try  {
                mapper.writerWithDefaultPrettyPrinter().writeValue(file,error);
            } catch (IOException x) {
                Loggers.RESULTS_XSS.error("Error exporting error to " + file.getAbsolutePath(),x);
            }
        }
    }
}
