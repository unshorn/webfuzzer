package nz.ac.wgtn.cornetto.listeners;

import com.google.common.io.Files;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.feedback.CoverageGraph;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.SpecProvenanceUtils;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import nz.ac.wgtn.cornetto.jee.war.MethodInvocation;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Records performance stats.
 * @author jens dietrich
 */
public class StatsRecorderFuzzerListener extends AbstractFuzzerListener  {

    private static final Logger LOGGER = LogSystem.getLogger("stats-service");
    private static final String CSV_SEP = "\t";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd - HH:mm:ss");

    private enum Key {REQUESTS_SENT, CONNECTION_ERROR_TIMEOUT, CONNECTION_ERROR_OTHER,REQUESTS_SENT_200,REQUESTS_SENT_300,REQUESTS_SENT_400,REQUESTS_SENT_500,REQUESTS_REJECTED,FEEDBACK_REQUESTS_SENT,COVERAGE_GRAPH_VERTEX_SIZE, COVERAGE_PARETO_FRONT_SIZE, COVERAGE_GRAPH_DEPTH, COVERAGE_AVG_SET_SIZE, NEW_METHODS_DISCOVERED, NEW_STACKTRACE_TO_CRITICAL_SINK_DISCOVERED, NEW_XSS_DISCOVERED_H,NEW_XSS_DISCOVERED_M,NEW_XSS_DISCOVERED_L, NEW_FORMS_DISCOVERED, NEW_REQUEST_PARAMETER_NAMES_DISCOVERED, REQUESTS_SENT_WITH_TAINT, NEW_TAINT_FLOWS, ALL_APPLICATION_METHODS, COVERED_APPLICATION_METHODS}
    private Map<Key,Integer> counts = new ConcurrentHashMap<>();
    private boolean fileInitialised = false;
    private File statsFile =  null;
    private long delay = 0;
    private int snapshotInterval = 60_000;

    public StatsRecorderFuzzerListener(int snapshotInterval) {
        this.snapshotInterval = snapshotInterval;
    }

    private Timer timer = null;

    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        this.statsFile = new File(outputRootFolder,"stats.csv");
        statsFile.delete();
        try {
            Files.touch(statsFile);
        }
        catch (IOException x) {
            LOGGER.error("Error creating stats service file", x);
        }

        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                // snapshot
                Map<Key,Integer> snapshot = new HashMap<>();
                synchronized (StatsRecorderFuzzerListener.this) {
                    snapshot.putAll(counts);
                    counts.clear();
                }

                try (PrintWriter writer = new PrintWriter(new FileWriter(statsFile,true))) {
                    if (!fileInitialised) {
                        String headerLine = Stream.of(Key.values())
                            .map(k -> k.name())
                            .map(n -> n.replace('_','-').toLowerCase())
                            .collect(Collectors.joining(CSV_SEP));
                        headerLine = headerLine + CSV_SEP + "timestamp";
                        writer.println(headerLine);
                        fileInitialised = true;
                    }

                    // coverage is only updated when the snapshot is taken
                    updateCoverage();

                    String nextRow = Stream.of(Key.values())
                        .map(k -> {
                            int v = snapshot.computeIfAbsent(k,k2 -> 0);
                            if (k==Key.COVERAGE_AVG_SET_SIZE) {
                                // scale
                                return ""+(((double)v)/100);
                            }
                            else {
                                return ""+v;
                            }
                        })
                        .collect(Collectors.joining(CSV_SEP));
                    writer.print(nextRow);
                    writer.print(CSV_SEP);
                    writer.print(DATE_FORMAT.format(new Date()));
                    writer.println();

                    LOGGER.info("Stats data appended to " + statsFile.getAbsolutePath());

                } catch (IOException e) {
                    LOGGER.error("Error in stats service", e);
                }


            }

        };
        this.timer = new Timer();
        timer.scheduleAtFixedRate(task,delay,snapshotInterval);

//        Thread thread = new Thread(task,"webfuzzer.stats-service");
//        thread.setPriority(Thread.MIN_PRIORITY);
//        thread.start();
        LOGGER.info("Stats service started");
    }

    @Override
    public void fuzzingEnds() {
        super.fuzzingEnds();
        this.timer.cancel();
    }

    private void updateCoverage() {
        Context context = Context.getDefault();
        Set<String> allApplicationMethods = context.getStaticModel().getApplicationMethods();
        Set<MethodInvocation> recordedApplicationMethods = context.getDynamicModel().getRecordedExecutionPoints().parallelStream()
            .filter(MethodInvocation.class::isInstance)
            .map(MethodInvocation.class::cast)
            .filter(m -> context.getStaticModel().isApplicationClass(m.getClassName()))
            .collect(Collectors.toSet());

        counts.put(Key.ALL_APPLICATION_METHODS,allApplicationMethods.size());
        counts.put(Key.COVERED_APPLICATION_METHODS,recordedApplicationMethods.size());

        // double coverage = (double)recordedApplicationMethods.size() / (double)allApplicationMethods.size();
        // counts.put(Key.METHOD_COVERAGE,(int)(coverage * 100));

        try {
            CoverageGraph graph = context.getDynamicModel().getCoverageGraph();
            counts.put(Key.COVERAGE_GRAPH_VERTEX_SIZE, graph.getVertexCount());
            counts.put(Key.COVERAGE_PARETO_FRONT_SIZE, graph.getParetoFronSize());
            counts.put(Key.COVERAGE_GRAPH_DEPTH, graph.getDepth());
            counts.put(Key.COVERAGE_AVG_SET_SIZE, (int) (100 * graph.getAvgVertexSizeCount()));
        }
        catch (Throwable x) {
            LOGGER.error("Error updating coverage",x);
        }

    }


    @Override
    public synchronized void requestRejected(RequestSpec request) {
        counts.compute(Key.REQUESTS_REJECTED,(k,v) -> v==null?1:v+1);
    }

    @Override
    public void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {
        int statusCode = response.getStatusCode();
        counts.compute(Key.REQUESTS_SENT,(k,v) -> v==null?1:v+1);
        if (SpecProvenanceUtils.extractTaintedParts(requestSpec).keySet().isEmpty()) {
            counts.compute(Key.REQUESTS_SENT_WITH_TAINT,(k,v) -> v==null?1:v+1);
        }
        if (statusCode>=200 && statusCode<300) {
            counts.compute(Key.REQUESTS_SENT_200,(k,v) -> v==null?1:v+1);
        }
        else if (statusCode>=300 && statusCode<400) {
            counts.compute(Key.REQUESTS_SENT_300,(k,v) -> v==null?1:v+1);
        }
        else if (statusCode>=400 && statusCode<500) {
            counts.compute(Key.REQUESTS_SENT_400,(k,v) -> v==null?1:v+1);
        }
        else if (statusCode>=500 && statusCode<600) {
            counts.compute(Key.REQUESTS_SENT_500,(k,v) -> v==null?1:v+1);
        }

        else {
            LOGGER.warn("request encountered with unknown status code: " + statusCode);
        }


    }

    @Override
    public synchronized void feedbackRequestSent(String ticketId, int statusCode) {
        counts.compute(Key.FEEDBACK_REQUESTS_SENT,(k,v) -> v==null?1:v+1);
    }

    @Override
    public synchronized void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {
        int count = executionPoints.size();
        if (count>0) {
            counts.compute(Key.NEW_METHODS_DISCOVERED,(k,v) -> v==null?count:v+count);
        }
    }

    @Override
    public synchronized void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {
        int count = requestParameterNames.size();
        if (count>0) {
            counts.compute(Key.NEW_REQUEST_PARAMETER_NAMES_DISCOVERED,(k,v) -> v==null?count:v+count);
        }
    }

    @Override
    public synchronized void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {
        counts.compute(Key.NEW_STACKTRACE_TO_CRITICAL_SINK_DISCOVERED,(k,v) -> v==null?1:v+1);
    }

    @Override
    public synchronized void newXSSDiscovered(XSSVulnerability xssVulnerability) {
        IssuePriority priority = xssVulnerability.getPriority();
        if (priority != IssuePriority.ZERO) {
            Key key = Key.NEW_XSS_DISCOVERED_M;
            if (priority == IssuePriority.HIGH) key = Key.NEW_XSS_DISCOVERED_H;
            else if (priority == IssuePriority.MEDIUM) key = Key.NEW_XSS_DISCOVERED_M;
            else if (priority == IssuePriority.LOW) key = Key.NEW_XSS_DISCOVERED_L;

            if (key == null) {  // IssuePriority.ZERO is ignored
                LOGGER.warn("Unknown XSS priority " + priority + " , will be reported as MEDIUM");
                key = Key.NEW_XSS_DISCOVERED_M;
            }
            counts.compute(key, (k, v) -> v == null ? 1 : v + 1);
        }
    }

    @Override
    public synchronized void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {
        int count = forms.size();
        if (count>0) {
            counts.compute(Key.NEW_FORMS_DISCOVERED,(k,v) -> v==null?count:v+count);
        }
    }

    @Override
    public void newInjectionDiscovered(InjectionVulnerability injection) {
        counts.compute(Key.NEW_TAINT_FLOWS,(k,v) -> v==null?1:v+1);
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "snapshotInterval=" + snapshotInterval + " ms}";
    }

    @Override
    public void connectionErrorDetected(HttpRequest request, ConnectionErrorType errorType) {
        if (errorType== ConnectionErrorType.TIMEOUT) {
            counts.compute(Key.CONNECTION_ERROR_TIMEOUT, (k, v) -> v == null ? 1 : v + 1);
        }
        else {
            counts.compute(Key.CONNECTION_ERROR_OTHER, (k, v) -> v == null ? 1 : v + 1);
        }
    }
}
