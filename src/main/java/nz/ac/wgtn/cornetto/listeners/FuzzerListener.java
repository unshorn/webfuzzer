package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.http.HttpRequest;

import java.net.URL;
import java.util.Set;

/**
 * Interface for fuzzer listeners. Can be used for logging, exporting results, statistics etc.
 * @author jens dietrich
 */
public interface FuzzerListener {

    enum ConnectionErrorType {TIMEOUT, OTHER}

    void fuzzingStarts(Options options);

    void fuzzingEnds();

    // requests rejected by the fuzzer, not sent
    void requestRejected(RequestSpec request);

    void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response);

    void feedbackRequestSent(String ticketId, int statusCode);

    void newExecutionPointsDiscovered (RequestSpec requestSpec, Set<ExecutionPoint> executionPoints);

    void newRequestParameterNamesDiscovered (RequestSpec requestSpec, Set<String> requestParameterNames);

    void newInvocationsOfCriticalMethodDiscovered (RequestSpec requestSpec, String stackTrace);

    void newFormsDiscovered(RequestSpec requestSpec,Set<Form> forms);

    void newLinksDiscovered(RequestSpec requestSpec,Set<URL> links);

    void connectionErrorDetected(HttpRequest request, ConnectionErrorType errorType);

    void newXSSDiscovered (XSSVulnerability xssVulnerability);

    void newServerErrorDiscovered (ServerError error);

    void newInjectionDiscovered(InjectionVulnerability injection);





}
