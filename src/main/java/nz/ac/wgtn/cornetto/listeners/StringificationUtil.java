package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.http.ParameterSpec;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import java.io.PrintWriter;

/**
 * Utility to print HTTP transaction data to a stream.
 * @author jens dietrich
 */
public class StringificationUtil {

    public static void print(PrintWriter out, RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {

        out.print(request.getRequestLine().getMethod());
        out.print(" ");
        out.println(request.getRequestLine().getUri());

        out.println(" ");
        out.println("# REQUEST HEADERS");  // get as much info from request, may differ from spec as additional headers are used
        for (Header header:request.getAllHeaders()) {
            out.print(header.getName());
            out.print(" : ");
            out.println(header.getValue());
        }

        out.println(" ");
        out.println("# REQUEST PARAMETERS (FROM SPEC)");
        for (ParameterSpec param:requestSpec.getParametersSpec().getParams()) {
            out.print(param.getKey().getValue());
            out.print(" : ");
            out.println(param.getValue().getValue());
        }

        out.println(" ");
        out.println("# RESPONSE STATUS");
        out.println(response.getResponse().getStatusLine());

        out.println(" ");
        out.println("# RESPONSE HEADERS");
        Header[] headers = response.getResponse().getAllHeaders();
        for (Header header:headers) {
            out.print(header.getName());
            out.print(" : ");
            out.println(header.getValue());
        }

        out.println(" ");
        out.println("# RESPONSE ENTITY");
        out.println(response.getContent());
    }

}
