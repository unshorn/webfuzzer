package nz.ac.wgtn.cornetto.listeners;

import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import nz.ac.wgtn.cornetto.xss.XSSInstanceGroup;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Exports detected XSS Vulnerabilities to json.
 * @author jens dietrich
 */
public class XSSExportFuzzerListener extends AbstractFuzzerListener  {

    private static final Map<XSSInstanceGroup,Integer> groupNumbers = new ConcurrentHashMap<>();
    private static final Map<XSSInstanceGroup,AtomicInteger> groupInstanceCounts = new ConcurrentHashMap<>();

    // can use IssuePriority.ZERO for debugging
    public static final IssuePriority MIN_REPORTED_PRIORITY = IssuePriority.MEDIUM;
    public static final int MAX_INSTANCES_PER_GROUP = Integer.MAX_VALUE;

    private static final AtomicInteger counter = new AtomicInteger(0);
    private int MAX_LOGS = 0;

    @Override
    public void fuzzingStarts(Options options) {
        super.fuzzingStarts(options);
        MAX_LOGS = options.getIntOption("maxXSSReported",Integer.MAX_VALUE,v -> v>=-1);
        if (MAX_LOGS==-1) MAX_LOGS = Integer.MAX_VALUE;
        Loggers.FUZZER.info("Upto " + MAX_LOGS + " XSS vulnerabilities details will be reported");
    }

    @Override
    public void newXSSDiscovered(XSSVulnerability xssVulnerability) {
        super.newXSSDiscovered(xssVulnerability);
        xssVulnerability.setTimestamp(System.currentTimeMillis() - this.fuzzingStartsTimestamp);

        int index = counter.incrementAndGet();
        if (index <= MAX_LOGS) {
            IssuePriority priority = xssVulnerability.getPriority();
            if (priority.compareTo(MIN_REPORTED_PRIORITY) >= 0) {

                String prio = priority.name().toLowerCase();
                XSSInstanceGroup group = new XSSInstanceGroup(xssVulnerability.getUri(), xssVulnerability.getTaintedInput());
                int groupNumber = groupNumbers.compute(group, (g, v) -> v == null ? groupNumbers.size() : v);
                AtomicInteger instanceCount = groupInstanceCounts.compute(group, (g, v) -> v == null ? new AtomicInteger(0) : v);
                instanceCount.incrementAndGet();

                if (instanceCount.get() <= MAX_INSTANCES_PER_GROUP) {

                    File file = new File(outputRootFolder, "xss-" + prio + "-" + groupNumber + "-" + instanceCount.get() + ".json");
                    ObjectMapper mapper = new ObjectMapper();
                    try {
                        mapper.writerWithDefaultPrettyPrinter().writeValue(file, xssVulnerability);
                    } catch (IOException x) {
                        Loggers.RESULTS_XSS.error("Error exporting xss to " + file.getAbsolutePath(), x);
                    }
                }
            }
        }
    }
}
