package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.html.Input;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.http.HttpRequest;
import org.apache.log4j.Logger;
import java.net.URL;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Listener that logs events.
 * It logs only to the console.
 * @author jens dietrich
 */
public class LoggingFuzzerListener implements FuzzerListener  {

    private AtomicLong time = new AtomicLong(System.currentTimeMillis());
    private AtomicInteger sentRequestCount = new AtomicInteger();
    private AtomicInteger rejectedRequestCount = new AtomicInteger();
    private int logProgressInterval = 100;

    public LoggingFuzzerListener(int logProgressInterval) {
        this.logProgressInterval = logProgressInterval;
    }

    @Override
    public void fuzzingStarts(Options options) { }

    @Override
    public void requestRejected(RequestSpec request) {
        rejectedRequestCount.incrementAndGet();
    }

    @Override
    public void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {
        int c = sentRequestCount.incrementAndGet();
        if (c % logProgressInterval == 0) {
            long now = System.currentTimeMillis();
            Loggers.FUZZER.info("Request #" + c + " , time: " + (now - time.get()) + " ms " + " ,  rejected (duplicated) requests: " + rejectedRequestCount);
            time.set(now);
        }
    }

    @Override
    public void feedbackRequestSent(String ticketId, int statusCode) {

    }

    @Override
    public void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {
        if (!executionPoints.isEmpty()) {
            Loggers.FUZZER.info("New execution points encountered for " + requestSpec + " (see log files for full details)");
            logDetails(Loggers.FUZZER,executionPoints,3);
            Loggers.COVERAGE_METHODS.info("New execution points encountered for " + requestSpec);
            logDetails(Loggers.COVERAGE_METHODS,executionPoints,Integer.MAX_VALUE);
            Loggers.COVERAGE_METHODS.info("");
        }
    }

    @Override
    public void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {
        if (!requestParameterNames.isEmpty()) {
            Loggers.FUZZER.info("New request parameters encountered for " + requestSpec + " (see log files for full details)");
            logDetails(Loggers.FUZZER,requestParameterNames,3);
            Loggers.COVERAGE_REQUEST_PARAMETERS.info("New request parameters encountered for " + requestSpec);
            logDetails(Loggers.COVERAGE_REQUEST_PARAMETERS,requestParameterNames,Integer.MAX_VALUE);
            Loggers.COVERAGE_REQUEST_PARAMETERS.info("");
        }
    }

    @Override
    public void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {
        Loggers.FUZZER.info("New invocation of critical method encountered for " + requestSpec + " (see log files for full stacktraces");
        logDetails(Loggers.FUZZER,stackTrace.split("\n"),3);
        Loggers.RESULTS_SINK_METHODS.info("New invocation of critical method encountered for " + requestSpec + " (see log files for full stacktraces");
        logDetails(Loggers.RESULTS_SINK_METHODS,stackTrace.split("\n"),Integer.MAX_VALUE);
        Loggers.RESULTS_SINK_METHODS.info("");
    }

    @Override
    public void newXSSDiscovered(XSSVulnerability xssVulnerability) {

        if (xssVulnerability.getPriority().compareTo(IssuePriority.MEDIUM)>=0) {
            Loggers.FUZZER.info("Potential XSS vulnerability with priority " + xssVulnerability.getPriority().name() + " encountered");
            Loggers.RESULTS_XSS.info("Potential XSS vulnerability encountered");
            Loggers.RESULTS_XSS.info("\trequest is: " + xssVulnerability.getUri());
            Loggers.RESULTS_XSS.info("\treplicated token is: " + xssVulnerability.getTaintedInput());
            Loggers.RESULTS_XSS.info("\trequest generation provenance: " + xssVulnerability.getRequestGenerationProvenance().stream().collect(Collectors.joining(",")));
        }
    }

    @Override
    public void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {
        if (!forms.isEmpty()) {
            Loggers.FUZZER.debug(forms.size() + " form(s) recorded: " + requestSpec);
            Loggers.COVERAGE_FORMS.info("Form(s) recorded: " + requestSpec);
            int count = 0;
            for (Form form : forms) {
                Loggers.COVERAGE_FORMS.info("form " + ++count);
                Loggers.COVERAGE_FORMS.info("\tmethod: " + form.getMethod());
                Loggers.COVERAGE_FORMS.info("\taction: " + form.getAction());
                for (Input input : form.getInputs()) {
                    Loggers.COVERAGE_FORMS.info("\tinput: " + input);
                }
            }
            Loggers.COVERAGE_FORMS.info("");
        }
    }

    @Override
    public void newLinksDiscovered(RequestSpec requestSpec, Set<URL> links) {
        if (!links.isEmpty()) {
            Loggers.FUZZER.debug(links.size() + " links(s) recorded: " + requestSpec);
            Loggers.COVERAGE_LINKS.info("Link(s) recorded: " + requestSpec);
            int count = 0;
            for (URL link : links) {
                Loggers.COVERAGE_LINKS.info("link " + ++count + " -- " + link);
            }
            Loggers.COVERAGE_LINKS.info("");
        }
    }

    private void logDetails(Logger logger, String[] details, int max) {
        Stream.of(details).limit(max).forEach(
                xp -> logger.info("\t" + xp)
        );
        if (details.length>max) {
            logger.info("\t .. and " + (details.length-max) + " more");
        }
    }

    private void logDetails(Logger logger, Collection details, int max) {
        details.stream().limit(max).forEach(
                xp -> logger.info("\t" + xp)
        );
        if (details.size()>max) {
            logger.info("\t .. and " + (details.size()-max) + " more");
        }
    }

    @Override
    public void connectionErrorDetected(HttpRequest request, ConnectionErrorType errorType) {}


    @Override
    public void newServerErrorDiscovered (ServerError error) {
        Loggers.FUZZER.info("Server error encountered");
        Loggers.FUZZER.info("Server error encountered");
        Loggers.RESULTS_500.info("\trequest is: " + error.getUri());
        Loggers.RESULTS_500.info("\tresponse code: " + error.getStatusCode());
        if (error.getStacktrace() != null) {
            Loggers.RESULTS_500.info("Stack trace: " + error.getStacktrace().getEntries());
        }
    }

    @Override
    public void newInjectionDiscovered(InjectionVulnerability injection) {
        Loggers.FUZZER.info("New taint flows encountered");
        Loggers.RESULTS_INJECTION.info("\trequest is: " + injection.getUri());
        Loggers.RESULTS_INJECTION.info("\tresponse code: " + injection.getStatusCode());
    }

    @Override
    public void fuzzingEnds() {}

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "logProgressInterval=" + logProgressInterval +
                '}';
    }
}
