package nz.ac.wgtn.cornetto.listeners;

import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResponseWrapper;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import org.apache.http.HttpRequest;
import java.io.File;import java.net.URL;
import java.util.Set;

/**
 * Abstract class to facilitate implementation of the FuzzerListener interface.
 * @author jens dietrich
 */
public class AbstractFuzzerListener implements FuzzerListener  {

    private static boolean outputFolderInitialised = false;
    protected File outputRootFolder = null;

    // timestamp when listener started, in ms
    protected long fuzzingStartsTimestamp = -1;


    // overriding methods should call super.fuzzingStarts !
    @Override
    public void fuzzingStarts(Options options) {
        String outputFolderName  = options.getStringOption("outputRootFolder","logs");
        this.outputRootFolder = new File(outputFolderName);

        synchronized (AbstractFuzzerListener.class) {
            if (!outputFolderInitialised) {
                if (outputRootFolder.exists()) {
                    for (File f : outputRootFolder.listFiles()) {
                        try {
                            f.delete();
                        } catch (Exception x) {
                            Loggers.FUZZER.fatal("Errors deleting on result / log file " + f.getAbsolutePath());
                        }
                    }
                } else {
                    outputRootFolder.mkdirs();
                }
                Loggers.FUZZER.info("Result output folder initialised: " + outputRootFolder.getAbsolutePath());
            }
            outputFolderInitialised = true;
        }

        fuzzingStartsTimestamp = System.currentTimeMillis();

    }

    @Override
    public void requestRejected(RequestSpec request) {}

    @Override
    public void requestSent(RequestSpec requestSpec, HttpRequest request, ResponseWrapper response) {}

    @Override
    public void feedbackRequestSent(String ticketId, int statusCode) {}

    @Override
    public void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {}

    @Override
    public void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {}

    @Override
    public void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {}

    @Override
    public void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {}

    @Override
    public void newLinksDiscovered(RequestSpec requestSpec, Set<URL> links) {}

    @Override
    public void connectionErrorDetected(HttpRequest request, ConnectionErrorType errorType) {}

    @Override
    public void fuzzingEnds() {}

    @Override
    public void newXSSDiscovered(XSSVulnerability xssVulnerability) {}

    @Override
    public void newServerErrorDiscovered (ServerError error) {}

    @Override
    public void newInjectionDiscovered(InjectionVulnerability injection) {}
}
