package nz.ac.wgtn.cornetto.html;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.http.MethodSpec;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.util.LinkResolver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * HTML parser utility, based on jsoup.
 * @author jens dietrich
 */
public class HtmlParser {

    // cache parsed form -- common use case: extract forms and links from the same form
    private static final CacheLoader<String, Document> cacheLoader = new CacheLoader<String, Document>() {
        @Override
        public Document load(String html) {
            try {
                return Jsoup.parse(html);
            }
            catch (Exception x) {
                Loggers.FUZZER.warn("Error parsing html");
            }
            return null;
        }
    };
    private static final LoadingCache<String,Document> cache = CacheBuilder
            .newBuilder()
            .softValues()
            .maximumSize(10_000)
            .expireAfterAccess(10,TimeUnit.MINUTES)
            .build(cacheLoader);

    public static Collection<Form> extractForms(Profile profile, String responseString, String owner) throws Exception {
        Document document = cache.get(responseString);
        return extractForms(profile,document,owner);
    }

    public static Collection<Form> extractForms(Profile profile, InputStream in,String owner) throws Exception {
        Document document = Jsoup.parse(in, Charset.defaultCharset().name(),profile.getServerAndPortURL());
        return extractForms(profile,document,owner);
    }

    public static Collection<Form> extractForms(Profile profile,Document document,String owner) {
        List<Form> forms = new ArrayList<>();
        Elements htmlForms = document.select("form");
        for (Element htmlForm : htmlForms) {
            Form form = new Form();
            form.setOwner(owner);
            String method = htmlForm.attributes().get("method");
            if (method != null) {
                method = method.toUpperCase();
                if (method.equals("GET")) {
                    form.setMethod(MethodSpec.GET);
                }
                else if (method.equals("POST")) {
                    form.setMethod(MethodSpec.POST);
                }
            }

            String action = htmlForm.attributes().get("action");
            try {
                URI uri = LinkResolver.resolveRelativeURL(profile, action);
                if (uri != null) {
                    form.setAction(uri);

                    Elements elements = htmlForm.select("input");
                    for (Element element : elements) {
                        Input input = new Input();
                        input.setName(element.attributes().get("name"));
                        input.setType(element.attributes().get("type"));
                        input.setValue(element.attributes().get("value"));
                        form.addInput(input);
                    }
                    elements = htmlForm.select("textarea");
                    for (Element element : elements) {
                        Input input = new Input();
                        input.setName(element.attributes().get("name"));
                        input.setType(element.attributes().get("type"));
                        if (input.getType() == null || input.getType().isEmpty()) {
                            input.setType("TEXT");
                        }
                        input.setValue(element.attributes().get("value"));
                        form.addInput(input);
                    }

                    elements = htmlForm.select("select");
                    for (Element element : elements) {
                        Input input = new Input();
                        input.setName(element.attributes().get("name"));
                        input.setType(element.attributes().get("type"));
                        input.setType("SELECT");

                        Elements elements2 = element.select("option");
                        List<String> options = new ArrayList<>();
                        for (Element element2 : elements2) {
                            options.add(element2.text());
                        }
                        input.setOptions(options);
                        form.addInput(input);
                    }

                    forms.add(form);
                }
            }
            catch (URISyntaxException x) {
                Loggers.FUZZER.error("Error resolving for action: " + action);
            }
        }
        return forms;
    }


    public static Collection<URL> extractLinks(Profile profile, String responseString) throws Exception {
        Document document = cache.get(responseString);
        return extractLinks(profile,document);
    }

    public static Collection<URL> extractLinks(Profile profile, InputStream in) throws Exception {
        Document document = Jsoup.parse(in, Charset.defaultCharset().name(),profile.getServerAndPortURL());
        return extractLinks(profile,document);
    }

    public static Collection<URL> extractLinks(Profile profile, Document document) {
        List<URL> links = new ArrayList<>();
        Elements htmlLinks = document.select("a");
        for (Element htmlLink : htmlLinks) {
            String href = htmlLink.attributes().get("href");
            if (href != null) {
                try {
                    URI uri = LinkResolver.resolveRelativeURL(profile, href);
                    if (uri != null) {
                        links.add(uri.toURL()); // TODO: prepend host + port + context
                    }
                }
                catch (Exception x) {
                    Loggers.FUZZER.warn("Error extracting link " + href);
                }
            }
        }

        return links;
    }

}
