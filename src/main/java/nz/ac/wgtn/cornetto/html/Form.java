package nz.ac.wgtn.cornetto.html;

import nz.ac.wgtn.cornetto.http.MethodSpec;
import java.net.URI;
import java.util.*;

/**
 * Representation of a html form.
 * This can be used to extract forms from responses, and use them for input generation.
 * @author jens dietrich
 */
public class Form  {

    private URI action = null;
    private MethodSpec method = MethodSpec.GET;
    private List<Input> inputs = new ArrayList<>();
    // the (relative) name or URL of the site or script generating the containing the form
    // if this has been inferred by means of static analysis , this is a reference to a file found in the web application
    // if this has been inferred by means of dynamic analysis , this is a reference to a URL
    private String owner = null;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public URI getAction() {
        return action;
    }

    public void setAction(URI action) {
        this.action = action;
    }

    public MethodSpec getMethod() {
        return method;
    }

    public void setMethod(MethodSpec method) {
        this.method = method;
    }

    public List<Input> getInputs() {
        return inputs;
    }

    public void addInput(Input input) {
        this.inputs.add(input);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return Objects.equals(action, form.action) &&
                Objects.equals(method, form.method) &&
                Objects.equals(inputs, form.inputs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(action, method, inputs);
    }


    @Override
    public String toString() {
        return "Form{" +
                "action='" + action + '\'' +
                ", method='" + method + '\'' +
                ", inputs=" + inputs +
                '}';
    }
}
