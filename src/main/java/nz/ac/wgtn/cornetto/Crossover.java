package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

/**
 * Crossover operation.
 * @author jens dietrich
 */
@FunctionalInterface
public interface Crossover<T extends Spec>  {
    T apply(SourceOfRandomness sourceOfRandomness, Context context, T parent1, T parent2);
}
