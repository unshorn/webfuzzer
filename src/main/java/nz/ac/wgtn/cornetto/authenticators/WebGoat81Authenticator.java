package nz.ac.wgtn.cornetto.authenticators;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import nz.ac.wgtn.cornetto.jee.ResetWebgoat;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.net.CookiePolicy;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Authenticator for WebGoat81.
 * @author jens dietrich
 */
public class WebGoat81Authenticator implements Authenticator {

    // username / password must be setup manually by launching webgoat and configuring user in
    // localhost:8080/WebGoat/login
    public static final String USERNAME = "scottscott";
    public static final String PASSWORD = "tigertiger";

    private boolean needsAuthentication = true;

    @Override
    public boolean isLogoutRequest(HttpUriRequest request) {
        if (request.getURI().toString().contains("/logout")) {
            Loggers.FUZZER.warn("Potential logout request " + request.getMethod() + " " + request.getURI() );
        }
        return false;
    }

    @Override
    public boolean needsAuthentication() {
        return this.needsAuthentication;
    }

    @Override
    public void checkAuthenticationStatus(HttpRequest request, HttpResponse response, String responseEntityData) {
        boolean loginFormDetected = responseEntityData.contains("<form action=\"/WebGoat/login\" method='POST'");
        this.needsAuthentication = loginFormDetected;
    }

    @Override
    public boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        return authenticate(profile, options,httpClient,sessionHeaderConsumer,true);
    }

    @Override
    public boolean reAuthenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        return authenticate(profile, options,httpClient,sessionHeaderConsumer,false);
    }

    private boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer, boolean first) {

        if (first) {
            try {
                Loggers.FUZZER.info("Try to create user account " + USERNAME + " / " + PASSWORD);
                URIBuilder uriBuilder = new URIBuilder()
                        .setScheme("http")
                        .setHost("localhost")
                        .setPort(8080)
                        .setPath("WebGoat/register.mvc");
                URI uri = uriBuilder.build();
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("username", USERNAME));
                params.add(new BasicNameValuePair("password", PASSWORD));
                params.add(new BasicNameValuePair("matchingPassword", PASSWORD));
                params.add(new BasicNameValuePair("agree", "agree"));
                HttpPost registrationRequest = new HttpPost(uri);
                registrationRequest.setEntity(new UrlEncodedFormEntity(params));
                HttpResponse response = httpClient.apply(registrationRequest);
                if (response.getStatusLine().getStatusCode() != 302) {
                    Loggers.FUZZER.fatal("User registration has failed, response was: " + response.getStatusLine());
                }
            } catch (Exception x) {
                Loggers.FUZZER.error("Error registering WebGoat account to be used for fuzzing",x);
            }
        }

        try {
            Loggers.AUTHENTICATOR.info("Authenticate");

            List<BasicNameValuePair> authParams = new ArrayList<BasicNameValuePair>();
            authParams.add(new BasicNameValuePair("username", USERNAME));
            authParams.add(new BasicNameValuePair("password", PASSWORD));
            HttpPost httpPost = new HttpPost(Authenticator.getURI(profile,"login"));

            //Add Parameters to request and send
            httpPost.setEntity(new UrlEncodedFormEntity(authParams, "UTF-8"));

            HttpResponse response = httpClient.apply(httpPost);

            if (response != null) {
                boolean success = response.getStatusLine().getStatusCode()==302 && Authenticator.hasHeaderSuchThat(response,"Location", location -> location.endsWith("welcome.mvc"));
                if (!success) {

                    Loggers.FUZZER.info("Try to create user account " + USERNAME + " / " + PASSWORD);
                    URIBuilder uriBuilder = new URIBuilder()
                        .setScheme("http")
                        .setHost("localhost")
                        .setPort(8080)
                        .setPath("WebGoat/register.mvc");
                    URI uri = uriBuilder.build();
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("username", USERNAME));
                    params.add(new BasicNameValuePair("password", PASSWORD));
                    params.add(new BasicNameValuePair("matchingPassword", PASSWORD));
                    params.add(new BasicNameValuePair("agree", "agree"));
                    HttpPost registrationRequest = new HttpPost(uri);
                    registrationRequest.setEntity(new UrlEncodedFormEntity(params));

                    response = httpClient.apply(registrationRequest);
                    if (response.getStatusLine().getStatusCode()!=302) {
                        Loggers.FUZZER.fatal("Authentication has failed");
                        System.exit(1);
                    }

                    String locationHeader = null;
                    Header[] locationHeaders = response.getHeaders("Location");
                    if (locationHeaders==null || locationHeaders.length==0) {
                        Loggers.FUZZER.info("No Location header returned indicating success");
                    }

                    Loggers.FUZZER.fatal("Authentication has failed");
                    Loggers.FUZZER.fatal("\tresponse status line was: " + response.getStatusLine());
                    Loggers.FUZZER.info("\thint: run " + ResetWebgoat.class.getName() + " to reset webgoat, or manually start webgoat, clear ~/.webgoat-v8.1.0, start webgoat and register a new user with username = " + USERNAME + " and password = " + PASSWORD);
                    System.exit(1);
                }

                // access this service to initilise session
                // http://localhost:8080/WebGoat/WebGoatIntroduction.lesson.lesson
                HttpGet getRequest = new HttpGet(Authenticator.getURI(profile,"WebGoatIntroduction.lesson.lesson"));
                Loggers.FUZZER.info("Accessing lesson to initialise session");
                // this may take really long
                response = httpClient.apply(getRequest);
                if (response != null && response.getStatusLine().getStatusCode() == 200) {
                    Loggers.FUZZER.info("Lesson initialised");
                } else {
                    Loggers.FUZZER.fatal("Authentication has failed");
                    System.exit(1);
                }
            } else {
                // exit
                Loggers.FUZZER.fatal("Authentication has failed");
                System.exit(1);
            }
        } catch (Exception x) {
            Loggers.FUZZER.fatal("Error during authentication", x);
            System.exit(1);
        }
        this.needsAuthentication = false;
        Loggers.FUZZER.info("Authentication success");

        sessionHeaderConsumer.accept(Collections.EMPTY_LIST);

        return true;
    }
}
