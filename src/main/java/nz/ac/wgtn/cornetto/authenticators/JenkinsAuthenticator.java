package nz.ac.wgtn.cornetto.authenticators;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import org.apache.http.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Authenticator for Jenkins.
 * @author jens dietrich
 */
public class JenkinsAuthenticator implements Authenticator {

    private boolean needsAuthentication = true;


    @Override
    public boolean isLogoutRequest(HttpUriRequest request) {
        if (request.getURI().toString().contains("logout")) {
            Loggers.FUZZER.warn("Potential logout request " + request.getMethod() + " " + request.getURI() );
            return true;
        }
        return false;
    }

    @Override
    public boolean needsAuthentication() {
        return this.needsAuthentication;
    }

    @Override
    public void checkAuthenticationStatus(HttpRequest request, HttpResponse response, String responseEntityData) {
        //  EXAMPLE OF A RESPONSE WHEN AUTHENTICATION IS LOST
        //            # REQUEST HEADERS
        //
        //            # REQUEST PARAMETERS
        //                    q : , Pattern=
        //
        //            # RESPONSE STATUS
        //                    HTTP/1.1 403 Forbidden
        //
        //            # RESPONSE HEADERS
        //                    Connection : close
        //                    Date : Fri, 29 Jan 2021 07:45:54 GM
        //                    X-Content-Type-Options : nosniff
        //                    WEBFUZZ-FEEDBACK-TICKET : 167287
        //                    Content-Type : text/html;charset=utf-8
        //                    X-Hudson : 1.395
        //                    X-Jenkins : 2.190.2
        //                    X-Jenkins-Session : a8f1ef9a
        //                    X-You-Are-Authenticated-As : anonymous
        //                    X-You-Are-In-Group-Disabled : JENKINS-39402: use -Dhudson.security.AccessDeniedException2.REPORT_GROUP_HEADERS=true or use /whoAmI to diagnose
        //                    X-Required-Permission : hudson.model.Hudson.Read
        //                    X-Permission-Implied-By : hudson.security.Permission.GenericRead
        //                    X-Permission-Implied-By : hudson.model.Hudson.Administer
        //                    Content-Length : 915
        //                    Server : Jetty(9.4.9.v20180320)
        //
        //            # RESPONSE ENITY
        //            <html><head><meta http-equiv='refresh' content='1;url=/jenkins.fuzz/login?from=%2Fjenkins.fuzz%2Fsearch%3Fq%3D%252C%2BPattern%253D'/><script>window.location.replace('/jenkins
        //                            .fuzz/login?from=%2Fjenkins.fuzz%2Fsearch%3Fq%3D%252C%2BPattern%253D');</script></head><body style='background-color:white; color:white;'>
        //
        //
        //                    Authentication required
        //            <!--
        //                            You are authenticated as: anonymous
        //                    Groups that you are in:
        //
        //                    Permission you need to have (but didn't): hudson.model.Hudson.Read
        //             ... which is implied by: hudson.security.Permission.GenericRead
        //             ... which is implied by: hudson.model.Hudson.Administer
        //                            -->
        //
        //            </body></html>

        if (response.getStatusLine().getReasonPhrase().contains("No valid crumb was included in the request")) {
            Loggers.AUTHENTICATOR.info("Crumb Issue Detected");
            Loggers.AUTHENTICATOR.info("Request: " + request.getRequestLine());
            Loggers.AUTHENTICATOR.info("Response Status: " + response.getStatusLine());
            for (Header header : response.getAllHeaders()) {
                Loggers.AUTHENTICATOR.info("\t" + header.getName() + " : " + header.getValue());
            }
            Loggers.AUTHENTICATOR.info("");
        }


        this.needsAuthentication =  Authenticator.authenticationNeededStatus(response) || Authenticator.hasHeader(response," X-You-Are-Authenticated-As","anonymous");
    }

    @Override
    public boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {

        Loggers.AUTHENTICATOR.info("Authenticate");
        // read initial secret -- assumes that jenkins is local, i.e. fuzzer and server are running on the same PC, a different authenticator must be used if this is not the case

        File home = new File(System.getProperty("user.home"));
        File jenkinsAdminPasswdFile = new File(home,".jenkins/secrets/initialAdminPassword");
        String password = null;
        try {
            List<String> lines = Files.readLines(jenkinsAdminPasswdFile, Charset.defaultCharset());
            Preconditions.checkState(lines.size()==1);
            password = lines.get(0);
        }
        catch (Exception x) {
            Loggers.AUTHENTICATOR.fatal("Cannot read jenkins password from " + jenkinsAdminPasswdFile.getAbsolutePath() + " -- a different authenticator must be used if fuzzed server is remote",x);
            System.exit(1);
        }


        try {
            HttpPost post = new HttpPost(Authenticator.getURI(profile,"j_spring_security_check"));
            post.setHeader("Content-Type","application/x-www-form-urlencoded");

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("j_username", "admin"));
            params.add(new BasicNameValuePair("j_password", password));
            post.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse response = httpClient.apply(post);

            Loggers.AUTHENTICATOR.info("Authentication results");
            Loggers.AUTHENTICATOR.info("Authentication request status: " + response.getStatusLine());
            Loggers.AUTHENTICATOR.info("Authentication request headers:");
            for (Header header:response.getAllHeaders()) {
                Loggers.AUTHENTICATOR.info("\t" + header.getName() + " : " + header.getValue());
            }

            boolean success = response.getStatusLine().getStatusCode()==302 && Authenticator.hasHeaderSuchThat(response,"Location",v -> true);

            if (success) {
                this.needsAuthentication = false;


                Loggers.AUTHENTICATOR.info("Trying to fetch crumb");
                // Loggers.AUTHENTICATOR.info("\thudson.security.csrf.GlobalCrumbIssuerConfiguration.DISABLE_CSRF_PROTECTION = " + System.getProperty("hudson.security.csrf.GlobalCrumbIssuerConfiguration.DISABLE_CSRF_PROTECTION"));

                HttpGet getCrumb = new HttpGet(Authenticator.getURI(profile,"crumbIssuer/api/json"));

                HttpResponse getCrumbResponse = httpClient.apply(getCrumb);
                String data = EntityUtils.toString(getCrumbResponse.getEntity());
                Loggers.AUTHENTICATOR.info(data);

                // example return:  {"_class":"hudson.security.csrf.DefaultCrumbIssuer","crumb":"eaec2e165a028228bc48ac9dba5121718458543250d86b2f8274dac0bc938582","crumbRequestField":"Jenkins-Crumb"}

                try {

                    JSONObject json = new JSONObject(data);
                    String headerKey = json.getString("crumbRequestField");
                    assert headerKey != null;
                    String headerValue = json.getString("crumb");
                    assert headerValue != null;

                    BasicNameValuePair header = new BasicNameValuePair(headerKey, headerValue);
                    List<BasicNameValuePair> headers = new ArrayList(1);
                    headers.add(header);

                    sessionHeaderConsumer.accept(headers);
                }
                catch (Exception x) {
                    Loggers.AUTHENTICATOR.warn("Error fetching crumb !", x);
                }

                return true;
            }
            else {
                return false;
            }

        } catch (Exception x) {
            Loggers.AUTHENTICATOR.fatal("Authentication request has failed",x);
            return false;
        }

    }
}
