package nz.ac.wgtn.cornetto.authenticators;

import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Authenticator for Derby.
 * @author jens dietrich
 */
public class DerbyAuthenticator implements Authenticator {

    @Override
    public boolean isLogoutRequest(HttpUriRequest request) {
        return false;
    }

    @Override
    public boolean needsAuthentication() {
        return false;
    }

    @Override
    public void checkAuthenticationStatus(HttpRequest request, HttpResponse response, String responseEntityData) {
        // nothing to do
    }

    @Override
    public boolean authenticate(Profile profile, Options options, Function<HttpUriRequest, HttpResponse> httpClient, Consumer<List<BasicNameValuePair>> sessionHeaderConsumer) {
        Loggers.AUTHENTICATOR.info("Authenticate (nothing todo)");
        return true;
    }
}
