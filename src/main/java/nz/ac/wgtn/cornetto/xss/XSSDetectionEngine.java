package nz.ac.wgtn.cornetto.xss;

import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import nz.ac.wgtn.cornetto.jee.HttpRecord;
import nz.ac.wgtn.cornetto.jee.Options;

import java.util.Collection;

/**
 * Engine to detect XSS in responses.
 * @author jens dietrich
 */
public interface XSSDetectionEngine {

    void init(Options options);

    void checkForXSS(HttpRecord record, String responseEntityContent, Collection<FuzzerListener> listeners);

}
