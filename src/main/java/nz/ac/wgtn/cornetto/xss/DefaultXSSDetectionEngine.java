package nz.ac.wgtn.cornetto.xss;

import com.google.common.collect.Multimap;
import com.googlecode.concurrenttrees.common.CharSequences;
import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.SpecProvenanceUtils;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import nz.ac.wgtn.cornetto.output.XSSVulnerability;
import nz.ac.wgtn.cornetto.jee.HttpRecord;
import nz.ac.wgtn.cornetto.jee.IssuePriority;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.Options;

import java.util.Collection;

/**
 * Default implementation of a XSS detection engine.
 * @author jens dietrich
 */
public class DefaultXSSDetectionEngine implements XSSDetectionEngine {

    public static final int SUBSTRING_SIZE_THRESHOLD = 10;

    @Override
    public void init(Options options) {
        // can implement passing of parameters here
    }

    @Override
    public void checkForXSS(HttpRecord record, String responseEntityContent, Collection<FuzzerListener> fuzzerListeners) {
        // only check normal responses by default
        if (record.getResponse().getStatusLine().getStatusCode() >= 400) return;

        try {
            Multimap<String, RequestSpec.RequestDataKind> requestData = SpecProvenanceUtils.extractTaintedParts(record.getRequestSpec());

            for (String token:requestData.keySet()) {

                boolean isExactMatch = responseEntityContent.contains(token);
                if (isExactMatch && XSSTokens.ALL.contains(token)) {
                    if (XSSTokens.SIMPLE.contains(token)) {
                        XSSCandidate xssCandidate = new XSSCandidate(record.getRequestSpec(),token);
                        XSSVulnerability vulnerability = XSSVulnerability.newFrom(record,null,responseEntityContent,token, IssuePriority.LOW) ;
                        fuzzerListeners.forEach(l -> l.newXSSDiscovered(vulnerability));
                        // report for further mutation to dynamic model
                        Context.getDefault().getDynamicModel().registerXSSCandidate(xssCandidate);
                    }
                    else {
                        XSSVulnerability vulnerability = XSSVulnerability.newFrom(record,null,responseEntityContent,token, IssuePriority.HIGH) ;
                        fuzzerListeners.forEach(l -> l.newXSSDiscovered(vulnerability));
                    }
                }
            }
        }
        catch (Exception x) {
            Loggers.FUZZER.info("Error processing http transactions to scan for XSS vulnerabilities",x );
        }
    }


    // can be used for approximate match
    private boolean matchTrackedString (String responseEntityContent,String token) {

        // to avoid java.lang.IllegalArgumentException: The document argument was zero-length in LCSubstringSolver
        if (responseEntityContent.length()< SUBSTRING_SIZE_THRESHOLD || token.length() < SUBSTRING_SIZE_THRESHOLD) {
            return false;
        }
        else {
            LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());
            solver.add(token);
            solver.add(responseEntityContent);
            String longestCommonSubstring = CharSequences.toString(solver.getLongestCommonSubstring());
            return longestCommonSubstring.length() >= SUBSTRING_SIZE_THRESHOLD;
        }
    }
}
