package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import java.util.function.BiFunction;

/**
 * Generator for random objects.
 * @author jens dietrich
 */
public interface Generator<T>  extends BiFunction<SourceOfRandomness,Context,T> {
}
