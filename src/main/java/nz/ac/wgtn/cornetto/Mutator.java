package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

/**
 * Mutates objects.
 * The mutation result should be a different object from the input.
 * @author jens dietrich
 */
@FunctionalInterface
public interface Mutator<T extends Spec>  {
    T apply(SourceOfRandomness sourceOfRandomness, Context context, T original);

    // TODO: implement default andThen
}
