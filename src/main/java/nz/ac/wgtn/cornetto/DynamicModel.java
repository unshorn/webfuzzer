package nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.feedback.CoverageGraph;
import nz.ac.wgtn.cornetto.feedback.TaintFlowSpec;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.xss.XSSCandidate;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;

import java.net.URL;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Dynamic model.
 * @author jens dietrich
 */
public interface DynamicModel {
    /**
     * Record executions (in most cases, methods) that have been encountered.
     * @param requestSpec
     * @param request
     * @param response
     * @param executions
     * @return newly added (as in: not seen before) execution points
     */
    Set<ExecutionPoint> executionsRecorded (
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<ExecutionPoint> executions
    );

    /**
     * Get all recorded execution points.
     * @return
     */
    public Set<ExecutionPoint> getRecordedExecutionPoints();

    /**
     * Record request parameters that have been encountered.
     * @param requestSpec
     * @param request
     * @param response
     * @param requestParameters
     * @return newly added (as in: not seen before) request parameter names
     */
    Set<String> requestParametersRecorded (
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<String> requestParameters
    );


    /**
     * Record forms returned by the server.
     * @param requestSpec
     * @param request
     * @param response
     * @param forms
     * @return newly found forms
     */
    Set<Form> formsRecorded(
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<Form> forms
    );

    /**
     * Record links returned by the server.
     * @param requestSpec
     * @param request
     * @param response
     * @param links
     * @return newly found links
     */
    Set<URL> linksRecorded(
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<URL> links
    );

    /**
     * Record a new stack trace for an invocation of a critical method.
     * @param requestSpec
     * @param request
     * @param response
     * @param stacktrace
     * @return
     */
    boolean stacktraceToCriticalMethodRecorded (
        RequestSpec requestSpec,
        HttpRequest request,
        HttpResponse response,
        String stacktrace
    );



    // NOTE: disabled, now all connections use a shared cookiestore
    //    /**
    //     * Get the cookies sent by the server so far.
    //     * @return a collection of cookies
    //     */
    //    Collection<HttpCookie> getCookies();

    /**
     * Get the top-performing request specs.
     * Can be used to mutate them.
     */
    Collection<RequestSpec> getTopRequestSpecs();


    /**
     * Get the top-performing request specs. This may include requests that are not "on the edge"
     * but also with a distance to the ends < threshold. E.g., his threshold can be computed using
     * an exponential distribution.
     */
    Collection<RequestSpec> getNearTopRequestSpecs(SourceOfRandomness sourceOfRandomness, int maxThreshold);

    /**
     * Get recorded forms.
     * @return
     */
    Collection<Form> getForms();

    /**
     * Get recorded links.
     * @return
     */
    Collection<URL> getLinks();

    /**
     * Get all request specs.
     * @author jens dietrich
     */
    Collection<RequestSpec> getRequestSpecs();

    Collection<RequestSpec> getRequestSpecsReachingUnsafeMethods();

    /**
     * Get the targets reached.
     * The info values are to be specified, this should contain status lines, methods invoked and further similar information,
     */

    Map<RequestSpec, Object> getTargets();


    /**
     * Get request parameter names encountered.
     * @return
     */
    Set<String> getRequestParameterNames();


    /**
     * Register a new taint flow.
     * @param requestSpec
     * @param request
     * @param response
     * @param taintFlow
     * @return true if the new taint flow has been successfully registered, false otherwise
     */
    boolean taintFlowRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, TaintFlowSpec taintFlow);

    /**
     * Register a request with a token that flows back to the client, is too simple for an XSS attack, but can be used as mutation seed.
     * An advanced implementation can chose to ignore candidates similar to candidates already seen (and mutated).
     * @param xssCandidate
     * @return true if the new XSS candidate has been successfully registered, false otherwise
     */
    boolean registerXSSCandidate(XSSCandidate xssCandidate);

    /**
     * Unregister an xss candidate, this should be done if no more mutations are possible.
     * @param xssCandidate
     * @return true if the new XSS candidate has been successfully unregistered, false otherwise
     */
    boolean unregisterXSSCandidate(XSSCandidate xssCandidate);

    /**
     * Get registered XSS candidates.
     * @return
     */
    Collection<XSSCandidate> getXSSCandidates();


    /**
     * get the coverage graph, e.g. to access and report statistics.
     * @return
     */
    CoverageGraph getCoverageGraph();

}
