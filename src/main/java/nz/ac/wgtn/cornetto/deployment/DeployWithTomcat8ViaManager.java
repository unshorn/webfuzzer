package nz.ac.wgtn.cornetto.deployment;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import javax.annotation.Nullable;
import java.io.*;
import java.net.URI;
import java.util.Properties;

/**
 * Deploy web app for fuzzing campaign on a running tomcat instance (i.e. it must have been started),
 * using the tomcat manager application.
 * IMPORTANT !!!  the aspectj weaver must be set manually in the tomcat executable,
 * e.g.  by adding this line to ${TOMCAT_HOME}/bin/catalina.sh :
 * CATALINA_OPTS=-javaagent:aspectjweaver-1.9.6.jar    (and add the respective jar to the folder)
 * @author jens dietrich
 */

public class DeployWithTomcat8ViaManager implements Deployment {

    private static final Logger LOGGER = LogSystem.getLogger("jee-fuzz-tomcat");
    private String contextName = null;
    private String userName = null;
    private String userPassword = null;

    public static final String ADMIN_PROPERTIES = "tomcat-admin.properties";


    @Override
    public boolean deploy(File war, File aspectJJar,String contextName) throws Exception {

        Preconditions.checkArgument(war!=null,"War file must not be null");
        Preconditions.checkArgument(war.exists(),"War file does not exist: " + war.getAbsolutePath());
        Preconditions.checkArgument(aspectJJar!=null,"AspectJ jar file must not be null");
        Preconditions.checkArgument(aspectJJar.exists(),"AspectJ jar file does not exist: " + aspectJJar.getAbsolutePath());
        Preconditions.checkArgument(contextName!=null,"contextName must not be null");

        //Tomcat requires a starting '/'
        if (!contextName.startsWith("/")) {
          contextName = "/" + contextName;
        }

        // read properties
        Properties adminSettings = new Properties();
        Reader adminSettingsReader = new FileReader(ADMIN_PROPERTIES);
        adminSettings.load(adminSettingsReader);
        userName = adminSettings.getProperty("ADMIN_USERNAME");
        Preconditions.checkArgument(userName!=null,"Cannot read ADMIN_USERNAME property from " + ADMIN_PROPERTIES);
        userPassword = adminSettings.getProperty("ADMIN_PASSWORD");
        Preconditions.checkArgument(userPassword!=null,"Cannot read ADMIN_PASSWORD property from " + ADMIN_PROPERTIES);

        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(userName, userPassword);
        provider.setCredentials(AuthScope.ANY, credentials);

        HttpClient client = HttpClientBuilder.create()
            .setDefaultCredentialsProvider(provider)
            .build();

        // if previsously deployed, undeploy
        LOGGER.info("Trying to undeploy " + contextName + " (if it exists)");
        URI uri = new URIBuilder()
            .setScheme("http")
            .setHost(getHost())
            .setPort(getPort())
            .setPath("manager/text/undeploy")
            .addParameter("path",contextName)
            .build();

        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        LOGGER.info("Undeploy request executed: " + uri);
        LOGGER.info("Response status: " + response.getStatusLine());
        String txt = EntityUtils.toString(response.getEntity());
        LOGGER.info("Response details: " + txt);

        LOGGER.info("Trying to (re)deploy");
        uri = new URIBuilder()
                .setScheme("http")
                .setHost(getHost())
                .setPort(getPort())
                .setPath("manager/text/deploy")
                .addParameter("path",contextName)
                .addParameter("war","file:"+war.getAbsolutePath())
                .build();

        request = new HttpGet(uri);
        response = client.execute(request);
        LOGGER.info("Deploy request executed: " + uri);
        LOGGER.info("Response status: " + response.getStatusLine());
        txt = EntityUtils.toString(response.getEntity());
        LOGGER.info("Response details: " + txt);

        return true;
    }


    @Override
    public void undeploy() throws Exception {
        // TODO - clean up redundancies with deploy

        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(userName, userPassword);
        provider.setCredentials(AuthScope.ANY, credentials);

        HttpClient client = HttpClientBuilder.create()
                .setDefaultCredentialsProvider(provider)
                .build();

        // if previsously deployed, undeploy
        LOGGER.info("Trying to undeploy " + contextName + " (if it exists)");
        URI uri = new URIBuilder()
                .setScheme("http")
                .setHost(getHost())
                .setPort(getPort())
                .setPath("manager/text/undeploy")
                .addParameter("path",contextName)
                .build();

        HttpGet request = new HttpGet(uri);
        HttpResponse response = client.execute(request);
        LOGGER.info("Undeploy request executed: " + uri);
        LOGGER.info("Response status: " + response.getStatusLine());
        String txt = EntityUtils.toString(response.getEntity());
        LOGGER.info("Response details: " + txt);

    }

    @Override
    public String getHost() {
        return "localhost";
    }

    @Override
    public int getPort() {
        return 8080;
    }

    @Override
    public String getURL() {
        return getHost() + ':' + getPort() + contextName;
    }

    @Nullable
    @Override
    public FuzzerListener getMaintenanceListener() {
        return null;
    }
}
