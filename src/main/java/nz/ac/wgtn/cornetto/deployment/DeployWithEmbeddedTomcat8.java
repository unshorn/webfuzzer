package nz.ac.wgtn.cornetto.deployment;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * Deploy web app for fuzzing campaign on tomcat.
 *
 */
@Deprecated
public class DeployWithEmbeddedTomcat8 implements Deployment {

    private static final Logger LOGGER = LogSystem.getLogger("jee-fuzz-tomcat");
    private static final File TOMCAT_EXECUTABLE = new File("tools/tomcat8-war-runner-3.0.jar");

    // deployment-specific state
    private Process activeDeployment = null;
    private String contextName = null;

    @Override
    public boolean deploy(File war, File aspectJJar,String contextName) throws Exception {

        Preconditions.checkState(activeDeployment==null,"Unique instances must be used for each deployment");

        Preconditions.checkArgument(war!=null,"War file must not be null");
        Preconditions.checkArgument(war.exists(),"War file does not exist: " + war.getAbsolutePath());
        Preconditions.checkArgument(aspectJJar!=null,"AspectJ jar file must not be null");
        Preconditions.checkArgument(aspectJJar.exists(),"AspectJ jar file does not exist: " + aspectJJar.getAbsolutePath());
        Preconditions.checkArgument(contextName!=null,"contextName must not be null");

        //Tomcat requires a starting '/'
        if (!contextName.startsWith("/")) {
          contextName = "/" + contextName;
        }

        this.contextName = contextName;

        LOGGER.info("Trying to start server to start fuzzing session @ " + getURL());
        FileUtils.deleteQuietly(new File("tomcat.standalone.properties"));
        FileUtils.deleteDirectory(new File(".extract"));

        File tomCatProperties = new File("tomcat.standalone.properties");
        FileUtils.forceMkdir(new File(".extract/webapps"));
        FileUtils.forceMkdir(new File(".extract/conf"));
        FileUtils.copyFileToDirectory(war, new File(".extract/webapps"));
        FileUtils.copyFileToDirectory(new File("users.xml"), new File(".extract"));

        Properties properties = new Properties();
        properties.setProperty("useServerXml", "false");
        properties.setProperty("wars", war.getName() + "|"+contextName);
        properties.setProperty("enableNaming", "true");

        FileOutputStream os = new FileOutputStream(tomCatProperties);
        properties.store(os, "tomcat");
        os.close();

        final Process process = new ProcessBuilder()
            .command(
                "java",
                "-javaagent:" + aspectJJar.getAbsolutePath(),
                "-Daj.weaving.verbose=true",
                "-cp",TOMCAT_EXECUTABLE.getAbsolutePath()+File.pathSeparator+".",
                "org.apache.tomcat.maven.runner.Tomcat8RunnerCli"
            )
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .start();

        Thread.sleep(5_000); // server needs some time to start !
        activeDeployment = process;
        return true;
    }

    @Override
    public void undeploy() throws Exception {
        Preconditions.checkState(activeDeployment!=null,"no active deployment available to undeploy");
        this.activeDeployment.destroy();
    }

    @Override
    public String getHost() {
        return "localhost";
    }

    @Override
    public int getPort() {
        return 8080;
    }

    @Override
    public String getURL() {
        return getHost() + ':' + getPort() + contextName;
    }

    @Nullable
    @Override
    public FuzzerListener getMaintenanceListener() {
        return null;
    }
}
