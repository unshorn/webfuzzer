package nz.ac.wgtn.cornetto.feedback;

import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.http.RequestSpec;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Data structure to keep track of coverage information.
 * Execution points are encoded as integers, and put in a digraph structure with the empty set as root.
 * By construction, the graph is acyclic.
 * The predecessors of a set are the largest subsets.
 * @author jens dietrich
 */
public class CoverageGraph {
    private DirectedGraph<BitSet,Integer> digraph = new DirectedSparseGraph();
    private Map<ExecutionPoint,Integer> encoding = new HashMap<>();
    private AtomicInteger idCounter = new AtomicInteger();
    private AtomicInteger edgeCounter = new AtomicInteger();
    private BitSet root = new BitSet();
    private Multimap<BitSet, RequestSpec> associations = HashMultimap.create();
    private Set<RequestSpec> requestSpecs = Collections.synchronizedSet(new HashSet<>());
    private Set<ExecutionPoint> executionPoints = new HashSet<>();

    public CoverageGraph() {
        this.digraph.addVertex(root); // empty set
    }

    /**
     * Adds a request spec and the execution points it triggers, and returns any newly added execution points.
     * @param requestSpec
     * @param executionPoints
     * @return
     */
    public synchronized Set<ExecutionPoint> add(RequestSpec requestSpec,Collection<? extends ExecutionPoint> executionPoints) {
        if (executionPoints.size()>0 && requestSpecs.add(requestSpec)) {
            BitSet encoding = encode(executionPoints);
            boolean keyExists = associations.containsKey(encoding);
            associations.put(encoding, requestSpec);
            if (!keyExists) {
                insert(encoding);

                //                System.out.println("DEBUGGING");
                //                this.debugGraph();
                //                System.out.println();

                Set<ExecutionPoint> newExecutionPoints = new HashSet<>();
                for (ExecutionPoint xp:executionPoints) {
                    if (this.executionPoints.add(xp)) {
                        newExecutionPoints.add(xp);
                    }
                }
                return newExecutionPoints;

            }
        }

        return Collections.EMPTY_SET;
    }

    public synchronized Set<ExecutionPoint> getAllVertices() {
        return executionPoints;
    }

    // keys associated with the best coverage
    public synchronized Collection<RequestSpec> getTopVertices() {
        return digraph.getVertices().parallelStream()
            .filter(bitset -> digraph.getOutEdges(bitset).isEmpty())
            .flatMap(bitset -> associations.get(bitset).stream())
            .collect(Collectors.toSet());
    }

    public Set<RequestSpec> getRequestSpecs() {
        return requestSpecs;
    }

    public synchronized Collection<RequestSpec> getTopVertices(int depth) {
        Preconditions.checkArgument(depth>=0);
        Collection<BitSet> vertices = Collections.newSetFromMap(new ConcurrentHashMap<>());
        Collection<BitSet> currentGeneration = digraph.getVertices().parallelStream()
            .filter(bitset -> digraph.getOutEdges(bitset).isEmpty())
            .collect(Collectors.toSet()); // depth=0

        vertices.addAll(currentGeneration);
        for (int i=1;i<=depth;i++) {
            currentGeneration = currentGeneration.parallelStream()
                .flatMap(v -> digraph.getPredecessors(v).stream())
                .collect(Collectors.toSet());
            if (currentGeneration.isEmpty()) {
                break;
            }
            else {
                vertices.addAll(currentGeneration);
            }
        }

        return vertices.parallelStream()
            .flatMap(bitset -> associations.get(bitset).stream())
            .collect(Collectors.toSet());

    }

    // public to be unit testable
    public synchronized void insert(BitSet encoding) {
        if (!this.getDigraph().containsVertex(encoding)) {
            insert(encoding, root);
        }
    }

    // public to be unit testable -- do not modify outside this class !
    public DirectedGraph<BitSet, Integer> getDigraph() {
        return digraph;
    }

    // public to be unit testable -- do not modify outside this class !
    public BitSet getRoot() {
        return root;
    }

    public void setRoot(BitSet root) {
        this.root = root;
    }

    private boolean insert(BitSet newVertex, BitSet visitedVertex) {
        boolean inserted = false;
        Set<Integer> edgesToReplace = new HashSet<>();
        if (isSubsetOf(visitedVertex,newVertex) && !newVertex.equals(visitedVertex)) {
            for (int edge:digraph.getOutEdges(visitedVertex)) {
                BitSet succ = digraph.getDest(edge);
                if (isSubsetOf(newVertex,succ) && !newVertex.equals(succ)) {
                    // replace edge visitedVertex -> successor by
                    // visitedVertex -> newVertex -> successor
                    edgesToReplace.add(edge);
                    inserted = true;
                }
                else if (isSubsetOf(succ,newVertex)) {
                    // recurse
                    inserted = inserted | insert(newVertex,succ);
                }
            }

            if (!inserted) {
                // isNew new leaf
                digraph.addEdge(edgeCounter.incrementAndGet(),visitedVertex,newVertex);
                inserted = true;
            }
        }

        // replace edge by two new edges with new vertex in between
        for (int edge:edgesToReplace) {
            BitSet succ = digraph.getDest(edge);
            digraph.removeEdge(edge);
            digraph.addEdge(edgeCounter.incrementAndGet(),visitedVertex,newVertex);
            digraph.addEdge(edgeCounter.incrementAndGet(),newVertex,succ);
        }

        return inserted;
    }

    private boolean isSubsetOf(BitSet part, BitSet set) {
        int notElementOf = part.stream().filter(slot -> !set.get(slot)).findAny().orElse(-1);
        return notElementOf==-1;
    }

    private BitSet encode(Collection<? extends ExecutionPoint> executionPoints) {
        BitSet bitSet = new BitSet();
        for (ExecutionPoint x:executionPoints) {
            int id = encoding.computeIfAbsent(x,xp -> idCounter.getAndIncrement());
            bitSet.set(id);
        }
        return bitSet;
    }

    public synchronized void debugGraph() {
        debugGraph(root,0);
    }

    public synchronized void debugGraph(BitSet v,int depth) {
        for (int i=0;i<depth;i++) {
            System.out.print('\t');
        }
        System.out.println(v);
        for (int e:digraph.getOutEdges(v)) {
            debugGraph(digraph.getDest(e),depth+1);
        }
    }


    // some metrics for reporting

    public int getVertexCount() {
            return this.digraph.getVertexCount();
    }

    // number of vertices that are optimal
    public int getParetoFronSize() {
        return (int) digraph.getVertices()
            .parallelStream()
            .filter(bitset -> digraph.getOutEdges(bitset).isEmpty())
            .count();
    }

    public synchronized int getDepth() {
        return getDepth(root);
    }

    private int getDepth(BitSet vertex) {

        // TODO cache
        if (digraph.outDegree(vertex)==0) {
            return 0;
        }

        int childDepth = -1;
        for (BitSet succ:digraph.getSuccessors(vertex)) {
            int d = getDepth(succ);
            if (d>childDepth) {
                childDepth = d;
            }
        }
        return childDepth+1;
    }


    // compute and report the average size of the vertices (representing sets)
    public double getAvgVertexSizeCount() {
        return this.digraph.getVertices()
            .stream()
            .collect(Collectors.averagingDouble(set -> set.cardinality()));
    }

}
