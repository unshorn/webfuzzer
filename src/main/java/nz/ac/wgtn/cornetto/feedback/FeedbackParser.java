package nz.ac.wgtn.cornetto.feedback;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import nz.ac.wgtn.cornetto.ExecutionPoint;
import nz.ac.wgtn.cornetto.jee.Loggers;
import nz.ac.wgtn.cornetto.jee.war.MethodInvocation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;

/**
 * Simple feedback parser.
 * @author jens dietrich
 */
public class FeedbackParser {

    public static Set<ExecutionPoint> extractMethodSpecs(JSONObject json) {
        try {
            JSONArray arr = json.getJSONArray("invokedMethods");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
            Set<ExecutionPoint> specs = new HashSet<>();
            for (int i = 0; i < arr.length(); i++) {
                String line = arr.getString(i);
                String[] parts = line.split("::");
                assert parts.length == 2;
                ExecutionPoint spec = new MethodInvocation(parts[0],parts[1].substring(0,parts[1].indexOf('(')),parts[1].substring(parts[1].indexOf('(')));
                specs.add(spec);
            }
            return specs;
        }
        catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }

    public static Set<String> extractRequestParameterNames(JSONObject json) {
        return convertFlatJSONArray(json, "requestParameterNames");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }

    private static Set<String> convertFlatJSONArray(JSONObject json,String key)  {
        try {
            JSONArray arr = json.getJSONArray(key);  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
            Set<String> values = new HashSet<>();
            for (int i = 0; i < arr.length(); i++) {
                values.add(""+arr.get(i));
            }
            return values;
        }
        catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }

    public static Set<String> extractStackTracesForUnsafeMethods(JSONObject json) {
        return convertFlatJSONArray(json,"unsafeSinkInvocationStackTraces");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }

    /**
     * @shawn
     * @param json
     * @return
     */
     public static List<TaintFlowSpec> extractTaintflows(JSONObject json) {
         if (!json.keySet().contains("taintFlows")) {
             return Collections.EMPTY_LIST;
         }

         JSONArray taintFlowData = json.getJSONArray("taintFlows");
         String serialized = taintFlowData.toString(4);  // still pretty print for debugging
         ObjectMapper mapper = new ObjectMapper();
         try {
             return mapper.readValue(serialized, new TypeReference<List<TaintFlowSpec>>() {});
         }
         catch (Exception x) {
             Loggers.RESULTS_INJECTION.error("Error parsing feedback",x);
             return Collections.EMPTY_LIST;
         }
     }

    public static Set<String> extractStackTracesForExceptions(JSONObject json) {
        return convertFlatJSONArray(json,"exceptions");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }
}
