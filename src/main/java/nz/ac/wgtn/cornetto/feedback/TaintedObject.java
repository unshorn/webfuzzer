package nz.ac.wgtn.cornetto.feedback;

import java.util.List;
import java.util.Objects;

/**
 * A taint object .
 * @author shawn
 */
public class TaintedObject {
    private List<TaintPropagationEvent> taintEvents = null;
    private String asString = null;

    public List<TaintPropagationEvent> getTaintEvents() {
        return taintEvents;
    }

    public void setTaintEvents(List<TaintPropagationEvent> taintEvents) {
        this.taintEvents = taintEvents;
    }

    private String type = null;
    private int hash = 0;

    public String getAsString() {
        return asString;
    }

    public void setAsString(String asString) {
        this.asString = asString;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getHash() {
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaintedObject that = (TaintedObject) o;
        return hash == that.hash &&
                Objects.equals(taintEvents, that.taintEvents) &&
                Objects.equals(asString, that.asString) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taintEvents, asString, type, hash);
    }

    public void setHash(int hash) {
        this.hash = hash;
    }


}
