package nz.ac.wgtn.cornetto.feedback;

import java.util.List;
import java.util.Objects;

/**
 * Describes taint flows.
 * @autihor shawn, jens  (refactored to be seriaqlizablle with databinding to facilitate post analysis).
 */
public class TaintFlowSpec {

    private List<String> stackTrace = null;
    private TaintFlow taintFlow = null;

    public List<String> getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(List<String> stackTrace) {
        this.stackTrace = stackTrace;
    }

    public TaintFlow getTaintFlow() {
        return taintFlow;
    }

    public void setTaintFlow(TaintFlow taintFlow) {
        this.taintFlow = taintFlow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaintFlowSpec that = (TaintFlowSpec) o;
        return Objects.equals(stackTrace, that.stackTrace) &&
                Objects.equals(taintFlow, that.taintFlow);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stackTrace, taintFlow);
    }
}
