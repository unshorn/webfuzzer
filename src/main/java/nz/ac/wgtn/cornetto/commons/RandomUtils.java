package nz.ac.wgtn.cornetto.commons;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.ParetoDistribution;
import org.apache.commons.math3.distribution.RealDistribution;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class RandomUtils {

    public static final Random RANDOM = new Random();


    public static String randomURLPart() {
        // TODO: improve: length, additional chars
        int length = RANDOM.nextInt(50);
        return RandomStringUtils.randomAlphanumeric(length);
    }

    /**
     * Get a random positive number from an exponential distribution, the number will be capped at max.
     * Generates values between 0 (inclusive) and max (exclusive).
     * @param mean
     * @param max
     * @return
     */
    public static int getRandomPositiveNumberFromExponentialDistribution(double mean,int max) {
        RealDistribution distribution = new ExponentialDistribution(mean);
        double dvalue = distribution.sample();
        int value = (int)Math.round(dvalue);
        if (value>max) {
            // try again -- careful with recursion at low max values !
            return getRandomPositiveNumberFromExponentialDistribution(mean,max);
        }
        else {
            return value;
        }
    }

    /**
     * Get a random positive number from a pareto distribution, the number will be capped at max.
     * Generates values between 0 (inclusive) and max (exclusive).
     * @param paretoIndex -- a value between 0 and 1, closer to 1 means the curve is steeper
     * @param max
     * @return
     */
    public static int getRandomPositiveNumberFromParetoDistribution(double paretoIndex,int max) {
        RealDistribution distribution = new ParetoDistribution(1, paretoIndex);
        double dvalue = distribution.sample();
        int value = (int)Math.round(dvalue);
        if (value>max || value<1) {
            // try again -- careful with recursion at low max values !
            return getRandomPositiveNumberFromParetoDistribution(paretoIndex, max);
        }
        else {
            return value-1; // to include 0 !
        }
    }

    /**
     * Get a random positive number from a pareto distribution, the number will be capped at max.
     * Generates values between 0 (inclusive) and max (exclusive).
     * @param max
     * @return
     */
    public static int getRandomPositiveNumberFromParetoDistribution(int max) {
        return getRandomPositiveNumberFromParetoDistribution(0.95D,max);
    }

    /**
     * Get a random positive number from a pareto distribution, the number will be capped at max.
     * @return
     */
    public static int getRandomPositiveNumberFromParetoDistribution() {
        return getRandomPositiveNumberFromParetoDistribution(0.95D,Integer.MAX_VALUE);
    }


}
