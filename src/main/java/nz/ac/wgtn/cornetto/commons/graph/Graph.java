package nz.ac.wgtn.cornetto.commons.graph;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simple data structure to represent a digraph.
 * @author jens dietrich
 */
public class Graph<V extends Vertex<E>,E extends Edge<V>> implements Serializable {
	
	private Map<String,V> index = new ConcurrentHashMap<String, V>();
    private static Logger LOGGER = Logger.getLogger(Graph.class);

	public boolean add(V v) {
		V old = index.put(v.getName(), v);
        if (old!=null) {
            LOGGER.debug("Replacing vertex " + old + " with vertex with same name in index" + v);
        }
		return old==null;
	}

    public boolean add(E e) {
        return e.getStart().addOutEdge(e) && e.getEnd().addInEdge(e);
    }
	
	public V getVertexByName(String name) {
		return this.index.get(name);
	}

    public V getOrAdd(String name, Function<? super String,? extends V> factory) {
        return index.computeIfAbsent(name,factory);
    }

    public Stream<V> vertices() {
        return index.values().parallelStream();
    }

    public Stream<E> edges() {
        return index.values().parallelStream().flatMap(v -> v.getOutEdges().stream());
    }
	
	public Collection<V> getVertices() {
		return Collections.unmodifiableCollection(index.values());
	}

	public Collection<E> getEdges() {
	    return Collections.unmodifiableSet(
            index.values().parallelStream()
                .flatMap(v -> v.getOutEdges().stream())
                .collect(Collectors.toSet())
        );
	}

	public long getVertexCount() {
		return this.index.values().size();
	}

    public long getEdgeCount() {
        return index.values().parallelStream()
            .flatMap(v -> v.getOutEdges().stream())
            .count();
    }


    // traverse the graph, assume that the graph is acyclic !!
    public void dfs (Predicate<V> vertexFilter, Predicate<E> edgeFilter, Consumer<V> action, V v) {
        if (vertexFilter.test(v)) {
            action.accept(v);
            for (E e:v.getOutEdges()) {
                if (edgeFilter.test(e)) {
                    dfs(vertexFilter, edgeFilter, action, e.end);
                }
            }
        }
    }

    // traverse the graph in reverse direction, assume that the graph is acyclic !!
    public void reverseDfs (Predicate<V> vertexFilter, Predicate<E> edgeFilter, Consumer<V> action, V v) {
        if (vertexFilter.test(v)) {
            action.accept(v);
            for (E e:v.getInEdges()) {
                if (edgeFilter.test(e)) {
                    reverseDfs(vertexFilter, edgeFilter, action, e.start);
                }
            }
        }
    }

    // ancestor search using DFS, assume that the graph is acyclic !!
    public V findOneAncestorSuchThat(Predicate<V> condition, Predicate<E> edgeFilter, V vertex) {
        if (condition.test(vertex)) {
            return vertex;
        }
        for (E e:vertex.getInEdges()) {
            if (edgeFilter.test(e)) {
                V ancestor = findOneAncestorSuchThat(condition, edgeFilter,e.start);
                if (ancestor!=null) {
                    return ancestor;
                }
            }
        }
        return null;
    }

    // descentant search using DFS, assume that the graph is acyclic !!
    public V findOneDescendentSuchThat(Predicate<V> condition, Predicate<E> edgeFilter, V vertex) {
        if (condition.test(vertex)) {
            return vertex;
        }
        for (E e:vertex.getOutEdges()) {
            if (edgeFilter.test(e)) {
                V ancestor = findOneDescendentSuchThat(condition, edgeFilter,e.end);
                if (ancestor!=null) {
                    return ancestor;
                }
            }
        }
        return null;
    }


}
