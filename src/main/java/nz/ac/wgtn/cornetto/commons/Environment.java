package nz.ac.wgtn.cornetto.commons;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Reference to system jars needed for the static pre-analysis.
 * The environment used is referenced as a static instance that can be set.
 * @author jens dietrich
 */
public interface Environment {

    public List<File> getJREJars();
    public List<File> getJEEJars();

    public static Environment DEFAULT = new Environment() {

        private final File JRE_PATH = new File("lib/jre/1.8.0_191");
        private final File JEE_PATH = new File("lib/jee/8.0");

        private File getFile(File folder,String name) {
            File file = new File(folder,name);
            assert file.exists();
            assert !file.isDirectory();
            return file;
        }

        @Override
        public List<File> getJREJars() {
            List<File> uris = new ArrayList<>(3);
            uris.add(getFile(JRE_PATH,"rt.jar"));
            uris.add(getFile(JRE_PATH,"jsse.jar"));
            uris.add(getFile(JRE_PATH,"jce.jar"));
            return uris;
        }

        @Override
        public List<File> getJEEJars() {
            List<File> uris = new ArrayList<>(3);
            uris.add(getFile(JEE_PATH,"javaee-api.jar"));
            return uris;
        }
    };
}
