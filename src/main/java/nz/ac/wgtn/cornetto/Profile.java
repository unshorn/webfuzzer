package nz.ac.wgtn.cornetto;

import com.google.common.collect.Sets;
import nz.ac.wgtn.cornetto.http.MethodSpec;
import nz.ac.wgtn.cornetto.jee.Loggers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A profile has static information about the kind of web application that is being analysed,
 * and the server, port and application path.
 * This is usually being supplied by the user.
 * @author jens dietrich
 */
public abstract class Profile {

    public enum Protocol {http,https}

    protected String host = null;
    protected int port = 80;
    protected List<String> path = null;
    protected Protocol protocol = Protocol.http;
    protected String webappRootURL = null;
    protected String serverURL = null;
    protected String serverAndPortURL = null;

    public Profile(Protocol protocol,String host,int port,String... path) {
        this.protocol = protocol;
        this.host = host;
        this.port = port;
        this.path = Arrays.asList(path);

        this.serverURL = this.protocol.name() + "://" + this.host;
        this.serverAndPortURL = this.serverURL + ':' + this.port;
        this.webappRootURL = this.serverAndPortURL + '/' + this.path.stream().collect(Collectors.joining("/"));

        Loggers.FUZZER.info("application root URL set to " + this.webappRootURL);

    }

    public Profile(Protocol protocol,String host,int port) {
        this(protocol,host,port,new String[]{});
    }

    public String getWebappRootURL() {
        return webappRootURL;
    }

    public String getServerURL() {
        return serverURL;
    }

    public String getServerAndPortURL() {
        return serverAndPortURL;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    // localhost etc
    public String getHost() {
        return host;
    }

    // 8080 etc
    public int getPort() {
        return port;
    }

    // if not null, all paths generated will start with these tokens separated by /
    public List<String> getPathFirstTokens() {
        return path;
    }

    // http, https etc, default is {"http"}, override if needed
    public Set<String> getSchemes() {
        return Collections.unmodifiableSet(Sets.newHashSet("http"));
    }

    // get used http methods
    public abstract Set<MethodSpec> getUsedMethods() ;

    // standard web applications, uses GET plus POST to process forms
    public static class WebApplicationProfile extends Profile {
        public WebApplicationProfile(String host,int port,String... path) {
            super(Protocol.http,host,port,path);
        }
        public WebApplicationProfile(String host,int port) {
            super(Protocol.http,host,port);
        }
        @Override public Set<MethodSpec> getUsedMethods() {
            return Collections.unmodifiableSet(Sets.newHashSet(MethodSpec.GET,MethodSpec.POST));
        }
    }

    // standard rest application
    public static class RestApplicationProfile extends Profile {
        public RestApplicationProfile(String host,int port,String... path) {
            super(Protocol.http,host,port,path);
        }
        public RestApplicationProfile(String host,int port) {
            super(Protocol.http,host,port);
        }
        @Override public Set<MethodSpec> getUsedMethods() {
            return Collections.unmodifiableSet(Sets.newHashSet(MethodSpec.GET,MethodSpec.POST,MethodSpec.DELETE,MethodSpec.PATCH,MethodSpec.PUT));
        }
    }

}
