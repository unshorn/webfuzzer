package nz.ac.wgtn.cornetto;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.feedback.CoverageGraph;
import nz.ac.wgtn.cornetto.feedback.TaintFlowSpec;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.xss.XSSCandidate;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Default dynamic model.
 * @author jens dietrich
 */
public class DefaultDynamicModel implements DynamicModel {

    private static Logger LOGGER = LogSystem.getLogger("dyn-model");
    private static final int XSSCANDIDATE_MAX_REGISTRY_SIZE = 5000;

    private enum ResponseType {OK,CLIENT_ERROR,SERVER_ERROR};
    private CoverageGraph coverageGraph = new CoverageGraph();
    private Map<RequestSpec,Object> targets = new ConcurrentHashMap<>();
    private Set<String> requestParameterNames = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Set<Form> forms = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Set<URL> links = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Map<RequestSpec,String> requestsReachingUnsafeSinks = new ConcurrentHashMap<>(); // values are stacktraces
    private Set<TaintFlowSpec> taintFlows = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Set<XSSCandidate> xssCandidates = Collections.newSetFromMap(new ConcurrentHashMap<>());

    @Override
    public Set<ExecutionPoint> executionsRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<ExecutionPoint> executions) {
        ResponseType responseType = getResponseType(response);
        Set<ExecutionPoint> newExecutionPoints = coverageGraph.add(requestSpec, executions);
        if (newExecutionPoints.size()>0) {
            LOGGER.info("Register " + requestSpec + " triggering " + newExecutionPoints.size() + " new executions");
        }
        if (responseType==ResponseType.SERVER_ERROR) {
            targets.put(requestSpec,response.getStatusLine()); // TODO: add more info
            //LOGGER.info("Target reached by " + requestSpec + " -- status line is: " + response.getStatusLine());
        }
        return newExecutionPoints == null ? Collections.emptySet() : newExecutionPoints ;
    }

    @Override
    public Set<ExecutionPoint> getRecordedExecutionPoints() {
        return this.coverageGraph.getAllVertices();
    }



    @Override
    public Set<String> requestParametersRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<String> requestParameterNames) {
        Set<String> newRequestParameterNames = new HashSet<>();
        for (String requestParameterName:requestParameterNames) {
            if (this.requestParameterNames.add(requestParameterName)) {
                newRequestParameterNames.add(requestParameterName);
            }
        }
        return newRequestParameterNames;
    }

    @Override
    public Set<Form> formsRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<Form> forms) {
        Set<Form> newForms = new HashSet<>();
        for (Form form:forms) {
            if (this.forms.add(form)) {
                newForms.add(form);
            }
        }
        return newForms;
    }

    @Override
    public Set<URL> linksRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<URL> links) {
        Set<URL> newLinks = new HashSet<>();
        for (URL link:links) {
            if (this.links.add(link)) {
                newLinks.add(link);
            }
        }
        return newLinks;
    }

    @Override
    public boolean stacktraceToCriticalMethodRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, String stacktrace) {
        return null == this.requestsReachingUnsafeSinks.put(requestSpec,stacktrace);
    }

    @Override
    public Collection<RequestSpec> getTopRequestSpecs() {
        return coverageGraph.getTopVertices();
    }

    @Override
    /**
     * Get the top-performing request specs. This does include requests that are not "on the edge"
     * but also with a distance to the ends < threshold. This threshold will be computed using
     * an exponential distribution.
     */
    public Collection<RequestSpec> getNearTopRequestSpecs(SourceOfRandomness sourceOfRandomness, int maxThreshold) {
        int threshold = sourceOfRandomness.getRandomPositiveNumberFromExponentialDistribution(2,maxThreshold);
        return coverageGraph.getTopVertices(threshold);
    }

    @Override
    public Collection<Form> getForms() {
        return forms;
    }

    @Override
    public Collection<URL> getLinks() {
        return links;
    }

    @Override
    public Collection<RequestSpec> getRequestSpecs() {
        return coverageGraph.getRequestSpecs();
    }

    @Override
    public Collection<RequestSpec> getRequestSpecsReachingUnsafeMethods() {
        return this.requestsReachingUnsafeSinks.keySet();
    }

    @Override
    public Map<RequestSpec,Object> getTargets() {
        return this.targets;
    }

    @Override
    public Set<String> getRequestParameterNames() {
        return requestParameterNames;
    }

    @Override
    public boolean taintFlowRecorded(RequestSpec originalRequestSpec, HttpRequest request, HttpResponse response, TaintFlowSpec taintFlow) {
        return taintFlows.add(taintFlow);
    }

    private static ResponseType getResponseType(HttpResponse response) {
        int responseCode = response.getStatusLine().getStatusCode();
        Preconditions.checkState(responseCode>=0,"Invalid status code: " + responseCode);
        Preconditions.checkState(responseCode<600,"Invalid status code: " + responseCode);

        if (responseCode<400) {
            return ResponseType.OK;
        }
        else if (responseCode<500) {
            return ResponseType.CLIENT_ERROR;
        }
        else if (responseCode<600) {
            return ResponseType.SERVER_ERROR;
        }
        else {
            assert false; // should be unreachable
            return null;
        }
    }

    @Override
    public synchronized boolean registerXSSCandidate(XSSCandidate xssCandidate) {
        if (this.xssCandidates.size()<XSSCANDIDATE_MAX_REGISTRY_SIZE) {
            if (!this.xssCandidates.contains(xssCandidate)) { // avoid replacing other candidates with same request already being mutated
                return xssCandidates.add(xssCandidate);
            }
            else {
                return false;
            }
        }
        else {
            LOGGER.info("Rejected xss candidate -- max size of registry reached: " + this.xssCandidates.size());
        }
        return false;
    }

    @Override
    public synchronized boolean unregisterXSSCandidate(XSSCandidate xssCandidate) {
        return this.xssCandidates.remove(xssCandidate);
    }

    @Override
    public synchronized Collection<XSSCandidate> getXSSCandidates() {
        return this.xssCandidates; // careful, mutable -- only for selection (do not enforce for performance)!
    }

    @Override
    public CoverageGraph getCoverageGraph() {
        return this.coverageGraph;
    }
}
