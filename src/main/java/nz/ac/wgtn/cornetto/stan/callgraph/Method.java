package nz.ac.wgtn.cornetto.stan.callgraph;

import nz.ac.wgtn.cornetto.commons.graph.Vertex;

/**
 * Representation of a method.
 * @author jens dietrich
 */
public class Method extends Vertex<Invocation> {

    private String className = null;
    private String methodName = null;
    private String descriptor = null;

    public Method(String id,String className,String methodName,String descriptor) {
        super(id);
        this.className = className;
        this.methodName = methodName;
        this.descriptor = descriptor;
    }

    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public String getDescriptor() {
        return descriptor;
    }

    @Override
    public String toString() {
        return "MethodSpec{" +
                "className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", descriptor=" + descriptor +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Method method = (Method) o;

        if (className != null ? !className.equals(method.className) : method.className != null) return false;
        if (methodName != null ? !methodName.equals(method.methodName) : method.methodName != null) return false;
        return descriptor != null ? descriptor.equals(method.descriptor) : method.descriptor == null;
    }

    @Override
    public int hashCode() {
        int result = className != null ? className.hashCode() : 0;
        result = 31 * result + (methodName != null ? methodName.hashCode() : 0);
        result = 31 * result + (descriptor != null ? descriptor.hashCode() : 0);
        return result;
    }
}
