package nz.ac.wgtn.cornetto.stan.pointsto;

import nz.ac.wgtn.cornetto.stan.callgraph.Method;

/**
 * Object abstracted by a allocation  site.
 * @author jens dietrich
 */
public class Allocation extends HObject {
    private Method method = null;
    private String allocSite = null;
    private String type = null;

    public Allocation(Method method, String allocSite,String type) {
        this.method = method;
        this.allocSite = allocSite;
        this.type = type;
    }

    public Method getMethod() {
        return method;
    }

    public String getAllocSite() {
        return allocSite;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Allocation that = (Allocation) o;

        if (!method.equals(that.method)) return false;
        if (!allocSite.equals(that.allocSite)) return false;
        return type.equals(that.type);
    }

    @Override
    public int hashCode() {
        int result = method.hashCode();
        result = 31 * result + allocSite.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Allocation{" +
                "method=" + method +
                ", type='" + type + '\'' +
                ", allocSite='" + allocSite + '\'' +
                '}';
    }
}
