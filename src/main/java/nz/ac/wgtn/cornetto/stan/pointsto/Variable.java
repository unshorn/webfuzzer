package nz.ac.wgtn.cornetto.stan.pointsto;

import nz.ac.wgtn.cornetto.stan.callgraph.Method;

public class Variable {

    private Method method = null;
    private String name = null;

    public Variable(Method method, String name) {
        this.method = method;
        this.name = name;
    }

    public Method getMethod() {
        return method;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Variable variable = (Variable) o;

        if (!method.equals(variable.method)) return false;
        return name.equals(variable.name);
    }

    @Override
    public int hashCode() {
        int result = method.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Variable{" +
                "method=" + method +
                ", name='" + name + '\'' +
                '}';
    }
}
