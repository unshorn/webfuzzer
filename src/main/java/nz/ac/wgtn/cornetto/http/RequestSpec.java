package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.RTConstants;
import nz.ac.wgtn.cornetto.Spec;
import nz.ac.wgtn.cornetto.SpecSource;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Representation of a http request.
 * // TODO: headers
 * @author jens dietrich
 */
public class RequestSpec implements Spec,VisitableSpec {

    // describes the parts of a request, can be used in metadata / provenance
    public enum RequestDataKind {
        HEADER_KEY,HEADER_VALUE,PATH,PARAMETER_KEY,PARAMETER_VALUE
    }


    private URISpec uriSpec = null;
    private MethodSpec methodSpec = null;
    private ParametersSpec parametersSpec = new ParametersSpec(); // initialise with empty container
    private HeadersSpec headersSpec = new HeadersSpec(); // initialise with empty container
    private EnumSet<SpecSource> source = null;

    // general purpose annotation
    private Map<String,Object> properties = new HashMap<>();

    // parents from which this request spec has been generated
    private Set<RequestSpec> parents = new HashSet<>();


    // keys for general purpose annotation
    public static final String FORM_OWNER = null; // the owner of the form from which the request was generated

    public void addParent(RequestSpec parent) {
        this.parents.add(parent);
    }

    public Set<RequestSpec> getParents() {
        return parents;
    }

    public void setParents(Set<RequestSpec> parents) {
        this.parents = parents;
    }

    public Object getProperty (String key) {
        return properties.get(key);
    }

    public Set<Object> getPropertyFromThisOrAncestor (String key) {
        Set<Object> values = new HashSet<>();
        values.add(properties.get(key));
        for (RequestSpec parent:parents) {
            values.addAll(parent.getPropertyFromThisOrAncestor(key));
        }
        return values;
    }

    // return has Map::put semantics
    public Object addProperty (String key,Object value) {
        return properties.put(key,value);
    }

    @Override
    public EnumSet<SpecSource> getSources() {
        return source;
    }

    public void setProvenance(EnumSet<SpecSource> source) {
        this.source = source;
    }

    public HeadersSpec getHeadersSpec() {
        return headersSpec;
    }

    public void setHeadersSpec(HeadersSpec headersSpec) {
        this.headersSpec = headersSpec;
    }

    public URISpec getUriSpec() {
        return uriSpec;
    }

    public void setUriSpec(URISpec uriSpec) {
        this.uriSpec = uriSpec;
    }

    public MethodSpec getMethodSpec() {
        return methodSpec;
    }

    public void setMethodSpec(MethodSpec methodSpec) {
        this.methodSpec = methodSpec;
    }

    public ParametersSpec getParametersSpec() {
        return parametersSpec;
    }

    public void setParametersSpec(ParametersSpec parametersSpec) {
        this.parametersSpec = parametersSpec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestSpec that = (RequestSpec) o;
        if (uriSpec != null ? !uriSpec.equals(that.uriSpec) : that.uriSpec != null) return false;
        if (methodSpec != that.methodSpec) return false;
        if (parametersSpec != null ? !parametersSpec.equals(that.parametersSpec) : that.parametersSpec != null)
            return false;
        return headersSpec != null ? headersSpec.equals(that.headersSpec) : that.headersSpec == null;
    }

    @Override
    public int hashCode() {
        int result = uriSpec != null ? uriSpec.hashCode() : 0;
        result = 31 * result + (methodSpec != null ? methodSpec.hashCode() : 0);
        result = 31 * result + (parametersSpec != null ? parametersSpec.hashCode() : 0);
        result = 31 * result + (headersSpec != null ? headersSpec.hashCode() : 0);
        return result;
    }

    public HttpUriRequest toRequest() throws URISyntaxException, UnsupportedEncodingException {
        URI uri = this.uriSpec.toURI();
        HttpUriRequest request = null;
        if (methodSpec==MethodSpec.GET) {
            if (this.parametersSpec!=null) {
                URIBuilder builder = new URIBuilder(uri);
                for (ParameterSpec parameterSpec:parametersSpec.getParams()) {
                    builder.addParameter(parameterSpec.getKey().getValue(),parameterSpec.getValue().getValue());
                }
                uri = builder.build();
            }
            request = new HttpGet(uri);
        }
        else if (methodSpec==MethodSpec.POST) {
            // TODO -- hardcoded form submission !
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            for (ParameterSpec parameterSpec:parametersSpec.getParams()) {
                params.add(new BasicNameValuePair(parameterSpec.getKey().getValue(), parameterSpec.getValue().getValue()));
            }
            request = new HttpPost(uri);
            ((HttpPost)request).setEntity(new UrlEncodedFormEntity(params));
        }

        if (request==null) {
            throw new RuntimeException("Support for generating request for this http method not yet implemented: " + methodSpec.name());
        }
        else {
            for (HeaderSpec headerSpec:headersSpec.getHeaders()) {
                request.setHeader(headerSpec.getKey().getValue(),headerSpec.getValue().getValue());
            }

            // set special header to mark tainted values if there are any
            Set<String> taintedRequestParts = SpecProvenanceUtils.extractTaintedParts(this).keySet();
            if (!taintedRequestParts.isEmpty()) {
                String headerValue = taintedRequestParts.stream().collect(Collectors.joining(","));
                request.setHeader(RTConstants.TAINTED_INPUT_HEADER,headerValue);
            }

            return request;
        }
    }

    @Override
    public String toString() {
        String adr = null;
        try {
            URIBuilder builder = new URIBuilder(this.uriSpec.toURI().toString());
            if (methodSpec==MethodSpec.GET && this.parametersSpec!=null) {
                for (ParameterSpec parameterSpec : parametersSpec.getParams()) {
                    builder.addParameter(parameterSpec.getKey().getValue(), parameterSpec.getValue().getValue());
                }
            }
            adr = builder.build().toString();

        }
        catch (Exception x) {
            adr = uriSpec.toString();
        }

        return "RequestSpec{" + methodSpec.name() + " " + adr + ")}";
    }


    @Override
    public void accept(SpecVisitor visitor) {
        if (visitor.visit(this)) {
            uriSpec.accept(visitor);
            methodSpec.accept(visitor);
            parametersSpec.accept(visitor);
            headersSpec.accept(visitor);
        }
        visitor.endVisit(this);
    }

}

