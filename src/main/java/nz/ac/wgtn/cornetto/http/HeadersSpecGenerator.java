package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

/**
 * Produces lists of header specs.
 * @author jens dietrich
 */
public class HeadersSpecGenerator implements Generator<HeadersSpec> {

    private static HeaderSpecGenerator headerGenerator = new HeaderSpecGenerator();

    public HeadersSpecGenerator() {
        super();
    }

    @Override
    public HeadersSpec apply(SourceOfRandomness sor, Context context) {

        HeadersSpec headers = new HeadersSpec();
        int count = sor.getRandomPositiveNumberFromExponentialDistribution(2,7);
        for (int i=0;i<count;i++) {
            headers.add(headerGenerator.apply(sor,context));
        }
        headers.add(SpecSource.RANDOM); // for random count

        return headers;
    }
}
