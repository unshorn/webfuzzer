package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

/**
 * Produces lists of parameters specs.
 * @author jens dietrich
 */
public class ParametersSpecGenerator implements Generator<ParametersSpec> {

    private static ParameterSpecGenerator parameterGenerator = new ParameterSpecGenerator();

    public ParametersSpecGenerator() {
        super();
    }

    @Override
    public ParametersSpec apply(SourceOfRandomness sor, Context context) {

        ParametersSpec params = new ParametersSpec();
        int count = sor.getRandomPositiveNumberFromExponentialDistribution(2,7);
        for (int i=0;i<count;i++) {
            params.add(parameterGenerator.apply(sor,context));
        }
        params.add(SpecSource.RANDOM); // for random count
        return params;
    }
}
