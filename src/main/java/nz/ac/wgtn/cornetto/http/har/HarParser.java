package nz.ac.wgtn.cornetto.http.har;

import com.google.common.base.Preconditions;
import de.sstoehr.harreader.HarReader;
import de.sstoehr.harreader.HarReaderMode;
import de.sstoehr.harreader.model.*;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.http.*;
import org.apache.log4j.Logger;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Stream;

/**
 * Converter to create RequestSpecs from recorded http requests.
 * @author jens dietrich
 */
public class HarParser {

    private static Logger LOGGER = LogSystem.getLogger("har-parser");


    public static void main(String[] args) throws Exception {
        Preconditions.checkArgument(args.length>0);
        String fileName = args[0];
        File harFile = new File(fileName);
        Preconditions.checkArgument(harFile.exists());

        new HarParser().parse(harFile);
    }

    /**
     * Parses a har file, returns a list of request specs. The order might matter (for instance,
     * the first element recorded might be a login), so results are returned as list.
     * @param harFile
     * @return a list of requests preserving the order in which they were recorded
     * @throws Exception
     */
    public static List<RequestSpec> parse(File harFile) throws Exception {
        List<RequestSpec> requests = new ArrayList<>();
        Har har = new HarReader().readFromFile(harFile, HarReaderMode.LAX);
        HarLog log = har.getLog();

        for (HarEntry entry:log.getEntries()) {
            HarRequest harRequest = entry.getRequest();

            try {
                RequestSpec request = new RequestSpec();

                HttpMethod method = harRequest.getMethod();
                MethodSpec methodSpec = convertMethod(harRequest.getMethod());
                request.setMethodSpec(methodSpec);

                URISpec uriSpec = convertURI(harRequest.getUrl());
                request.setUriSpec(uriSpec);

                HeadersSpec headersSpec = convertHeaders(harRequest.getHeaders());
                request.setHeadersSpec(headersSpec);

                if (harRequest.getQueryString()!=null && harRequest.getQueryString().size()>0) {
                    assert method==HttpMethod.GET;

                    ParametersSpec paramsSpec = convertQueryParameters(harRequest.getQueryString());
                    request.setParametersSpec(paramsSpec);
                }

                if (harRequest.getPostData()!=null && harRequest.getPostData().getParams().size()>0) {
                    assert method==HttpMethod.POST || method==HttpMethod.PUT || method==HttpMethod.PATCH;
                    assert harRequest.getPostData().getMimeType().equals("application/x-www-form-urlencoded");

                    ParametersSpec paramsSpec = convertBodyParameters(harRequest.getPostData().getParams());
                    request.setParametersSpec(paramsSpec);
                }

                if ((method==HttpMethod.POST || method==HttpMethod.PUT || method==HttpMethod.PATCH) && !harRequest.getPostData().getMimeType().equals("application/x-www-form-urlencoded")) {
                    LOGGER.warn("Import and replay of POST/PUT/PATCH requests with non-form body is not yet supported");
                }

                request.setProvenance(EnumSet.of(SpecSource.PRERECORDED));
                requests.add(request);
            }
            catch (URISyntaxException x) {
                LOGGER.error("Error parsing imported url: " + harRequest.getUrl() );
                return null;
            }

        }

        for (RequestSpec req:requests) {
            System.out.println(req);
        }

        return requests;
    }

    private static MethodSpec convertMethod(HttpMethod harMethod) {
        Preconditions.checkArgument(harMethod!=null);
        switch (harMethod) {
            case GET:  return MethodSpec.GET;
            case POST:  return MethodSpec.POST;
            case PUT:  return MethodSpec.PUT;
            case DELETE:  return MethodSpec.DELETE;
            case PATCH:  return MethodSpec.PATCH;
            case HEAD:  return MethodSpec.HEAD;
            case CONNECT:  return MethodSpec.CONNECT;
            case TRACE:  return MethodSpec.TRACE;
            case OPTIONS:  return MethodSpec.OPTIONS;
            default: throw new IllegalArgumentException();
        }

    }

    private static URISpec convertURI(String url) throws URISyntaxException {
        Preconditions.checkArgument(url!=null);
        URI uri = new URI(url);
        URISpec uriSpec = new URISpec();
        uriSpec.setHost(uri.getHost());
        uriSpec.setPort(uri.getPort());
        uriSpec.setScheme(uri.getScheme());

        String path = uri.getPath();
        if (path!=null) {
            Stream.of(path.split("/"))
                    .filter(p -> p.trim().length()>0)
                    .forEach(p -> uriSpec.addToPath(new ValueSpec<>(p,EnumSet.of(SpecSource.PRERECORDED))));
        }

        return uriSpec;

    }

    private static HeadersSpec convertHeaders(List<HarHeader> headers) throws URISyntaxException {
        HeadersSpec headersSpec = new HeadersSpec();
        for (HarHeader harHeader:headers) {
            HeaderSpec headerSpec = new HeaderSpec(
                new ValueSpec(harHeader.getName(),EnumSet.of(SpecSource.PRERECORDED)),
                new ValueSpec<>(harHeader.getValue(),EnumSet.of(SpecSource.PRERECORDED)),
                EnumSet.of(SpecSource.PRERECORDED)
            );
            headersSpec.add(headerSpec);
        }
        return headersSpec;
    }

    private static ParametersSpec convertQueryParameters(List<HarQueryParam> params) throws URISyntaxException {
        ParametersSpec paramsSpec = new ParametersSpec();
        for (HarQueryParam harParam:params) {
            ParameterSpec paramSpec = new ParameterSpec(
                    new ValueSpec(harParam.getName(),EnumSet.of(SpecSource.PRERECORDED)),
                    new ValueSpec<>(harParam.getValue(),EnumSet.of(SpecSource.PRERECORDED)),
                    EnumSet.of(SpecSource.PRERECORDED)
            );
            paramsSpec.add(paramSpec);
        }
        return paramsSpec;
    }

    private static ParametersSpec convertBodyParameters(List<HarPostDataParam> params) throws URISyntaxException {
        ParametersSpec paramsSpec = new ParametersSpec();
        for (HarPostDataParam harParam:params) {
            ParameterSpec paramSpec = new ParameterSpec(
                    new ValueSpec(harParam.getName(),EnumSet.of(SpecSource.PRERECORDED)),
                    new ValueSpec<>(harParam.getValue(),EnumSet.of(SpecSource.PRERECORDED)),
                    EnumSet.of(SpecSource.PRERECORDED)
            );
            paramsSpec.add(paramSpec);
        }
        return paramsSpec;
    }
}
