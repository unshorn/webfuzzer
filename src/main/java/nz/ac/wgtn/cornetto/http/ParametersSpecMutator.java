package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Mutator;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

/**
 * Modifies an existing parameter Spec.
 * @author jens dietrich
 */
public class ParametersSpecMutator implements Mutator<ParametersSpec> {

    // all in %
    public static final int PROBABILITY_TO_MUTATE_EACH = 20;
    public static final int PROBABILITY_TO_ADD_ONE = 20;
    public static final int PROBABILITY_TO_REMOVE_EACH = 10;

    private ParameterSpecMutator paramMutator = new ParameterSpecMutator();
    private ParameterSpecGenerator paramGenerator = new ParameterSpecGenerator();

    @Override
    public ParametersSpec apply(SourceOfRandomness sourceOfRandomness, Context context, ParametersSpec parametersSpec) {

        ParametersSpec mutatedParamSpec = new ParametersSpec();

        for (ParameterSpec paramSpec:parametersSpec.getParams()) {
            if (sourceOfRandomness.trueAt(PROBABILITY_TO_REMOVE_EACH)) {
                // remove, i.e., dont add
            }
            else if (sourceOfRandomness.trueAt(PROBABILITY_TO_MUTATE_EACH)) {
                mutatedParamSpec.add(paramMutator.apply(sourceOfRandomness,context,paramSpec));
            }
        }
        if (sourceOfRandomness.trueAt(PROBABILITY_TO_ADD_ONE)) {
            mutatedParamSpec.add(paramGenerator.apply(sourceOfRandomness,context));
        }

        return mutatedParamSpec;
    }

}
