package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.net.URL;
import java.util.*;

/**
 * Generate a request from a link.
 * @author jens dietrich
 */
public abstract class AbstractRequestFromCapturedLinkGenerator implements Generator<RequestSpec> {

    // this is used to point to the origin of the link
    protected abstract EnumSet<SpecSource> getProvenance();

    protected abstract URL getLink(SourceOfRandomness sourceOfRandomness, Context context);
    protected ParameterValueGenerator defaultParameterValueGenerator = new ParameterValueGenerator();

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        URL url = getLink(sourceOfRandomness,context);

        URISpec uriSpec = new URISpec();
        uriSpec.setScheme(url.getProtocol());
        uriSpec.setHost(url.getHost());
        uriSpec.setPort(url.getPort()==-1?context.getProfile().getPort():url.getPort());
        String[] path = url.getPath().split("/");
        for (String part:path) {
            uriSpec.addToPath(new ValueSpec<>(part,getProvenance()));
        }
        EnumSet<SpecSource> provenance = getProvenance();
        uriSpec.setProvenance(provenance);

        RequestSpec requestSpec = new RequestSpec();
        requestSpec.setUriSpec(uriSpec);
        requestSpec.setMethodSpec(MethodSpec.GET);

        // TODO use some defaults here : useragent , accepts etc
        requestSpec.setHeadersSpec(new HeadersSpec());
        requestSpec.setProvenance(getProvenance());
        return requestSpec;
    }
}
