package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.util.Collection;
import java.util.EnumSet;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Produces request specs based on mutating hi-scoring previous requests.
 * @author jens dietrich
 */
public class RequestSpecMutator implements Generator<RequestSpec> {

    private ParametersSpecMutator parametersMutator = new ParametersSpecMutator();
    private URISpecMutator uriMutator = new URISpecMutator();
    // TODO: headers, methods

    public RequestSpecMutator() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sor, Context context) {

        Collection<RequestSpec> highScoringSpecs = context.getDynamicModel().getNearTopRequestSpecs(sor,3);
        Preconditions.checkState(highScoringSpecs.size()>0);
        Collection<RequestSpec> specs = context.getDynamicModel().getRequestSpecs();
        Preconditions.checkState(specs.size()>0);

        // pick one -- 50%  chance to pick high-scoring, 50% any
        RequestSpec original = sor.nextBoolean() ? sor.choose(highScoringSpecs) : sor.choose(specs);

        RequestSpec requestSpec = new RequestSpec();

        requestSpec.setParametersSpec(this.parametersMutator.apply(sor,context,original.getParametersSpec()));
        requestSpec.setUriSpec(this.uriMutator.apply(sor, context, original.getUriSpec()));

        // TODO: mutate those as well
        requestSpec.setHeadersSpec(original.getHeadersSpec());
        requestSpec.setMethodSpec(original.getMethodSpec());

        EnumSet<SpecSource> provenance = original.getSources().clone();
        provenance.add(SpecSource.MUTATED);
        requestSpec.setProvenance(provenance);

        // copy selected meta data
        requestSpec.addParent(original);

        return requestSpec;
    }
}
