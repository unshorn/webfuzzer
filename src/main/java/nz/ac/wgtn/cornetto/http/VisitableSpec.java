package nz.ac.wgtn.cornetto.http;

/**
 * Visitable spec.
 * @author jens dietrich
 */
public interface VisitableSpec {

    void accept(SpecVisitor visitor);
}
