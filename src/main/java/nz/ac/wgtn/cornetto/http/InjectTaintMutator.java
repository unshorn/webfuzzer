package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

/**
 * Produces request by injecting taint values into promising requests that
 * reach unsafe methods.
 * @author jens dietrich
 */
public class InjectTaintMutator implements Generator<RequestSpec> {

    public InjectTaintMutator() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sor, Context context) {

        Collection<RequestSpec> highScoringSpecs = context.getDynamicModel().getRequestSpecsReachingUnsafeMethods();
        Preconditions.checkState(highScoringSpecs.size()>0);

        RequestSpec originalRequestSpec = sor.choose(highScoringSpecs);

        boolean canMutateURL = originalRequestSpec.getUriSpec().getPath().size()> 0;
        boolean canMutateParameters = originalRequestSpec.getParametersSpec().getParams().size() > 0;
        boolean canMutateHeaders = originalRequestSpec.getHeadersSpec().getHeaders().size() > 0;

        int random = sor.nextInt(10);

        RequestSpec requestSpec = new RequestSpec();
        requestSpec.setMethodSpec(originalRequestSpec.getMethodSpec());

        URISpec originalUriSpec = originalRequestSpec.getUriSpec();
        URISpec uriSpec = new URISpec();
        uriSpec.setHost(originalUriSpec.getHost());
        uriSpec.setScheme(originalUriSpec.getScheme());
        uriSpec.setPort(originalUriSpec.getPort());
        requestSpec.setUriSpec(uriSpec);

        List<ValueSpec<String>> originalPathSpec = originalUriSpec.getPathSpec();
        List<ValueSpec<String>> pathSpec = null;

        ParametersSpec originalParamsSpec = originalRequestSpec.getParametersSpec();
        ParametersSpec paramsSpec = null;

        HeadersSpec originalHeadersSpec = originalRequestSpec.getHeadersSpec();
        HeadersSpec headersSpec = null;

        if (random < 3 && canMutateURL) {
            pathSpec = new ArrayList<>();
            assert !originalPathSpec.isEmpty();

            int taintIndex = sor.nextInt(originalPathSpec.size());
            for (int i=0;i<originalPathSpec.size();i++) {
                if (i==taintIndex) {
                    ValueSpec taint = TaintedStringValueGenerator.STRINGS.apply(sor,context);
                    pathSpec.add(taint);
                }
                else {
                    pathSpec.add(originalPathSpec.get(i));
                }
            }
        }
        else if (random<9 && canMutateParameters) {
            paramsSpec = new ParametersSpec();
            assert !originalParamsSpec.getParams().isEmpty();

            ParameterSpec toBeTainted = sor.choose(originalParamsSpec.getParams());
            for (ParameterSpec param:originalParamsSpec.getParams()) {
                if (param==toBeTainted) {
                    boolean taintKey = sor.nextBoolean();
                    ValueSpec taint = TaintedStringValueGenerator.DEFAULT.apply(sor,context);
                    ValueSpec key = taintKey ? taint : param.getKey();
                    ValueSpec value = taintKey ? param.getValue() : taint;
                    ParameterSpec taintedParam = new ParameterSpec(key,value,param.getSources());
                    paramsSpec.add(taintedParam);
                }
                else {
                    paramsSpec.add(param);
                }
            }
        }
        else if (canMutateHeaders) {
            headersSpec = new HeadersSpec();
            assert !originalHeadersSpec.getHeaders().isEmpty();

            HeaderSpec toBeTainted = sor.choose(originalHeadersSpec.getHeaders());
            for (HeaderSpec header:originalHeadersSpec.getHeaders()) {
                if (header==toBeTainted) {
                    boolean taintKey = sor.nextBoolean();
                    ValueSpec taint = TaintedStringValueGenerator.DEFAULT.apply(sor,context);
                    ValueSpec key = taintKey ? taint : header.getKey();
                    ValueSpec value = taintKey ? header.getValue() : taint;
                    HeaderSpec taintedHeader = new HeaderSpec(key,value,header.getSources());
                    headersSpec.add(taintedHeader);
                }
                else {
                    headersSpec.add(header);
                }
            }
        }
        else {
            // no mutation
            return originalRequestSpec;
        }

        if (pathSpec==null) {
            pathSpec = new ArrayList<>(originalPathSpec);
        }
        uriSpec.addAllToPath(pathSpec);

        if (paramsSpec==null) {
            paramsSpec = new ParametersSpec();
            for (ParameterSpec param:originalParamsSpec.getParams()) {
                paramsSpec.add(param);
            }
        }
        requestSpec.setParametersSpec(paramsSpec);

        if (headersSpec==null) {
            headersSpec = new HeadersSpec();
            for (HeaderSpec header:originalHeadersSpec.getHeaders()) {
                headersSpec.add(header);
            }
        }
        requestSpec.setHeadersSpec(headersSpec);

        EnumSet<SpecSource> provenance = originalRequestSpec.getSources().clone();
        provenance.add(SpecSource.MUTATED);
        requestSpec.setProvenance(provenance);

        // copy selected meta data
        requestSpec.addParent(originalRequestSpec);

        return requestSpec;
    }
}
