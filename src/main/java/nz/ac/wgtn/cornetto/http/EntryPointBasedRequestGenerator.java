package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.commons.ProbabilisticSupplierFactory;
import nz.ac.wgtn.cornetto.commons.RandomUtils;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Generates request specs from entry points.
 * @author jens dietrich
 */
public class EntryPointBasedRequestGenerator implements Generator<RequestSpec> {

    private static ParametersSpecGenerator parametersGenerator = new ParametersSpecGenerator();
    private static HeadersSpecGenerator headersGenerator = new HeadersSpecGenerator();

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        Set<EntryPoint> entryPoints = context.getStaticModel().getEntryPoints();
        Preconditions.checkState(!entryPoints.isEmpty());
        EntryPoint entryPoint = sourceOfRandomness.choose(entryPoints);


        RequestSpec requestSpec = new RequestSpec();

        // record bindings of variables like "*" and "{id}", incl provenance information
        // for instance, in case the string is a taint token
        List<ValueSpec<String>> variableBindings = new ArrayList<>();

        BiFunction<EntryPoint.VariablePathPart,Integer,String> bindingFunction = (var,index) -> {
            Set<String> literals = context.getStaticModel().getStringLiterals(EnumSet.of(StaticModel.Scope.APPLICATION, StaticModel.Scope.LIBRARIES));

            Supplier<ValueSpec<String>> supplier = ProbabilisticSupplierFactory.create()
                    .withProbability(40).createValue(() -> literals.isEmpty()
                            ?new ValueSpec<>(randomAlphanumeric(RandomUtils.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))
                            :new ValueSpec<>(sourceOfRandomness.choose(literals),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                    .withProbability(20).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sourceOfRandomness,context))
                    .withProbability(20).createValue(() -> new ValueSpec<>(randomAlphanumeric(RandomUtils.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
                    .withProbability(10).createValue(() -> new ValueSpec<>("" + sourceOfRandomness.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
                    .withProbability(5).createValue(() -> new ValueSpec<>("" + -sourceOfRandomness.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
                    .withProbability(5).createValue(() -> new ValueSpec<>("" +sourceOfRandomness.nextDouble(-100,1000),EnumSet.of(SpecSource.RANDOM)))
                    .build() ;


            ValueSpec<String> value = supplier.get();
            variableBindings.add(value);
            return value.getValue();
        };

        ValueSpec<String> pathSpec = new ValueSpec<>(entryPoint.bindVariables(bindingFunction), EnumSet.of(SpecSource.STATIC_ANALYSIS));

        URISpec uriSpec = new URISpec();
        uriSpec.setScheme(sourceOfRandomness.choose(context.getProfile().getSchemes()));
        uriSpec.setHost(context.getProfile().getHost());
        uriSpec.setPort(context.getProfile().getPort());
        for (String pathElement:context.getProfile().getPathFirstTokens()) {
            uriSpec.addToPath(new ValueSpec(pathElement,EnumSet.of(SpecSource.FIXED)));
        }
        uriSpec.addToPath(pathSpec);

        EnumSet<SpecSource> uriProvenance = EnumSet.of(SpecSource.STATIC_ANALYSIS);
        if (entryPoint.hasVariables()) {
            uriProvenance.addAll(pathSpec.getSources());
        }
        uriSpec.setProvenance(uriProvenance);

        requestSpec.setUriSpec(uriSpec);

        MethodSpec methodSpec = sourceOfRandomness.choose(context.getProfile().getUsedMethods());
        requestSpec.setMethodSpec(methodSpec);

        // headers and parameters are random
        if (methodSpec==MethodSpec.GET || methodSpec==MethodSpec.POST) {
            ParametersSpec parametersSpec = parametersGenerator.apply(sourceOfRandomness,context);
            requestSpec.setParametersSpec(parametersSpec);
        }

        HeadersSpec headersSpec = headersGenerator.apply(sourceOfRandomness,context);
        requestSpec.setHeadersSpec(headersSpec);

        requestSpec.setProvenance(EnumSet.of(SpecSource.STATIC_ANALYSIS,SpecSource.RANDOM));

        return requestSpec;

    }
}
