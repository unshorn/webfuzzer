package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Mutator;
import nz.ac.wgtn.cornetto.SpecSource;
import java.util.EnumSet;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Mutator for URIs.
 * @author shawn
 */
public class URISpecMutator implements Mutator<URISpec> {

    @Override
    public URISpec apply(SourceOfRandomness sor, Context context, URISpec originalUri) {

        URISpec uri = new URISpec();

        EnumSet<SpecSource> provenance = originalUri.getSources()==null?EnumSet.noneOf(SpecSource.class):originalUri.getSources().clone();
        provenance.add(SpecSource.MUTATED);
        uri.setProvenance(provenance);
        uri.setScheme(originalUri.getScheme());
        uri.setHost(originalUri.getHost());
        uri.setPort(originalUri.getPort());uri.addAllToPath(originalUri.getPathSpec());
        if (originalUri.getPath().size() < 5) {
            uri.addToPath(URISpecGenerator.randomURLString(sor,context));
        }
        return uri;
    }

}
