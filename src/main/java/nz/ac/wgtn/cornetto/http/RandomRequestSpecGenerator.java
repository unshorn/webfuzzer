package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.util.EnumSet;

/**
 * Produces request specs.
 * @author jens dietrich
 */
public class RandomRequestSpecGenerator implements Generator<RequestSpec> {

    private static ParametersSpecGenerator parametersGenerator = new ParametersSpecGenerator();
    private static HeadersSpecGenerator headersGenerator = new HeadersSpecGenerator();
    private static URISpecGenerator uriGenerator = new URISpecGenerator();
    private static MethodSpecGenerator methodGenerator = new MethodSpecGenerator();

    public RandomRequestSpecGenerator() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {

        EnumSet<SpecSource> provenance = EnumSet.of(SpecSource.RANDOM);

        MethodSpec methodSpec = methodGenerator.apply(sourceOfRandomness,context);
        provenance.addAll(methodSpec.getSources());
        URISpec uriSpec = uriGenerator.apply(sourceOfRandomness,context);
        provenance.addAll(uriSpec.getSources());

        RequestSpec requestSpec = new RequestSpec();

        requestSpec.setMethodSpec(methodSpec);
        requestSpec.setUriSpec(uriSpec);

        if (methodSpec==MethodSpec.GET || methodSpec==MethodSpec.POST) {
            ParametersSpec parametersSpec = parametersGenerator.apply(sourceOfRandomness,context);
            requestSpec.setParametersSpec(parametersSpec);
        }

        HeadersSpec headersSpec = headersGenerator.apply(sourceOfRandomness,context);
        requestSpec.setHeadersSpec(headersSpec);
        provenance.addAll(headersSpec.getSources());

        requestSpec.setProvenance(provenance);

        return requestSpec;
    }
}
