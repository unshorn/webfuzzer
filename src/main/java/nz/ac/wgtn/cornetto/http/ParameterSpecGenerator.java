package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.SpecSource;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.util.EnumSet;

/**
 * Produces key-value pair to be used as http parameters.
 * Note that they are not encoded, this is done when the actual requests are built.
 * @author jens dietrich
 */
public class ParameterSpecGenerator implements Generator<ParameterSpec> {

    private ParameterKeyGenerator keyGenerator = new ParameterKeyGenerator();
    private ParameterValueGenerator valueGenerator = new ParameterValueGenerator();

    @Override
    public ParameterSpec apply(SourceOfRandomness sor, Context context) {
        ValueSpec<String> key = this.keyGenerator.apply(sor,context);
        ValueSpec<String> value = this.valueGenerator.apply(sor,context);
        return new ParameterSpec(key, value, SpecProvenanceUtils.merge(key.getSources(),value.getSources(), EnumSet.of(SpecSource.RANDOM)));
    }
}
