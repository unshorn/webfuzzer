package nz.ac.wgtn.cornetto.http;

import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.Generator;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;

import java.net.URL;
import java.util.Collection;

/**
 * Generates requests from links captured in the static pre-analysis, or from feedback.
 * @author jens dietrich
 */
public class RequestFromCapturedLinkGenerator implements Generator<RequestSpec> {

    private RequestFromDynamicallyCapturedLinkGenerator generateFromDynamicallyCapturedLinks = new RequestFromDynamicallyCapturedLinkGenerator();
    private RequestFromStaticallyCapturedLinkGenerator generateFromStaticallyCapturedLinks = new RequestFromStaticallyCapturedLinkGenerator();

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        Collection<URL> sforms = context.getStaticModel().getLinks();
        Collection<URL> dforms = context.getDynamicModel().getLinks();
        boolean selection = sourceOfRandomness.trueBasedOnSizeRatio(sforms,dforms);
        RequestSpec requestSpec =  selection ? generateFromStaticallyCapturedLinks.apply(sourceOfRandomness,context) : generateFromDynamicallyCapturedLinks.apply(sourceOfRandomness,context);
        return requestSpec;
    }
}
