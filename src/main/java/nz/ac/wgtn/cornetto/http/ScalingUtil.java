package nz.ac.wgtn.cornetto.http;

import com.google.common.base.Preconditions;

/**
 * Utility function to scale an array of positive ints up so that the sum adds up.
 * @author jens dietrich
 */
public class ScalingUtil {
    public static int[] scaleTo100(int[] numbers) {
        int sum = 0;
        for (int i=0;i<numbers.length;i++) {
            Preconditions.checkArgument(i>=0);
            sum = sum + numbers[i];
        }

        Preconditions.checkArgument(sum<=100);
        Preconditions.checkArgument(sum>0);
        double factor = (double)100 / (double)sum;

        for (int i=0;i<numbers.length;i++) {
            numbers[i] = (int) (numbers[i] * factor);
        }


        int sum2 = 0;
        for (int i=0;i<numbers.length;i++) {
            sum2 = sum2 + numbers[i];
        }
        int diff = sum2-100;

        // correct rounding -- just detect one from each
        while (diff>0) {
            for (int i=0;i<numbers.length;i++) {
                if (numbers[i]>0) {
                    numbers[i] = numbers[i]-1;
                    diff = diff-1;
                    if (diff==0) break;
                }
            }
        }

        while (diff<0) {
            for (int i=0;i<numbers.length;i++) {
                if (numbers[i]>0) {
                    numbers[i] = numbers[i]+1;
                    diff = diff+1;
                    if (diff==0) break;
                }
            }
        }

        return numbers;
    }
}
