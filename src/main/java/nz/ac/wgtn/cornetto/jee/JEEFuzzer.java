package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.DefaultDynamicModel;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.deployment.DeployWithJetty9;
import nz.ac.wgtn.cornetto.deployment.Deployment;
import nz.ac.wgtn.cornetto.listeners.FuzzerListener;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import java.io.File;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Executable to fuzz a war file.
 * @author jens dietrich
 */

public class JEEFuzzer extends AbstractFuzzer {

    // private AtomicInteger duplicateCounter = new AtomicInteger();
    private static int logProgressInterval = 100;

    private File war = null;
    private String contextName = "fuzzed";
    private Deployment deployment = null;

    public static void main(String[] args) throws Exception {

        Loggers.FUZZER.info(Stream.of(args).collect(Collectors.joining(" \t ")));

        org.apache.commons.cli.Options options = new org.apache.commons.cli.Options()
            .addOption("war", true, "The (instrumented) war file to be fuzzed (required)")
            // .addOption("doopOutput", true, "The the folder containing the doop preanalysis (required)")
            .addOption("logInterval", true, "The interval to report progress using logging (optional, default is " + logProgressInterval + ")")
            .addOption("trials", true, "The max number of generated requests (optional, default is " + DEFAULT_MAX_TRIALS + ")")
            .addOption("authenticator", true, "The logic used to authenticate, name of an instantiable class implementing the Authenticator interface")
            .addOption("contextName", true, "Context name, this is the name of the web application that will be deployed during the fuzzing campaign'")
            .addOption("deployment", true, "Deployment -- adapter for a particular server used for the fuzzing campaign, name of an instantiable class implementing nz.ac.vuw.httpfuzz.deployment.Deployment")
            .addOption("randomseed", true, "Control randomness for the fuzzing run")
            .addOption("httpKeepAlive", true, "Set http client to use persistent connections")
            .addOption("extraHeaders",true, "Additional headers to send to server with all requests")
            .addOption("requestexecutionthreads",true, "The number of threads to be used to execute requests")
            .addOption("responseevaluationthreads",true, "The number of threads to be used to evaluate responses")
            .addOption("requestgenerationthreads",true, "The number of threads to be used to generate requests")
            .addOption("requestqueuesize",true, "The size of the request queue to be used")
            .addOption("responsequeuesize",true, "The size of the response queue to be used")
            .addOption("requesttimeout",true,"The http request timeout in s")
            .addOption("applicationPackages",true,"Prefixes of application packages, comma-separated")
            .addOption("probInterval",true,"The interval to log probs (request/response details)")
            .addOption("campaignLength",true,"Campaign length")
            .addOption("outputRootFolder",true,"Output root folder")
            .addOption("max500Reported",true,"The maximum number of requests resulting in a 500+ error code for which details are reported")
            .addOption("maxInjectionsReported",true,"The max number of injection flaws that will be reported")
            .addOption("maxXSSReported",true,"The max number of XSS vulnerabilities that will be reported")
            ;

        CommandLine cmd = new DefaultParser().parse(options, args);

//        // debugging only
//        Options opt = Options.fromCLIArguments(cmd);
//
//        int requestExecutionTaskCount = opt.getIntOption("requestexecutionthreads",1,i -> i>0);
//        Loggers.FUZZER.info("Using " + requestExecutionTaskCount + " threads for request execution");
//        int responseEvaluationTaskCount = opt.getIntOption("responseevaluationthreads",1,i -> i>0);
//        Loggers.FUZZER.info("Using " + responseEvaluationTaskCount + " threads for response evaluation");
//        int requestGeneratorTaskCount = opt.getIntOption("requestgenerationthreads",1,i -> i>0);
//        Loggers.FUZZER.info("Using " + requestGeneratorTaskCount + " threads for request generation");
//        int requestQueueSize = opt.getIntOption("requestqueuesize",100,i -> i>0);
//        Loggers.FUZZER.info("Set up request queue with size " + requestQueueSize);
//        int responseQueueSize = opt.getIntOption("responsequeuesize",100,i -> i>0);
//        Loggers.FUZZER.info("Set up response queue with size " + responseQueueSize);
//
//
//        Loggers.FUZZER.info("arg string: " + Stream.of(args).collect(Collectors.joining(" ")));
//        System.exit(0);
//        // debugging code ends


        if (!cmd.hasOption("war")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java <options> " + JEEFuzzer.class.getName(), options);
            System.exit(0);
        }

        JEEFuzzer fuzzer = new JEEFuzzer();
        fuzzer.fuzz(Options.fromCLIArguments(cmd));
    }

    @Override
    protected void init(Options options) throws Exception {
        super.init(options);

        this.war = new File(options.getStringOption("war"));
        Preconditions.checkState(this.war.exists(),"War file " + this.war.getAbsolutePath() + " does not exist");

        if (options.hasOption("deployment")) {
            String deploymentClassName = options.getStringOption("deployment");
            try {
                Class clazz = Class.forName(deploymentClassName);
                this.deployment = (Deployment)clazz.newInstance();
                Loggers.FUZZER.info("Deploying fuzzing campaign using " + clazz.getName());
            }
            catch (Exception x) {
                Loggers.FUZZER.error("Cannot deploy",x);
                System.exit(1);
            }
        }
        else {
            Loggers.FUZZER.warn("No deployment set, using default: " + DeployWithJetty9.class.getName());
            this.deployment = new DeployWithJetty9();
        }

        if (options.hasOption("contextName")) {
            contextName = options.getStringOption("contextName");
        }

        String applicationPackageDefs = options.getStringOption("applicationPackages");
        Set<String> applicationPackagePrefixes = Options.toSet(applicationPackageDefs);
        assert !applicationPackagePrefixes.isEmpty();
        Loggers.FUZZER.info("Recording application methods in packages starting with any of: " + applicationPackagePrefixes.stream().collect(Collectors.joining(",")));
        Predicate<String> isApplicationClass = cl -> {
            for (String applicationPackagePrefix:applicationPackagePrefixes) {
                if (cl.startsWith(applicationPackagePrefix)) {
                    return true;
                }
            }
            return false;
        };

        Profile profile = new Profile.WebApplicationProfile(deployment.getHost(),deployment.getPort(),contextName);
        Context context = new Context(
                profile,
                new JEEStaticModel(war,isApplicationClass,profile),  // callgraph not used in example
                new DefaultDynamicModel()
        );
        context.setApplicationPackagePrefixes(applicationPackageDefs);

        context.install(); // will be referenced by generators as singleton

    }


    @Override
    protected void deploy(Options options) throws Exception {

        Loggers.FUZZER.info("Starting phase: deploy");
        Loggers.FUZZER.info("Deploying and starting web application under analysis");

        boolean deploymentStatus = deployment.deploy(
            war,
            new File("tools/aspectjweaver-1.9.4.jar"),
            contextName);

        if (deploymentStatus) {
            Loggers.FUZZER.info("Web application deployed successfully for fuzzing campaign: " + deployment.getURL());
            final Runnable shutdown = new Runnable() {
                @Override public void run() {
                    try {
                        tearDown();
                        undeploy(options);
                    } catch (Exception e) {
                        Loggers.FUZZER.error("Error undeploying web application",e);
                    }
                }
            };

            Runtime.getRuntime().addShutdownHook(new Thread(shutdown));
        }
        else {
            Loggers.FUZZER.error("Error deploying web application");
        }

        FuzzerListener listener = deployment.getMaintenanceListener();
        if (listener!=null) {
            this.addListener(listener);
        }
        startTime = System.currentTimeMillis();
        Loggers.FUZZER.info("Finishing phase: deploy");
    }

    @Override
    protected void undeploy(Options options) throws Exception {
        Loggers.FUZZER.info("Starting phase: undeploy");

        try {
            deployment.undeploy();
            Loggers.FUZZER.info("fuzzing campaign stopped");
        }
        catch (Throwable x) {
            Loggers.FUZZER.warn("Error stopping fuzzing campaign, server may still be running on " + deployment.getHost() + ":" + deployment.getPort() + "  and needs to be shutdown manually");
        }
        Loggers.FUZZER.info("Finishing phase: undeploy");
    }



}
