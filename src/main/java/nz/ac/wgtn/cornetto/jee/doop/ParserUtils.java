package nz.ac.wgtn.cornetto.jee.doop;

import nz.ac.wgtn.cornetto.stan.callgraph.Method;

/**
 * Utilities to be used by various parsers.
 * @author jens dietrich
 */
class ParserUtils {
    static Method parse(String t) {
        // <JEEDriver: void main(java.lang.String[])>

        // strip <  > if  present
        if (t.charAt(0)=='<') {
            assert t.charAt(t.length() - 1) == '>';
            t = t.substring(1, t.length() - 1);
        }

        // TODO: performance hotspot -- replace !
        String[] tokens = t.split(" ");

        assert tokens.length==3;
        String typeName = tokens[0];
        assert typeName.charAt(typeName.length()-1)==':';
        typeName = typeName.substring(0,typeName.length()-1);
        String returnTypeName = tokens[1];
        String rest = tokens[2];
        int pos = rest.indexOf('(');
        assert pos>0;
        assert rest.charAt(rest.length()-1)==')';
        String methodName = rest.substring(0,pos);
        String params = rest.substring(pos);
        String descriptor = params+returnTypeName;

        return new Method(t,typeName,methodName,descriptor);

    }
}
