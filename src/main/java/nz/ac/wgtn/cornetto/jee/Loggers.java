package nz.ac.wgtn.cornetto.jee;

import nz.ac.wgtn.cornetto.commons.LogSystem;
import org.apache.log4j.Logger;

/**
 * Central definition of some frequently used loggers.
 * @author jens dietrich
 */
public interface Loggers {

    Logger FUZZED_SERVER = LogSystem.getLogger("fuzzed-server");
    Logger FUZZER = LogSystem.getLogger("fuzzer");
    Logger AUTHENTICATOR = LogSystem.getLogger("authenticator");
    Logger REPLAY_RECORDED = LogSystem.getLogger("replay-recorded");

    Logger COVERAGE_METHODS = LogSystem.getLogger("coverage-methods");
    Logger COVERAGE_REQUEST_PARAMETERS = LogSystem.getLogger("coverage-request-parameters");
    Logger COVERAGE_FORMS = LogSystem.getLogger("coverage-forms");
    Logger COVERAGE_LINKS = LogSystem.getLogger("coverage-links");

    Logger RESULTS_SINK_METHODS = LogSystem.getLogger("results-sinkmethods");
    Logger RESULTS_INJECTION = LogSystem.getLogger("results-injection");
    Logger RESULTS_500 = LogSystem.getLogger("results-500");
    Logger RESULTS_XSS = LogSystem.getLogger("results-xss");

    Logger APPLICATION_METHODS = LogSystem.getLogger("application-methods");
}