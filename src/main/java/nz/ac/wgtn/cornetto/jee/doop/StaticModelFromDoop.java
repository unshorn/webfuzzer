package nz.ac.wgtn.cornetto.jee.doop;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.EntryPoint;
import nz.ac.wgtn.cornetto.StaticModel;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.MethodSpec;
import nz.ac.wgtn.cornetto.stan.callgraph.Callgraph;
import nz.ac.wgtn.cornetto.stan.pointsto.Pointsto;
import nz.ac.wgtn.cornetto.stan.pointsto.StringLiteral;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Static analysis model created from doop model.
 * IMPORTANT: PointstoBuilder uses filters for performance reasons, those filters must be kept consistent to ensure
 * that the information parsed here is sound.
 * @author jens dietrich
 */
public class StaticModelFromDoop implements StaticModel {

   private static Logger LOGGER = LogSystem.getLogger("static-pre-analysis-doop");

   /**
    * Constructor.
    * @param callgraph
    * @param pointto
    * @param isApplicationClass -- a filter defining which classes to analyse further, usually filtered by package name
    */
   public StaticModelFromDoop(Callgraph callgraph, Pointsto pointto, Predicate<String> isApplicationClass) {
      this.init(callgraph,pointto,isApplicationClass);
   }

   // high level models
   private Set<String> stringliterals = null;
   private Set<String> requestKeys = null;
   private Set<String> headerKeys = null;

   private void init(Callgraph callgraph, Pointsto pointsto,Predicate<String> isApplicationClass) {
      initStringliterals(pointsto,isApplicationClass);
      initRequestKeys(pointsto,isApplicationClass);
      initHeaderKeys(pointsto,isApplicationClass);

      // release foundation models
      LOGGER.info("Releasing pointsto and callgraph");
   }

   private void initStringliterals(Pointsto pointsto,Predicate<String> isApplicationClass) {
      Preconditions.checkNotNull(pointsto);
      LOGGER.info("Initialising string literals");

      this.stringliterals = pointsto.getVariables().parallelStream()
           .filter(var -> isApplicationClass.test(var.getMethod().getClassName()))
           .filter(var -> var.getName().startsWith("$stringconstant"))
           .flatMap(var -> pointsto.getObjects(var).stream())
           .filter(obj -> obj instanceof StringLiteral)
           .map(obj -> (StringLiteral)obj)
           .map(s -> s.getValue())
           .filter(s -> s!=null)
           .collect(Collectors.toSet());

      LOGGER.info("Initialised string literals, " + stringliterals.size() + " found");
   }

   // mock method: <giga.jee.mock.javax.servlet.http.MockHttpServletRequest: java.lang.String getParameter(java.lang.String)>/@parameter0
   private void initRequestKeys(Pointsto pointsto,Predicate<String> isApplicationClass) {
      Preconditions.checkNotNull(pointsto);
      Preconditions.checkNotNull(stringliterals);
      LOGGER.info("Initialising HttpServletRequest getParameter keys");
      this.requestKeys = pointsto.getVariables().parallelStream()
           .filter(var -> var.getMethod().getMethodName().equals("getParameter"))
           .filter(var -> var.getMethod().getDescriptor().equals("(java.lang.String)java.lang.String"))
           .filter(var -> var.getName().equals("@parameter0"))
           .filter(var -> var.getMethod().getClassName().equals("giga.jee.mock.javax.servlet.http.MockHttpServletRequest"))
           .flatMap(var -> pointsto.getObjects(var).stream())
           .filter(obj -> obj instanceof StringLiteral)
           .map(obj -> (StringLiteral)obj)
           .map(s -> s.getValue())
           .filter(v -> stringliterals.contains(v))
           .collect(Collectors.toSet());
      LOGGER.info("Initialised HttpServletRequest getParameter keys, " + requestKeys.size() + " found");
   }

   // mock method: <giga.jee.mock.javax.servlet.http.MockHttpServletRequest: java.lang.String getParameter(java.lang.String)>/@parameter0
   private void initHeaderKeys(Pointsto pointsto,Predicate<String> isApplicationClass) {
      Preconditions.checkNotNull(pointsto);
      Preconditions.checkNotNull(stringliterals);
      LOGGER.info("Initialising HttpServletRequest getHeader keys");
      this.headerKeys = pointsto.getVariables().parallelStream()
           .filter(var -> var.getMethod().getMethodName().equals("getHeader"))
           .filter(var -> var.getMethod().getDescriptor().equals("(java.lang.String)java.lang.String"))
           .filter(var -> var.getName().equals("@parameter0"))
           .filter(var -> var.getMethod().getClassName().equals("giga.jee.mock.javax.servlet.http.MockHttpServletRequest"))
           .flatMap(var -> pointsto.getObjects(var).stream())
           .filter(obj -> obj instanceof StringLiteral)
           .map(obj -> (StringLiteral)obj)
           .map(s -> s.getValue())
           .filter(v -> stringliterals.contains(v))
           .collect(Collectors.toSet());
      LOGGER.info("Initialised HttpServletRequest getHeader keys, " + requestKeys.size() + " found");
   }

   public Set<String> getStringLiterals(EnumSet<Scope> scopes) {
      if (!scopes.containsAll(EnumSet.allOf(Scope.class))) {
         LOGGER.warn("Doop string literal extractor extracts all literals -- it does not distinguish between application, libraries and system");
      }
      return stringliterals;
   }

   @Override
   public Set<String> getReflectiveNames(EnumSet<Scope> scopes) {
      throw new UnsupportedOperationException();
   }

   public Set<String> getRequestParameterNames() {
      return requestKeys;
   }

   public Set<String> getHeaderNames() {
      return headerKeys;
   }

   // not supported
   @Override
   public Set<EntryPoint> getEntryPoints() {
      throw new UnsupportedOperationException();
   }

   @Override
   public Set<String> getEntryPointsURLPatterns() {
      throw new UnsupportedOperationException();
   }

   @Override
   public Set<Integer> getIntLiterals(EnumSet<Scope> scopes) {
      throw new UnsupportedOperationException();
   }

   @Override
   public Set<MethodSpec> getMethods() {
      throw new UnsupportedOperationException();
   }

   @Override
   public Set<Form> getForms() {
      throw new UnsupportedOperationException();
   }

   @Override
   public Set<URL> getLinks() {
      throw new UnsupportedOperationException();
   }

   @Override
   public Set<String> getApplicationMethods() {
      throw new UnsupportedOperationException();
   }

   @Override
   public boolean isApplicationClass(String className) {
      throw new UnsupportedOperationException();
   }
}
