package nz.ac.wgtn.cornetto.jee;

/**
 * Filter to indentify stacktraces to critical methods that are *not critical*, i.e. analysis false positives.
 * Could be refactored to make configurable in the future.
 * @author jens dietrich
 */
public class CriticalMethodInvocationStacktraceFilter {
    public static boolean isCritical(String stackTrace) {
        // pattern found in jenkins
        return !(stackTrace.contains("java.io.ByteArrayOutputStream.write") && stackTrace.contains("org.kohsuke.stapler.TokenList.decode")) &&
                !(stackTrace.contains("java.io.OutputStream.write") && stackTrace.contains("org.kohsuke.stapler.Stapler.serveStaticResource"))

                ;
    }
}
