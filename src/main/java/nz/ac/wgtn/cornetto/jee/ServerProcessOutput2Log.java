package nz.ac.wgtn.cornetto.jee;

import org.apache.log4j.Level;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Utility to redirect process output to log appenders.
 * @author jens dietrich
 */
public class ServerProcessOutput2Log implements Runnable {

    public static void redirectOutAndError(Process process,Level outLevel, Level errorLevel) {

        ServerProcessOutput2Log outputGobbler = new ServerProcessOutput2Log(process.getInputStream(), outLevel);
        ServerProcessOutput2Log errorGobbler = new ServerProcessOutput2Log(process.getErrorStream(), errorLevel);

        new Thread(outputGobbler,"webfuzz.serverlog.out").start();
        new Thread(errorGobbler,"webfuzz.serverlog.err").start();
    }


    private InputStream inputStream;
    private Level level = null;

    public ServerProcessOutput2Log(InputStream inputStream, Level level) {
        this.inputStream = inputStream;
        this.level = level;
    }

    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).lines().forEach(
                line -> Loggers.FUZZED_SERVER.log(level,line)
        );
    }
}
