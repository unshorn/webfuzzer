package nz.ac.wgtn.cornetto.jee.war;

import org.objectweb.asm.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Utility to extract literals from class files.
 * @author jens dietrich
 */
public class LiteralExtractor {

    private static class StringCollector extends ClassVisitor {
        private Set<String> literals = null;

        public StringCollector(Set<String> literals) {
            super(ASMSettings.VERSION);
            this.literals = literals;
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            return new MethodVisitor(ASMSettings.VERSION) {
                @Override
                public void visitLdcInsn(Object value) {
                    if (value != null && value instanceof String) {
                        literals.add((String) value);
                    }
                }
            };
        }
    }

    private static class IntCollector extends ClassVisitor {
        private Set<Integer> literals = null;

        public IntCollector(Set<Integer> literals) {
            super(ASMSettings.VERSION);
            this.literals = literals;
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            return new MethodVisitor(ASMSettings.VERSION) {
                @Override
                public void visitLdcInsn(Object value) {
                    if (value != null && value instanceof Integer) {
                        literals.add((Integer) value);
                    }
                }

                @Override
                public void visitInsn(int opcode) {
                    switch (opcode) {
                        case Opcodes.ICONST_M1:
                            literals.add(-1);
                            break;
                        case Opcodes.ICONST_0:
                            literals.add(0);
                            break;
                        case Opcodes.ICONST_1:
                            literals.add(1);
                            break;
                        case Opcodes.ICONST_2:
                            literals.add(2);
                            break;
                        case Opcodes.ICONST_3:
                            literals.add(3);
                            break;
                        case Opcodes.ICONST_4:
                            literals.add(4);
                            break;
                        case Opcodes.ICONST_5:
                            literals.add(5);
                            break;
                    }
                }

                @Override
                public void visitIntInsn(int opcode, int operand) {
                    switch (opcode) {
                        case Opcodes.BIPUSH:
                            literals.add(operand);
                            break;
                        case Opcodes.SIPUSH:
                            literals.add(operand);
                            break;
                    }
                }
            };
        }

    }

    public Set<String> extractStringLiterals(InputStream classDef) throws IOException {
        Set<String> literals = new HashSet<>();
        ClassVisitor visitor = new StringCollector(literals);
        new ClassReader(classDef).accept(visitor, 0);
        return literals;
    }

    public Set<Integer> extractIntLiterals(InputStream classDef) throws IOException {
        Set<Integer> literals = new HashSet<>();
        ClassVisitor visitor = new IntCollector(literals);
        new ClassReader(classDef).accept(visitor, 0);
        return literals;
    }
}