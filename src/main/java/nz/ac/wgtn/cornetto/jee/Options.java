package nz.ac.wgtn.cornetto.jee;

import org.apache.commons.cli.CommandLine;
import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Abstract representation for options, passed vua runtime parameters, files or similar.
 * @author jens dietrich
 */
public abstract class Options {

    public static Options fromCLIArguments(@Nonnull final CommandLine cmd) {

        return new Options() {

            @Override
            public Iterable<String> getOptionNames() {
                return Stream.of(cmd.getOptions())
                    .map(option -> option.getOpt())
                    .sorted()
                    .collect(Collectors.toList());
            }

            @Override
            public boolean hasOption(String key) {
                return cmd.hasOption(key);
            }

            @Nonnull
            @Override
            public String getStringOption(@Nonnull String key) {
                if (!cmd.hasOption(key)) {
                    throw new IllegalArgumentException("No such option: " + key);
                }
                return cmd.getOptionValue(key);
            }
        };
    }

    public abstract Iterable<String> getOptionNames();

    public abstract boolean hasOption(String key);

    /**
     * Get a string option.
     * @param key
     * @return the option value
     * @throws IllegalArgumentException if no option with this name exists, method should be guarded with hasOption
     */
    @Nonnull
    public abstract String getStringOption(@Nonnull String key);

    /**
     * Get a string option. Returns the default value if option does not exist.
     * @param key
     * @param defaultValue
     * @return the option value
     */
    public String getStringOption(@Nonnull String key,@Nonnull String defaultValue) {
        if (hasOption(key)) {
            return getStringOption(key);
        }
        else {
            return defaultValue;
        }
    }

    /**
     * Get a string option. Throws an exception if the value fails the validation test.
     * @param key
     * @param validationTest
     * @return the option value
     * @throws IllegalArgumentException if no option with this name exists, method should be guarded with hasOption
     * @throws IllegalStateException if an option exists but fails the test
     */
    @Nonnull
    public String getStringOption(@Nonnull String key,@Nonnull Predicate<String> validationTest) {
        String value = getStringOption(key);
        if (!validationTest.test(value)) {
            throw new IllegalStateException("Illegal option: \"" + value + "\" is not a permissible value for key \"" + key + "\"");
        }
        return value;
    }

    /**
     * Get a string option. Returns the default value if option does not exist. Throws an exception if the value fails the validation test.
     * @param key
     * @param defaultValue
     * @param validationTest
     * @return the option value
     * @throws IllegalArgumentException if no option with this name exists, method should be guarded with hasOption
     * @throws IllegalStateException if an option exists but fails the test
     */
    @Nonnull
    public String getStringOption(@Nonnull String key,@Nonnull String defaultValue, @Nonnull Predicate<String> validationTest) {
        String value = null;
        try {
            value = getStringOption(key);
        }
        catch (IllegalArgumentException x) {
            value = defaultValue;
        }
        if (!validationTest.test(value)) {
            throw new IllegalStateException("Illegal option: \"" + value + "\" is not a permissible value for key \"" + key + "\"");
        }
        return value;
    }


    /**
     * Get an int option.
     * @param key
     * @return the option value
     * @throws NumberFormatException if the value cannot be parsed to an int
     * @throws IllegalArgumentException if no option with this name exists, method should be guarded with hasOption
     */
    public int getIntOption(@Nonnull String key) {

        return Integer.parseInt(getStringOption(key));
    }

    /**
     * Get an int option. Returns the default value if option does not exist.
     * @param key
     * @param defaultValue
     * @return the option value
     * @throws NumberFormatException if the value cannot be parsed to an int
     */
    public int getIntOption(@Nonnull String key,int defaultValue) {
        if (hasOption(key)) {
            return getIntOption(key);
        }
        else {
            return defaultValue;
        }
    }

    /**
     * Get an int option. Throws an exception if the value fails the validation test.
     * @param key
     * @param validationTest
     * @return the option value
     * @throws IllegalArgumentException if no option with this name exists, method should be guarded with hasOption
     * @throws IllegalStateException if an option exists but fails the test
     * @throws NumberFormatException if the value cannot be parsed to an int
     */
    public int getIntOption(@Nonnull String key,@Nonnull Predicate<Integer> validationTest) {
        int value = getIntOption(key);
        if (!validationTest.test(value)) {
            throw new IllegalStateException("Illegal option: \"" + value + "\" is not a permissible value for key \"" + key + "\"");
        }
        return value;
    }

    /**
     * Get an int option. Returns the default value if option does not exist. Throws an exception if the value fails the validation test, or cannot be parsed.
     * @param key
     * @param defaultValue
     * @param validationTest
     * @return the option value
     * @throws IllegalArgumentException if no option with this name exists, method should be guarded with hasOption
     * @throws IllegalStateException if an option exists but fails the test
     * @throws NumberFormatException if the value cannot be parsed to an int
     */
    @Nonnull
    public int getIntOption(@Nonnull String key,int defaultValue, @Nonnull Predicate<Integer> validationTest) {
        int value = getIntOption(key,defaultValue);
        if (!validationTest.test(value)) {
            throw new IllegalStateException("Illegal option: \"" + value + "\" is not a permissible value for key \"" + key + "\"");
        }
        return value;
    }


    /**
     * Get a boolean option.
     * @param key
     * @return the option value
     * @throws IllegalArgumentException if no option with this name exists, or the value is neither true nor false. This method should be guarded with hasOption
     */
    public boolean getBooleanOption(@Nonnull String key) {
        String value = getStringOption(key).trim();
        if ("true".equals(value)) {
            return true;
        }
        else if ("false".equals(value)) {
            return false;
        }
        else {
            throw new IllegalArgumentException("The option value for key \"" + key + "\" is not a valid boolean: " + value);
        }
    }

    /**
     * Get an int option. Returns the default value if option does not exist.
     * @param key
     * @param defaultValue
     * @return the option value
     * @throws IllegalArgumentException if the value is neither true nor false. This method should be guarded with hasOption
     */
    public boolean getBooleanOption(@Nonnull String key,boolean defaultValue) {
        if (hasOption(key)) {
            return getBooleanOption(key);
        }
        else {
            return defaultValue;
        }
    }

    public static final String MANY_SEPARATOR = ",";

    // utilities for lists and sets
    public static List<String> toList(String optionString) {
        return Stream.of(optionString.split(MANY_SEPARATOR)).collect(Collectors.toList());
    }

    public static Set<String> toSet(String optionString) {
        return Stream.of(optionString.split(MANY_SEPARATOR)).collect(Collectors.toSet());
    }


}

