package nz.ac.wgtn.cornetto.jee.war;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import java.io.File;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Utility to extract application methods (as strings, using the notation <classname>::<methodname><descriptor>, in the descriptor, byte code syntax is used
 * (as defined in the JVM Spec, e.g "(ILjava/util/List;)Z".
 *
 * Note that application classes are defined by package prefixes, not by the structure of the war. This is to support
 * complex applications that may package application classes into multiple jars.
 * @author jens dietrich
 */
public class ApplicationMethodExtractor extends AbstractWarClassAnalyser<String> {

    private Predicate<String> isApplicationClass = cl -> true;

    public ApplicationMethodExtractor(WarScopeModel scopeModel,Predicate<String> isApplicationClass) {
        super(scopeModel);
        this.isApplicationClass = isApplicationClass;
    }

    public static Set<String> extractApplicationMethods(File war, Predicate<String> isApplicationClass) throws Exception {
        WarScopeModel scopeModel = new WarScopeModel() {
            @Override
            public boolean isApplicationClass(String outerName, String innerName) {
                return true;
            }

            @Override
            public boolean isLibClass(String outerName, String innerName) {
                return false;
            }
        };
        Set<String> applicationMethods = new ApplicationMethodExtractor(scopeModel,isApplicationClass).extract(war,true,true);
        LOGGER.warn("Extracted " + applicationMethods.size() + " application methods");

        return applicationMethods;
    }


    private class MethodCollector extends ClassVisitor {

        private Set<String> applicationMethodNames = new HashSet<>();
        private boolean applicationClass = true;
        private String className = null;

        public MethodCollector() {
            super(ASMSettings.VERSION);
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            super.visit(version, access, name, signature, superName, interfaces);
            this.className = name.replace('/','.');
            this.applicationClass = isApplicationClass.test(className);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if (applicationClass) {
                String method = className + "::" + name + descriptor;
                this.getApplicationMethodNames().add(method);
            }
            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }

        public Set<String> getApplicationMethodNames() {
            return applicationMethodNames;
        }
    }

    @Override
    public Set<String> extractFromClassFile(InputStream in) throws Exception {
        MethodCollector nameExtractor = new MethodCollector();
        new ClassReader(in).accept(nameExtractor, 0);
        return nameExtractor.getApplicationMethodNames();
    }
}
