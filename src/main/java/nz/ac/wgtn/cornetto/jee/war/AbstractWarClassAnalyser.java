package nz.ac.wgtn.cornetto.jee.war;

import nz.ac.wgtn.cornetto.commons.LogSystem;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Abstract analyser for class files in wars.
 * @author jens dietrich
 */
public abstract class AbstractWarClassAnalyser<T> {

    public static final Logger LOGGER = LogSystem.getLogger("analyse-war");
    private WarScopeModel scopeModel = null;

    public AbstractWarClassAnalyser(WarScopeModel scopeModel) {
        this.scopeModel = scopeModel;
    }

    public Set<T> extract (File war, boolean  extractFromAppClasses, boolean extractFromLibClasses) throws Exception {
        Set<T> literals = new HashSet<>();
        ZipFile zip = new ZipFile(war);
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class")) {
                if (extractFromAppClasses && scopeModel.isApplicationClass(null,name)) {
                    InputStream in = zip.getInputStream(e);
                    literals.addAll(extractFromClassFile(in));
                    in.close();
                }
                if (extractFromLibClasses && scopeModel.isLibClass(null,name)) {
                    InputStream in = zip.getInputStream(e);
                    literals.addAll(extractFromClassFile(in));
                    in.close();
                }
            }
            if ((name.startsWith("WEB-INF/lib/") || name.startsWith("BOOT-INF/lib/")) && name.endsWith(".jar")) {
                InputStream in = zip.getInputStream(e);
                ZipInputStream input = new ZipInputStream(in);
                ZipEntry e2 = null;
                while ((e2 = input.getNextEntry()) != null ) {
                    if (e2.getName().endsWith(".class")) {
                        if (extractFromAppClasses && scopeModel.isApplicationClass(name, e2.getName())) {
                            //InputStream in2 = zip.getInputStream(e2);
                            literals.addAll(extractFromClassFile(input));
                            //in2.close();
                        }
                        if (extractFromLibClasses && scopeModel.isLibClass(name, e2.getName())) {
                            //InputStream in2 = zip.getInputStream(e2);
                            literals.addAll(extractFromClassFile(input));
                            //in2.close();
                        }
                    }
                }
                input.close();
            }
        }
        return literals;
    }

    public abstract Set<T> extractFromClassFile(InputStream in) throws Exception;


}
