package nz.ac.wgtn.cornetto.jee.war;

import org.objectweb.asm.*;
import java.beans.Introspector;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Utility to extract string literals used as names for types, methods and fields, and
 * therefore potentially used by reflection, from wars.
 * NOTE: (names of) annotations used and theor members are not extracted
 * @author jens dietrich
 */
public class ReflectiveNamesExtractor extends AbstractWarClassAnalyser<String> {

    public ReflectiveNamesExtractor(WarScopeModel scopeModel) {
        super(scopeModel);
    }

    private static class NameCollector extends ClassVisitor {
        private Set<String> names = new HashSet<>();
        private Set<String> properties = new HashSet<>();

        public NameCollector() {
            super(ASMSettings.VERSION);
        }

        @Override
        public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
            this.names.add(name);
            return super.visitField(access, name, descriptor, signature, value);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            this.names.add(name);
            addProperty(name);
            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }

        private boolean addProperty(String name) {

            final String ADD_PREFIX = "add";
            final String REMOVE_PREFIX = "remove";
            final String GET_PREFIX = "get";
            final String SET_PREFIX = "set";
            final String IS_PREFIX = "is";

            if (name.length() <= 3 && !name.startsWith(IS_PREFIX)) {
                return false;
            }

            int index = -1;
            if (name.startsWith(GET_PREFIX) || name.startsWith(SET_PREFIX) || name.startsWith(ADD_PREFIX)) {
                index = 3;
            }
            if (name.startsWith(IS_PREFIX)) {
                index = 2;
            }
            if (name.startsWith(REMOVE_PREFIX)) {
                index = 6;
            }

            if (-1 != index) {
                properties.add(Introspector.decapitalize(name.substring(index)));
                return true;
            }

            return false;
        }

        private Set<String> getNames() {
            HashSet<String> namesAndProperties = new HashSet<>();
            namesAndProperties.addAll(names);
            namesAndProperties.addAll(properties);
            return namesAndProperties;
        }
    }

    @Override
    public Set<String> extractFromClassFile(InputStream in) throws Exception {
        NameCollector nameExtractor = new NameCollector();
        new ClassReader(in).accept(nameExtractor, 0);
        return nameExtractor.getNames();
    }
}
