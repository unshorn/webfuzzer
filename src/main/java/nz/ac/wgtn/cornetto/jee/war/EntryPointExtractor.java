package nz.ac.wgtn.cornetto.jee.war;

import com.google.common.collect.Sets;
import nz.ac.wgtn.cornetto.EntryPoint;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.http.MethodSpec;
import org.apache.log4j.Logger;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * API of a web app, extracted with a lightweight static analysis.
 * @author jens dietrich
 */
public class EntryPointExtractor {

    public static final Logger LOGGER = LogSystem.getLogger(EntryPointExtractor.class);

    public static Set<EntryPoint> extractEntryPoints(File war, Profile profile) throws Exception {

        Set<EntryPoint> entryPoints = new HashSet<>();
        extractEntryPointsFromWebXML(war,entryPoints,profile);
        extractEntryPointsFromJEEAnnotations(war,entryPoints,profile);
        extractEntryPointsFromSpringAnnotations(war,entryPoints,profile);

        LOGGER.warn("Extraction of JSP entry points is not yet supported");

        for (EntryPoint entryPoint:entryPoints) {
            LOGGER.info("Extracted entry point " + entryPoint);
        }

        return entryPoints;
    }

    private static void extractEntryPointsFromJEEAnnotations(File war, Set<EntryPoint> entryPoints, Profile profile) throws Exception {
        try {
            new WarVisitor().visit(war,true,
                entry -> entry.getName().endsWith(".class"),
                (name,in) -> {
                    try {
                        Set<EntryPoint> extracted = doExtractEntryPointsFromJEEAnnotations(in);
                        entryPoints.addAll(extracted);
                    }
                    catch (Exception x) {
                        LOGGER.warn("Error extracting entry points",x);
                    }
                }
            );
        }
        catch (Exception x) {
            LOGGER.error("Exception extracting supported http methods from war, fall back to profile", x);
        }
    }

    private static Set<EntryPoint> doExtractEntryPointsFromJEEAnnotations(InputStream in) throws Exception {
        Set<EntryPoint> entryPoints = new HashSet<>();
        ClassVisitor visitor = new ClassVisitor(ASMSettings.VERSION) {
            String className = null;
            Set<String> urlPatterns = new HashSet<>();
            Set<MethodSpec> methodSpecs = new HashSet<>();

            @Override
            public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                super.visit(version, access, name, signature, superName, interfaces);
                this.className = name;
            }

            @Override
            public void visitEnd() {
                for (String urlPattern:urlPatterns) {
                    // extract wildcards
                    List<EntryPoint.VariablePathPart> varParts = scanJEEURLWildcards(urlPattern);
                    EntryPoint entryPoint = new EntryPoint(urlPattern,this.methodSpecs,this.className,varParts);
                    entryPoints.add(entryPoint);
                }
            }

            @Override
            public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
                // JEE annotations
                if (Objects.equals(descriptor,"Ljavax/servlet/annotation/WebServlet;")) {
                    return new AnnotationVisitor(ASMSettings.VERSION) {
                        @Override
                        public AnnotationVisitor visitArray(String name) {
                            if (Objects.equals(name,"urlPatterns")) {
                                return new AnnotationVisitor(ASMSettings.VERSION) {
                                    @Override
                                    public void visit(String name, Object value) {
                                        super.visit(name, value); // catch value -- this is one URL pattern
                                        String urlPattern = (""+value).trim();
                                        urlPattern = sanitiseUrlPattern(urlPattern);
                                        urlPatterns.add(urlPattern);
                                    }
                                };
                            }
                            return null;
                        }
                    };
                }
                return null;
            }

            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                if (urlPatterns.isEmpty()) {
                    return null;
                }
                MethodSpec httpMethod = getSupportedHttpMethod(access,name,descriptor,signature,exceptions);
                if (httpMethod!=null) {
                    methodSpecs.add(httpMethod);
                }
                return null;
            }
        };
        new ClassReader(in).accept(visitor, 0);
        return entryPoints;
    }


    private static List<EntryPoint.VariablePathPart> scanJEEURLWildcards(String urlPattern) {
        List<EntryPoint.VariablePathPart> varParts = new ArrayList<>();
        int index = -1;
        while ((index=urlPattern.indexOf("*",index+1))>-1) {
            EntryPoint.VariablePathPart varPart = new EntryPoint.VariablePathPart(urlPattern,"*",index,String.class);
            varParts.add(varPart);
        }
        return varParts;
    }

    private static void extractEntryPointsFromSpringAnnotations(File war, Set<EntryPoint> entryPoints, Profile profile) throws Exception {
        try {
            new WarVisitor().visit(war,true,
                entry -> entry.getName().endsWith(".class"),
                (name,in) -> {
                    try {
                        Set<EntryPoint> extracted = doExtractEntryPointsFromSpringAnnotations(in);
                        entryPoints.addAll(extracted);
                    }
                    catch (Exception x) {
                        LOGGER.warn("Error extracting entry points",x);
                    }
                }
            );
        }
        catch (Exception x) {
            LOGGER.error("Exception extracting supported http methods from war, fall back to profile", x);
        }
    }

    private static Set<EntryPoint> doExtractEntryPointsFromSpringAnnotations(InputStream in) throws Exception {
        Set<EntryPoint> entryPoints = new HashSet<>();
        ClassVisitor visitor = new ClassVisitor(ASMSettings.VERSION) {
            String className = null;

            @Override
            public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                super.visit(version, access, name, signature, superName, interfaces);
                this.className = name.replace('/','.');
            }

            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                return new MethodVisitor(ASMSettings.VERSION) {
                    Set<MethodSpec> methodSpecs = null;
                    @Override
                    public AnnotationVisitor visitAnnotation(String descriptor, boolean visible) {
                        if (Objects.equals("Lorg/springframework/web/bind/annotation/GetMapping;",descriptor)) {
                            methodSpecs = Sets.newHashSet(MethodSpec.GET);
                        }
                        else if (Objects.equals("Lorg/springframework/web/bind/annotation/PostMapping;",descriptor)) {
                            methodSpecs = Sets.newHashSet(MethodSpec.POST);
                        }
                        else if (Objects.equals("Lorg/springframework/web/bind/annotation/PutMapping;",descriptor)) {
                            methodSpecs = Sets.newHashSet(MethodSpec.PUT);
                        }
                        else if (Objects.equals("Lorg/springframework/web/bind/annotation/DeleteMapping;",descriptor)) {
                            methodSpecs = Sets.newHashSet(MethodSpec.DELETE);
                        }
                        else if (Objects.equals("Lorg/springframework/web/bind/annotation/PatchMapping;",descriptor)) {
                            methodSpecs = Sets.newHashSet(MethodSpec.PATCH);
                        }
                        else if (Objects.equals("Lorg/springframework/web/bind/annotation/RequestMapping;",descriptor)) {
                            methodSpecs = EnumSet.allOf(MethodSpec.class); // TODO: investigate method field to refine
                        }


                        if (methodSpecs==null) {
                            return null;
                        }
                        else {
                            return new AnnotationVisitor(ASMSettings.VERSION) {

                                private String urlPattern = null;
                                private List<EntryPoint.VariablePathPart> varPathParts = new ArrayList<>();
                                private Set<MethodSpec> methodSpecs2 = new HashSet<>();

                                @Override
                                public void visitEnd() {
                                    super.visitEnd();
                                    if (urlPattern!=null) {
                                        EntryPoint entryPoint = new EntryPoint(urlPattern, methodSpecs2.isEmpty() ? methodSpecs : methodSpecs2, className, varPathParts);
                                        entryPoints.add(entryPoint);
                                    }
                                }

                                @Override
                                public AnnotationVisitor visitArray(String name) {
                                    if (Objects.equals(name, "path") || Objects.equals(name, "value")) {  // path aliases value: https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/RequestMapping.html#value--
                                        return new AnnotationVisitor(ASMSettings.VERSION) {
                                            @Override
                                            public void visit(String name, Object value) {
                                                urlPattern = sanitiseUrlPattern("" + value);
                                                varPathParts = new ArrayList<>();
                                                int pos = -1;
                                                boolean varMode = false;
                                                StringBuffer sb = new StringBuffer();

                                                for (int i = 0; i < urlPattern.length(); i++) {
                                                    char c = urlPattern.charAt(i);
                                                    if (!varMode && c == '{') {
                                                        varMode = true;
                                                        sb.append(c);
                                                        pos = i;
                                                    } else if (varMode) {
                                                        if (c == '}') {
                                                            // add new varPathPart
                                                            // TODO infer type from method param
                                                            sb.append(c);
                                                            EntryPoint.VariablePathPart next = new EntryPoint.VariablePathPart(urlPattern, sb.toString(), pos, String.class);
                                                            varPathParts.add(next);
                                                            varMode = false;
                                                            sb.setLength(0);
                                                        } else {
                                                            sb.append(c);
                                                        }
                                                    }
                                                }
                                            }
                                        };
                                    } else if (Objects.equals(name, "method")) {
                                        return new AnnotationVisitor(ASMSettings.VERSION) {
                                            @Override
                                            public void visit(String name, Object value) {
                                                System.out.println(value);
                                            }

                                            @Override
                                            public void visitEnum(String name, String descriptor, String value) {
                                                MethodSpec methodSpec = MethodSpec.valueOf(value);
                                                if (methodSpec!=null) {
                                                    methodSpecs2.add(methodSpec);
                                                }
                                            }

                                            @Override
                                            public AnnotationVisitor visitArray(String name) {
                                                return super.visitArray(name);
                                            }
                                        };
                                    } else {
                                        return null;
                                    }
                                }

                            };
                        }
                    }
                };
            }
        };
        new ClassReader(in).accept(visitor, 0);
        return entryPoints;
    }

    private static void extractEntryPointsFromWebXML(File war, Set<EntryPoint> entryPoints, Profile profile) throws Exception {

        Map<String,String> servletClassesByName = new HashMap<>();
        Map<String,String> urlMappingsByName = new HashMap<>();

        Set<Document> webxmls;
        webxmls = extractWebXML(war);
        for (Document webxml:webxmls) {
            if (webxml != null) {
                XPath xPath = XPathFactory.newInstance().newXPath();

                NodeList nodeList = (NodeList) xPath.compile("web-app/servlet").evaluate(webxml, XPathConstants.NODESET);
                for (int i = 0; i < nodeList.getLength(); i++) {
                    NodeList children = nodeList.item(i).getChildNodes();
                    String servletName = null;
                    String servletClassName = null;
                    for (int j = 0; j < children.getLength(); j++) {
                        Node next = children.item(j);
                        if (next.getNodeName().equals("servlet-name")) {
                            servletName = next.getTextContent().trim();
                        } else if (next.getNodeName().equals("servlet-class")) {
                            servletClassName = next.getTextContent().trim();
                        }
                    }
                    if (servletName != null && servletClassName != null) {
                        servletClassesByName.put(servletName, servletClassName);
                    } else {
                        LOGGER.warn("Incomplete <servlet> spec encountered in web.xml" + (servletName == null ? "" : (" servletName=" + servletName)));
                    }
                }

                nodeList = (NodeList) xPath.compile("web-app/servlet-mapping").evaluate(webxml, XPathConstants.NODESET);
                for (int i = 0; i < nodeList.getLength(); i++) {
                    NodeList children = nodeList.item(i).getChildNodes();
                    String servletName = null;
                    String urlPattern = null;
                    for (int j = 0; j < children.getLength(); j++) {
                        Node next = children.item(j);
                        if (next.getNodeName().equals("servlet-name")) {
                            servletName = next.getTextContent().trim();
                        } else if (next.getNodeName().equals("url-pattern")) {
                            urlPattern = next.getTextContent().trim();
                            urlPattern = sanitiseUrlPattern(urlPattern);
                        }
                    }
                    if (servletName != null && urlPattern != null) {
                        urlMappingsByName.put(servletName, urlPattern);
                    } else {
                        LOGGER.warn("Incomplete <servlet-mapping> spec encountered in web.xml" + (servletName == null ? "" : (" servletName=" + servletName)));
                    }
                }
            }
        }

        // merge spec
        Set<String> servletNames = Sets.intersection(urlMappingsByName.keySet(), servletClassesByName.keySet());

        // extract methods, by inspecting servlets for implemented methods (doGet, doPost, ..)
        Map<String, Set<MethodSpec>> supportedMethodsByServletName = new HashMap<>();
        for (String servletName : servletNames) {
            String className = servletClassesByName.get(servletName);
            supportedMethodsByServletName.put(servletName, extractSupportedMethods(war, className, profile));
        }


        for (String servletName : servletNames) {
            String urlPattern = urlMappingsByName.get(servletName);
            List<EntryPoint.VariablePathPart> varParts = scanJEEURLWildcards(urlPattern);
            EntryPoint entryPoint = new EntryPoint(urlPattern, supportedMethodsByServletName.get(servletName),servletClassesByName.get(servletName),varParts);
            entryPoints.add(entryPoint);
        }
    }


    private static Set<MethodSpec> extractSupportedMethods(File war,String className,Profile profile) {
        Set<MethodSpec> methods = null;
        try {
            ZipFile zip = new ZipFile(war);
            Enumeration<? extends ZipEntry> en = zip.entries();
            className = className.replace('.','/') + ".class";

            while (en.hasMoreElements()) {
                ZipEntry e = en.nextElement();
                String name = e.getName();
                if (name.startsWith("WEB-INF/classes/") && name.endsWith(className)) {
                    InputStream in = zip.getInputStream(e);
                    methods = extractSupportedMethods(in);
                }
                if (name.startsWith("WEB-INF/lib/") && name.endsWith(".jar")) {
                    InputStream in = zip.getInputStream(e);
                    ZipInputStream input = new ZipInputStream(in);
                    ZipEntry e2 = null;
                    while ((e2 = input.getNextEntry()) != null) {
                        if (e2.getName().endsWith(className)) {
                            methods = extractSupportedMethods(input);
                        }
                    }
                    in.close();
                }
            }
        }
        catch (Exception x) {
            LOGGER.error("Exception extracting supported http methods from war, fall back to profile", x);
        }

        return methods==null || methods.isEmpty() ? profile.getUsedMethods() : methods;
    }

    private static Set<MethodSpec> extractSupportedMethods(InputStream bytecode) throws IOException {
        final Set<MethodSpec> methodSpecs = new HashSet<>();
        ClassVisitor visitor = new ClassVisitor(ASMSettings.VERSION) {
            @Override
            public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                super.visit(version, access, name, signature, superName, interfaces);
            }

            @Override
            public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
                MethodSpec httpMethod = getSupportedHttpMethod(access,name,descriptor,signature,exceptions);
                if (httpMethod!=null) {
                    methodSpecs.add(httpMethod);
                }
                return null;
            }
        };
        new ClassReader(bytecode).accept(visitor, 0);
        return methodSpecs;
    }

    // match Java methods to HTTP methods (doGet, doPost etc).
    // coarse -- TODO refine (access, descriptor)
    private static MethodSpec getSupportedHttpMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
        if (name.startsWith("do") && descriptor.equals("(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V")) {
            // map doGet, doPost etc
            String methodName = name.substring(2).toUpperCase();
            try {
                return MethodSpec.valueOf(methodName);
            }
            catch (IllegalArgumentException x) {}
        }
        return null;
    }

    // extract WEB-INF/web.xml and WEB-INF/lib/*.jar//META-INF/web-fragment.xml documents
    private static Set<Document> extractWebXML(File war) throws Exception {

        Set<Document> documents = new HashSet<>();
        new WarVisitor().visit(war,true,
            entry -> entry.getName().endsWith("WEB-INF/web.xml") || entry.getName().endsWith("META-INF/web-fragment.xml"),
            (name,in) -> {
                try {
                    LOGGER.info("Extracting entry points from metadata in " + name);
                    documents.add(parseWebxml(in));
                }
                catch (Exception x) {
                    LOGGER.warn("Error parsing metadata in " + name,x);
                }
            }
        );

        return documents;
    }

    private static Document parseWebxml(InputStream in) throws Exception {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setValidating(false);
        builderFactory.setSchema(null);
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        return builder.parse(in);
    }

    private static String sanitiseUrlPattern(String urlPattern) {
        while (urlPattern.startsWith("/") && urlPattern.length() > 0) {
            urlPattern = urlPattern.substring(1);
        }
        return urlPattern;
    }

}


