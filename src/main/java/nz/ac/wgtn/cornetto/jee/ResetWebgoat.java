package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.authenticators.WebGoat81Authenticator;
import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility to reset webgoat to start a fuzzing campeign on localhost.
 * This will attempt to remove the local folder containing webgoat data, then restart the server and register a new user
 * with username=scottscott and password=tigertiger.
 * @author jens dietrich
 */
public class ResetWebgoat {
    public static void main (String[] args) throws Exception {

        System.out.println("Removing webgoat files");
        File userHome = new File(System.getProperty("user.home"));
        Preconditions.checkState(userHome.exists());
        File webgoatHome = new File(userHome,".webgoat-v8.1.0");
        if (!webgoatHome.exists()) {
            System.out.println("Cannot locate local webgoat file in " + webgoatHome.getAbsolutePath() + " -- will abort");
            return;
        }
        System.out.println("Webgoat local files located in " + webgoatHome.getAbsolutePath());
        FileUtils.deleteDirectory(webgoatHome);

        System.out.println("Starting webgoat");
        Process process = new ProcessBuilder()
            // .inheritIO()  //enable to redirect server logs
            .command(
                "java", "-jar", FuzzWebGoat81.webgoatInstrumentedJar.getAbsolutePath()
            )
            .start();
        Thread.sleep(20_000); // server needs some time to start !

        System.out.println("Registering new user");

        Loggers.FUZZER.info("Try to create user account " + WebGoat81Authenticator.USERNAME + " / " + WebGoat81Authenticator.PASSWORD);
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("http")
                .setHost("localhost")
                .setPort(8080)
                .setPath("WebGoat/register");
        URI uri = uriBuilder.build();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("username", WebGoat81Authenticator.USERNAME));
        params.add(new BasicNameValuePair("password", WebGoat81Authenticator.PASSWORD));
        params.add(new BasicNameValuePair("matchingPassword", WebGoat81Authenticator.PASSWORD));
        params.add(new BasicNameValuePair("agree", "agree"));
        HttpPost registrationRequest = new HttpPost(uri);
        registrationRequest.setEntity(new UrlEncodedFormEntity(params));

        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient httpClient = builder.build();

        HttpResponse response = httpClient.execute(registrationRequest);

        System.out.println("User registration request sent");
        System.out.println("\trequest status is " + response.getStatusLine() + " -- ok: " + (response.getStatusLine().getStatusCode()==302));

        Header[] locationHeaders = response.getHeaders("Location");
        if (locationHeaders==null || locationHeaders.length==0) {
            Loggers.FUZZER.info("\tno Location header returned as expected");
        }
        Header locationHeader = locationHeaders[0];
        System.out.println("\treturned location header is " + locationHeader.getValue());

        System.out.println("Stopping webgoat");
        process.destroy();
        Thread.sleep(10_000);

        System.out.println("Exit value is " + process.exitValue());

        System.exit(0);


    }
}
