package nz.ac.wgtn.cornetto.jee;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import nz.ac.wgtn.cornetto.*;
import nz.ac.wgtn.cornetto.authenticators.Authenticator;
import nz.ac.wgtn.cornetto.commons.SourceOfRandomness;
import nz.ac.wgtn.cornetto.feedback.FeedbackParser;
import nz.ac.wgtn.cornetto.feedback.TaintFlowSpec;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.html.HtmlParser;
import nz.ac.wgtn.cornetto.http.HeaderSpec;
import nz.ac.wgtn.cornetto.http.RequestSpec;
import nz.ac.wgtn.cornetto.http.RequestSpecGenerator;
import nz.ac.wgtn.cornetto.listeners.*;
import nz.ac.wgtn.cornetto.output.InjectionVulnerability;
import nz.ac.wgtn.cornetto.output.ServerError;
import nz.ac.wgtn.cornetto.output.StackTrace;
import nz.ac.wgtn.cornetto.xss.DefaultXSSDetectionEngine;
import nz.ac.wgtn.cornetto.xss.XSSDetectionEngine;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONObject;
import javax.annotation.Nonnull;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.management.ManagementFactory;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Base class to implement fuzzers using multiple threads.
 * @author jens dietrich
 */
public abstract class AbstractFuzzer {

    public static final int DEFAULT_MAX_TRIALS = Integer.MAX_VALUE;
    public static final int DEFAULT_CAMPAIGN_LENGTH = 1440; // one day
    public static final int DEFAULT_REQUEST_TIMEOUT = 5_000; // in millis

    protected int trials = DEFAULT_MAX_TRIALS;
    protected int campaignLength = DEFAULT_CAMPAIGN_LENGTH;
    protected boolean httpKeepAlive = false;
    protected PoolingHttpClientConnectionManager poolingmgr;
    protected History history = new DefaultHistory();
    protected Authenticator authenticator = null;

    protected XSSDetectionEngine xssDetectionEngine = new DefaultXSSDetectionEngine();

    protected List<HttpRecord> buffer = Collections.synchronizedList(new LinkedList<>());

    protected Cache<String, RequestSpec> requestSpecsByTicketId = CacheBuilder.newBuilder()
            .maximumSize(50_000)
            .expireAfterWrite(60, TimeUnit.MINUTES)
            .build();

    protected SourceOfRandomness sourceOfRandomness = new SourceOfRandomness();
    protected BasicCookieStore cookieStore = new BasicCookieStore();
    protected static int logProgressInterval = 100;
    protected long startTime = -1;

    // flag to initiate shutdown
    protected boolean shutdown = false;

    // headers globally set in options
    protected List<BasicNameValuePair> extraHeaders = Collections.EMPTY_LIST;

    // headers set in authentication (but note that cookies are handled through a cookie store)
    protected List<BasicNameValuePair> sessionHeaders = Collections.EMPTY_LIST;

    protected Collection<FuzzerListener> listeners = new ArrayList<>();
    protected ArrayBlockingQueue<RequestSpec> requestsQueue = null;
    protected ArrayBlockingQueue<HttpRecord> responseQueue = null;

    public interface QueueMonitoringMXBean {
        int getGeneratedButUnprocessedRequestCount();
        int getUnprocessedResponseCount();
        int getGeneratedButUnprocessedRequestQueueRemainingCapacity();
        int getUnprocessedResponseQueueRemainingCapacity();
    }

    public class QueueMonitoringMBeanImpl implements QueueMonitoringMXBean {
        public int getGeneratedButUnprocessedRequestCount() {
            return requestsQueue.size();
        }
        public int getGeneratedButUnprocessedRequestQueueRemainingCapacity() {
            return requestsQueue.remainingCapacity();
        }
        public int getUnprocessedResponseCount() {
            return responseQueue.size();
        }
        public int getUnprocessedResponseQueueRemainingCapacity() {
            return responseQueue.remainingCapacity();
        }

    }

    public AbstractFuzzer() {
        Loggers.FUZZER.info("Initialising connection manager");
        poolingmgr = new PoolingHttpClientConnectionManager(); // use the same connection mgr for all requests
        poolingmgr.setMaxTotal(100);
        poolingmgr.setDefaultMaxPerRoute(50);

    }

    public void fuzz(Options options) throws Exception {
        this.init(options);
        this.deploy(options);
        if (isServerReady()) {
            this.authenticate(options);
            this.startFuzzer(options);
            this.replayRecorded(options);
            this.startInputGeneration(options);
        }

        while (!this.shutdown) {
            Thread.sleep(1000);
        }

        tearDown();
        undeploy(options);

    }

    protected void replayRecorded(@Nonnull Options options) throws Exception {
        @Nonnull List<RequestSpec> requests = this.getRecordedRequests(options);
        if (requests.isEmpty()) {
            Loggers.REPLAY_RECORDED.info("No prerecorded requests to replay");
        }
        else {
            Loggers.REPLAY_RECORDED.info("Replaying " + requests.size() + " prerecorded requests");
        }
        Thread producerThread = new Thread(() -> {
            for (RequestSpec req : requests) {
                try {
                    this.requestsQueue.put(req);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"webfuzzer-replay-prerecorded");
        producerThread.start();
        try {
            producerThread.join();
        } catch (InterruptedException x) {
            Loggers.FUZZER.error("Error running pre-recorded requests",x);
        }
    }

    /**
     * Returns a list of imported (e.g. from har files via HarParser) requests to be replayed (after authentication).
     * @param options configuration options
     * @return a list of requests to be replayed
     */
    protected List<RequestSpec> getRecordedRequests(Options options) throws Exception {
        return Collections.EMPTY_LIST;
    }


    private boolean isServerReady() throws Exception {
        Loggers.FUZZER.info("Waiting for web application to become available");
        int DELAY = 5;
        int i = 0;
        int MAX_CHECKS = 50;
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient httpClient = builder.build();

        while (true) {

            String url = Context.getDefault().getProfile().getWebappRootURL();
            HttpGet httpGet = new HttpGet(url);
            addHeaders(httpGet);
            // Loggers.FUZZER.info("Url: " + url);

            try(CloseableHttpResponse response =httpClient.execute(httpGet);) {
                if (i++ > MAX_CHECKS) {
                    Loggers.FUZZER.error("Could not detect service at: " + url);
                    return false;
                }
                if (response.getStatusLine().getStatusCode() >= 500) { // catch 503 (service not available).
                    Loggers.FUZZER.error("Waiting for service at: " + url);
                    TimeUnit.SECONDS.sleep(DELAY);
                    continue;
                }
                else
                    return true;
            } catch (Exception e) {
                TimeUnit.SECONDS.sleep(DELAY);
            }

        }
    }


    /**
     * Initialise fuzzer from commandline parameters.
     * @param options configuration options
     */
    protected void init(Options options) throws Exception {

        // culling old logs
        File LOG_FOLDER = new File("logs");
        if (LOG_FOLDER.exists()) {
            System.out.println("Culling log files in " + LOG_FOLDER.getAbsolutePath()); // log system is not yet running !
            for (File f : LOG_FOLDER.listFiles(f -> f.getName().endsWith(".log"))) {
                f.delete();
            }
        }

        PropertyConfigurator.configure("log4j.properties");

        this.trials = options.getIntOption("trials", DEFAULT_MAX_TRIALS, i -> i>0);
        Loggers.FUZZER.info("Setting trials to: " + trials);

        this.campaignLength = options.getIntOption("campaignLength",DEFAULT_CAMPAIGN_LENGTH,i -> i>0);
        Loggers.FUZZER.info("Setting campaign length to: " + campaignLength + " mins");

        this.logProgressInterval = options.getIntOption("logProgressInterval",1_000,i -> i>0);
        Loggers.FUZZER.info("Setting logProgressInterval to: " + logProgressInterval);

        if (options.hasOption("randomseed")) {
            int randomSeed = options.getIntOption("randomseed");
            Loggers.FUZZER.info("Setting seed to: " + randomSeed);
            this.sourceOfRandomness.setSeed(randomSeed);
        }

        if (options.hasOption("extraHeaders")) {
          extraHeaders = new ArrayList<BasicNameValuePair>();
          String[] split = options.getStringOption("extraHeaders").split(",");
          for (String kv : split) {
            String[] split2 = kv.split(":");
            extraHeaders.add(new BasicNameValuePair(split2[0].trim(), split2[1].trim()));
            Loggers.FUZZER.info("Extra header added: " + split2[0] + " : " + split2[1]);
          }
        }

        if (options.hasOption("authenticator")) {
            String authenticatorClassName = options.getStringOption("authenticator");
            try {
                Class authenticatorClass = Class.forName(authenticatorClassName);
                this.authenticator = (Authenticator) authenticatorClass.newInstance();
            } catch (Exception x) {
                if (authenticator == null) {
                    Loggers.FUZZER.warn("Authenticator class " + authenticatorClassName + " not found or not implemented correctly -- must be instantiable without arguments and implement " + Authenticator.class.getName(), x);
                }
            }
        }

        if (authenticator == null) {
            Loggers.FUZZER.fatal("Authenticator not set");
            System.exit(1);
        }
        else {
            Loggers.FUZZER.info("Using authenticator: " + this.authenticator);
        }

        this.httpKeepAlive = options.getBooleanOption("httpKeepAlive",false);
        Loggers.FUZZER.warn("HTTP persistent connections set to : " + httpKeepAlive);

        this.addListener(new LoggingFuzzerListener(logProgressInterval));
        this.addListener(new StatsRecorderFuzzerListener(60_000));
        this.addListener(new XSSExportFuzzerListener());
        this.addListener(new InjectionExportFuzzerListener());
        this.addListener(new GCFuzzerListener());
        this.addListener(new TransactionProbListener());
        this.addListener(new CoverageDetailsListener(()->fetchSystemInvocations(),60_000));
        this.addListener(new ServerErrorExportListener());
        this.addListener(new RecordConfigurationListener());

        // init queues

        int requestQueueSize = options.getIntOption("requestqueuesize",100,i -> i>0);
        requestsQueue = new ArrayBlockingQueue<>(requestQueueSize);
        Loggers.FUZZER.info("Set up request queue with size " + requestQueueSize);

        int responseQueueSize = options.getIntOption("responsequeuesize",100,i -> i>0);
        responseQueue = new ArrayBlockingQueue<>(responseQueueSize);
        Loggers.FUZZER.info("Set up response queue with size " + responseQueueSize);

        // add beans to monitor queues
        try {
            ObjectName objectName = new ObjectName("nz.ac.vuw.httpfuzz:type=basic,name=queues");
            MBeanServer server = ManagementFactory.getPlatformMBeanServer();
            server.registerMBean(new QueueMonitoringMBeanImpl(), objectName);
            Loggers.FUZZER.info("Registered mbean for monitoring: " + objectName.getCanonicalName());
        } catch (Exception x) {
            Loggers.FUZZER.error("Error registering mbean for monitoring",x);
        }

    }

    protected void addHeaders(HttpUriRequest request) {
        boolean DEBUG = Loggers.FUZZER.isDebugEnabled();
      if (request != null) {
          if (DEBUG) Loggers.FUZZER.debug("Setting custom headers");
          if (extraHeaders != null && extraHeaders.size() > 0) {
              for (BasicNameValuePair p : extraHeaders) {
                  if (DEBUG) Loggers.FUZZER.debug("Setting extra request header " + p.getName() + " "+  p.getValue());
                  request.setHeader(p.getName(), p.getValue());
              }
          }
          if (sessionHeaders != null && sessionHeaders.size() > 0) {
              for (BasicNameValuePair p : sessionHeaders) {
                  if (DEBUG) Loggers.FUZZER.debug("Setting extra session header " + p.getName() + " "+  p.getValue());
                  request.setHeader(p.getName(), p.getValue());
              }
          }
      }
    }

    protected void authenticate(Options options) throws Exception {

        Loggers.FUZZER.info("Starting phase: authentication");

        int timeout = options.getIntOption("requesttimeout",DEFAULT_REQUEST_TIMEOUT,t -> t>0);
        Loggers.FUZZER.info("Request timeout is " + timeout + " ms");

        boolean result = this.authenticator.authenticate(Context.getDefault().getProfile(),options, req -> {
                try {
                    return sendAuthenticationRequest(req, timeout);
                } catch (IOException x) {
                    Loggers.AUTHENTICATOR.warn("Sending authentication request failed",x);
                    return null;
                }
            },
                headers -> this.sessionHeaders = headers

        );
        Loggers.AUTHENTICATOR.info("Authentication result: " + result);

    }

    protected void reAuthenticate(Options options) throws Exception {

        Loggers.AUTHENTICATOR.info("re-authentication");

        int timeout = options.getIntOption("requesttimeout",DEFAULT_REQUEST_TIMEOUT,t -> t>0);
        Loggers.FUZZER.info("Request timeout is " + timeout + " ms");

        boolean result = this.authenticator.reAuthenticate(Context.getDefault().getProfile(),options, req -> {
                try {
                    return sendAuthenticationRequest(req, timeout);
                } catch (IOException x) {
                    Loggers.AUTHENTICATOR.warn("Sending authentication request failed",x);
                    return null;
                }
            },
            headers -> this.sessionHeaders = headers
        );
        Loggers.AUTHENTICATOR.info("Authentication result: " + result);

    }

    /**
     * Changed settings for authentication requests with longer timeouts, and reconnects if necessary.
     * @param req
     * @param timeout
     * @return
     * @throws IOException
     */
    protected HttpResponse sendAuthenticationRequest(HttpUriRequest req, int timeout) throws IOException {

        int authenticationTimeout = timeout * 2;
        int maxAttempts = 5;
        return sendRequest(req, authenticationTimeout + 1000,maxAttempts,authenticationTimeout);

    }


    protected void startFuzzer(Options options) throws Exception {

        Loggers.FUZZER.info("Starting phase: start fuzzer");

        Loggers.FUZZER.info("\tSettings:");
        for (String key:options.getOptionNames()) {
            Loggers.FUZZER.info("\t" + key + " = " + options.getStringOption(key));
        }

        this.listeners.forEach(l -> l.fuzzingStarts(options));
        this.startTime = System.currentTimeMillis();

        Loggers.FUZZER.info("Starting fuzzer");

        int requestExecutionTaskCount = options.getIntOption("requestexecutionthreads",1,i -> i>0);
        Loggers.FUZZER.info("Using " + requestExecutionTaskCount + " threads for request execution");
        int responseEvaluationTaskCount = options.getIntOption("responseevaluationthreads",1,i -> i>0);
        Loggers.FUZZER.info("Using " + responseEvaluationTaskCount + " threads for response evaluation");

        int timeout = options.getIntOption("requesttimeout",DEFAULT_REQUEST_TIMEOUT,t -> t>0);
        Loggers.FUZZER.info("Request timeout is " + timeout);

        AtomicInteger threadCount2 = new AtomicInteger();
        AtomicInteger threadCount3 = new AtomicInteger();
        ExecutorService requestExecutionExecutor = Executors.newFixedThreadPool(requestExecutionTaskCount, runnable -> new Thread(runnable,"webfuzz.request-execution." + threadCount2.incrementAndGet()));
        ExecutorService responseEvaluationExecutor = Executors.newFixedThreadPool(responseEvaluationTaskCount, runnable -> new Thread(runnable,"webfuzz.response-evaluation." + threadCount3.incrementAndGet()));

        Runnable requestExecutionTask = () -> {
            while (!shutdown) {
                try {
                    if (this.authenticator.needsAuthentication()) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {}
                    }
                    else {
                        RequestSpec nextRequest = this.requestsQueue.take();
                        if (!shutdown) {
                            HttpRecord httpRecord = this.sendRequest(options, nextRequest, timeout);
                            if (httpRecord != null) {
                                this.responseQueue.put(httpRecord);
                            }
                        }
                    }
                } catch (Throwable x) {
                    Loggers.FUZZER.error("Error executing requests",x);
                }
            }
            Loggers.FUZZER.info("Shutdown request detected, stop executing requests");

        };


        Runnable responseEvaluationTask = () -> {
            while (!shutdown) {
                if (this.authenticator.needsAuthentication()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {}
                }
                else {
                    try {
                        HttpRecord nextResponse = this.responseQueue.take();
                        if (!shutdown && nextResponse!=null) {
                            processFeedback(nextResponse, timeout, options);
                        }
                    } catch (Throwable x) {
                        Loggers.FUZZER.error("Error analysing response", x);
                    }
                }
            }
            Loggers.FUZZER.info("Shutdown request detected, stop evaluating responses");

        };

        for (int i=0;i<requestExecutionTaskCount;i++) {
            requestExecutionExecutor.execute(requestExecutionTask);
        }
        for (int i=0;i<responseEvaluationTaskCount;i++) {
            responseEvaluationExecutor.execute(responseEvaluationTask);
        }

        requestExecutionExecutor.shutdown();
        responseEvaluationExecutor.shutdown();

        Loggers.FUZZER.info("Finishing phase: starting fuzzer");

    }

    protected void startInputGeneration(Options options) throws Exception {
        Loggers.FUZZER.info("Starting phase: request generation");

        AtomicInteger requestCount = new AtomicInteger();
        RequestSpecGenerator generator = new RequestSpecGenerator();
        int requestGeneratorTaskCount = options.getIntOption("requestgenerationthreads",1,i -> i>0);
        Loggers.FUZZER.info("Using " + requestGeneratorTaskCount + " threads for request generation");

        AtomicInteger threadCount1 = new AtomicInteger();
        ExecutorService requestGenerationExecutor = Executors.newFixedThreadPool(requestGeneratorTaskCount, runnable -> new Thread(runnable,"webfuzz.request-generation." + threadCount1.incrementAndGet()));

        boolean debugOn = Loggers.FUZZER.isDebugEnabled();
        AtomicBoolean shutDownLog = new AtomicBoolean(false);
        final long stopTime = System.currentTimeMillis() + (this.campaignLength * 60 * 1000);

        Runnable requestGenerationTask = () -> {
            while (!shutdown) {
                try {
                    shutdown = requestCount.get() > trials || System.currentTimeMillis() >= stopTime;
                    RequestSpec nextRequest = generator.apply(this.sourceOfRandomness, Context.getDefault());
                    assert nextRequest!=null;
                    if (nextRequest!=null) {
                        if (debugOn) Loggers.FUZZER.debug("adding request " + nextRequest);
                        this.requestsQueue.put(nextRequest);
                        if (debugOn) Loggers.FUZZER.debug("request queue size " + this.requestsQueue.size());
                    }
                } catch (Throwable x) {
                    Loggers.FUZZER.error("Error generating new requests",x);
                }
                finally {
                    requestCount.incrementAndGet();
                }
            }

            // do some exit logging
            if (shutDownLog.getAndSet(true)) { // i.e. if the old value was false -- do this only once
                if (requestCount.get() < trials) {
                    Loggers.FUZZER.info("Stopped input generation as max number of requests has been is reached");
                }

                if (System.currentTimeMillis() >= stopTime) {
                    Loggers.FUZZER.info("Stopped input generation as max campaign time is reached");
                }
                Loggers.FUZZER.info("Number of trials is " + requestCount.get() + " max configured is " + this.trials);
                Loggers.FUZZER.info("Timestamp is " + System.currentTimeMillis() + " campaign was set to stop at " + stopTime + " (start time + " + this.campaignLength + " mins)");
            }
        };

        for (int i=0;i<requestGeneratorTaskCount;i++) {
            requestGenerationExecutor.execute(requestGenerationTask);
        }

        requestGenerationExecutor.shutdown();
        Loggers.FUZZER.info("Finishing phase: request generation (workers have started)");

    }

    protected abstract void deploy(Options options) throws Exception;

    protected abstract void undeploy(Options options) throws Exception;

    protected CloseableHttpResponse sendRequest(HttpUriRequest request, int timeBetweenAttempts, int maxAttempts,int attemptCounter,int connectionTimeout) throws IOException {

        if (attemptCounter>=maxAttempts) {
            Loggers.FUZZER.info("max attempts exceeded for request: " + request);
            return null;
        }
        if (shutdown) return null;

        if (!httpKeepAlive) {
            request.setHeader("Connection", "close");
        }
        CloseableHttpResponse response = null;
        addHeaders(request);
        try {
            RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(connectionTimeout)
                .setConnectionRequestTimeout(connectionTimeout)
                .setSocketTimeout(connectionTimeout).build();

            CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .setConnectionManager(poolingmgr)
                .setDefaultRequestConfig(config)
                .build();

            response = httpClient.execute(request);
            return response;
        }
        catch (Exception x) {
            // Loggers.FUZZER.error("Error sending request (timeout = " + connectionTimeout + ") - " + x.getMessage());
            boolean timeout = x.getMessage().contains("Read timed out");
            this.listeners.stream().forEach(l -> l.connectionErrorDetected(request,timeout?FuzzerListener.ConnectionErrorType.TIMEOUT:FuzzerListener.ConnectionErrorType.OTHER));
            if (maxAttempts>1) {
                if (attemptCounter == 0) {
                    Loggers.FUZZER.debug("Network problem, resending request " + request.getMethod() + " " + request.getURI());
                }
                // x.printStackTrace();
                try {
                    Thread.sleep(timeBetweenAttempts);
                } catch (InterruptedException e) {
                    Loggers.FUZZER.error("Exception: " + x.getMessage(), x);
                }
                if (response != null) {
                    response.close();
                }
                return sendRequest(request, timeBetweenAttempts, maxAttempts, attemptCounter + 1,connectionTimeout);
            }
            return null;
        }
    }


    protected CloseableHttpResponse sendRequest(HttpUriRequest request, int timeBetweenAttempts, int maxAttempts,int connectionTimeout) throws IOException {
        return sendRequest(request, timeBetweenAttempts,maxAttempts,0,connectionTimeout);
    }


    protected CloseableHttpResponse sendRequest(HttpUriRequest request,int connectionTimeout) throws IOException {
        return sendRequest(request, 100,1,connectionTimeout);
    }

    /**
     * Send a request generated from a request spec.
     * @param requestSpec
     * @param requestTimeout
     * @return
     */
    public HttpRecord sendRequest(Options options,RequestSpec requestSpec,int requestTimeout) throws Exception {
        Context context = Context.getDefault();
        HttpUriRequest request = null;
        try {
            request = requestSpec.toRequest();
        }
        catch (Exception x) {
            Loggers.FUZZER.error("Error generating request",x);
        }

        if (request!=null && history.isNew(requestSpec) && !this.authenticator.isLogoutRequest(request)) {
            // Loggers.FUZZER.info("sending next request: " + request);
            try (CloseableHttpResponse response = sendRequest(request,requestTimeout);) {

                if (response == null) {
                    Loggers.FUZZER.debug("response for request was null: " + request);
                    return null;
                }

                ResponseWrapper responseWrapper = new ResponseWrapper(response);

                checkAuthenticationStatusAndAuthenticateIfNecessary(options,request,response,responseWrapper.getContent());

                HttpUriRequest request2 = request; // to make ref final for lambda
                listeners.forEach(l -> l.requestSent(requestSpec, request2,responseWrapper));

                // if this is html, parse response for forms
                Header contentTypeHeader = response.getFirstHeader("Content-Type");

                if (contentTypeHeader != null && (contentTypeHeader.getValue().startsWith("text/html") || contentTypeHeader.getValue().startsWith("application/xhtml+xml"))) {
                    try {
                        Collection<Form> forms = HtmlParser.extractForms(context.getProfile(), responseWrapper.getContent(), request.getURI().toString());
                        if (forms != null && forms.size() > 0) {
                            Set<Form> newForms = context.getDynamicModel().formsRecorded(requestSpec, request, response, forms);
                            listeners.forEach(l -> l.newFormsDiscovered(requestSpec,newForms));
                        }
                    } catch (Exception x) {
                        // LOGGER.warn("Exception parsing HTML form returned by response");
                    }

                    try {
                        Collection<URL> links = HtmlParser.extractLinks(context.getProfile(), responseWrapper.getContent());
                        if (links != null && links.size() > 0) {
                            Set<URL> newLinks = context.getDynamicModel().linksRecorded(requestSpec, request, response, links);
                            listeners.forEach(l -> l.newLinksDiscovered(requestSpec,newLinks));
                        }
                    } catch (Exception x) {
                        // LOGGER.warn("Exception parsing HTML form returned by response");
                    }
                }

                // pickup and process feedback
                Header ticketIdHeader = response.getFirstHeader(RTConstants.FUZZING_FEEDBACK_TICKET_HEADER);

                // START DEBUGGING CODE
//                if (!request.getURI().toString().contains("__fuzzing")) {
//                    LOGGER.info("request " + request + "\tgenerated by " + requestSpec.getGenerator().name());
//                    LOGGER.info("\t" + response.getStatusLine());
//                    LOGGER.info("\tticket: #" + ticketIdHeader);
//                }
                // END DEBUGGING CODE

                if (ticketIdHeader == null) {
                    // Loggers.FUZZER.info("No feedback header returned for " + request);
                    // there is an issue that the header is not set when a server error (500) occurs
                    if (response.getStatusLine().getStatusCode() >= 500) {
                        Loggers.FUZZER.info("Response code 500 encountered: " + requestSpec);
                        context.getDynamicModel().executionsRecorded(requestSpec, request, response, Collections.EMPTY_LIST);
                    }
                } else {
                    // register record
                    HttpRecord record = new HttpRecord(ticketIdHeader.getValue(), requestSpec, request, response,responseWrapper.getContent());
                    requestSpecsByTicketId.put(ticketIdHeader.getValue(), requestSpec);
                    history.keep(requestSpec);

                    xssDetectionEngine.checkForXSS(record, responseWrapper.getContent(),listeners);

                    if (response.getStatusLine().getStatusCode() >= 500) {
                        // TODO extract stacktrace(s) from feedback, and attach
                        ServerError error = ServerError.newFrom(record,null,responseWrapper.getContent(),IssuePriority.MEDIUM,null);
                        listeners.forEach(l -> l.newServerErrorDiscovered(error));
                    }
                    return record;
                }

            } catch (Exception x) {
                try {
                    Loggers.FUZZER.debug("Error sending request " + x.getMessage() + " (change log level to DEBUG for details)");
                    Loggers.FUZZER.debug("Error sending request " + requestSpec.toRequest());
                    Loggers.FUZZER.debug("\theaders:");
                    for (HeaderSpec headerSpec : requestSpec.getHeadersSpec().getHeaders()) {
                        Loggers.FUZZER.debug("\t\t" + headerSpec);
                    }
                } catch (Exception xx) {
                    Loggers.FUZZER.debug("Error sending request " + requestSpec);
                }
                Loggers.FUZZER.debug("Details: " + x.getMessage(), x);
            }
        }
        else {
            listeners.forEach(l -> l.requestRejected(requestSpec));
        }

        return null;
    }

    /**
     * Fetch the list of methods invoked by the server outside request handling threads.
     * @return
     * @throws Exception
     */
    public Set<String> fetchSystemInvocations()  {
        Context context = Context.getDefault();
        Profile profile = context.getProfile();
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("http")
                .setHost(profile.getHost())
                .setPort(profile.getPort());

        String path = profile.getPathFirstTokens() == null ? "" : profile.getPathFirstTokens().stream().collect(Collectors.joining("/"));
        path = path + RTConstants.FUZZING_FEEDBACK_PATH_TOKEN + '/' + RTConstants.SYSTEM_INVOCATIONS_TICKET;
        uriBuilder.setPath(path);
        uriBuilder.addParameter(RTConstants.SYSTEM_INVOCATIONS_APPLICATION_PACKAGE_PREFIXES_PARAMETER,context.getApplicationPackagePrefixes());

        try {
            URI uri = uriBuilder.build();
            Loggers.FUZZER.debug("special feedback request: " + uri);
            HttpUriRequest feedbackRequest = new HttpGet(uri);

            CloseableHttpResponse response = sendRequest(feedbackRequest, 10_000);
            if (response!=null) {
                String data = EntityUtils.toString(response.getEntity());
                BufferedReader br = new BufferedReader(new StringReader(data));
                return br.lines().collect(Collectors.toSet());
            }
            else {
                Loggers.FUZZER.warn("No data returned fetching system invocations");
                return Collections.EMPTY_SET;
            }
        }
        catch (Exception x) {
            // wrapper to use in lambdas
            throw new RuntimeException (x);
        }
    }


    /**
     * Parses the feedback encoded in JSON returned by a dedicated feedback request.
     * shawn - added extract taintflow from feedback, record new taintflows in dynamicmodel and log them
     * @param record the http record
     * @param requestTimeout
     * @param options
     */
    protected void processFeedback(HttpRecord record, int requestTimeout,Options options) {

        Context context = Context.getDefault();

        Profile profile = context.getProfile();
        URIBuilder uriBuilder = new URIBuilder()
            .setScheme("http")
            .setHost(profile.getHost())
            .setPort(profile.getPort());

        RequestSpec originalRequestSpec = requestSpecsByTicketId.getIfPresent(record.ticketId);
        requestSpecsByTicketId.invalidate(record.ticketId);

        String path = profile.getPathFirstTokens() == null ? "" : profile.getPathFirstTokens().stream().collect(Collectors.joining("/"));
        path = path + RTConstants.FUZZING_FEEDBACK_PATH_TOKEN + '/' + record.ticketId;

        uriBuilder.setPath(path);
        URI uri = null;

        try {
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            return;
        }
        Loggers.FUZZER.debug("feedback request: " + uri);
        HttpUriRequest feedbackRequest = new HttpGet(uri);

        try (CloseableHttpResponse feedbackResponse = sendRequest(feedbackRequest,requestTimeout);) {

            listeners.forEach(l -> l.feedbackRequestSent(record.ticketId,feedbackResponse.getStatusLine().getStatusCode()));
            // pick up dynamic callgraph information

            if (feedbackResponse == null)
                return; // session cancelled !
            assert feedbackResponse.getFirstHeader("Content-Type").getValue().startsWith("application/json");
            assert feedbackResponse.getStatusLine().getStatusCode() == 200;

            Set<ExecutionPoint> invocations = new HashSet<>();
            Set<String> requestParameterNames = new HashSet<>();
            Set<String> stacktracesForCriticalMethods = new HashSet<>();
            List<TaintFlowSpec> taintFlows = new ArrayList<>();
            Set<String> stacktracesForExceptions = Collections.EMPTY_SET;


            if (feedbackResponse.getStatusLine().getStatusCode() == 200) {
                String data = EntityUtils.toString(feedbackResponse.getEntity());
                checkAuthenticationStatusAndAuthenticateIfNecessary(options,feedbackRequest,feedbackResponse,data);
                boolean auth = this.authenticator.needsAuthentication();
                try {

                    JSONObject json = new JSONObject(data);
                    invocations = FeedbackParser.extractMethodSpecs(json);
                    requestParameterNames = FeedbackParser.extractRequestParameterNames(json);
                    stacktracesForCriticalMethods = FeedbackParser.extractStackTracesForUnsafeMethods(json);
                    taintFlows = FeedbackParser.extractTaintflows(json);
                    stacktracesForExceptions = FeedbackParser.extractStackTracesForExceptions(json);
                }
                catch (Exception x) {
                    if (auth) {
                        Loggers.FUZZER.warn("Error processing feedback (could be caused by an authentication problem)", x);
                    }
                    else {
                        Loggers.FUZZER.error("Error processing feedback", x);
                    }
                }
            }

            else {
                Loggers.FUZZER.debug("Feedback request to " + uri + " has failed, response status: " + feedbackResponse.getStatusLine());
            }

            Set<ExecutionPoint> newlyDiscoveredExecutionPoints = context.getDynamicModel().executionsRecorded(originalRequestSpec, record.request, record.response, invocations);
            listeners.forEach(l -> l.newExecutionPointsDiscovered(originalRequestSpec,newlyDiscoveredExecutionPoints));

            Set<String> newlyDiscoveredRequestParameterNames = context.getDynamicModel().requestParametersRecorded(originalRequestSpec, record.request, record.response, requestParameterNames);
            listeners.forEach(l -> l.newRequestParameterNamesDiscovered(originalRequestSpec,newlyDiscoveredRequestParameterNames));

            for (String stackTrace : stacktracesForCriticalMethods) {
                if (CriticalMethodInvocationStacktraceFilter.isCritical(stackTrace)) {
                    if (context.getDynamicModel().stacktraceToCriticalMethodRecorded(originalRequestSpec, record.request, record.response, stackTrace)) {
                        listeners.forEach(l -> l.newInvocationsOfCriticalMethodDiscovered(originalRequestSpec, stackTrace));
                    }
                }
            }

            for (TaintFlowSpec taintFlow : taintFlows) {
                if (context.getDynamicModel().taintFlowRecorded(originalRequestSpec, record.request, record.response, taintFlow)) {
                    InjectionVulnerability injection = InjectionVulnerability.newFrom(record,null,record.getResponseEntityContent(),taintFlow,IssuePriority.HIGH);
                    listeners.forEach(l -> l.newInjectionDiscovered(injection));
                }
            }

            for (String stackTraceStr : stacktracesForExceptions) {
                if (record.getResponse().getStatusLine().getStatusCode() >= 500) {
                    StackTrace stackTrace = new StackTrace();
                    String[] parts = stackTraceStr.split("\n");
                    List<String> entries = Arrays.asList(parts);
                    String exceptionMessage = entries.get(0);
                    stackTrace.addEntries(entries.subList(1, entries.size()));
                    stackTrace.setExceptionMessage(exceptionMessage);
                    ServerError error = ServerError.newFrom(record, null, record.getResponseEntityContent(), IssuePriority.MEDIUM, stackTrace);
                    listeners.forEach(l -> l.newServerErrorDiscovered(error));

                }
            }

        } catch (IOException x) {
            Loggers.FUZZER.error("Exception: " + x.getMessage(), x);
        }
    }

    protected void checkAuthenticationStatusAndAuthenticateIfNecessary(Options options, HttpUriRequest feedbackRequest, CloseableHttpResponse feedbackResponse, String data) {
        this.authenticator.checkAuthenticationStatus(feedbackRequest,feedbackResponse,data);
        if (this.authenticator.needsAuthentication()) {
            try {
                Loggers.FUZZER.info("Re-authenticating");
                this.reAuthenticate(options);
            }
            catch (Exception x) {
                Loggers.FUZZER.info("Error re-authenticating",x);
            }
        }
    }


    public Collection<FuzzerListener> getListeners() {
        return Collections.unmodifiableCollection(listeners);
    }

    public boolean addListener(FuzzerListener listener) {
        boolean success = this.listeners.add(listener);
        if (success) {
            Loggers.FUZZER.info("Installed listener: " + listener);
        }
        else {
            Loggers.FUZZER.warn("Failed to install listener: " + listener);
        }
        return success;
    }

    public boolean removeListener(FuzzerListener listener) {
        boolean success = this.listeners.remove(listener);
        if (success) {
            Loggers.FUZZER.info("Uninstalled listener: " + listener);
        }
        else {
            Loggers.FUZZER.warn("Failed to uninstall listener: " + listener);
        }
        return success;
    }

    /**
     * Final Reporting and clean-up.
     * @throws Exception
     */
    protected void tearDown() throws Exception {

        Loggers.FUZZER.info("Teardown: clearing queues");
        this.responseQueue.clear();
        this.requestsQueue.clear();
        Thread.sleep(5000);

        Loggers.FUZZER.info("Teardown: notifing listeners");
        this.listeners.forEach(l -> l.fuzzingEnds());
        Thread.sleep(5000);

        Loggers.FUZZER.info("Fuzzing session has ended");

    }
}
