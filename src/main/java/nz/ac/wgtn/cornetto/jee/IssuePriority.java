package nz.ac.wgtn.cornetto.jee;

/**
 * Simple enum that can be used for an approximate classification of detected issues.
 * @author jens dietrich
 */
public enum IssuePriority {
    ZERO , LOW , MEDIUM , HIGH
}
