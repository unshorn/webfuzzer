package nz.ac.wgtn.cornetto.jee;

import nz.ac.wgtn.cornetto.http.RequestSpec;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;

import java.util.Objects;

/**
 * Data structure to be used by fuzzers to manage http transaction related data.
 * @autorr jens dietrich
 */

public class HttpRecord {
    String ticketId = null;
    RequestSpec requestSpec = null;
    HttpRequest request = null;
    HttpResponse response = null;
    String responseEntityContent = null;

    public HttpRecord(String ticketId, RequestSpec requestSpec, HttpRequest request, HttpResponse response,String responseEntityContent) {
        this.ticketId = ticketId;
        this.requestSpec = requestSpec;
        this.request = request;
        this.response = response;
        this.responseEntityContent = responseEntityContent;
    }

    public String getTicketId() {
        return ticketId;
    }

    public RequestSpec getRequestSpec() {
        return requestSpec;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public String getResponseEntityContent() {
        return responseEntityContent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HttpRecord that = (HttpRecord) o;
        return Objects.equals(ticketId, that.ticketId) &&
                Objects.equals(requestSpec, that.requestSpec) &&
                Objects.equals(request, that.request) &&
                Objects.equals(response, that.response) &&
                Objects.equals(responseEntityContent, that.responseEntityContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ticketId, requestSpec, request, response, responseEntityContent);
    }
}
