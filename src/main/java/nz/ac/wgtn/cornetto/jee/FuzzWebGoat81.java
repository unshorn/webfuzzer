package nz.ac.wgtn.cornetto.jee;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.Context;
import nz.ac.wgtn.cornetto.DefaultDynamicModel;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.authenticators.WebGoat81Authenticator;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import java.io.File;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Adhoc fuzzing for webgoat 8.1.
 * @author jens dietrich
 */
public class FuzzWebGoat81 extends AbstractFuzzer {


    // private AtomicInteger duplicateCounter = new AtomicInteger();
    private static int logProgressInterval = 100;

    static final File webgoatInstrumentedJar = new File("examples/webgoat/webgoat-server-v8.1.0.jar");

    private static final File prerecordedRootFolder = null; // new File("examples/webgoat/pre-recorded");

    public static void main(String[] args) throws Exception {

        Preconditions.checkState(webgoatInstrumentedJar.exists());

        org.apache.commons.cli.Options cliOptions = new org.apache.commons.cli.Options()
            .addOption("logInterval", true, "The interval to report progress using logging (optional, default is " + logProgressInterval + ")")
            .addOption("trials", true, "The max number of generated requests (default is " + DEFAULT_MAX_TRIALS + ")")
            .addOption("campaignLength",true,"The max runtime of a fuzzing campaign, in minutes (default is " + DEFAULT_CAMPAIGN_LENGTH + ")")
            .addOption("randomseed", true, "Control randomness for the fuzzing run")
            .addOption("httpKeepAlive", true, "Set http client to use persistent connections")
            .addOption("requestgenerationthreads",true,"The number of thread used to generate requests")
            .addOption("requestexecutionthreads",true,"The number of thread used to execute requests")
            .addOption("responseevaluationthreads",true,"The number of thread used to evaluate responses")
            .addOption("requestgenerationthreads",true,"The number of thread used to generate requests")
            .addOption("requestexecutionthreads",true,"The number of thread used to execute requests")
            .addOption("responseevaluationthreads",true,"The number of thread used to evaluate responses")
            .addOption("requestqueuesize",true,"The size of the request queue")
            .addOption("responsequeuesize",true,"The size of the response queue")
            .addOption("requesttimeout",true,"The http request timeout in s")
            .addOption("probInterval",true,"The interval to log probs (request/response details)")
            .addOption("outputRootFolder",true,"The output root folder")
            .addOption("max500Reported",true,"The max number of 500 errors that will be reported")
            .addOption("maxXSSReported",true,"The max number of XSS vulnerabilities that will be reported")
            .addOption("maxInjectionsReported",true,"The max number of injection flaws that will be reported")

            ;

        System.out.println(Stream.of(args).collect(Collectors.joining(" ")));
        CommandLine cmd = new DefaultParser().parse(cliOptions, args);

        FuzzWebGoat81 fuzzer = new FuzzWebGoat81();
        Options options = Options.fromCLIArguments(cmd);

        fuzzer.authenticator = new WebGoat81Authenticator();

        fuzzer.fuzz(options);
    }


//    @Override
//    protected List<RequestSpec> getRecordedRequests(Options options) throws Exception {
//        if (prerecordedRootFolder!=null && prerecordedRootFolder.exists()) {
//            List<RequestSpec> requests = new ArrayList<>();
//            Files.walk(prerecordedRootFolder.toPath())
//                    .map(p -> p.toFile())
//                    .filter(f -> f.getName().endsWith(".har"))
//                    .filter(f -> !f.isDirectory())
//                    .sorted(Comparator.comparing(File::getAbsolutePath))
//                    .forEach(f -> {
//                        try {
//                            requests.addAll(HarParser.parse(f));
//                        } catch (Exception e) {
//                            Loggers.REPLAY_RECORDED.error("Exception importing requests from " + f.getAbsolutePath(), e);
//                        }
//                    });
//
//            Loggers.REPLAY_RECORDED.info(requests.size() + " pre-recorded requests imported from " + prerecordedRootFolder + " to be replayed");
//            return requests;
//        }
//        else {
//            Loggers.REPLAY_RECORDED.info("no pre-recorded requests found in " + (prerecordedRootFolder==null?"null":prerecordedRootFolder.getAbsolutePath()));
//            return Collections.EMPTY_LIST;
//        }
//    }


    @Override
    protected void init(Options options) throws Exception {
        super.init(options);

        Profile profile = new Profile.WebApplicationProfile("localhost",8080,"WebGoat");
        Context context = new Context(
                profile,
                new JEEStaticModel(webgoatInstrumentedJar,s->s.startsWith("org.owasp.webgoat"),profile),  // callgraph not used in example
                new DefaultDynamicModel()
        );
        context.setApplicationPackagePrefixes("org.owasp.webgoat");
        context.install(); // will be referenced by generators as singleton

    }

    @Override
    protected void deploy(Options options) throws Exception {

        Loggers.FUZZER.warn("Deployment of webgoat server is manually - start server using this executable jar " + webgoatInstrumentedJar.getAbsolutePath() + " with JVM option \"-javaagent:\" " + new File("tools/aspectjweaver-1.9.6.jar").getAbsolutePath()+"\"");
        Loggers.FUZZER.warn("\tExample:");
        Loggers.FUZZER.warn("\tjava -javaagent:" + new File("tools/aspectjweaver-1.9.6.jar").getAbsolutePath() + " -jar " + webgoatInstrumentedJar.getAbsolutePath());


        // Thread.sleep(1000*30);

        startTime = System.currentTimeMillis();

    }

    @Override
    protected void undeploy(Options options) throws Exception {
        // nothing to do here
    }
}
