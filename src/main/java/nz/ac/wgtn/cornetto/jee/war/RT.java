package nz.ac.wgtn.cornetto.jee.war;

/**
 * Support to locate runtime artifacts.
 * @author jens dietrich
 */
public class RT {
    static boolean isFuzzerRuntimeClass(String name) {
        return name.contains("nz/ac/wgtn/cornetto/jee/") || name.contains("InvocationTracking");
    }
}
