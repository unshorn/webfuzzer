package nz.ac.wgtn.cornetto.jee.war;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.EntryPoint;
import nz.ac.wgtn.cornetto.Profile;
import nz.ac.wgtn.cornetto.StaticModel;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.html.Form;
import nz.ac.wgtn.cornetto.http.MethodSpec;
import nz.ac.wgtn.cornetto.jee.Loggers;
import org.apache.log4j.Logger;
import java.io.File;
import java.net.URL;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Static analysis model created from doop model.
 * @author jens dietrich
 */
public class StaticModelFromWar implements StaticModel {

   private static Logger LOGGER = LogSystem.getLogger("static-pre-analysis-war");

   private static final Pattern PARAM_NAME_MATCHER = Pattern.compile("(\\w|:|-|\\$|\\/|\\\\)*");

   private Set<EntryPoint> entryPoints = null;
   private Set<String> entryPaths = null;
   private Set<String> stringLiteralsInApp = null;
   private Set<String> stringLiteralsInLibs = null;
   private Set<String> reflectiveNamesInApp = null;
   private Set<String> reflectiveNamesInLibs = null;
   private Set<Integer> intLiteralsInApp = null;
   private Set<Integer> intLiteralsInLibs = null;
   private Set<String> requestParameterNames = null;
   private Set<String> headerNames = null;
   private Set<MethodSpec> supportedMethods = null;
   private Set<String> applicationMethods = null;
   private Set<Form> forms = null;
   private Set<URL> links = null;
   private Predicate<String> applicationFilter = null;

   public StaticModelFromWar(File war, WarScopeModel scopeModel, Profile profile, Predicate<String> applicationFilter) throws Exception {
      Preconditions.checkArgument(war.exists());
      Preconditions.checkNotNull(scopeModel);

      this.applicationFilter = applicationFilter;

      Set<String> resourceKeys = new ResourceBundleKeyExtractor().extract(war,true,true);
      entryPoints = EntryPointExtractor.extractEntryPoints(war,profile).stream()
              .filter(ep -> !ep.getUrlPattern().contains("__fuzzing_feedback"))
              .collect(Collectors.toSet());
      entryPaths = entryPoints.stream().map(ep -> ep.getUrlPattern()).collect(Collectors.toSet());
      LOGGER.info("Registering " + entryPoints.size() + " entry points, removing injected feedback entry points with __fuzzing_feedback URLs" );
      stringLiteralsInApp = new StringLiteralExtractor(scopeModel).extract(war,true,false);
      stringLiteralsInLibs = new StringLiteralExtractor(scopeModel).extract(war,false,true);
      reflectiveNamesInApp = new ReflectiveNamesExtractor(scopeModel).extract(war,true,false);

      stringLiteralsInApp.removeAll(resourceKeys);
      stringLiteralsInLibs.removeAll(resourceKeys);

      reflectiveNamesInLibs = new ReflectiveNamesExtractor(scopeModel).extract(war,false,true);
      intLiteralsInApp = new IntLiteralExtractor(scopeModel).extract(war,true,false);
      intLiteralsInLibs = new IntLiteralExtractor(scopeModel).extract(war,false,true);

      applicationMethods = ApplicationMethodExtractor.extractApplicationMethods(war,applicationFilter);
      // log for debugging
      for (String appMethod:applicationMethods) {
         Loggers.APPLICATION_METHODS.info(appMethod);
      }

      requestParameterNames = stringLiteralsInApp.parallelStream()
        .filter(n -> PARAM_NAME_MATCHER.matcher(n).matches())
        .collect(Collectors.toSet());

      // same as requestParameterNames  !! sep script for better maintanance
      headerNames = stringLiteralsInApp.parallelStream()
           .filter(n -> PARAM_NAME_MATCHER.matcher(n).matches())
           .collect(Collectors.toSet());

      supportedMethods = entryPoints.parallelStream()
           .flatMap(ep -> ep.getSupportedMethods().stream())
           .collect(Collectors.toSet());

      forms = new HtmlFormExtractor().extractForms(war,profile);

      links = new HtmlLinkExtractor().extractLinks(war,profile);

   }

   @Override
   public Set<Form> getForms() {
      return forms;
   }

   @Override
   public Set<URL> getLinks() {
      return links;
   }

   @Override
   public Set<EntryPoint> getEntryPoints() {
      return entryPoints;
   }

   @Override
   public Set<String> getEntryPointsURLPatterns() {
      return entryPaths;
   }

   @Override
   public Set<String> getStringLiterals(EnumSet<Scope> scopes) {
      if (scopes.contains(Scope.SYSTEM)) {
         throw new IllegalArgumentException("literal extraction for system classes is not supported");
      }
      Set<String> literals = new HashSet<>();
      if (scopes.contains(Scope.APPLICATION)) {
         literals.addAll(this.stringLiteralsInApp);
      }
      if (scopes.contains(Scope.LIBRARIES)) {
         literals.addAll(this.stringLiteralsInLibs);
      }
      return literals;
   }

   @Override
   public Set<Integer> getIntLiterals(EnumSet<Scope> scopes) {
      if (scopes.contains(Scope.SYSTEM)) {
         throw new IllegalArgumentException("literal extraction for system classes is not supported");
      }
      Set<Integer> literals = new HashSet<>();
      if (scopes.contains(Scope.APPLICATION)) {
         literals.addAll(this.intLiteralsInApp);
      }
      if (scopes.contains(Scope.LIBRARIES)) {
         literals.addAll(this.intLiteralsInLibs);
      }
      return literals;
   }



   @Override
   public Set<String> getReflectiveNames(EnumSet<Scope> scopes) {
      if (scopes.contains(Scope.SYSTEM)) {
         throw new IllegalArgumentException("reflective name extraction for system classes is not supported");
      }
      Set<String> literals = new HashSet<>();
      if (scopes.contains(Scope.APPLICATION)) {
         literals.addAll(this.reflectiveNamesInApp);
      }
      if (scopes.contains(Scope.LIBRARIES)) {
         literals.addAll(this.reflectiveNamesInLibs);
      }
      return literals;
   }

   @Override
   public Set<String> getRequestParameterNames() {
      return this.requestParameterNames;
   }

   @Override
   public Set<String> getHeaderNames() {
      return this.headerNames;
   }

   @Override
   public Set<MethodSpec> getMethods() {
      return this.supportedMethods;
   }

   @Override
   public Set<String> getApplicationMethods() {
      return applicationMethods;
   }


   @Override
   public boolean isApplicationClass(String className) {
      return this.applicationFilter.test(className);
   }
}
