package nz.ac.wgtn.cornetto.jee.doop;

import com.google.common.base.Splitter;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.stan.callgraph.Method;
import nz.ac.wgtn.cornetto.stan.pointsto.*;
import org.apache.log4j.Logger;
import java.io.*;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Utility to build pointsto from the VarPointsTo.csv files produced by a doop analysis.
 * @author jens dietrich
 */
public class PointstoBuilder {

    private static final Logger LOGGER = LogSystem.getLogger(PointstoBuilder.class);

    static {
        Null.TYPE_NAME = "null";
        StringLiteral.TYPE_NAME = String.class.getName();
        ClassObject.TYPE_NAME = Class.class.getName();
        OtherObject.TYPE_NAME = Object.class.getName();
    }

    // var.getName().startsWith("$stringconstant")

    //    .filter(var -> var.getMethod().getMethodName().equals("getParameter"))
    //    .filter(var -> var.getMethod().getDescriptor().equals("(java.lang.String)java.lang.String"))
    //    .filter(var -> var.getName().equals("@parameter0"))
    //    .filter(var -> var.getMethod().getClassName().equals("giga.jee.mock.javax.servlet.http.MockHttpServletRequest"))

    //           .filter(var -> var.getMethod().getMethodName().equals("getHeader"))
    //            .filter(var -> var.getMethod().getDescriptor().equals("(java.lang.String)java.lang.String"))
    //            .filter(var -> var.getName().equals("@parameter0"))
    //            .filter(var -> var.getMethod().getClassName().equals("giga.jee.mock.javax.servlet.http.MockHttpServletRequest"))


    /**
     * IMPORTANT: the filter must be kept consistent with the information parsed in StaticModelFromDoop to keep this sound !!!
     */
    public static class Config {
        // Filters can be used to restrict what is included in the callgraph, and edge is added if tests for all filters pass.
        // the line filter is applied to the raw definitions in doop format, this is the most effective way of excluding information
        // in terms of parsing speed.
        public Predicate<Variable> variableFilter = var -> {
            String methodName = var.getMethod().getMethodName();
            String varName = var.getName();
            String className = var.getMethod().getClassName();
            return  (methodName.equals("getParameter") && varName.equals("@parameter0") && className.equals("giga.jee.mock.javax.servlet.http.MockHttpServletRequest"))
                ||  (methodName.equals("getParameter") && varName.equals("@parameter0") && className.equals("giga.jee.mock.javax.servlet.http.MockHttpServletRequest"))
                ||  (varName.startsWith("$stringconstant"));
        };
        public Predicate<HObject> objectFilter = m -> true;
        // consistent with variableFilter, prevents some parsing by filtering earlier
        public Predicate<String> lineFilter = s -> s.contains("getParameter") || s.contains("$stringconstant");

        public boolean tolerateErrors = true;
    }

    public Pointsto build (File file, Config config) throws IOException {
        return build(new FileReader(file),config);
    }

    public Pointsto build (File file) throws IOException {
        return build(new FileReader(file),new Config());
    }

    public Pointsto build (InputStream in) throws IOException {
        return build(new InputStreamReader(in),new Config());
    }

    public Pointsto build (InputStream in,Config config) throws IOException {
        return build(new InputStreamReader(in),config);
    }

    public Pointsto build (Reader in) throws IOException {
        return build(in,new Config());
    }

    public Pointsto build (Reader in,Config config) throws IOException {
        try (BufferedReader reader = new BufferedReader(in)) {
            return build(reader.lines(),config);
        }
    }

    public Pointsto build (Stream<String> lines, Config config) {
        Multimap<Variable, HObject> index = //synchronizedMultimap(HashMultimap.create());
                Multimaps.newSetMultimap(new ConcurrentHashMap<>(), ConcurrentHashMap::newKeySet);
        AtomicInteger counter = new AtomicInteger();
        LOGGER.info("Importing doop pointsto");
        long startTime = System.currentTimeMillis();
        AtomicLong t1 = new AtomicLong(startTime);
        AtomicLong t2 = new AtomicLong(startTime);

        lines
            .forEach(line -> {
                if (counter.incrementAndGet() % 1_000_000 == 0) {
                    t2.set(System.currentTimeMillis());
                    String t = NumberFormat.getInstance().format(t2.get()-t1.get());
                    LOGGER.info("\t" + NumberFormat.getInstance().format(counter.intValue()) + " lines parsed, this took " + t + " ms");
                    t1.set(System.currentTimeMillis());
                }

                if (config.lineFilter.test(line)) {
                    Iterator<String> tokens = Splitter.on('\t').split(line).iterator();
                    tokens.next(); // context
                    String objDef = tokens.next();
                    HObject obj = parseObject(objDef, config);
                    if (config.objectFilter.test(obj)) {
                        tokens.next(); // context
                        String varDef = tokens.next();
                        assert !tokens.hasNext();
                        Variable var = parseVar(varDef);
                        if (config.variableFilter.test(var)) {
                            index.put(var, obj);
                        }
                    }
                }
            }
            );

        LOGGER.info("Imported doop pointsto, this took " + (System.currentTimeMillis()-startTime) + "ms");
        return new Pointsto(() -> index);
    }

    private Variable parseVar(String varDef) {
        // example: <sun.util.resources.LocaleData: java.util.ResourceBundle getBreakIteratorInfo(java.util.Locale)>/@parameter0
        int sep = varDef.indexOf('/');
        assert sep>0;
        String methodDef = varDef.substring(0,sep);
        Method method = ParserUtils.parse(methodDef);
        assert method!=null;

        String varName = varDef.substring(sep+1);

        return new Variable(method,varName);
    }

    // <java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0
    private HObject parseObject(String objDef,Config config) {
        try {
            if (objDef.equals("<<null pseudo heap>>")) {
                // example: <<null pseudo heap>>
                return Null.DEFAULT;
            } else if (objDef.startsWith("<class ")) {
                assert objDef.endsWith(">");
                return parseAsClassObject(objDef.substring(6, objDef.length() - 1).trim());
            }
            if (objDef.startsWith("<")) {
                if (objDef.startsWith("<<")) {
                    // example: <<\\" Signature not available\\">>
                    assert objDef.endsWith(">>");
                    return parseAsStringLiteral(objDef.substring(2, objDef.length() - 3));
                }
                else if (objDef.equals("<all permissions>")) {
                    return parseAsStringLiteral(objDef);
                }
                else if (objDef.equals("<init>")) {
                    // TODO double check  this, perhaps use OtherObject ??
                    return parseAsStringLiteral(objDef);
                }
                else {
                    // example: <java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0
                    return parseAsOtherObject(objDef.substring(1, objDef.length()));
                }
            } else {
                // example: ICU data file error: Not an ICU data file
                return parseAsStringLiteral(objDef);
            }
        }
        // catch assertions
        catch (Throwable t) {
            if (config.tolerateErrors) {
                LOGGER.debug("Cannot parse \"" + objDef + "\" " + ", creating special object" );
                return new OtherObject(objDef);
            }
            else {
                throw t;
            }
        }
    }

    private HObject parseAsClassObject(String value) {
        return new ClassObject(value);
    }

    private HObject parseAsStringLiteral(String value) {
        return new StringLiteral(value);
    }

    // <java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0
    private HObject parseAsOtherObject(String def) {
        // use lastIndexOf , in case method name is <init> or <clinit>
        int sep = def.lastIndexOf('>');
        assert sep>0;
        String methodDef = def.substring(0,sep);
        Method method = ParserUtils.parse(methodDef);
        assert method!=null;
        sep = def.indexOf('/');
        String allocSite = def.substring(sep);
        assert allocSite.startsWith("/new ");
        String typeDef = allocSite.substring(5);
        sep = typeDef.indexOf('/');
        if (sep>-1) {
            typeDef = typeDef.substring(0,sep);
        }

        return new Allocation(method,allocSite,typeDef);
    }


}
