package nz.ac.wgtn.cornetto.jee.examples;

import com.google.common.base.Preconditions;
import nz.ac.wgtn.cornetto.commons.LogSystem;
import nz.ac.wgtn.cornetto.jee.JEEFuzzer;
import org.apache.log4j.Logger;
import java.io.File;

/**
 * Script to run the example session.
 * The preferred approach is to run this using ant, this is mainly for debugging.
 * @author jens dietrich
 */
public class FuzzExample1 {

    private static Logger LOGGER = LogSystem.getLogger("example1");

    // example code how to run this from main
    public static void main (String[] args) throws Exception {

        File EXAMPLE_FOLDER = new File("examples/example1");

        // File war = new File(EXAMPLE_FOLDER,"instrumented/example1.war");
        File war = new File(EXAMPLE_FOLDER,"target/example1-1.0-SNAPSHOT.war");
        File prepWar = new File(EXAMPLE_FOLDER,"prepared/example1.war");
        prepWar.getParentFile().mkdirs();
        Preconditions.checkState(war.exists(),"War file " + war.getAbsolutePath() + " does not exist");

        File doopVarPointsTo = new File(EXAMPLE_FOLDER,"staticanalysis/doop/VarPointsTo.csv");
        Preconditions.checkState(doopVarPointsTo.exists(),"Doop VarPointsTo file " + doopVarPointsTo.getAbsolutePath() + " does not exist, run doop first to create this");


        LOGGER.info("Prepare war");
        Process process = new ProcessBuilder()
            .command(
                "java",
                "-jar","war-preparer/target/war-preparer.jar",
                "-war",war.getAbsolutePath(),
                "-out",prepWar.getAbsolutePath(),
                "-fuzzrt","webfuzz-rt/target/webfuzz-rt-1.0-SNAPSHOT.jar"
            )
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .start();
        int result = process.waitFor();
        if (result==0) {
            LOGGER.info("Done preparing war: " + result);
        }
        else {
            LOGGER.warn("War preparation has failed");
            System.exit(1);
        }

        String[] arguments = {
            "-war",prepWar.getAbsolutePath(),
            "-doopOutput",doopVarPointsTo.getParentFile().getAbsolutePath(),
            "-logInterval","1000",
            "-trials","200000"
        };

        JEEFuzzer.main(arguments);

    }
}
