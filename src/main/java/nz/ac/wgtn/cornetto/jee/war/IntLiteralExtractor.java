package nz.ac.wgtn.cornetto.jee.war;

import java.io.InputStream;
import java.util.Set;

/**
 * Utility to extract int literals from a war.
 * @author jens dietrich
 */
public class IntLiteralExtractor extends AbstractWarClassAnalyser<Integer> {

    public IntLiteralExtractor(WarScopeModel scopeModel) {
        super(scopeModel);
    }

    @Override
    public Set<Integer> extractFromClassFile(InputStream in) throws Exception {
        LiteralExtractor literalExtractor = new LiteralExtractor();
        return literalExtractor.extractIntLiterals(in);
    }

}
