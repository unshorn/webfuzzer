package nz.ac.wgtn.cornetto.jee.war;

import java.io.InputStream;
import java.util.Set;

/**
 * Utility to extract string literals from a war.
 * @author jens dietrich
 */
public class StringLiteralExtractor extends AbstractWarClassAnalyser<String> {

    public StringLiteralExtractor(WarScopeModel scopeModel) {
        super(scopeModel);
    }

    @Override
    public Set<String> extractFromClassFile(InputStream in) throws Exception {
        LiteralExtractor literalExtractor = new LiteralExtractor();
        return literalExtractor.extractStringLiterals(in);
    }
}
