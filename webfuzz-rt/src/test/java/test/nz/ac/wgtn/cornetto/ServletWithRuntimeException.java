package test.nz.ac.wgtn.cornetto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class ServletWithRuntimeException extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            try {
                divByZero();
            } catch (Exception e) {

            }
            try {
                divByZero();
            } catch (Exception e) {

            }
            resp.setStatus(HttpServletResponse.SC_OK);
        }

        private void divByZero() {
            int i = 0/0;
            System.err.println(i);
        }

}

