package test.nz.ac.wgtn.cornetto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public class ServletWithInvocationsOfSystemSinks extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        foo1();
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    private void foo1() {
        foo2();
    }

    private void foo2() {
        try {
            Runtime.getRuntime().exec("___foo");
        }
        catch (Exception x) {}

        try {
            Method m = String.class.getMethod("length",new Class[]{});
            int l = (Integer)m.invoke("foo");
        }
        catch (Exception x) {}

        Thread t = new Thread("foo");
        System.out.println(t.getName());
    }
}
