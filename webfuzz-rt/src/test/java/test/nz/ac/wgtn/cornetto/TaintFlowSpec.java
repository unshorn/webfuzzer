package test.nz.ac.wgtn.cornetto;

import java.util.List;
import java.util.Objects;


/**
 * Representation for taintflow data from feedback
 * @author shawn
 */

public class TaintFlowSpec {
    public String sinkName;
    public List sourceStrings;
    public String provenance;
    public TaintFlowSpec(String sinkName, List sourceStrings, String provenance) {
        this.sinkName = sinkName;
        this.sourceStrings = sourceStrings;
        this.provenance = provenance;
    }
    public String toString() {
        return "TaintFlow{" + sinkName + ", " + sourceStrings + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaintFlowSpec that = (TaintFlowSpec) o;
        return sinkName.equals(that.sinkName) &&
                sourceStrings.equals(that.sourceStrings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sinkName, sourceStrings);
    }
}
