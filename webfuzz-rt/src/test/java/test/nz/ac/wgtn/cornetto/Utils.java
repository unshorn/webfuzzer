package test.nz.ac.wgtn.cornetto;

import nz.ac.wgtn.cornetto.jee.rt.DataKind;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Misc utils uses across tests.
 * @author jens dietrich
 * @author shawn
 * shawn - added methods for extracting taint flow, added method for extracting request header names
 */
public class Utils {

    static List<MethodSpec> extractInvocations(JSONObject json) {
        JSONArray invocationData = json.getJSONArray(DataKind.invokedMethods.name());
        List<MethodSpec> methods = new ArrayList<>(invocationData.length());
        for (int i=0;i<invocationData.length();i++) {
            String def = invocationData.getString(i);
            String[] parts = def.split("::");
            MethodSpec methodSpec = new MethodSpec(parts[0],parts[1].substring(0,parts[1].indexOf('(')),parts[1].substring(parts[1].indexOf('(')));
            methods.add(methodSpec);
        }
        return methods;
    }

    static List<String> extractRequestParameterNames(JSONObject json) {
        JSONArray reqParamData = json.getJSONArray(DataKind.requestParameterNames.name());
        List<String> names = new ArrayList<>();
        for (int i=0;i<reqParamData.length();i++) {
            names.add(reqParamData.getString(i));
        }
        return names;
    }

    static List<String> extractRequestHeaderNames(JSONObject json) {
        JSONArray reqParamData = json.getJSONArray(DataKind.requestHeaderNames.name());
        List<String> names = new ArrayList<>();
        for (int i=0; i < reqParamData.length(); i++) {
            names.add(reqParamData.getString(i));
        }
        return names;
    }

    static List<String> extractStackTraces(JSONObject json) {
        JSONArray stackTraceData = json.getJSONArray(DataKind.unsafeSinkInvocationStackTraces.name());
        List<String> stackTraces = new ArrayList<>();
        for (int i=0;i<stackTraceData.length();i++) {
            stackTraces.add(stackTraceData.getString(i));
        }
        return stackTraces;
    }

     static List<TaintFlowSpec> extractTaintflows(JSONObject json) {
         JSONArray taintFlowsData = json.getJSONArray(DataKind.taintFlows.name());
         List<TaintFlowSpec> taintFlows = new ArrayList<>();
         for (int i=0; i < taintFlowsData.length(); i++) {
             JSONObject taintFlow = taintFlowsData.getJSONObject(i);
             JSONArray stackTrace = taintFlow.getJSONArray("stackTrace");
             JSONArray sourceStrings = taintFlow.getJSONObject("taintFlow").getJSONArray("sourceStrings");
             String sinkMethod = stackTrace.getString(0);
             taintFlows.add(new TaintFlowSpec(sinkMethod, sourceStrings.toList(), taintFlow.toString()));
         }
         return taintFlows;
    }

}


