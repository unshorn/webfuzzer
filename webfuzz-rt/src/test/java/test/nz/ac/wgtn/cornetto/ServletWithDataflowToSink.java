package test.nz.ac.wgtn.cornetto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class ServletWithDataflowToSink extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            ArrayList<String> l = new ArrayList<>();
            l.add("goo");
            directFlowToSink1();
            indirectFlowToSink1();
            indirectFlowToSink2();
            resp.setStatus(HttpServletResponse.SC_OK);
        }

        private void directFlowToSink1() {
            try {
                Runtime.getRuntime().exec("___foo");
            }
            catch (Exception x) {}
        }
    private void indirectFlowToSink1() {
        try {
            Class clazz = Class.forName("java.lang.String");
            Method[] methods = clazz.getDeclaredMethods();
            for(int i = 0; i < methods.length; i++) {
                if (methods[i].getName().equals("toString")) {
                    methods[i].invoke(null);
                }
            }
        }
        catch (Exception x) {}
    }
    private static void indirectFlowToSink2() {
        try {
            Method m = String.class.getMethod("length",new Class[]{});
            int l = (Integer)m.invoke("foo");
        }
        catch (Exception x) {}
    }

}

