package nz.ac.wgtn.cornetto.jee.taint;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Data structure for tracking taint.
 * @author shawn
 */

public class Tainter {

    static ThreadLocal<List<String>> taintSourceStrings = new ThreadLocal<>();
    static ThreadLocal<Set<Object>> taintedObjects = new ThreadLocal<>();
    static ThreadLocal<Map<Object, List<Provenance>>> taintProvenance = new ThreadLocal<>();
    static ThreadLocal<Boolean> trackingOn = new ThreadLocal<>();

    /**
    * Initialised by the filter with set of tainted string values
     * from http request header
     * a weak set of taintedObjects, initially empty
     * and a weakmap for provenance information for tainted objects
     * */
    static public void initialise(Set<String> taintedStrs) {
        taintSourceStrings.set(new ArrayList<>());
        taintSourceStrings.get().addAll(taintedStrs);

        taintedObjects.set(Collections.newSetFromMap(new WeakIdentityHashMap<>()));
        taintProvenance.set(new WeakIdentityHashMap<>());
    }

    static public void setTracking(boolean value) {
        if (taintSourceStrings.get() == null) {
           value = false;
        } else if (taintSourceStrings.get().isEmpty()) {
           value = false;
        }
        trackingOn.set(value);
    }

    static public boolean isTrackingOn() {
        if (trackingOn.get() != null) {
            return trackingOn.get();
        } else {
            return false;
        }
    }

    static public void clear() {
        taintSourceStrings.set(null);
        taintedObjects.set(null);
        taintProvenance.set(null);
    }


    /**
     * returns set of tainted arguments for the input call site
     * @param base receiver object for invocation
     * @param args array of arguments
     * @return list of tainted arguments
     */
    static public Map<Integer, Object> getTaintedArgsOfSink(Object base, Object[] args) {
        // System.out.println(args.length+1);
        Map<Integer, Object> result = new HashMap<>();
        List<Object> argList = new ArrayList<>(Arrays.asList(args));
        argList.add(0, base);
        for(int i = 0; i < argList.size(); i++) {
            if (taintedObjects.get().contains(argList.get(i))) {
                result.put(i, argList.get(i));
            }
        }
        return result;
    }
    /**
     * returns map of taint source strings, provenance objects for input set of tainted values
     * @param values set of tainted value arguments at call site
     * @return map of taint source strings, and provenance
     */
    static public Map getProvenance(Map<Integer, Object> arguments) {

        List<Object> sourceStrings = new ArrayList<>();
        Set<Object> processed = Collections.newSetFromMap(new IdentityHashMap<>());
        Map<String, Object> objectsByHash = new HashMap<>();

        List<Map> provenance = new ArrayList<>();
        List<Object> worklist = new ArrayList<>();

        for(Object v: arguments.values()) {
            worklist.add(v);
        }

        while(!worklist.isEmpty()) {
            Object obj = worklist.remove(0);
            if (processed.contains(obj))
                continue;

            processed.add(obj);
            HashMap taintedObjDetails = new HashMap();
            String strRep = "";
            if (obj instanceof String || obj instanceof Method || obj instanceof Class) {
                strRep = obj.toString();
                taintedObjDetails.put("asString", strRep);
            }

            taintedObjDetails.put("hash", String.valueOf(System.identityHashCode(obj)));
            taintedObjDetails.put("type", obj.getClass());
            taintedObjDetails.put("taintEvents", new ArrayList());
            List<Provenance> l = taintProvenance.get().get(obj);
            assert l != null;
            for(Provenance p: l) {
                HashMap e = new HashMap();
                e.put("taintedBy", p.getDesc());
                e.put("context", p.getContext());
                Set<Object> sources = p.getSources();
                List<String> sourcesList = new ArrayList<>();
                sourcesList.addAll(sources.stream().map(o -> String.valueOf(System.identityHashCode(o))).collect(Collectors.toList()));
                if (p.sourceType != TaintSource.SOURCE_STRING) {
                    e.put("sources", sourcesList);
                    for(Object o: sources) {
                        if (!processed.contains(o))
                            worklist.add(o);
                    }
                } else {
                    e.put("sources", sourcesList);
                    sourceStrings.addAll(p.getSources());
                }
                ((List)taintedObjDetails.get("taintEvents")).add(e);
            }
            provenance.add(taintedObjDetails);
        }


        Map map = new HashMap<>();
        Map args = new HashMap<>();
        for(Integer key: arguments.keySet()) {
            args.put(key.toString(), String.valueOf(System.identityHashCode(arguments.get(key))));
        }
        map.put("arguments", args);
        map.put("sourceStrings", sourceStrings.stream().distinct().collect(Collectors.toList()));
        map.put("provenance", provenance);
        return map;
    }

    /**
     * taint object if tainted value is stored in a field belonging to the object
     * @param base object into which field is stored
     * @param fieldName name of field
     * @param value the value into the base object
     */
    static public void taintByFieldStore(String context, Object base, String fieldName, Object value) {
        if (taintedObjects.get().contains(value)) {
            Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(value)),
                    TaintSource.FIELD_STORE, base, fieldName, context);
            taint(base, provenance);
            taintedObjects.get().add(base);
        }
    }

    /**
     * taint value loaded from field if object is tainted
     * @param base object from which field is loaded
     * @param fieldName name of field
     * @param value the value loaded from the base object
     */
    static public void taintByFieldLoad(String context, Object base, String fieldName, Object value) {
        if (taintedObjects.get().contains(base)) {
            Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(base)),
                    TaintSource.FIELD_LOAD, value, fieldName, context);
           taint(value, provenance);
        }
    }

    /**
     * taint collection if added value is tainted
     * @param base collection
     * @param value the value added to the collection
     */
    static public void taintCollection(String context, Object base, Object value) {
        if (taintedObjects.get().contains(value)) {
            Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(value)),
                    TaintSource.COLLECTION_ADD, value, "collection add", context);
            taint(base, provenance);
        }
    }

    /**
     * taint the return value of an invocation
     * @param sig signature of target method
     * @param base receiver object for invocation
     * @param args the arguments for the invocation
     * @param returnVal the value returned by the invocation, not null
     */
    static public void taintByInvocation(String context, String sig, Object base, Object[] args, Object returnVal) {
        if (returnVal == null)
            return;
        List<Object> argList = new ArrayList<>(Arrays.asList(args));
        argList.add(0, base);

        Set<Object> taintSources = (Collections.newSetFromMap(new WeakIdentityHashMap<>()));

        List taintedArguments = new ArrayList<>();

        for(int i = 0; i < argList.size(); i++) {
            Object arg = argList.get(i);
            if (taintedObjects.get().contains(arg)) {
                String strRep = String.valueOf(System.identityHashCode(arg));
                AbstractMap.SimpleEntry<Integer, Integer> argument = new AbstractMap.SimpleEntry(i, strRep);
                taintedArguments.add(argument);
                taintSources.add(arg);
            }

        }

        if (!taintedObjects.get().contains(returnVal)) {
            if (!taintSources.isEmpty()) {
                Provenance provenance = new Provenance(taintSources, TaintSource.ARG_TO_RETURN, returnVal, "call: " + sig + " with arguments:(" + taintedArguments.toString() + ") propagates taint to returned value", context);
                taint(returnVal, provenance);
                if (returnVal.getClass().isArray()) {
                    if ((new Object[0]).getClass().isAssignableFrom(returnVal.getClass())) {
                        Object[] arr = (Object[]) returnVal;
                        for (int i = 0; i < arr.length; i++) {
                            if (arr[i] == null)
                                continue;
                            taint(arr[i], provenance);
                        }
                    }
                }
            }
        }
    }

    /**
     *  introduce new taint if any arguments or the return value match the initial set of tainted string values read from
     *  the http request header: WEBFUZZ-TAINTED-VALUES
     *  these arguments/return value are added to the set of taintedObjects
     *  and provenance recorded in taintProvenance
     * @param sig signature of target method
     * @param base receiver object for invocation
     * @param args the arguments for the invocation
     * @param returnVal the value returned by the invocation
     */
    //
    public static void introduceNewTaint(String context, String sig, Object base, Object[] args, Object returnVal) {
        if (returnVal != null && returnVal instanceof String) {
            if (taintSourceStrings.get().contains(returnVal)) {
                Object val = taintSourceStrings.get().get(taintSourceStrings.get().indexOf(returnVal)); // string from header to be set as source for this value (ref that won't be gc'ed)
                Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(val)), TaintSource.SOURCE_STRING, returnVal, "call: " + sig +", introduces taint in returned string", context);
                taint(returnVal, provenance);
            }
        }

        for(int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg instanceof String) {
                if (taintSourceStrings.get().contains(arg)) {
                    Object val = taintSourceStrings.get().get(taintSourceStrings.get().indexOf(arg));
                    Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(val)), TaintSource.SOURCE_STRING, arg, sig + ", introduces taint in string arg:" + String.valueOf(i+1), context);
                    taint(arg, provenance);
                }
            }
        }

        if (base != null && base instanceof String) {
            if (taintSourceStrings.get().contains(base)) {
                Object val = taintSourceStrings.get().get(taintSourceStrings.get().indexOf(base));
                Provenance provenance = new Provenance(new HashSet<>(Arrays.asList(val)), TaintSource.SOURCE_STRING, base, sig + ", introduces taint in string arg: 0" , context);
                taint(base, provenance);
            }
        }

    }
    public static void taint(Object obj, Provenance p) {
        if (obj == null || obj == String.class)
            return;
        if (taintedObjects.get().contains(obj))
            return;
        List<Provenance> provenanceList = taintProvenance.get().get(obj);
        if (provenanceList == null) {
            provenanceList = new ArrayList<>();
            taintProvenance.get().put(obj, provenanceList);
        }
        provenanceList.add(p);
        taintedObjects.get().add(obj);
    }

}
