package nz.ac.wgtn.cornetto.jee.instrumentation;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.List;
import nz.ac.wgtn.cornetto.jee.rt.DataKind;


/**
 * Aspect to track exceptions.
 * @author shawn
 */
public aspect  ExceptionTracking {


    after() throwing(RuntimeException e) :  execution(* *.*(..)) && !within(nz.ac.wgtn.cornetto.jee.rt.*) && !within(nz.ac.wgtn.cornetto.jee.instrumentation.*) && !within(nz.ac.wgtn.cornetto.jee.taint.*)  {
        StackTraceElement[] stackTrace = e.getStackTrace();
        List<String> encodedStackTrace = Stream.of(stackTrace).map(ste -> ste.toString()).collect(Collectors.toList());
        ArrayList<String> exceptionWithStackTrace = new ArrayList<>();
        exceptionWithStackTrace.add(e.getClass().toString());
        exceptionWithStackTrace.addAll(encodedStackTrace);
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.exceptions, exceptionWithStackTrace);
    }

}
