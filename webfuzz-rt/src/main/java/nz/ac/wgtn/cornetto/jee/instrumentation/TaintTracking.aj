package nz.ac.wgtn.cornetto.jee.instrumentation;

import nz.ac.wgtn.cornetto.jee.taint.Tainter;

/**
 * Aspect to track taint.
 * @author shawn
 */
public aspect  TaintTracking {

    /**
    * taint return value of all callsites if any argument is tainted
     * also, taint string arguments/return value if it is equal to any of the injected tainted strings from request
     * TODO: weak assumption that tainted arguments propagate taint to returned values for all callsites?
     * alternative is to enumerate taint propagating methods (e.g. string transformations, reflective calls.. etc)

     * aspectj  mangles the bytecode for ClassWriter (due to a method with the same signature but different return types)
     * and callsite's with target ScriptBytecodeAdapter.unwrap results in invalid bytecode after instrumentation
     * (a register load is in an execution path without a store)
     */
    after() returning(Object returnValue) : call((!void && !byte && !short && !int && !long && !float && !double && !boolean && !char) *.*(..)) &&
    !call(* *.clone(..)) && !call(* org.objectweb.asm.ClassWriter.*(..)) && !call(* com.google.inject.internal.asm.$ClassWriter.*(..)) &&
            !call(* groovyjarjarasm.asm.ClassWriter.*(..)) &&
            !call(* org.codehaus.groovy.runtime.ScriptBytecodeAdapter.*(..)) &&
            !within(groovy..*) &&
            !within(org.springframework.boot.**) &&
    !within(nz.ac.wgtn.cornetto.jee..**) {
        if (Tainter.isTrackingOn()) {
            String context = thisEnclosingJoinPointStaticPart.getSignature().toLongString() + ", " + thisJoinPoint.getSourceLocation();
            // taint string arguments/return value if they match tainted input strings from request header
            Tainter.introduceNewTaint(context, thisJoinPoint.getSignature().toString(), thisJoinPoint.getTarget(), thisJoinPoint.getArgs(), returnValue);
            // taint result of invocation if any args are tainted
            if (returnValue != null)
                Tainter.taintByInvocation(context, thisJoinPoint.getSignature().toString(), thisJoinPoint.getTarget(), thisJoinPoint.getArgs(), returnValue);
        }
    }

/*
    @Before(
            "call(* java.util.Collection+.add(..)) && " +
                "!within(nz.ac.wgtn.cornetto.jee..**) "
    )
    public void taintCollection(JoinPoint joinPoint) {
        if (Tainter.isTrackingOn()) {
            Tainter.taintCollection(joinPoint.getTarget(), joinPoint.getArgs()[0]);
        }

    }
*/



}
