package nz.ac.wgtn.cornetto.jee.instrumentation;

import nz.ac.wgtn.cornetto.jee.rt.DataKind;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Before;

/**
 * Aspect to add header tracking.
 * @author shawn , jens.dietrich@gmail.comn
 */
public aspect ServletRequestHeaderTracking {

    /** Tracking of
     javax.servlet.http.HttpServletRequest::getDateHeader(java.lang.String name)long
     javax.servlet.http.HttpServletRequest::getHeader(java.lang.String name)String
     javax.servlet.http.HttpServletRequest::getHeaders(java.lang.String name)java.util.Enumeration<java.lang.String>
     javax.servlet.http.HttpServletRequest::getIntHeader(java.lang.String name)int
    **/


    @Before("call(* javax.servlet.http.HttpServletRequest.getDateHeader(..))")
    public void trackRequestHeader1(JoinPoint joinPoint) {
        Object[] params = joinPoint.getArgs();
        assert params.length == 1;
        String value = (String) params[0];
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.requestHeaderNames, value);
    }

    @Before("call(* javax.servlet.http.HttpServletRequest.getHeader(..))")
    public void trackRequestHeader2(JoinPoint joinPoint) {
        Object[] params = joinPoint.getArgs();
        assert params.length == 1;
        String value = (String) params[0];
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.requestHeaderNames, value);
    }

    @Before("call(* javax.servlet.http.HttpServletRequest.getHeaders(..))")
    public void trackRequestHeader3(JoinPoint joinPoint) {
        Object[] params = joinPoint.getArgs();
        assert params.length == 1;
        String value = (String) params[0];
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.requestHeaderNames, value);
    }

    @Before("call(* javax.servlet.http.HttpServletRequest.getIntHeader(..))")
    public void trackRequestHeader4(JoinPoint joinPoint) {
        Object[] params = joinPoint.getArgs();
        assert params.length == 1;
        String value = (String) params[0];
        nz.ac.wgtn.cornetto.jee.rt.InvocationTracker.DEFAULT.track(DataKind.requestHeaderNames, value);
    }



}