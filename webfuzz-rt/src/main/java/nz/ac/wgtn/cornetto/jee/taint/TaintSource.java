package nz.ac.wgtn.cornetto.jee.taint;

/**
 * Taint source types
 * @author shawn
 */

public enum TaintSource {
    ARG_TO_RETURN, FIELD_STORE, FIELD_LOAD, SOURCE_STRING, COLLECTION_ADD, MAP_PUT
}
