package nz.ac.vuw.httpfuzz.jee.instrumentation;

import nz.ac.vuw.httpfuzz.jee.rt.DataKind;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Before;

/**
 * Aspect to add invocation tracking.
 * @author jens dietrich
 */
public aspect ServletRequestParameterTracking {


    /**
     * Tracking of javax.servlet.ServletRequest::getParameter
     */
    @Before("call(* javax.servlet.ServletRequest.getParameter(..))")
    public void trackRequestParameter(JoinPoint joinPoint) {
        Object[] params = joinPoint.getArgs();
        assert params.length == 1;
        String value = (String) params[0];
        nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.requestParameterNames,value);
    }

}