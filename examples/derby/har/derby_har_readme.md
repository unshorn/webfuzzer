# Derby

All requests are created from options found in /derbynet.
The developers decided to use GET requests with URL parameters to adjust those options.

## logging

Enables logging with URL parameter "logform=Logging+on"

## Start/Stop

Starts the network server (if it has been stopped).
URL parameter: "form=Start"

## Threads

Adjusts number of threads used by network server
URL parameter: "newmaxthreads=2&newtimeslice=1&doaction=Set+Network+Server+Parameters"

## Trace

Enables/disables tracing for specified thread
URL parameter: "sessionid=0&doaction=Trace+On%2FOff"

# Trace Directory

Enables tracing off a specified directory (E.g. /users)
URL parameter: "tracedirectory=%2Fusers%2F123&doaction=Trace+directory"