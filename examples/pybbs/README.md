MYSQL
-----
Requires a MYSQL database to be running.
Uses root user with a blank password.
pybbs will create a self-named schema with required tables on startup if not present already.

Tomcat8
-------
Deployment requires an already active Tomcat8 server with application management enabled.

tomcat-users.xml should contain the following:
<role rolename="manager-gui"/>
<user username="tomcat" password="s3cret" roles="manager-gui,manager-script,manager-jmx"/>

Also ensure the manager will accept files beyond the default limit. 
This can be configured under "multipart-config" in tomcat8\webapps\manager\WEB-INF\web.xml

Authentication
--------------
Handled by pybbs_authentication.har
Default user/password is admin/123123