import example2.MainServlet;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import java.io.IOException;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MainServletTest {

    @Before
    public void setup() {
        MainServlet.FOO_INVOKED = false;
    }

    @Test
    public void test1() throws IOException  {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("method_name","foo");
        MockHttpServletResponse response = new MockHttpServletResponse();
        new MainServlet().doGet(request,response);
        assertTrue(MainServlet.FOO_INVOKED);
    }

    @Test
    public void test2() throws IOException  {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("method_name","notFoo");
        MockHttpServletResponse response = new MockHttpServletResponse();
        new MainServlet().doGet(request,response);
        assertFalse(MainServlet.FOO_INVOKED);
    }

    @Test
    public void test3() throws IOException  {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setParameter("some_other_key","foo");
        MockHttpServletResponse response = new MockHttpServletResponse();
        new MainServlet().doGet(request,response);
        assertFalse(MainServlet.FOO_INVOKED);
    }
}
