# Shopizer

All of the Shopizer HARs are performed from the pages found under the administration section.
Each action involves using form urlencoded data and POST to make changes.
As such, each seperate HAR file is not described here, as the names already detail their general purpose.
Furthermore, additional HAR files could be create from the remaining features on the admin page, but were not added due to the highly similar nature of their respective purposes.