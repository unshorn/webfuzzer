# Tuning and Monitoring

## Engines 


The _simple fuzzer_ refers to `JEEFuzzer` or any other subclass of `AbstractFuzzer`.

The _concurrent fuzzer_ is the engine `nz.ac.wgtn.cornetto.jee.JEEFuzzer` or any other subclass of `AbstractFuzzer`.

When running the fuzzer using the ant script, the engine can be set as follows: 

1.  In `pom.xm`, change the entry of the `<mainClass>` attribute in the `maven-shade-plugin` to the respective class.
2. Rebuild the fuzzer jar with `mvn clean package`.


## Logging (simple and concurrent)

The log engine used in log4j, logging is configured in `loog4j.properties`. By default, log files are generated in `logs/`.


## Monitoring (concurrent only)

The concurrent engine uses separate threads to do the following:

1. generate requests
2. process requests
3. process responses 

The respective tasks communicate through queues. This process can be monitored as follows using a JMX client such as JVisualVM.

1. monitor thread utilisation, the respective threads all have names starting with `webfuzz-`
2. monitor queue sizes, there are mbeans for this purpose named `nz.ac.vuw.httpfuzz`

## Tuning (concurrent only)

The concurrent fuzzer supports a some parameters that can be used to tune it. 

| parameter                | deascription                                         | default value |
| ------------------------ | --------------------------------------------------- | ------------- |
| requestexecutionthreads  | number of threads used to execute requests          | 3             |
| responseevaluationthreads| number of threads used to evaluate responses        | 1             |
| requestgenerationthreads | number of threads used to generate requests         | 1             |
| requestqueuesize | size of the queue for generated requests waiting to be sent | 100           |
| responsequeuesize| size of the queue for received responses waiting to be analysed | 100       |


If `responseevaluationthreads` is set to values greater than one, different concurrent client processes
will interact with the same server session (due to the fuzzer currently using a shared cookie store). 
This can lead to issues with server stability (this is a known issue for jenkins), which can of course
be seen as a valueable issue to be discovered.



