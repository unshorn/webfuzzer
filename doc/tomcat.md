# Tomcat Setup for Fuzzing

To use an external (i.e., not embedded) Tomcat (7 or 8) to fuzz, the following setup is required:

1. copy `aspectjweaver-1.9.6.jar` into `${TOMCAT_HOME}/bin/`
2. in `${TOMCAT_HOME}/bin/catalina.sh` add the following line to add JVM arguments: `JAVA_OPTS="-javaagent:aspectjweaver-1.9.6.jar -Xms1024m -Xmx16g"` , this also increases the memory being available
3. in `${TOMCAT_HOME}/config/tomcat-users.xml` add the following line:`<user username="admin" password="secret" roles="manager-script"/>
` , the values for username and password must match the entries in `tomcat-admin.properties`
3. use the following parameter to start the fuzzer client: `deployment=nz.ac.wgtn.cornetto.deployment.DeployWithTomcat8ViaManager` -- this will also work for tomcat9, this deployer uses the manager application network interface
4. before starting the fuzzer, tomcat must be started manually by running `${TOMCAT_HOME}/bin/startup.sh`

