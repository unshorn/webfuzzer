# Fuzzer Evaluation Methodology


Follow guidelines in [Klees et al "Evaluating Fuzz Testing" [CCS18]](https://dl.acm.org/doi/10.1145/3243734.3243804).
## Run multiple trials and employ statistical tests to evaluate results

1. Measure convergence with coverage metrics
2. Plot charts / or do tests on number of "raw" faults/ crashes found


## Choice of Seed Files

1. make explicit -- this is the static analysis
2. pitch har files here

## Longer Timeouts

1. opt for higher - can run on cloud for 24h

## Benchmark

## Assess the Fuzzer

1. no ground truth for bugs 
2. stackhashes : 3-5 deep is common -- might not work well



