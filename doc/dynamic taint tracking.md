
Taint Tracking for *webfuzzer*
===


*webfuzzer* sends randomly generated requests to a Java web
application and reports cross-site scripting (XSS) vulnerabilities, and
security-sensitive methods executed on the server. Dynamic taint
tracking was initially implemented to report data-dependencies between
user inputs and arguments to these methods. It is limited to reporting
potential SQL and reflection injection vulnerabilities. Blackbox taint
inference is used at the client for detecting XSS vulnerabilities.\
The approach is similar to string-only tracking but with the addition of
tracking for reflection objects such as instances of `java.lang.Class`
and `java.lang.Method`. Another difference is the use of taint inference
[@sekar] to introduce taint rather than using a list of predetermined
taint sources.

# Taint Policy

The taint policy is described in terms of taint introduction, taint
propagation and taint checking. The process computes the $Tainted$
relation, which contains tainted objects introduced or tracked during
request handling. Initially, the relation is empty. Note that taint
tracking is activated selectively when a *tainted* http request is
received. These are special requests with URIs that contain string(s)
selected from a pre-set list of strings labelled as tainted.

## Taint Introduction

Taint is introduced at any executed method call if an argument or the
return value equals any string from the list of tainted strings
identified in the http request header.


![](https://bitbucket.org/jensdietrich/webfuzzer/raw/master/doc/images/dta1.png)

## Taint Propagation

For propagation via method calls, methods in the classes `String`,
`StringBuilder`, `StringBuffer` are considered, which is for
string-level tracking as in [@haldar; @loch]. In addition to this,
native method calls return taint if either the base or a parameter is
tainted, and field/array stores taint the base objects in the store
statement.

![](https://bitbucket.org/jensdietrich/webfuzzer/raw/master/doc/images/dta2.png)

## Taint Checking at Sinks

If taint reaches the argument of a security-sensitive method/sink, it is
reported as a violation of the policy.


![](https://bitbucket.org/jensdietrich/webfuzzer/raw/master/doc/images/dta3.png)

Reflective calls and SQL statement execution calls are enumerated as
sinks.

# Implementation

AspectJ (configured for load-time weaving) is used to instrument field
access statements and method invocations. The aspects for introducing
and propagating taint are at:
<https://bitbucket.org/jensdietrich/webfuzzer/src/master/webfuzz-rt/src/main/java/nz/ac/vuw/httpfuzz/jee/instrumentation/TaintTracking.aj>.
This aspect uses the `Tainter` data structure in
<https://bitbucket.org/jensdietrich/webfuzzer/src/master/webfuzz-rt/src/main/java/nz/ac/vuw/httpfuzz/jee/taint/Tainter.java>
for recording and tracking taint.

For instance, the policy for propagating taint is implemented as the
listed aspect:

    pointcut propagateTaint():
                call(* *.*(..))
                                    ;
                
    after() returning (Object o) : propagateTaint() {
        if (Tainter.isTrackingOn()) {
            Tainter.introduceTaint(..);
            Tainter.taintByInvocation(..);
        }
    }
          

## Data Structure for Storing Taint

Instead of using a taint flag as a field in the tracked objects (used in
other approaches [^loch] [^haldar] [^chin]) a weak hashmap (to
avoid memory leaks) is used for tracking the set of tainted objects .
`WeakIdentityHashMap` is used instead of `WeakHashMap` from the standard
library. The difference is that it uses `System::identityHashCode`
instead of `hashCode` and identity checks with `==` instead of `equals`.
Note that hash collisions can be a source of imprecision [^livshit].


    public class Tainter {

        static ThreadLocal<List<String>> taintSourceStrings = new ThreadLocal<>();
        static ThreadLocal<Set<Object>> taintedObjects = new ThreadLocal<>();
        static ThreadLocal<Map<Object, Provenance>> taintProvenance = new ThreadLocal<>();
        static ThreadLocal<Boolean> trackingOn = new ThreadLocal<>();
    ...

The `taintedObjects` field keeps the set of tainted objects tracked thus
far. `taintProvenance` is a map from tainted objects to information on
how it derived taint (taint introduction, propagation) in conjunction
with a list of source objects associated with it.

## Issues

Java compiles string concatenation into operations on the StringBuffer
class. For instance `str1 + str2` is compiled to
`new StringBuffer(str1)).append(str2).toString()`. In this case, taint
propagation works under the [ReturnTaint1]{.smallcaps} rule. However,
this does not work for Java 9+, which uses Indy String concatenation.
Pre-Java 9 string concatenation can be enabled with the compiler option
`-XDstringConcat=inline`.

AspectJ problems with the after return advice for call sites that have
methods with return type overloading as targets.

# Roadmap

-   Character-level taint-tracking to improve precision
    [^chin] [^loch].

-   Use static analysis to instrument only relevant slices [^loch].

-   Migrate from AspectJ to ASM.

-   Compare performance against Phosphor (general-purpose taint tracking
    that tags all variables) [^bell]. Phosphor claims average 52%
    overhead on dacapo. In contrast, string-level tracking as in
    [^chin] cites a 0-15% runtime overhead for one particular web
    application. 



[^sekar]: Sekar, R *An Efficient Black-box Technique for Defeating Web Application Attacks* 2009.

[^chin]: Chin, Erika and Wagner, David *Efficient Character-level Taint Tracking for Java* 2009.

[^loch]: Loch, Johns, Hecker, Mohr, Snelting *Hybrid taint analysis for Java EE* 2020.

[^haldar]: Haldar, Chandra and Franz *Dynamic Taint Propagation for Java* 2006.

[^bell]: Bell, Jonathan and Kaiser, Gail *Dynamic Taint Tracking for Java with Phosphor* 2015.
Livshits, Benjamin *Dynamic Taint Tracking in Managed Runtimes* 2012.


