#!/bin/bash

## SCRIPT TO RUN FUZZING CAMPAIGNS FOR JENKINS IN BATCH MODE
NUMBER_OF_EXPERIMENTS=5
#after testing set back to : 300
CAMPAIGN_RUNTIME=300

echo "buildig preparer"
cd ./war-preparer
mvn clean package
echo "building fuzzer"
cd ..
mvn clean package

for ((i=1; i<=${NUMBER_OF_EXPERIMENTS}; i++))
do
   echo "iteration ${i}"
   ant -buildfile run.xml -propertyfile campaigns/jenkins.properties -DcampaignLength=${CAMPAIGN_RUNTIME} -DoutputRootFolder=results-jenkins-${i}
done

echo "done !"